/*
Navicat MySQL Data Transfer

Source Server         : local
Source Server Version : 50722
Source Host           : localhost:21003
Source Database       : muyu_balance

Target Server Type    : MYSQL
Target Server Version : 50722
File Encoding         : 65001

Date: 2020-07-12 08:58:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for act_evt_log
-- ----------------------------
DROP TABLE IF EXISTS `act_evt_log`;
CREATE TABLE `act_evt_log` (
  `LOG_NR_` bigint(20) NOT NULL AUTO_INCREMENT,
  `TYPE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TIME_STAMP_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3) ON UPDATE CURRENT_TIMESTAMP(3),
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DATA_` longblob,
  `LOCK_OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  `IS_PROCESSED_` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`LOG_NR_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_evt_log
-- ----------------------------

-- ----------------------------
-- Table structure for act_ge_bytearray
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_bytearray`;
CREATE TABLE `act_ge_bytearray` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BYTES_` longblob,
  `GENERATED_` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_BYTEARR_DEPL` (`DEPLOYMENT_ID_`),
  CONSTRAINT `ACT_FK_BYTEARR_DEPL` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ge_bytearray
-- ----------------------------

-- ----------------------------
-- Table structure for act_ge_property
-- ----------------------------
DROP TABLE IF EXISTS `act_ge_property`;
CREATE TABLE `act_ge_property` (
  `NAME_` varchar(64) COLLATE utf8_bin NOT NULL,
  `VALUE_` varchar(300) COLLATE utf8_bin DEFAULT NULL,
  `REV_` int(11) DEFAULT NULL,
  PRIMARY KEY (`NAME_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ge_property
-- ----------------------------
INSERT INTO `act_ge_property` VALUES ('next.dbid', '1', '1');
INSERT INTO `act_ge_property` VALUES ('schema.history', 'create(5.22.0.0)', '1');
INSERT INTO `act_ge_property` VALUES ('schema.version', '5.22.0.0', '1');

-- ----------------------------
-- Table structure for act_hi_actinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_actinst`;
CREATE TABLE `act_hi_actinst` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8_bin NOT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CALL_PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ACT_TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `ASSIGNEE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_ACT_INST_START` (`START_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_ACT_INST_PROCINST` (`PROC_INST_ID_`,`ACT_ID_`),
  KEY `ACT_IDX_HI_ACT_INST_EXEC` (`EXECUTION_ID_`,`ACT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_actinst
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_attachment
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_attachment`;
CREATE TABLE `act_hi_attachment` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `URL_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `CONTENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_comment
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_comment`;
CREATE TABLE `act_hi_comment` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACTION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `MESSAGE_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `FULL_MSG_` longblob,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_comment
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_detail
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_detail`;
CREATE TABLE `act_hi_detail` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TIME_` datetime(3) NOT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_DETAIL_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_ACT_INST` (`ACT_INST_ID_`),
  KEY `ACT_IDX_HI_DETAIL_TIME` (`TIME_`),
  KEY `ACT_IDX_HI_DETAIL_NAME` (`NAME_`),
  KEY `ACT_IDX_HI_DETAIL_TASK_ID` (`TASK_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_detail
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_identitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_identitylink`;
CREATE TABLE `act_hi_identitylink` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_TASK` (`TASK_ID_`),
  KEY `ACT_IDX_HI_IDENT_LNK_PROCINST` (`PROC_INST_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_identitylink
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_procinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_procinst`;
CREATE TABLE `act_hi_procinst` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `BUSINESS_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  `START_USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `START_ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `END_ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SUPER_PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `PROC_INST_ID_` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_PRO_INST_END` (`END_TIME_`),
  KEY `ACT_IDX_HI_PRO_I_BUSKEY` (`BUSINESS_KEY_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_procinst
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_taskinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_taskinst`;
CREATE TABLE `act_hi_taskinst` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `START_TIME_` datetime(3) NOT NULL,
  `CLAIM_TIME_` datetime(3) DEFAULT NULL,
  `END_TIME_` datetime(3) DEFAULT NULL,
  `DURATION_` bigint(20) DEFAULT NULL,
  `DELETE_REASON_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `PRIORITY_` int(11) DEFAULT NULL,
  `DUE_DATE_` datetime(3) DEFAULT NULL,
  `FORM_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_TASK_INST_PROCINST` (`PROC_INST_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_taskinst
-- ----------------------------

-- ----------------------------
-- Table structure for act_hi_varinst
-- ----------------------------
DROP TABLE IF EXISTS `act_hi_varinst`;
CREATE TABLE `act_hi_varinst` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `VAR_TYPE_` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `REV_` int(11) DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` datetime(3) DEFAULT NULL,
  `LAST_UPDATED_TIME_` datetime(3) DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_HI_PROCVAR_PROC_INST` (`PROC_INST_ID_`),
  KEY `ACT_IDX_HI_PROCVAR_NAME_TYPE` (`NAME_`,`VAR_TYPE_`),
  KEY `ACT_IDX_HI_PROCVAR_TASK_ID` (`TASK_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_hi_varinst
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_group
-- ----------------------------
DROP TABLE IF EXISTS `act_id_group`;
CREATE TABLE `act_id_group` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_id_group
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_info
-- ----------------------------
DROP TABLE IF EXISTS `act_id_info`;
CREATE TABLE `act_id_info` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `USER_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `VALUE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PASSWORD_` longblob,
  `PARENT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_id_info
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_membership
-- ----------------------------
DROP TABLE IF EXISTS `act_id_membership`;
CREATE TABLE `act_id_membership` (
  `USER_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `GROUP_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`USER_ID_`,`GROUP_ID_`),
  KEY `ACT_FK_MEMB_GROUP` (`GROUP_ID_`),
  CONSTRAINT `ACT_FK_MEMB_GROUP` FOREIGN KEY (`GROUP_ID_`) REFERENCES `act_id_group` (`ID_`),
  CONSTRAINT `ACT_FK_MEMB_USER` FOREIGN KEY (`USER_ID_`) REFERENCES `act_id_user` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_id_membership
-- ----------------------------

-- ----------------------------
-- Table structure for act_id_user
-- ----------------------------
DROP TABLE IF EXISTS `act_id_user`;
CREATE TABLE `act_id_user` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `FIRST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LAST_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EMAIL_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PWD_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PICTURE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_id_user
-- ----------------------------

-- ----------------------------
-- Table structure for act_procdef_info
-- ----------------------------
DROP TABLE IF EXISTS `act_procdef_info`;
CREATE TABLE `act_procdef_info` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `INFO_JSON_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_UNIQ_INFO_PROCDEF` (`PROC_DEF_ID_`),
  KEY `ACT_IDX_INFO_PROCDEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_INFO_JSON_BA` (`INFO_JSON_ID_`),
  CONSTRAINT `ACT_FK_INFO_JSON_BA` FOREIGN KEY (`INFO_JSON_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_INFO_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_procdef_info
-- ----------------------------

-- ----------------------------
-- Table structure for act_re_deployment
-- ----------------------------
DROP TABLE IF EXISTS `act_re_deployment`;
CREATE TABLE `act_re_deployment` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  `DEPLOY_TIME_` timestamp(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_re_deployment
-- ----------------------------

-- ----------------------------
-- Table structure for act_re_model
-- ----------------------------
DROP TABLE IF EXISTS `act_re_model`;
CREATE TABLE `act_re_model` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LAST_UPDATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `VERSION_` int(11) DEFAULT NULL,
  `META_INFO_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EDITOR_SOURCE_VALUE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EDITOR_SOURCE_EXTRA_VALUE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_MODEL_SOURCE` (`EDITOR_SOURCE_VALUE_ID_`),
  KEY `ACT_FK_MODEL_SOURCE_EXTRA` (`EDITOR_SOURCE_EXTRA_VALUE_ID_`),
  KEY `ACT_FK_MODEL_DEPLOYMENT` (`DEPLOYMENT_ID_`),
  CONSTRAINT `ACT_FK_MODEL_DEPLOYMENT` FOREIGN KEY (`DEPLOYMENT_ID_`) REFERENCES `act_re_deployment` (`ID_`),
  CONSTRAINT `ACT_FK_MODEL_SOURCE` FOREIGN KEY (`EDITOR_SOURCE_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_MODEL_SOURCE_EXTRA` FOREIGN KEY (`EDITOR_SOURCE_EXTRA_VALUE_ID_`) REFERENCES `act_ge_bytearray` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_re_model
-- ----------------------------

-- ----------------------------
-- Table structure for act_re_procdef
-- ----------------------------
DROP TABLE IF EXISTS `act_re_procdef`;
CREATE TABLE `act_re_procdef` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `KEY_` varchar(255) COLLATE utf8_bin NOT NULL,
  `VERSION_` int(11) NOT NULL,
  `DEPLOYMENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RESOURCE_NAME_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DGRM_RESOURCE_NAME_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `HAS_START_FORM_KEY_` tinyint(4) DEFAULT NULL,
  `HAS_GRAPHICAL_NOTATION_` tinyint(4) DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  UNIQUE KEY `ACT_UNIQ_PROCDEF` (`KEY_`,`VERSION_`,`TENANT_ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_re_procdef
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_event_subscr
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_event_subscr`;
CREATE TABLE `act_ru_event_subscr` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `EVENT_TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `EVENT_NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACTIVITY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `CONFIGURATION_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `CREATED_` timestamp(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_EVENT_SUBSCR_CONFIG_` (`CONFIGURATION_`),
  KEY `ACT_FK_EVENT_EXEC` (`EXECUTION_ID_`),
  CONSTRAINT `ACT_FK_EVENT_EXEC` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_event_subscr
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_execution
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_execution`;
CREATE TABLE `act_ru_execution` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BUSINESS_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `SUPER_EXEC_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `ACT_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `IS_ACTIVE_` tinyint(4) DEFAULT NULL,
  `IS_CONCURRENT_` tinyint(4) DEFAULT NULL,
  `IS_SCOPE_` tinyint(4) DEFAULT NULL,
  `IS_EVENT_SCOPE_` tinyint(4) DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) DEFAULT NULL,
  `CACHED_ENT_STATE_` int(11) DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `LOCK_TIME_` timestamp(3) NULL DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_EXEC_BUSKEY` (`BUSINESS_KEY_`),
  KEY `ACT_FK_EXE_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_EXE_PARENT` (`PARENT_ID_`),
  KEY `ACT_FK_EXE_SUPER` (`SUPER_EXEC_`),
  KEY `ACT_FK_EXE_PROCDEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_EXE_PARENT` FOREIGN KEY (`PARENT_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_EXE_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_EXE_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `ACT_FK_EXE_SUPER` FOREIGN KEY (`SUPER_EXEC_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_execution
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_identitylink
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_identitylink`;
CREATE TABLE `act_ru_identitylink` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `GROUP_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `USER_ID_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_IDENT_LNK_USER` (`USER_ID_`),
  KEY `ACT_IDX_IDENT_LNK_GROUP` (`GROUP_ID_`),
  KEY `ACT_IDX_ATHRZ_PROCEDEF` (`PROC_DEF_ID_`),
  KEY `ACT_FK_TSKASS_TASK` (`TASK_ID_`),
  KEY `ACT_FK_IDL_PROCINST` (`PROC_INST_ID_`),
  CONSTRAINT `ACT_FK_ATHRZ_PROCEDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_IDL_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TSKASS_TASK` FOREIGN KEY (`TASK_ID_`) REFERENCES `act_ru_task` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_identitylink
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_job
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_job`;
CREATE TABLE `act_ru_job` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `LOCK_EXP_TIME_` timestamp(3) NULL DEFAULT NULL,
  `LOCK_OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `EXCLUSIVE_` tinyint(1) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROCESS_INSTANCE_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `RETRIES_` int(11) DEFAULT NULL,
  `EXCEPTION_STACK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `EXCEPTION_MSG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `DUEDATE_` timestamp(3) NULL DEFAULT NULL,
  `REPEAT_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_TYPE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `HANDLER_CFG_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  PRIMARY KEY (`ID_`),
  KEY `ACT_FK_JOB_EXCEPTION` (`EXCEPTION_STACK_ID_`),
  CONSTRAINT `ACT_FK_JOB_EXCEPTION` FOREIGN KEY (`EXCEPTION_STACK_ID_`) REFERENCES `act_ge_bytearray` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_job
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_task
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_task`;
CREATE TABLE `act_ru_task` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_DEF_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `PARENT_TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DESCRIPTION_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TASK_DEF_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `OWNER_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `ASSIGNEE_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `DELEGATION_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PRIORITY_` int(11) DEFAULT NULL,
  `CREATE_TIME_` timestamp(3) NULL DEFAULT NULL,
  `DUE_DATE_` datetime(3) DEFAULT NULL,
  `CATEGORY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `SUSPENSION_STATE_` int(11) DEFAULT NULL,
  `TENANT_ID_` varchar(255) COLLATE utf8_bin DEFAULT '',
  `FORM_KEY_` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_TASK_CREATE` (`CREATE_TIME_`),
  KEY `ACT_FK_TASK_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_TASK_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_TASK_PROCDEF` (`PROC_DEF_ID_`),
  CONSTRAINT `ACT_FK_TASK_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCDEF` FOREIGN KEY (`PROC_DEF_ID_`) REFERENCES `act_re_procdef` (`ID_`),
  CONSTRAINT `ACT_FK_TASK_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_task
-- ----------------------------

-- ----------------------------
-- Table structure for act_ru_variable
-- ----------------------------
DROP TABLE IF EXISTS `act_ru_variable`;
CREATE TABLE `act_ru_variable` (
  `ID_` varchar(64) COLLATE utf8_bin NOT NULL,
  `REV_` int(11) DEFAULT NULL,
  `TYPE_` varchar(255) COLLATE utf8_bin NOT NULL,
  `NAME_` varchar(255) COLLATE utf8_bin NOT NULL,
  `EXECUTION_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `PROC_INST_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `TASK_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `BYTEARRAY_ID_` varchar(64) COLLATE utf8_bin DEFAULT NULL,
  `DOUBLE_` double DEFAULT NULL,
  `LONG_` bigint(20) DEFAULT NULL,
  `TEXT_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  `TEXT2_` varchar(4000) COLLATE utf8_bin DEFAULT NULL,
  PRIMARY KEY (`ID_`),
  KEY `ACT_IDX_VARIABLE_TASK_ID` (`TASK_ID_`),
  KEY `ACT_FK_VAR_EXE` (`EXECUTION_ID_`),
  KEY `ACT_FK_VAR_PROCINST` (`PROC_INST_ID_`),
  KEY `ACT_FK_VAR_BYTEARRAY` (`BYTEARRAY_ID_`),
  CONSTRAINT `ACT_FK_VAR_BYTEARRAY` FOREIGN KEY (`BYTEARRAY_ID_`) REFERENCES `act_ge_bytearray` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_EXE` FOREIGN KEY (`EXECUTION_ID_`) REFERENCES `act_ru_execution` (`ID_`),
  CONSTRAINT `ACT_FK_VAR_PROCINST` FOREIGN KEY (`PROC_INST_ID_`) REFERENCES `act_ru_execution` (`ID_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- ----------------------------
-- Records of act_ru_variable
-- ----------------------------

-- ----------------------------
-- Table structure for mat_inventory
-- ----------------------------
DROP TABLE IF EXISTS `mat_inventory`;
CREATE TABLE `mat_inventory` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '公司编号',
  `company_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '公司名称',
  `office_id` bigint(20) DEFAULT NULL COMMENT '机构编号',
  `office_name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '机构名称',
  `parent_id` bigint(20) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '所有父级编号',
  `leaf` tinyint(1) NOT NULL COMMENT '叶子',
  `level` tinyint(2) DEFAULT NULL COMMENT '层级',
  `type` int(11) DEFAULT NULL COMMENT '类别',
  `no` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '自编号',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `manager` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '管理员',
  `sort` int(11) NOT NULL COMMENT '排序',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库存盘点';

-- ----------------------------
-- Records of mat_inventory
-- ----------------------------
INSERT INTO `mat_inventory` VALUES ('100', '100226334', '成都墨安科技有限公司', null, null, '0', '0', '1', '1', null, '10001', '我的库房', '于其先', '10', '100225847', '于其先', '2020-06-27 23:06:07', '100225847', '于其先', '2020-06-27 23:06:07', null, '0');
INSERT INTO `mat_inventory` VALUES ('101', '100226334', '成都墨安科技有限公司', null, null, '100', '0,100', '1', '2', null, '10002', '你的库房', '于安若', '10', '100225847', '于其先', '2020-06-27 23:07:03', '100225847', '于其先', '2020-06-27 23:07:03', null, '0');

-- ----------------------------
-- Table structure for mat_inventory_detail
-- ----------------------------
DROP TABLE IF EXISTS `mat_inventory_detail`;
CREATE TABLE `mat_inventory_detail` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '公司编号',
  `company_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '公司名称',
  `office_id` bigint(20) DEFAULT NULL COMMENT '机构编号',
  `office_name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '机构名称',
  `invoice_id` bigint(20) DEFAULT NULL COMMENT '单据编号',
  `invoice_name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '单据名称',
  `parent_id` bigint(20) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '所有父级编号',
  `leaf` tinyint(1) NOT NULL COMMENT '叶子',
  `level` tinyint(2) DEFAULT NULL COMMENT '层级',
  `type` int(11) DEFAULT NULL COMMENT '类别',
  `no` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '自编号',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `manager` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '管理员',
  `sort` int(11) NOT NULL COMMENT '排序',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库存盘点明细';

-- ----------------------------
-- Records of mat_inventory_detail
-- ----------------------------

-- ----------------------------
-- Table structure for mat_invoice
-- ----------------------------
DROP TABLE IF EXISTS `mat_invoice`;
CREATE TABLE `mat_invoice` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '公司编号',
  `company_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '公司名称',
  `store_id` bigint(20) NOT NULL COMMENT '库房编号',
  `store_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '库房名称',
  `no` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `type` tinyint(2) NOT NULL COMMENT '单据类型',
  `manager` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '库管员',
  `status` tinyint(2) NOT NULL COMMENT '状态',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='出入库单据';

-- ----------------------------
-- Records of mat_invoice
-- ----------------------------
INSERT INTO `mat_invoice` VALUES ('500043004', '100226334', '成都墨安科技有限公司', '101', '你的库房', '123', '123', '0', '123', '0', '100225847', '于其先', '2020-07-06 19:20:35', '100225847', '于其先', '2020-07-06 19:20:35', null, '0');
INSERT INTO `mat_invoice` VALUES ('500043005', '100226334', '成都墨安科技有限公司', '101', '你的库房', '123132', '123', '0', null, '0', '100225847', '于其先', '2020-07-02 12:59:40', '100225847', '于其先', '2020-07-09 12:59:49', null, '0');
INSERT INTO `mat_invoice` VALUES ('500043009', '100226334', '成都墨安科技有限公司', '100', '我的库房', '123', '123', '0', '123', '0', '100225847', '于其先', '2020-07-03 13:13:56', '100225847', '于其先', '2020-07-09 13:14:00', null, '0');

-- ----------------------------
-- Table structure for mat_invoice_detail
-- ----------------------------
DROP TABLE IF EXISTS `mat_invoice_detail`;
CREATE TABLE `mat_invoice_detail` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '公司编号',
  `company_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '公司名称',
  `store_id` bigint(20) NOT NULL COMMENT '库房编码',
  `store_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '库房名称',
  `material_id` bigint(20) NOT NULL COMMENT '物资',
  `material_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '物资名称',
  `invoice_id` bigint(20) NOT NULL COMMENT '单据编号',
  `invoice_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '单据名称',
  `unit` varchar(20) DEFAULT NULL COMMENT '单位',
  `num` decimal(16,2) NOT NULL DEFAULT '0.00' COMMENT '数量',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='出入库明细';

-- ----------------------------
-- Records of mat_invoice_detail
-- ----------------------------
INSERT INTO `mat_invoice_detail` VALUES ('100', '100226334', '成都墨安科技有限公司', '100', '我的库房', '500037002', '铅笔盒', '500041001', 'asdf', '123', '123.00', '100225847', '于其先', '2020-07-01 21:53:29', '100225847', '于其先', '2020-07-01 21:53:29', null, '0');
INSERT INTO `mat_invoice_detail` VALUES ('500042001', '100226334', '成都墨安科技有限公司', '100', '我的库房', '500037002', '铅笔盒', '500042000', '123', '123', '123.00', '100225847', '于其先', '2020-07-01 22:10:18', '100225847', '于其先', '2020-07-01 22:10:18', null, '0');
INSERT INTO `mat_invoice_detail` VALUES ('500043005', '100226334', '成都墨安科技有限公司', '101', '你的库房', '500037002', '铅笔盒', '500043004', '123', '123', '123.00', '100225847', '于其先', '2020-07-06 19:20:36', '100225847', '于其先', '2020-07-06 19:20:36', null, '0');
INSERT INTO `mat_invoice_detail` VALUES ('500043006', '100226334', '成都墨安科技有限公司', '101', '你的库房', '500043003', '铅笔', '500043005', '123', '123', '33.00', '100225847', '于其先', '2020-07-09 12:59:49', '100225847', '于其先', '2020-07-09 12:59:49', '1233', '0');
INSERT INTO `mat_invoice_detail` VALUES ('500043010', '100226334', '成都墨安科技有限公司', '100', '我的库房', '500043003', '铅笔', '500043009', '123', '123', '123.00', '100225847', '于其先', '2020-07-09 13:14:00', '100225847', '于其先', '2020-07-09 13:14:00', null, '0');

-- ----------------------------
-- Table structure for mat_material
-- ----------------------------
DROP TABLE IF EXISTS `mat_material`;
CREATE TABLE `mat_material` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '机构编号',
  `company_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '机构名称',
  `category` int(11) NOT NULL COMMENT '分类',
  `no` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '编号',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `type` tinyint(2) DEFAULT NULL COMMENT '类别',
  `unit` varchar(20) NOT NULL COMMENT '单位',
  `specification` varchar(20) NOT NULL COMMENT '规格',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pk_material_name` (`name`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库存主表';

-- ----------------------------
-- Records of mat_material
-- ----------------------------
INSERT INTO `mat_material` VALUES ('500043003', '100226334', '成都墨安科技有限公司', '1', '100', '铅笔', null, '只', '测试', '100225847', '于其先', '2020-07-09 08:40:44', '100225847', '于其先', '2020-07-09 08:40:44', '测试', '0');

-- ----------------------------
-- Table structure for mat_store
-- ----------------------------
DROP TABLE IF EXISTS `mat_store`;
CREATE TABLE `mat_store` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '公司编号',
  `company_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '公司名称',
  `office_id` bigint(20) DEFAULT NULL COMMENT '机构编号',
  `office_name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '机构名称',
  `parent_id` bigint(20) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '所有父级编号',
  `leaf` tinyint(1) NOT NULL COMMENT '叶子',
  `level` tinyint(2) DEFAULT NULL COMMENT '层级',
  `type` int(11) DEFAULT NULL COMMENT '类别',
  `no` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '自编号',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `manager` varchar(255) CHARACTER SET utf8mb4 NOT NULL COMMENT '管理员',
  `sort` int(11) NOT NULL COMMENT '排序',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库存主表';

-- ----------------------------
-- Records of mat_store
-- ----------------------------
INSERT INTO `mat_store` VALUES ('100', '100226334', '成都墨安科技有限公司', null, null, '0', '0', '1', '1', null, '10001', '我的库房', '于其先', '10', '100225847', '于其先', '2020-06-27 23:06:07', '100225847', '于其先', '2020-06-27 23:06:07', null, '0');
INSERT INTO `mat_store` VALUES ('101', '100226334', '成都墨安科技有限公司', null, null, '100', '0,100', '1', '2', null, '10002', '你的库房', '于安若', '10', '100225847', '于其先', '2020-06-27 23:07:03', '100225847', '于其先', '2020-06-27 23:07:03', null, '0');

-- ----------------------------
-- Table structure for mat_store_detail
-- ----------------------------
DROP TABLE IF EXISTS `mat_store_detail`;
CREATE TABLE `mat_store_detail` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '工程',
  `company_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '工程名称',
  `store_id` bigint(20) NOT NULL COMMENT '库房编号',
  `store_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '库房名称',
  `material_id` bigint(20) NOT NULL COMMENT '物资编号',
  `material_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '物资名称',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '数量',
  `inbound_num` int(11) NOT NULL COMMENT '累计入库数量',
  `outbound_num` int(11) NOT NULL COMMENT '累计出库数量',
  `alarm_num` int(11) DEFAULT NULL COMMENT '预警数量',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库存明细表';

-- ----------------------------
-- Records of mat_store_detail
-- ----------------------------
INSERT INTO `mat_store_detail` VALUES ('500043007', '100226334', '成都墨安科技有限公司', '101', '你的库房', '500037002', '铅笔盒', '123', '123', '0', null, '100225847', '于其先', '2020-07-06 19:20:36', '100225847', '于其先', '2020-07-06 19:20:36', null, '0');
INSERT INTO `mat_store_detail` VALUES ('500043008', '100226334', '成都墨安科技有限公司', '101', '你的库房', '500043003', '铅笔', '33', '33', '0', null, '100225847', '于其先', '2020-07-09 12:59:49', '100225847', '于其先', '2020-07-09 12:59:49', null, '0');
INSERT INTO `mat_store_detail` VALUES ('500043012', '100226334', '成都墨安科技有限公司', '100', '我的库房', '500043003', '铅笔', '123', '123', '0', null, '100225847', '于其先', '2020-07-09 13:14:00', '100225847', '于其先', '2020-07-09 13:14:00', null, '0');

-- ----------------------------
-- Table structure for mat_store_log
-- ----------------------------
DROP TABLE IF EXISTS `mat_store_log`;
CREATE TABLE `mat_store_log` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '公司编号',
  `company_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '公司名称',
  `material_id` bigint(20) NOT NULL COMMENT '物品',
  `material_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '物资名称',
  `invoice_id` bigint(20) NOT NULL COMMENT '单据号',
  `invoice_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '单据名称',
  `store_id` bigint(20) NOT NULL COMMENT '库房编号',
  `store_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '库房名称',
  `type` tinyint(2) NOT NULL COMMENT '类别',
  `num` int(11) NOT NULL DEFAULT '0' COMMENT '变更数量',
  `before_num` int(11) NOT NULL DEFAULT '0' COMMENT '变更前数量',
  `current_num` int(11) NOT NULL DEFAULT '0' COMMENT '当前数量',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库存变更记录表';

-- ----------------------------
-- Records of mat_store_log
-- ----------------------------
INSERT INTO `mat_store_log` VALUES ('500043006', '100226334', '成都墨安科技有限公司', '500037002', '铅笔盒', '500043004', '123', '101', '你的库房', '0', '123', '0', '123', '100225847', '于其先', '2020-07-06 19:20:36', '100225847', '于其先', '2020-07-06 19:20:36', null, '0');
INSERT INTO `mat_store_log` VALUES ('500043007', '100226334', '成都墨安科技有限公司', '500043003', '铅笔', '500043005', '123', '101', '你的库房', '0', '33', '0', '33', '100225847', '于其先', '2020-07-09 12:59:49', '100225847', '于其先', '2020-07-09 12:59:49', null, '0');
INSERT INTO `mat_store_log` VALUES ('500043011', '100226334', '成都墨安科技有限公司', '500043003', '铅笔', '500043009', '123', '100', '我的库房', '0', '123', '0', '123', '100225847', '于其先', '2020-07-09 13:14:00', '100225847', '于其先', '2020-07-09 13:14:00', null, '0');

-- ----------------------------
-- Table structure for oa_execution_ext
-- ----------------------------
DROP TABLE IF EXISTS `oa_execution_ext`;
CREATE TABLE `oa_execution_ext` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `proc_key` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '流程定义',
  `proc_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '流程名称',
  `proc_inst_id` bigint(20) NOT NULL COMMENT '流程实例',
  `business_id` bigint(20) NOT NULL,
  `status` tinyint(2) NOT NULL COMMENT '状态',
  `version` int(11) NOT NULL COMMENT '流程版本',
  `task_id` bigint(20) NOT NULL COMMENT '任务环节',
  `task_name` varchar(100) NOT NULL COMMENT '任务名称',
  `remark` varchar(50) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='业务流程表';

-- ----------------------------
-- Records of oa_execution_ext
-- ----------------------------

-- ----------------------------
-- Table structure for oa_form
-- ----------------------------
DROP TABLE IF EXISTS `oa_form`;
CREATE TABLE `oa_form` (
  `id` int(11) NOT NULL COMMENT '编号',
  `label` varchar(50) NOT NULL COMMENT '说明',
  `name` varchar(200) NOT NULL COMMENT '名称',
  `class_name` varchar(100) NOT NULL COMMENT '实体类名称',
  `type` smallint(1) NOT NULL COMMENT '列表类型',
  `edit_type` smallint(1) NOT NULL COMMENT '编辑类型',
  `gen_type` smallint(1) NOT NULL COMMENT '生成方式',
  `comment` varchar(20) NOT NULL COMMENT '备注',
  `package_name` varchar(50) DEFAULT NULL COMMENT '包名',
  `version` int(11) NOT NULL COMMENT '版本',
  `status` int(11) NOT NULL COMMENT '状态',
  `create_by` int(20) NOT NULL COMMENT '创建人',
  `create_date` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6) COMMENT '创建日期',
  `update_by` int(20) NOT NULL COMMENT '更新人',
  `update_date` datetime(6) NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) DEFAULT NULL COMMENT '备注',
  `del_flag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='动态表单定义主表';

-- ----------------------------
-- Records of oa_form
-- ----------------------------
INSERT INTO `oa_form` VALUES ('146257336', 'test', 'test', 'test', '0', '0', '5', '测试', null, '0', '1', '100225847', '2020-03-03 13:03:06.000000', '100225847', '2020-03-05 16:54:08.338000', null, '0');

-- ----------------------------
-- Table structure for oa_form_field
-- ----------------------------
DROP TABLE IF EXISTS `oa_form_field`;
CREATE TABLE `oa_form_field` (
  `id` int(11) NOT NULL COMMENT '编号',
  `form_id` int(11) NOT NULL COMMENT '表名',
  `comments` varchar(11) NOT NULL COMMENT '说明',
  `java_type` varchar(50) NOT NULL COMMENT 'JAVA类型',
  `name` varchar(50) NOT NULL COMMENT 'JAVA字段',
  `is_null` int(11) DEFAULT NULL COMMENT '可为空',
  `is_edit` int(11) DEFAULT NULL COMMENT '可编辑',
  `is_query` int(11) DEFAULT NULL COMMENT '可查询',
  `show_type` varchar(30) DEFAULT NULL COMMENT '显示类型',
  `dict_type` varchar(50) DEFAULT NULL COMMENT '字典类型',
  `sort` int(11) DEFAULT NULL COMMENT '排序号',
  `length` int(11) DEFAULT NULL COMMENT '长度',
  `align` int(11) DEFAULT NULL COMMENT '对齐',
  `create_by` int(11) NOT NULL COMMENT '创建人',
  `create_date` datetime(6) NOT NULL COMMENT '创建日期',
  `update_by` int(11) NOT NULL COMMENT '更新人',
  `update_date` datetime(6) NOT NULL COMMENT '更新日期',
  `remarks` varchar(10) DEFAULT NULL COMMENT '备注',
  `del_flag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='动态表单字段定义表';

-- ----------------------------
-- Records of oa_form_field
-- ----------------------------
INSERT INTO `oa_form_field` VALUES ('146257337', '146257336', '名字', 'String', 'ename', '0', '1', '1', '', '', null, null, null, '100225847', '2020-03-03 13:03:06.618000', '100225847', '2020-03-05 16:54:02.558000', null, '0');
INSERT INTO `oa_form_field` VALUES ('146257338', '146257336', '年龄', 'Integer', 'age', '0', '1', '1', '', '', null, null, null, '100225847', '2020-03-03 13:03:06.618000', '100225847', '2020-03-05 16:54:02.558000', null, '0');
INSERT INTO `oa_form_field` VALUES ('146257339', '146257336', '生日', 'Date', 'birthday', '0', '1', '0', '', '', null, null, null, '100225847', '2020-03-03 13:03:06.618000', '100225847', '2020-03-05 16:54:02.558000', null, '0');
INSERT INTO `oa_form_field` VALUES ('146257340', '146257336', '工作', 'String', 'work', '0', '1', '0', '', '', null, null, null, '100225847', '2020-03-03 13:03:06.618000', '100225847', '2020-03-05 16:54:02.558000', null, '0');
INSERT INTO `oa_form_field` VALUES ('146257341', '146257336', '薪水', 'BigDecimal', 'slary', '0', '1', '0', '', '', null, null, null, '100225847', '2020-03-03 13:03:06.618000', '100225847', '2020-03-05 16:54:02.558000', null, '0');

-- ----------------------------
-- Table structure for oa_process_ext
-- ----------------------------
DROP TABLE IF EXISTS `oa_process_ext`;
CREATE TABLE `oa_process_ext` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `key_` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '标志',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `type` int(11) NOT NULL COMMENT '类型',
  `status` int(11) NOT NULL COMMENT '状态',
  `category` int(11) NOT NULL COMMENT '是否需要选择合同',
  `permission` int(11) NOT NULL COMMENT '授权',
  `business_table` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '业务表',
  `sort` int(11) NOT NULL COMMENT '排序号',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`,`key_`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='流程定义';

-- ----------------------------
-- Records of oa_process_ext
-- ----------------------------
INSERT INTO `oa_process_ext` VALUES ('140457310', 'proc_quantity_change_apply', '变更索赔审批', '4', '1', '1', '0', '', '10', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467289', 'proc_quantity_change_cost', '变更费用', '4', '0', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467294', 'proc_middle_quantity_visa_apply', '中期计量-工程量', '5', '1', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467299', 'proc_middle_electric_visa_apply', '中期计量-电、水费', '5', '1', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467304', 'proc_middle_examine_visa_apply', '中期计量-考核', '5', '1', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467309', 'proc_middle_material_visa_apply', '中期计量-材料', '5', '1', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467314', 'proc_middle_inventory_visa_apply', '中期计量-盘库', '5', '1', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467319', 'proc_middle_balance_apply', '（普通）中期结算', '6', '1', '1', '0', '', '10', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467324', 'proc_middle_material_deducation_apply', '（普通）中期材料核销', '6', '1', '1', '0', '', '20', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467329', 'proc_final_quantity_visa_apply', '完工计量-工程量', '5', '1', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467334', 'proc_final_electric_visa_apply', '完工计量-电、水费', '5', '1', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467339', 'proc_final_material_visa_apply', '完工计量-材料', '5', '1', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467344', 'proc_final_inventory_visa_apply', '完工计量-盘库', '5', '1', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467349', 'proc_final_examine_visa_apply', '完工计量考核', '5', '1', '1', '0', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467354', 'proc_final_balance_apply', '（普通）完工结算', '6', '1', '1', '0', '', '30', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467359', 'proc_final_material_deducation_apply', '（普通）完工材料核销', '6', '1', '1', '0', '', '40', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467364', 'proc_small_middle_balance_apply', '（小额）中期结算', '6', '1', '1', '1', '', '50', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467369', 'proc_small_final_balance_apply', '（小额）合同完工结算', '6', '1', '1', '1', '', '60', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467374', 'proc_pay_certificate', '（大额）支付证书', '7', '1', '1', '2', '', '0', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140467379', 'proc_pay_apply', '支付申请', '7', '0', '1', '2', '', '20', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140907399', 'proc_oa_purchase_apply', '用品采购', '2', '1', '0', '2', '', '30', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140947441', 'proc_oa_normal_apply', '普通申请', '2', '1', '0', '2', '', '50', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140947502', 'proc_oa_business_trip_apply', '差旅申请', '2', '1', '0', '2', '', '40', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140947568', 'proc_oa_hospitality_apply', '业务招待费', '2', '0', '0', '2', '', '50', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140947622', 'proc_oa_rebate_apply', '现场交际费', '2', '1', '0', '2', '', '60', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('140947690', 'proc_oa_bonus_apply', '出国补贴', '2', '1', '0', '2', '', '70', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('141087335', 'proc_oa_expense_apply', '费用报销单', '2', '1', '0', '2', '', '10', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('141107370', 'proc_phrchase_nonbid_apply', '非招标采购', '0', '0', '0', '0', '', '10', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('142587356', 'proc_oa_borrow_apply', '借款申请', '2', '1', '0', '0', '', '80', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('142707416', 'proc_oa_hospitality_new_apply', '招待费申请', '2', '1', '0', '2', '', '90', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');
INSERT INTO `oa_process_ext` VALUES ('147517344', 'proc_budget_apply', '归概流程', '6', '1', '0', '2', '', '70', '0', '', '0000-00-00 00:00:00', '0', '', '0000-00-00 00:00:00', null, '0');

-- ----------------------------
-- Table structure for pm_re_estimate
-- ----------------------------
DROP TABLE IF EXISTS `pm_re_estimate`;
CREATE TABLE `pm_re_estimate` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `project_id` bigint(20) NOT NULL COMMENT '工程',
  `project_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '工程名称',
  `parent_id` bigint(20) NOT NULL COMMENT '父级编号',
  `parent_ids` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '所有父级编号',
  `level` tinyint(2) DEFAULT NULL COMMENT '层级',
  `type` int(11) DEFAULT NULL COMMENT '类别',
  `leaf` tinyint(1) DEFAULT NULL COMMENT '叶子',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `no` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '自编号',
  `amount` decimal(16,2) DEFAULT '0.00' COMMENT '概算金额',
  `budget_amount` decimal(16,2) DEFAULT '0.00' COMMENT '预算金额',
  `percent` decimal(6,2) DEFAULT '0.00' COMMENT '投资占比',
  `sort` int(11) NOT NULL COMMENT '排序',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`),
  KEY `pk_project_id` (`project_id`),
  CONSTRAINT `pk_project_id` FOREIGN KEY (`project_id`) REFERENCES `pm_re_project` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='项目概算表';

-- ----------------------------
-- Records of pm_re_estimate
-- ----------------------------
INSERT INTO `pm_re_estimate` VALUES ('139069508', '139069302', '华电（印尼）玻雅2×660MW坑口电站工程(EPC)', '0', '0', '1', null, '0', '在岸合同（onshore）', '一', '0.00', '0.00', '0.80', '10', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('139387651', '139069302', '华电（印尼）玻雅2×660MW坑口电站工程(EPC)', '0', '0', '1', null, '0', '离岸合同（offshore）', '二', '0.00', '123.00', '0.20', '20', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('139387755', '139069302', '', '139387651', '0,139387651', '2', null, '1', '咨询服务类（offshore）', '2', '0.00', '0.00', null, '20', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('139387860', '139069302', '', '139387651', '0,139387651', '2', null, '1', '其他服务类（offshore）', '3', '0.00', '0.00', null, '30', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('141710445', '141710123', '', '0', '0', '1', null, '1', 'EPC总承包合同（13.64）', '一', '9039637200.00', '0.00', null, '10', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('141710446', '141710123', '', '0', '0', '1', null, '0', '业主其他费用（3.17）', '二', '2100854100.00', '0.00', null, '20', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('141718150', '141710123', '', '141710446', '0,141710446', '2', null, '1', '动态费用（1.82）', '（二）', '1206168600.00', '0.00', null, '20', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('141718435', '141710123', '', '141710446', '0,141710446', '2', null, '1', '流动资金（0.18）', '（三）', '119291400.00', '0.00', null, '30', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('141739348', '141710123', '', '141710446', '0,141710446', '2', null, '1', '业主管理费（1.17）', '（一）', '775394100.00', '0.00', null, '10', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('143550600', '141710123', '', '0', '0', '1', null, '1', '总投资计划及完成情况', '三', '0.00', '0.00', null, '30', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('143874474', '139069302', '', '139069508', '0,139069508', '2', null, '1', '现场辅助类（onshore）', '2', '0.00', '0.00', null, '20', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('144931031', '139069302', '', '139069508', '0,139069508', '2', null, '1', '主辅建安类（onshore）', '1', '0.00', '0.00', null, '10', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('144931356', '139069302', '华电（印尼）玻雅2×660MW坑口电站工程(EPC)', '139387651', '0,139387651', '2', null, '1', '主辅购销类（offshore）', '1', '0.00', '123.00', null, '10', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');
INSERT INTO `pm_re_estimate` VALUES ('145963983', '139069302', '', '139069508', '0,139069508', '2', null, '1', '现场其他类（onshore）', '3', '0.00', '0.00', null, '30', '0', '', '2020-06-26 06:16:22', '0', '', '2020-06-26 06:16:22', null, '0');

-- ----------------------------
-- Table structure for pm_re_project
-- ----------------------------
DROP TABLE IF EXISTS `pm_re_project`;
CREATE TABLE `pm_re_project` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '业主公司',
  `company_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '业务单位名称',
  `designer_id` bigint(20) NOT NULL COMMENT '设计单位',
  `designer_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '设计单位名称',
  `no` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '编号',
  `name` varchar(50) NOT NULL COMMENT '名称',
  `type` int(11) NOT NULL COMMENT '类型',
  `approve_no` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '批复部门及文号',
  `estimate_amount` decimal(16,4) DEFAULT NULL COMMENT '概算金额',
  `budget_amount` decimal(16,4) DEFAULT NULL COMMENT '预算金额',
  `start_date` datetime DEFAULT NULL COMMENT '开始日期',
  `end_date` datetime DEFAULT NULL COMMENT '结束日期',
  `days` int(11) DEFAULT NULL COMMENT '合同工期',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='工程项目表';

-- ----------------------------
-- Records of pm_re_project
-- ----------------------------
INSERT INTO `pm_re_project` VALUES ('139069302', '139039905', '', '139069371', '', 'epc', '华电（印尼）玻雅2×660MW坑口电站工程(EPC)', '0', null, '0.0000', '0.0000', '2018-12-31 00:00:00', null, null, '0', '', '2020-06-17 14:35:35', '0', '', '2020-06-17 14:35:35', null, '0');
INSERT INTO `pm_re_project` VALUES ('141710123', '139289157', '', '139069371', '', 'BA', '华电（印尼）玻雅2*660MW坑口电站工程(BA)', '0', null, '11140491300.0000', '0.0000', '2018-12-31 00:00:01', null, null, '0', '', '2020-06-17 14:35:35', '0', '', '2020-06-17 14:35:35', null, '0');

-- ----------------------------
-- Table structure for sys_area
-- ----------------------------
DROP TABLE IF EXISTS `sys_area`;
CREATE TABLE `sys_area` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `parent_id` bigint(20) NOT NULL COMMENT '父编号',
  `parent_ids` text CHARACTER SET utf8mb4 NOT NULL COMMENT '所有父级编号',
  `level` int(11) NOT NULL COMMENT '层级',
  `leaf` tinyint(1) NOT NULL COMMENT '是否叶子',
  `sort` int(11) NOT NULL COMMENT '排序号',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `type` int(11) NOT NULL COMMENT '类型',
  `code` varchar(11) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '编码',
  `create_by` bigint(20) NOT NULL COMMENT '创建者',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建者姓名',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新者',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新者姓名',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='区域信息表';

-- ----------------------------
-- Records of sys_area
-- ----------------------------
INSERT INTO `sys_area` VALUES ('100226545', '0', '0', '1', '0', '10', '中国', '0', '', '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226546', '100226545', '0,100226545', '2', '0', '10', '北京', '1', '', '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226547', '100226546', '0,100226545,100226546', '3', '1', '10', '西城区', '2', '', '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226548', '100226545', '0,100226545', '2', '0', '10', '四川', '1', '', '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226549', '100226548', '0,100226545,100226548', '0', '1', '30', '成都市', '3', null, '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226550', '100226548', '0,100226545,100226548', '0', '1', '30', '自贡市', '3', null, '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226551', '100226548', '0,100226545,100226548', '0', '1', '30', '攀枝花市', '3', null, '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226552', '100226548', '0,100226545,100226548', '0', '1', '30', '泸州市', '3', null, '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226553', '100226548', '0,100226545,100226548', '0', '1', '30', '德阳市', '3', null, '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226554', '100226548', '0,100226545,100226548', '0', '1', '30', '绵阳市', '3', null, '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226555', '100226548', '0,100226545,100226548', '0', '1', '30', '广元市', '3', null, '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226556', '100226548', '0,100226545,100226548', '0', '1', '30', '遂宁市', '3', null, '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226557', '100226548', '0,100226545,100226548', '0', '1', '30', '内江市', '3', null, '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('100226558', '100226548', '0,100226545,100226548', '0', '1', '30', '凉山州', '3', null, '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('141480422', '0', '0', '1', '1', '10', '印尼', '0', '', '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('141480423', '0', '0', '1', '1', '10', '越南', '0', '', '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');
INSERT INTO `sys_area` VALUES ('141480424', '100226546', '0,100226545,100226546', '3', '1', '10', '东城区', '2', '', '0', '', '2020-06-09 14:30:21', '0', '', '2020-06-09 14:30:21', null, '0');

-- ----------------------------
-- Table structure for sys_attachment
-- ----------------------------
DROP TABLE IF EXISTS `sys_attachment`;
CREATE TABLE `sys_attachment` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `name` varchar(400) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `origin_name` varchar(400) CHARACTER SET utf8mb4 NOT NULL COMMENT '原始名称',
  `suffix` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '后缀名',
  `size` bigint(20) NOT NULL COMMENT '大小',
  `path` text CHARACTER SET utf8mb4 NOT NULL COMMENT '保存路径',
  `business_id` bigint(20) DEFAULT NULL COMMENT '业务表编号',
  `external_id` bigint(20) DEFAULT NULL COMMENT '扩展编号',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC COMMENT='系统附件表';

-- ----------------------------
-- Records of sys_attachment
-- ----------------------------

-- ----------------------------
-- Table structure for sys_diary
-- ----------------------------
DROP TABLE IF EXISTS `sys_diary`;
CREATE TABLE `sys_diary` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT '编号',
  `company_id` int(11) NOT NULL COMMENT '公司',
  `office_id` int(11) NOT NULL COMMENT '部门',
  `project_id` int(11) NOT NULL COMMENT '登录名',
  `title` varchar(64) NOT NULL COMMENT '标题',
  `content` varchar(32) DEFAULT NULL COMMENT '编号',
  `type` varchar(32) NOT NULL COMMENT '类型',
  `auditor_id` varchar(20) DEFAULT NULL COMMENT '审核人',
  `auditor_name` varchar(100) DEFAULT NULL COMMENT '审核人名字',
  `audit_status` int(11) NOT NULL COMMENT '审核状态',
  `audit_date` datetime DEFAULT NULL COMMENT '审核日期',
  `create_by` int(11) NOT NULL DEFAULT '1' COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` int(11) NOT NULL DEFAULT '1' COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(10) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(11) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_login_name` (`project_id`,`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户日志表';

-- ----------------------------
-- Records of sys_diary
-- ----------------------------

-- ----------------------------
-- Table structure for sys_dict
-- ----------------------------
DROP TABLE IF EXISTS `sys_dict`;
CREATE TABLE `sys_dict` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `value` varchar(11) CHARACTER SET utf8mb4 NOT NULL COMMENT '键值',
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `type` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '编码',
  `code` varchar(50) DEFAULT NULL COMMENT '国际化键值',
  `external1` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '扩展属性',
  `external1_desc` varchar(50) DEFAULT NULL COMMENT '扩展属性1描述',
  `external2` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '扩展属性',
  `external2_desc` varchar(50) DEFAULT NULL COMMENT '扩展属性2描述',
  `description` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '描述信息',
  `sort` int(11) NOT NULL COMMENT '排序号',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `dict_value` (`id`,`value`) USING BTREE,
  UNIQUE KEY `dict_type_value` (`type`,`value`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='字典表';

-- ----------------------------
-- Records of sys_dict
-- ----------------------------
INSERT INTO `sys_dict` VALUES ('100000033', '0', '否', 'sys.bool', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('100000228', '0', '是仅保存配置', 'gen.gen.type', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('100000229', '1', '仅生成服务端代码', 'gen.gen.type', null, null, null, null, null, null, '2');
INSERT INTO `sys_dict` VALUES ('100000230', '2', '仅生成客户端代码', 'gen.gen.type', null, null, null, null, null, null, '3');
INSERT INTO `sys_dict` VALUES ('100000231', '3', '仅生成实体', 'gen.gen.type', null, null, null, null, null, null, '4');
INSERT INTO `sys_dict` VALUES ('100000232', '4', '仅生成前端列属性', 'gen.gen.type', null, null, null, null, null, null, '5');
INSERT INTO `sys_dict` VALUES ('100000233', '5', '全部生成', 'gen.gen.type', null, null, null, null, null, null, '6');
INSERT INTO `sys_dict` VALUES ('100000234', '0', '列表', 'gen.type', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('100000235', '1', '树形', 'gen.type', null, null, null, null, null, null, '2');
INSERT INTO `sys_dict` VALUES ('100000236', '0', '弹出编辑框', 'gen.edit.type', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('100000237', '1', '行内编辑', 'gen.edit.type', null, null, null, null, null, null, '2');
INSERT INTO `sys_dict` VALUES ('100000971', '1', '省', 'area.type', null, null, null, null, null, '区域类型', '1');
INSERT INTO `sys_dict` VALUES ('100000972', '3', '市', 'area.type', null, null, null, null, null, '区域类型', '3');
INSERT INTO `sys_dict` VALUES ('100001029', '0', '公司', 'sys.office.type', null, null, null, null, null, '机构类型', '1');
INSERT INTO `sys_dict` VALUES ('100001030', '1', '部门', 'sys.office.type', null, null, null, null, null, '机构类型', '2');
INSERT INTO `sys_dict` VALUES ('100001031', '0', '一级', 'sys.office.grade', null, null, null, null, null, '机构级别', '10');
INSERT INTO `sys_dict` VALUES ('100001032', '1', '二级', 'sys.office.grade', null, null, null, null, null, '机构级别', '20');
INSERT INTO `sys_dict` VALUES ('100001036', '0', '火电', 'project.type', null, null, null, null, null, '工程类型', '1');
INSERT INTO `sys_dict` VALUES ('100001037', '1', '水电', 'project.type', null, null, null, null, null, '工程类型', '2');
INSERT INTO `sys_dict` VALUES ('100001038', '2', '风电', 'project.type', null, null, null, null, null, '工程类型', '3');
INSERT INTO `sys_dict` VALUES ('100001518', '0', '招标中', 'contract.status', null, null, null, null, null, '合同状态', '1');
INSERT INTO `sys_dict` VALUES ('100001519', '1', '签合同', 'contract.status', null, null, null, null, null, '合同状态', '2');
INSERT INTO `sys_dict` VALUES ('100001520', '2', '执行中', 'contract.status', null, null, null, null, null, '合同状态', '2');
INSERT INTO `sys_dict` VALUES ('100001521', '3', '结束', 'contract.status', null, null, null, null, null, '合同状态', '4');
INSERT INTO `sys_dict` VALUES ('100001522', '0', '公开', 'contract.bid.mode', null, null, null, null, null, '招标方式', '1');
INSERT INTO `sys_dict` VALUES ('100001523', '1', '邀请', 'contract.bid.mode', null, null, null, null, null, '招标方式', '1');
INSERT INTO `sys_dict` VALUES ('100001524', '2', '询价', 'contract.bid.mode', null, null, null, null, null, '招标方式', '1');
INSERT INTO `sys_dict` VALUES ('100001525', '3', '单一', 'contract.bid.mode', null, null, null, null, null, '招标方式', '1');
INSERT INTO `sys_dict` VALUES ('100001526', '4', '委托', 'contract.bid.mode', null, null, null, null, null, '招标方式', '1');
INSERT INTO `sys_dict` VALUES ('100001527', '5', '竞争', 'contract.bid.mode', null, null, null, null, null, '招标方式', '1');
INSERT INTO `sys_dict` VALUES ('100001528', '0', '物资类', 'contract.bid.type', null, null, null, null, null, '招标类型', '1');
INSERT INTO `sys_dict` VALUES ('100001529', '1', '服务类', 'contract.bid.type', null, null, null, null, null, '招标类型', '1');
INSERT INTO `sys_dict` VALUES ('100001530', '2', '基建类', 'contract.bid.type', null, null, null, null, null, '招标类型', '1');
INSERT INTO `sys_dict` VALUES ('100001531', '3', '设备类', 'contract.bid.type', null, null, null, null, null, '招标类型', '1');
INSERT INTO `sys_dict` VALUES ('100001532', '4', '投资类', 'contract.bid.type', null, null, null, null, null, '招标类型', '1');
INSERT INTO `sys_dict` VALUES ('100001533', '5', '销售类', 'contract.bid.type', null, null, null, null, null, '招标类型', '1');
INSERT INTO `sys_dict` VALUES ('100001534', '6', '融资类', 'contract.bid.type', null, null, null, null, null, '招标类型', '1');
INSERT INTO `sys_dict` VALUES ('100001535', '7', '其他类', 'contract.bid.type', null, null, null, null, null, '招标类型', '1');
INSERT INTO `sys_dict` VALUES ('100001536', '0', '资质后审', 'contract.qualif.type', null, null, null, null, null, '招标类型', '1');
INSERT INTO `sys_dict` VALUES ('100001537', '1', '资质预审', 'contract.qualif.type', null, null, null, null, null, '招标类型', '1');
INSERT INTO `sys_dict` VALUES ('100001538', '0', '普通合同', 'contract.type', null, null, null, null, null, '合同类型', '1');
INSERT INTO `sys_dict` VALUES ('100001539', '1', '小额合同', 'contract.type', null, null, null, null, null, '合同类型', '1');
INSERT INTO `sys_dict` VALUES ('100002685', '0', '钢筋', 'material.type', null, null, null, null, null, '材料类型', '1');
INSERT INTO `sys_dict` VALUES ('100002686', '1', '水泥', 'material.type', null, null, null, null, null, '材料类型', '1');
INSERT INTO `sys_dict` VALUES ('100002687', '2', '粉煤灰', 'material.type', null, null, null, null, null, '材料类型', '1');
INSERT INTO `sys_dict` VALUES ('100002688', '3', '型钢', 'material.type', null, null, null, null, null, '材料类型', '1');
INSERT INTO `sys_dict` VALUES ('100002729', '0', '水泥砂浆', 'mix.type', null, null, null, null, null, '配合比类型', '2');
INSERT INTO `sys_dict` VALUES ('100002730', '1', '混凝土', 'mix.type', null, null, null, null, null, '配合比类型', '2');
INSERT INTO `sys_dict` VALUES ('100002731', '2', '锚杆', 'mix.type', null, null, null, null, null, '配合比类型', '2');
INSERT INTO `sys_dict` VALUES ('100002732', '3', '钢支撑', 'mix.type', null, null, null, null, null, '配合比类型', '2');
INSERT INTO `sys_dict` VALUES ('100002733', '4', '钢结构', 'mix.type', null, null, null, null, null, '配合比类型', '2');
INSERT INTO `sys_dict` VALUES ('100002736', '7', '钢筋制安', 'mix.type', null, null, null, null, null, '配合比类型', '2');
INSERT INTO `sys_dict` VALUES ('100005629', '0', '变更模型', 'act.model.type', null, null, null, null, null, '模型类型', '2');
INSERT INTO `sys_dict` VALUES ('100005630', '1', '签证模型', 'act.model.type', null, null, null, null, null, '模型类型', '2');
INSERT INTO `sys_dict` VALUES ('100005631', '3', '汇总表模型', 'act.model.type', null, null, null, null, null, '模型类型', '3');
INSERT INTO `sys_dict` VALUES ('100005941', '0', '承包人合同管理员', 'contract.role.type', null, '0', null, '合同施工单位', null, '合同角色类型', '0');
INSERT INTO `sys_dict` VALUES ('100005942', '1', '承包人项目经理', 'contract.role.type', null, '0', null, '合同施工单位', null, '合同角色类型', '1');
INSERT INTO `sys_dict` VALUES ('100005943', '2', '监理合同管理员', 'contract.role.type', null, '1', null, '合同监理单位', null, '合同角色类型', '2');
INSERT INTO `sys_dict` VALUES ('100005944', '3', '总监理工程师', 'contract.role.type', null, '1', null, '合同监理单位', null, '合同角色类型', '3');
INSERT INTO `sys_dict` VALUES ('100005945', '4', '现场工程师', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '4');
INSERT INTO `sys_dict` VALUES ('100006048', '5', '现场负责人', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '5');
INSERT INTO `sys_dict` VALUES ('100006049', '6', '工程管理员', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '6');
INSERT INTO `sys_dict` VALUES ('100006050', '7', '工程管理负责人', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '7');
INSERT INTO `sys_dict` VALUES ('100006052', '11', '安监部管理员', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '11');
INSERT INTO `sys_dict` VALUES ('100006053', '12', '安监部负责人', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '12');
INSERT INTO `sys_dict` VALUES ('100006054', '13', '商法&责任部门管理员', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '13');
INSERT INTO `sys_dict` VALUES ('100006055', '14', '商法&责任部门负责人', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '14');
INSERT INTO `sys_dict` VALUES ('100006057', '16', '（工程）副总', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '100');
INSERT INTO `sys_dict` VALUES ('100006058', '20', '董事长', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '220');
INSERT INTO `sys_dict` VALUES ('100006059', '19', '总经理', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '200');
INSERT INTO `sys_dict` VALUES ('100008328', '0', '中期工程量签证', 'visa.category', null, null, null, null, null, '签证单类型', '1');
INSERT INTO `sys_dict` VALUES ('100008329', '1', '中期材料签证', 'visa.category', null, null, null, null, null, '签证单类型', '1');
INSERT INTO `sys_dict` VALUES ('100008330', '2', '中期电费签证', 'visa.category', null, null, null, null, null, '签证单类型', '1');
INSERT INTO `sys_dict` VALUES ('100008331', '3', '中期考核签证', 'visa.category', null, null, null, null, null, '签证单类型', '1');
INSERT INTO `sys_dict` VALUES ('100008332', '4', '中期其他签证', 'visa.category', null, null, null, null, null, '签证单类型', '1');
INSERT INTO `sys_dict` VALUES ('100008333', '5', '中期盘库签证', 'visa.category', null, null, null, null, null, '签证单类型', '1');
INSERT INTO `sys_dict` VALUES ('100009064', '0', '中期', 'balanceStyle.type', null, '1', null, '2', null, '合同结算类型', '1');
INSERT INTO `sys_dict` VALUES ('100009065', '1', '完工', 'balanceStyle.type', null, null, null, null, null, '合同结算类型', '1');
INSERT INTO `sys_dict` VALUES ('100009422', '0', '草稿', 'proc.status', null, null, null, null, null, '签证单状态', '1');
INSERT INTO `sys_dict` VALUES ('100009423', '1', '流程中', 'proc.status', null, null, null, null, null, '签证单状态', '1');
INSERT INTO `sys_dict` VALUES ('100009424', '2', '流程完成', 'proc.status', null, null, null, null, null, '签证单状态', '1');
INSERT INTO `sys_dict` VALUES ('100009735', '0', '奖励', 'visa.examine.detail.type', null, null, null, null, null, '考核类型', '1');
INSERT INTO `sys_dict` VALUES ('100009736', '1', '罚款', 'visa.examine.detail.type', null, null, null, null, null, '考核类型', '1');
INSERT INTO `sys_dict` VALUES ('100009936', '0', '手工录入', 'balanceStyle.balance.mode', null, null, null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100009937', '1', '工程量签证', 'balanceStyle.balance.mode', null, null, null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100009938', '7', '公式汇总', 'balanceStyle.balance.mode', null, null, null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100009939', '9', '合同项目', 'balanceStyle.balance.mode', null, null, null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100009940', '2', '电费签证', 'balanceStyle.balance.mode', null, null, null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100009941', '6', '分级汇总', 'balanceStyle.balance.mode', null, null, null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100009942', '4', '材料核销', 'balanceStyle.balance.mode', null, null, null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100009943', '5', '考核签证', 'balanceStyle.balance.mode', null, null, null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100009944', '8', '其他签证', 'balanceStyle.balance.mode', null, null, null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100009945', '3', '按比例预设', 'balanceStyle.balance.mode', null, null, null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100011353', '0', '中期签证', 'visa.type', null, null, null, null, null, null, '10');
INSERT INTO `sys_dict` VALUES ('100011354', '1', '完工签证', 'visa.type', null, null, null, null, null, null, '20');
INSERT INTO `sys_dict` VALUES ('100011547', '0', '未签收', 'task.status', null, null, null, null, null, '任务状态', '1');
INSERT INTO `sys_dict` VALUES ('100011548', '1', '已签收', 'task.status', null, null, null, null, null, '任务状态', '1');
INSERT INTO `sys_dict` VALUES ('100011582', '17', '（安监）副总', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '111');
INSERT INTO `sys_dict` VALUES ('100011583', '18', '（合同）副总', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '112');
INSERT INTO `sys_dict` VALUES ('100011584', '9', '物资管理员', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '9');
INSERT INTO `sys_dict` VALUES ('100011585', '10', '物资部负责人', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '10');
INSERT INTO `sys_dict` VALUES ('100011654', '0', '按比例分摊', 'contract.divide.type', null, null, null, null, null, '合同分摊方式', '0');
INSERT INTO `sys_dict` VALUES ('100011655', '1', '固定金额分摊', 'contract.divide.type', null, null, null, null, null, '合同分摊方式', '0');
INSERT INTO `sys_dict` VALUES ('100015387', '100226045', '业主单位代码', 'pm.proprietor.office.id', null, null, null, null, null, '业主单位代码', '0');
INSERT INTO `sys_dict` VALUES ('100015487', '0', '工程', 'contract.account.type', null, null, null, null, null, '台账类型', '0');
INSERT INTO `sys_dict` VALUES ('100015488', '1', '概算', 'contract.account.type', null, null, null, null, null, '台账类型', '1');
INSERT INTO `sys_dict` VALUES ('100015489', '2', '合同', 'contract.account.type', null, null, null, null, null, '台账类型', '2');
INSERT INTO `sys_dict` VALUES ('100015490', '3', '分摊', 'contract.account.type', null, null, null, null, null, '台账类型', '3');
INSERT INTO `sys_dict` VALUES ('100278290', '5', '安全文明措施费', 'change.mode', null, null, null, null, null, null, '5');
INSERT INTO `sys_dict` VALUES ('100278293', '4', '工程索赔', 'change.mode', null, null, null, null, null, null, '4');
INSERT INTO `sys_dict` VALUES ('100278604', '1', '流程中', 'process.status', null, null, null, null, null, null, '20');
INSERT INTO `sys_dict` VALUES ('100278605', '2', '完成', 'process.status', null, null, null, null, null, null, '30');
INSERT INTO `sys_dict` VALUES ('100282286', '21', '生技部合同管理员', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '21');
INSERT INTO `sys_dict` VALUES ('100282287', '22', '生技资部负责人', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '22');
INSERT INTO `sys_dict` VALUES ('100287369', '1', '变更费用', 'task.type', null, null, null, null, null, null, '2');
INSERT INTO `sys_dict` VALUES ('100287371', '3', '完工签证', 'task.type', null, null, null, null, null, null, '4');
INSERT INTO `sys_dict` VALUES ('100287372', '0', '变更立项', 'task.type', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('100287374', '4', '中期结算', 'task.type', null, null, null, null, null, null, '5');
INSERT INTO `sys_dict` VALUES ('100287375', '2', '中期签证', 'task.type', null, null, null, null, null, null, '3');
INSERT INTO `sys_dict` VALUES ('100287376', '5', '完工结算', 'task.type', null, null, null, null, null, null, '6');
INSERT INTO `sys_dict` VALUES ('100287377', '6', '中期核销', 'task.type', null, null, null, null, null, null, '7');
INSERT INTO `sys_dict` VALUES ('100287378', '7', '完工核销', 'task.type', null, null, null, null, null, null, '8');
INSERT INTO `sys_dict` VALUES ('100288620', '10', '结算模板', 'template.type', null, null, null, null, null, '模板类型', '0');
INSERT INTO `sys_dict` VALUES ('100288887', '10', '税金', 'balanceStyle.balance.mode', null, '可输入比例,也可以手动输入', null, null, null, '汇总模式', '2');
INSERT INTO `sys_dict` VALUES ('100289290', '4', '砂石料', 'material.type', null, null, null, null, null, '材料类型', '1');
INSERT INTO `sys_dict` VALUES ('100289291', '5', '木材', 'material.type', null, null, null, null, null, '材料类型', '1');
INSERT INTO `sys_dict` VALUES ('100289292', '6', '柴油', 'material.type', null, null, null, null, null, '材料类型', '1');
INSERT INTO `sys_dict` VALUES ('100289293', '7', '外加剂', 'material.type', null, null, null, null, null, '材料类型', '1');
INSERT INTO `sys_dict` VALUES ('100392173', '3', '光伏', 'project.type', null, null, null, null, null, '工程类型', '2');
INSERT INTO `sys_dict` VALUES ('101768199', '0', '施工费用', 'fee.type', null, null, null, null, null, '', '1');
INSERT INTO `sys_dict` VALUES ('101768200', '1', '监理费用', 'fee.type', null, null, null, null, null, null, '2');
INSERT INTO `sys_dict` VALUES ('101768201', '2', '建管部费用', 'fee.type', null, null, null, null, null, null, '3');
INSERT INTO `sys_dict` VALUES ('101768202', '3', '工程部费用', 'fee.type', null, null, null, null, null, null, '4');
INSERT INTO `sys_dict` VALUES ('102235215', '0', '按合同汇总', 'material.search.type', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('102235216', '1', '按材料汇总', 'material.search.type', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('102242708', '11', '工程量调价', 'balanceStyle.balance.mode', null, '', null, null, null, '汇总模式', '11');
INSERT INTO `sys_dict` VALUES ('102805139', '0', '变更流程', 'process.draft.type', null, null, null, null, null, '模型类型', '1');
INSERT INTO `sys_dict` VALUES ('102805140', '1', '签证流程', 'process.draft.type', null, null, null, null, null, '模型类型', '2');
INSERT INTO `sys_dict` VALUES ('102890537', '5', '灌浆', 'mix.type', null, null, null, null, null, '配合比类型', '5');
INSERT INTO `sys_dict` VALUES ('102890538', '6', '锚索', 'mix.type', null, null, null, null, null, '配合比类型', '6');
INSERT INTO `sys_dict` VALUES ('102894327', '8', '管棚', 'mix.type', null, null, null, null, null, '配合比类型', '8');
INSERT INTO `sys_dict` VALUES ('106228944', '0', '通知', 'sys.notify.type', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('106228945', '1', '公告', 'sys.notify.type', null, null, null, null, null, null, '2');
INSERT INTO `sys_dict` VALUES ('106228946', '2', '即时', 'sys.notify.type', null, null, null, null, null, null, '3');
INSERT INTO `sys_dict` VALUES ('108439476', '0', '未导入', 'change.importStatus', null, null, null, null, null, null, '0');
INSERT INTO `sys_dict` VALUES ('108439477', '1', '立项已导入', 'change.importStatus', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('108439478', '2', '费用已导入', 'change.importStatus', null, null, null, null, null, null, '2');
INSERT INTO `sys_dict` VALUES ('131403783', '0', '中期结算', 'balance.type', null, null, null, null, null, '结算类别', '1');
INSERT INTO `sys_dict` VALUES ('131403784', '1', '完工结算', 'balance.type', null, null, null, null, null, '结算类别', '1');
INSERT INTO `sys_dict` VALUES ('139097286', '0', '分部工程类型1', 'project.branch.type', null, null, null, null, null, '模型类型', '2');
INSERT INTO `sys_dict` VALUES ('139097287', '1', '分部工程类型2', 'project.branch.type', null, null, null, null, null, '模型类型', '2');
INSERT INTO `sys_dict` VALUES ('139097288', '0', '待评定', 'project.branch.status', null, null, null, null, null, '分部工程状态', '0');
INSERT INTO `sys_dict` VALUES ('139097289', '1', '已评定', 'project.branch.status', null, null, null, null, null, '分部工程状态', '1');
INSERT INTO `sys_dict` VALUES ('139097290', '2', '验收中', 'project.branch.status', null, null, null, null, null, '分部工程状态', '2');
INSERT INTO `sys_dict` VALUES ('139097291', '3', '已验收', 'project.branch.status', null, null, null, null, null, '分部工程状态', '3');
INSERT INTO `sys_dict` VALUES ('139307583', '0', '电汇', 'pm.pay.type', null, null, null, null, null, '付款方式', '1');
INSERT INTO `sys_dict` VALUES ('139307584', '1', '转账支票', 'pm.pay.type', null, null, null, null, null, '付款方式', '2');
INSERT INTO `sys_dict` VALUES ('139307585', '2', '现金支票', 'pm.pay.type', null, null, null, null, null, '付款方式', '3');
INSERT INTO `sys_dict` VALUES ('139307586', '3', '现金', 'pm.pay.type', null, null, null, null, null, '付款方式', '4');
INSERT INTO `sys_dict` VALUES ('139307587', '0', '发票未收到', 'pm.pay.status', null, null, null, null, null, '发票状态', '3');
INSERT INTO `sys_dict` VALUES ('139307588', '1', '发票已收到', 'pm.pay.status', null, null, null, null, null, '发票状态', '2');
INSERT INTO `sys_dict` VALUES ('139307589', '0', '已经交货', 'pm.pay.goods.status', null, null, null, null, null, '发票状态', '1');
INSERT INTO `sys_dict` VALUES ('139307590', '1', '尚未交货', 'pm.pay.goods.status', null, null, null, null, null, '发票状态', '2');
INSERT INTO `sys_dict` VALUES ('139307591', '2', '款到发货', 'pm.pay.goods.status', null, null, null, null, null, '发票状态', '3');
INSERT INTO `sys_dict` VALUES ('139357298', '205', '纪委书记', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '550');
INSERT INTO `sys_dict` VALUES ('139407287', '0', '人民币', 'currency.type', null, null, null, null, null, '货币种类', '1');
INSERT INTO `sys_dict` VALUES ('139407288', '1', '美元', 'currency.type', null, null, null, null, null, '货币种类', '2');
INSERT INTO `sys_dict` VALUES ('139407289', '0', '形式发票', 'paycert.attachment.type', null, null, null, null, null, '支付证书附件类别', '1');
INSERT INTO `sys_dict` VALUES ('139407290', '1', 'VAT发票', 'paycert.attachment.type', null, null, null, null, null, '支付证书附件类别', '2');
INSERT INTO `sys_dict` VALUES ('139407291', '2', '收据', 'paycert.attachment.type', null, null, null, null, null, '支付证书附件类别', '3');
INSERT INTO `sys_dict` VALUES ('139407292', '3', '公司税单卡', 'paycert.attachment.type', null, null, null, null, null, '支付证书附件类别', '4');
INSERT INTO `sys_dict` VALUES ('139407293', '4', 'SBU证书', 'paycert.attachment.type', null, null, null, null, null, '支付证书附件类别', '5');
INSERT INTO `sys_dict` VALUES ('139407294', '5', '承诺书', 'paycert.attachment.type', null, null, null, null, null, '支付证书附件类别', '6');
INSERT INTO `sys_dict` VALUES ('139407295', '6', '其他合同约定付款支持文件', 'paycert.attachment.type', null, null, null, null, null, '支付证书附件类别', '7');
INSERT INTO `sys_dict` VALUES ('139447286', '103', '财务管理部', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '442');
INSERT INTO `sys_dict` VALUES ('139447287', '108', '质安环境部', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '448');
INSERT INTO `sys_dict` VALUES ('139447288', '200', '副总经理1', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '460');
INSERT INTO `sys_dict` VALUES ('139447289', '201', '副总经理2', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '470');
INSERT INTO `sys_dict` VALUES ('139447290', '204', '总经济师', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '500');
INSERT INTO `sys_dict` VALUES ('139447291', '203', '总会计师', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '490');
INSERT INTO `sys_dict` VALUES ('139447292', '202', '副总经理3', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '480');
INSERT INTO `sys_dict` VALUES ('139647286', '23', '综合办公室', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '23');
INSERT INTO `sys_dict` VALUES ('139647287', '24', '财务部', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '24');
INSERT INTO `sys_dict` VALUES ('139647288', '25', '人资部', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '25');
INSERT INTO `sys_dict` VALUES ('139647289', '26', '质安部', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '26');
INSERT INTO `sys_dict` VALUES ('139647290', '27', '技术部', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '27');
INSERT INTO `sys_dict` VALUES ('139647291', '28', '政工部', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '28');
INSERT INTO `sys_dict` VALUES ('139647296', '29', '（财务）副总', 'contract.role.type', null, '2', null, '合同业主单位', null, '合同角色类型', '112');
INSERT INTO `sys_dict` VALUES ('139677290', '100', '综合部', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '440');
INSERT INTO `sys_dict` VALUES ('139677291', '101', '计划管理部', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '441');
INSERT INTO `sys_dict` VALUES ('139677292', '102', '工程管理部', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '442');
INSERT INTO `sys_dict` VALUES ('139677293', '104', '政工部', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '444');
INSERT INTO `sys_dict` VALUES ('139677294', '105', '人资部', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '445');
INSERT INTO `sys_dict` VALUES ('139677295', '106', '运营部', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '446');
INSERT INTO `sys_dict` VALUES ('139677296', '107', '工程技术部', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '447');
INSERT INTO `sys_dict` VALUES ('139677297', '206', '总经理', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '560');
INSERT INTO `sys_dict` VALUES ('139677298', '220', '党委书记(董事长)', 'contract.role.type', null, '4', null, '华电香港公司', null, '合同角色类型', '570');
INSERT INTO `sys_dict` VALUES ('139987300', '2', '印尼盾', 'currency.type', null, null, null, null, null, '货币种类', '3');
INSERT INTO `sys_dict` VALUES ('140277306', '60', '工程部', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '300');
INSERT INTO `sys_dict` VALUES ('140277307', '61', '技术部', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '310');
INSERT INTO `sys_dict` VALUES ('140277308', '62', '质安部', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '320');
INSERT INTO `sys_dict` VALUES ('140277309', '66', '物资部', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '360');
INSERT INTO `sys_dict` VALUES ('140277310', '63', '商法部', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '330');
INSERT INTO `sys_dict` VALUES ('140277311', '64', '财务部', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '340');
INSERT INTO `sys_dict` VALUES ('140277312', '65', '办公室', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '350');
INSERT INTO `sys_dict` VALUES ('140277314', '80', '（工程）副总', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '380');
INSERT INTO `sys_dict` VALUES ('140277315', '81', '（安监）副总', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '381');
INSERT INTO `sys_dict` VALUES ('140277316', '82', '（合同）副总', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '382');
INSERT INTO `sys_dict` VALUES ('140277317', '83', '（财务）副总', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '383');
INSERT INTO `sys_dict` VALUES ('140277318', '84', '总经理', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '384');
INSERT INTO `sys_dict` VALUES ('140277319', '85', '董事长', 'contract.role.type', null, '3', null, '华电印尼PE公司', null, '合同角色类型', '385');
INSERT INTO `sys_dict` VALUES ('140457311', '0', '未启用', 'procdef.status', null, null, null, null, null, '流程定义状态', '0');
INSERT INTO `sys_dict` VALUES ('140457312', '1', '启用', 'procdef.status', null, null, null, null, null, '流程定义状态', '1');
INSERT INTO `sys_dict` VALUES ('140457313', '7', '财务支付', 'procdef.type', null, null, null, null, null, '流程定义类型', '70');
INSERT INTO `sys_dict` VALUES ('140457315', '6', '结算核销', 'procdef.type', null, null, null, null, null, '流程定义类型', '60');
INSERT INTO `sys_dict` VALUES ('140457316', '5', '签证管理', 'procdef.type', null, null, null, null, null, '流程定义类型', '50');
INSERT INTO `sys_dict` VALUES ('140487288', '0', '普通合同', 'proc.permission', null, null, null, null, null, '适用合同类型', '0');
INSERT INTO `sys_dict` VALUES ('140487289', '1', '小额合同', 'proc.permission', null, null, null, null, null, '适用合同类型', '1');
INSERT INTO `sys_dict` VALUES ('140487290', '2', '全部合同', 'proc.permission', null, null, null, null, null, '适用合同类型', '2');
INSERT INTO `sys_dict` VALUES ('140718093', '2', '区', 'area.type', null, null, null, null, null, '区域类型', '2');
INSERT INTO `sys_dict` VALUES ('140718094', '0', '国家', 'area.type', null, null, null, null, null, '区域类型', '0');
INSERT INTO `sys_dict` VALUES ('140907391', '4', '费用报销模型', 'act.model.type', null, null, null, null, null, '模型类型', '4');
INSERT INTO `sys_dict` VALUES ('140907400', '2', '综合管理', 'procdef.type', null, null, null, null, null, '流程定义类型', '20');
INSERT INTO `sys_dict` VALUES ('140917350', '0', '办公室用品', 'oa.purchase.type', null, null, null, null, null, '用品采购类别', '0');
INSERT INTO `sys_dict` VALUES ('140917351', '1', '低值易耗品', 'oa.purchase.type', null, null, null, null, null, '用品采购类别', '1');
INSERT INTO `sys_dict` VALUES ('140917352', '2', '固定资产', 'oa.purchase.type', null, null, null, null, null, '用品采购类别', '2');
INSERT INTO `sys_dict` VALUES ('140917353', '3', '其他', 'oa.purchase.type', null, null, null, null, null, '用品采购类别', '3');
INSERT INTO `sys_dict` VALUES ('140967357', '0', '餐费', 'hospitality.type', null, null, null, null, null, '业务招待费类型', '0');
INSERT INTO `sys_dict` VALUES ('140967358', '1', '礼品费', 'hospitality.type', null, null, null, null, null, '业务招待费类型', '1');
INSERT INTO `sys_dict` VALUES ('140967359', '2', '食品费', 'hospitality.type', null, null, null, null, null, '业务招待费类型', '2');
INSERT INTO `sys_dict` VALUES ('140967360', '3', '其他', 'hospitality.type', null, null, null, null, null, '业务招待费类型', '3');
INSERT INTO `sys_dict` VALUES ('140967364', '0', '飞机', 'traffic.type', null, null, null, null, null, '交通方式', '0');
INSERT INTO `sys_dict` VALUES ('140967365', '1', '火车', 'traffic.type', null, null, null, null, null, '交通方式', '1');
INSERT INTO `sys_dict` VALUES ('140967366', '2', '汽车', 'traffic.type', null, null, null, null, null, '交通方式', '2');
INSERT INTO `sys_dict` VALUES ('140967367', '3', '船', 'traffic.type', null, null, null, null, null, '交通方式', '3');
INSERT INTO `sys_dict` VALUES ('140967368', '4', '其他', 'traffic.type', null, null, null, null, null, '交通方式', '4');
INSERT INTO `sys_dict` VALUES ('141087287', '4', '变更管理', 'procdef.type', null, null, null, null, null, '流程定义类型', '40');
INSERT INTO `sys_dict` VALUES ('141757354', '0', '普通申请', 'oa.record.type', null, null, null, null, null, '附件记录类型', '0');
INSERT INTO `sys_dict` VALUES ('141757355', '1', '出差（国）补贴', 'oa.record.type', null, null, null, null, null, '附件记录类型', '1');
INSERT INTO `sys_dict` VALUES ('141757356', '2', '差旅申请', 'oa.record.type', null, null, null, null, null, '附件记录类型', '2');
INSERT INTO `sys_dict` VALUES ('141757357', '3', '招待费说明', 'oa.record.type', null, null, null, null, null, '附件记录类型', '3');
INSERT INTO `sys_dict` VALUES ('141757358', '4', '采购申请', 'oa.record.type', null, null, null, null, null, '附件记录类型', '4');
INSERT INTO `sys_dict` VALUES ('141757359', '5', '交际费申请', 'oa.record.type', null, null, null, null, null, '附件记录类型', '5');
INSERT INTO `sys_dict` VALUES ('141837344', '0', '01 文具费 Stationery feeStationery fee', 'expense.office.type', null, null, null, null, null, '费用报办公费类型', '0');
INSERT INTO `sys_dict` VALUES ('141837345', '1', '02 印刷费 Printing fee', 'expense.office.type', null, null, null, null, null, '费用报办公费类型', '1');
INSERT INTO `sys_dict` VALUES ('141837346', '2', '03 邮寄费 Postage', 'expense.office.type', null, null, null, null, null, '费用报办公费类型', '2');
INSERT INTO `sys_dict` VALUES ('141837347', '3', '04 资料费 Material fee', 'expense.office.type', null, null, null, null, null, '费用报办公费类型', '3');
INSERT INTO `sys_dict` VALUES ('141837348', '4', '05 修理费 Repair fee', 'expense.office.type', null, null, null, null, null, '费用报办公费类型', '4');
INSERT INTO `sys_dict` VALUES ('141837349', '5', '06 翻译费Translation', 'expense.office.type', null, null, null, null, null, '费用报办公费类型', '5');
INSERT INTO `sys_dict` VALUES ('141837350', '7', '08 其他费 Other fee', 'expense.office.type', null, null, null, null, null, '费用报办公费类型', '7');
INSERT INTO `sys_dict` VALUES ('141837351', '0', '01 机票Flight Ticket', 'expense.travel.type', null, null, null, null, null, '费用报办公费类型', '0');
INSERT INTO `sys_dict` VALUES ('141837352', '1', '02 火车票Train Ticket', 'expense.travel.type', null, null, null, null, null, '费用报办公费类型', '1');
INSERT INTO `sys_dict` VALUES ('141837353', '2', '03 住宿费Accommodation ', 'expense.travel.type', null, null, null, null, null, '费用报差旅费类型', '2');
INSERT INTO `sys_dict` VALUES ('141837355', '3', '04 打车费Taxi', 'expense.travel.type', null, null, null, null, null, '费用报差旅费类型', '3');
INSERT INTO `sys_dict` VALUES ('141837356', '4', '05 出差补助Travel Allowance', 'expense.travel.type', null, null, null, null, null, '费用报差旅费类型', '4');
INSERT INTO `sys_dict` VALUES ('141837357', '5', '06 餐费Meals', 'expense.travel.type', null, null, null, null, null, '费用报差旅费类型', '5');
INSERT INTO `sys_dict` VALUES ('141837358', '6', '07 其他费 Other fee', 'expense.travel.type', null, null, null, null, null, '费用报差旅费类型', '6');
INSERT INTO `sys_dict` VALUES ('141837359', '0', '01 固定电话费 Telephone fee', 'expense.communication.type', null, null, null, null, null, '费用报通讯费类型', '0');
INSERT INTO `sys_dict` VALUES ('141837360', '1', '02 手机话费 Mobile fee', 'expense.communication.type', null, null, null, null, null, '费用报通讯费类型', '1');
INSERT INTO `sys_dict` VALUES ('141837361', '2', '03 网络费Internet', 'expense.communication.type', null, null, null, null, null, '费用报通讯费类型', '2');
INSERT INTO `sys_dict` VALUES ('141837362', '0', '01 餐费 Meals fee', 'expense.entertainment.type', null, null, null, null, null, '费用报招待费类型', '0');
INSERT INTO `sys_dict` VALUES ('141837365', '1', '02 礼品费 Gift fee', 'expense.entertainment.type', null, null, null, null, null, '费用报招待费类型', '1');
INSERT INTO `sys_dict` VALUES ('141837368', '2', '03 其他费 Other fee', 'expense.entertainment.type', null, null, null, null, null, '费用报招待费类型', '2');
INSERT INTO `sys_dict` VALUES ('141837369', '0', '01 汽油费 Petrol fee', 'expense.motor.type', null, null, null, null, null, '费用报销汽车费类型', '0');
INSERT INTO `sys_dict` VALUES ('141837370', '1', '02 汽车修理费 Repair fee', 'expense.motor.type', null, null, null, null, null, '费用报销汽车费类型', '1');
INSERT INTO `sys_dict` VALUES ('141837371', '2', '03 停车&过路费Parking&Toll', 'expense.motor.type', null, null, null, null, null, '费用报销汽车费类型', '2');
INSERT INTO `sys_dict` VALUES ('141837372', '4', '05 其他费 Other fee', 'expense.motor.type', null, null, null, null, null, '费用报销汽车费类型', '4');
INSERT INTO `sys_dict` VALUES ('141837373', '0', '01 水电费Water&Electricity', 'expense.other.type', null, null, null, null, null, '费用报销汽车费类型', '0');
INSERT INTO `sys_dict` VALUES ('141837374', '1', '02 劳动保护费Labor Protection', 'expense.other.type', null, null, null, null, null, '费用报销汽车费类型', '1');
INSERT INTO `sys_dict` VALUES ('141837375', '2', '03 物业费Property Service', 'expense.other.type', null, null, null, null, null, '费用报销汽车费类型', '2');
INSERT INTO `sys_dict` VALUES ('141837376', '3', '04 广告费Advertisement', 'expense.other.type', null, null, null, null, null, '费用报销汽车费类型', '3');
INSERT INTO `sys_dict` VALUES ('141837377', '4', '05 伙食费Food Expense', 'expense.other.type', null, null, null, null, null, '费用报销汽车费类型', '4');
INSERT INTO `sys_dict` VALUES ('141867339', '0', '01 办公室Office ', 'expense.rental.type', null, null, null, null, null, '费用报租赁费类型', '0');
INSERT INTO `sys_dict` VALUES ('141867340', '1', '02 宿舍Apartment', 'expense.rental.type', null, null, null, null, null, '费用报租赁费类型', '1');
INSERT INTO `sys_dict` VALUES ('141867341', '2', '03 租车费Car', 'expense.rental.type', null, null, null, null, null, '费用报租赁费类型', '2');
INSERT INTO `sys_dict` VALUES ('141867342', '3', '04 租赁设备Equipment', 'expense.rental.type', null, null, null, null, null, '费用报租赁费类型', '3');
INSERT INTO `sys_dict` VALUES ('141867343', '4', '05 其他Other', 'expense.rental.type', null, null, null, null, null, '费用报租赁费类型', '4');
INSERT INTO `sys_dict` VALUES ('141867344', '0', '01 预扣所得税 WHT4.2', 'expense.tax.type', null, null, null, null, null, '费用报租赁费类型', '0');
INSERT INTO `sys_dict` VALUES ('141867345', '1', '02 预扣所得税 WHT23', 'expense.tax.type', null, null, null, null, null, '费用报租赁费类型', '1');
INSERT INTO `sys_dict` VALUES ('141867346', '2', '03 预扣所得税 WHT26', 'expense.tax.type', null, null, null, null, null, '费用报租赁费类型', '2');
INSERT INTO `sys_dict` VALUES ('141867347', '3', '04 代扣个税 WHT21', 'expense.tax.type', null, null, null, null, null, '费用报租赁费类型', '3');
INSERT INTO `sys_dict` VALUES ('141867348', '4', '05 增值税 VAT', 'expense.tax.type', null, null, null, null, null, '费用报租赁费类型', '4');
INSERT INTO `sys_dict` VALUES ('141867349', '5', '06 分支机构利润税 PPH26.4', 'expense.tax.type', null, null, null, null, null, '费用报租赁费类型', '5');
INSERT INTO `sys_dict` VALUES ('141867350', '6', '07 其他Other', 'expense.tax.type', null, null, null, null, null, '费用报租赁费类型', '6');
INSERT INTO `sys_dict` VALUES ('141867351', '0', '01 办公类Office', 'expense.low.value.type', null, null, null, null, null, '费用报租赁费类型', '0');
INSERT INTO `sys_dict` VALUES ('141867352', '1', '02 生活类Living', 'expense.low.value.type', null, null, null, null, null, '费用报租赁费类型', '1');
INSERT INTO `sys_dict` VALUES ('141867353', '2', '03 易耗类Consumables', 'expense.low.value.type', null, null, null, null, null, '费用报租赁费类型', '2');
INSERT INTO `sys_dict` VALUES ('141867354', '3', '04 其他Other', 'expense.low.value.type', null, null, null, null, null, '费用报租赁费类型', '3');
INSERT INTO `sys_dict` VALUES ('141867355', '0', '01 工作签证Work Visa', 'expense.visa.type', null, null, null, null, null, '费用报租赁费类型', '0');
INSERT INTO `sys_dict` VALUES ('141867356', '1', '02 印尼VOA', 'expense.visa.type', null, null, null, null, null, '费用报租赁费类型', '1');
INSERT INTO `sys_dict` VALUES ('141867357', '2', '03 签证代理费Visa Agent', 'expense.visa.type', null, null, null, null, null, '费用报租赁费类型', '2');
INSERT INTO `sys_dict` VALUES ('141867358', '3', '04 其他Other', 'expense.visa.type', null, null, null, null, null, '费用报租赁费类型', '3');
INSERT INTO `sys_dict` VALUES ('141867359', '0', '01 律师费Legal Service', 'expense.intermediary.type', null, null, null, null, null, '费用报租赁费类型', '0');
INSERT INTO `sys_dict` VALUES ('141867360', '1', '02 税务费用Tax Service', 'expense.intermediary.type', null, null, null, null, null, '费用报租赁费类型', '1');
INSERT INTO `sys_dict` VALUES ('141867361', '2', '03 审计费用Audit Service', 'expense.intermediary.type', null, null, null, null, null, '费用报租赁费类型', '2');
INSERT INTO `sys_dict` VALUES ('141867362', '3', '04 代理费Agent Service', 'expense.intermediary.type', null, null, null, null, null, '费用报租赁费类型', '3');
INSERT INTO `sys_dict` VALUES ('141867363', '4', '05 其他Other', 'expense.intermediary.type', null, null, null, null, null, '费用报租赁费类型', '4');
INSERT INTO `sys_dict` VALUES ('141867364', '0', '01 工资Salary', 'expense.labor.cost.type', null, null, null, null, null, '费用报租赁费类型', '0');
INSERT INTO `sys_dict` VALUES ('141867365', '1', '02 社保BPJS', 'expense.labor.cost.type', null, null, null, null, null, '费用报租赁费类型', '1');
INSERT INTO `sys_dict` VALUES ('141867366', '2', '03 医疗费Medical', 'expense.labor.cost.type', null, null, null, null, null, '费用报租赁费类型', '2');
INSERT INTO `sys_dict` VALUES ('141867367', '3', '04 福利费Welfare', 'expense.labor.cost.type', null, null, null, null, null, '费用报租赁费类型', '3');
INSERT INTO `sys_dict` VALUES ('141867368', '4', '05 其他Other', 'expense.labor.cost.type', null, null, null, null, null, '费用报租赁费类型', '4');
INSERT INTO `sys_dict` VALUES ('141867370', '5', '06 运输费Transportation', 'expense.other.type', null, null, null, null, null, '费用报销汽车费类型', '5');
INSERT INTO `sys_dict` VALUES ('141867371', '6', '07 捐赠支出Donation', 'expense.other.type', null, null, null, null, null, '费用报销汽车费类型', '6');
INSERT INTO `sys_dict` VALUES ('141867374', '6', '07 会议费Meeting', 'expense.office.type', null, null, null, null, null, '费用报办公费类型', '6');
INSERT INTO `sys_dict` VALUES ('141867375', '3', '04 其他Other', 'expense.communication.type', null, null, null, null, null, '费用报通讯费类型', '3');
INSERT INTO `sys_dict` VALUES ('141867376', '3', '04 保险费Insurance', 'expense.motor.type', null, null, null, null, null, '费用报销汽车费类型', '3');
INSERT INTO `sys_dict` VALUES ('142241945', '0', '用车', 'oa.apply.type', null, null, null, null, null, '普通申请类别', '0');
INSERT INTO `sys_dict` VALUES ('142241946', '1', '用印', 'oa.apply.type', null, null, null, null, null, '普通申请类别', '1');
INSERT INTO `sys_dict` VALUES ('142241947', '2', '会务', 'oa.apply.type', null, null, null, null, null, '普通申请类别', '2');
INSERT INTO `sys_dict` VALUES ('142241948', '4', '其他', 'oa.apply.type', null, null, null, null, null, '普通申请类别', '4');
INSERT INTO `sys_dict` VALUES ('142289078', '0', '银行转账', 'oa.expense.pay.type', null, null, null, null, null, '费用报销支付方式', '0');
INSERT INTO `sys_dict` VALUES ('142289079', '1', '现金支付', 'oa.expense.pay.type', null, null, null, null, null, '费用报销支付方式', '1');
INSERT INTO `sys_dict` VALUES ('142289080', '0', '未支付', 'oa.expense.pay.status', null, null, null, null, null, '费用报销支付方式', '0');
INSERT INTO `sys_dict` VALUES ('142289081', '1', '已支付', 'oa.expense.pay.status', null, null, null, null, null, '费用报销支付方式', '1');
INSERT INTO `sys_dict` VALUES ('142297344', '0', '未支付', 'pm.pay.result', null, null, null, null, null, null, '0');
INSERT INTO `sys_dict` VALUES ('142297345', '1', '已支付', 'pm.pay.result', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('142518173', '3', '更新', 'sys.notify.type', null, null, null, null, null, null, '4');
INSERT INTO `sys_dict` VALUES ('142548586', '3', '翻译', 'oa.apply.type', null, null, null, null, null, '普通申请类别', '3');
INSERT INTO `sys_dict` VALUES ('142587360', '6', '借款申请', 'oa.record.type', null, null, null, null, null, '附件记录类型', '6');
INSERT INTO `sys_dict` VALUES ('142621811', '2', '冲销备用金', 'oa.expense.pay.type', null, null, null, null, null, '费用报销支付方式', '2');
INSERT INTO `sys_dict` VALUES ('142839175', '7', '08 固定资产Fixed assets', 'expense.other.type', null, null, null, null, null, '费用报销其他类型', '7');
INSERT INTO `sys_dict` VALUES ('143197504', '3', '新币', 'currency.type', null, null, null, null, null, '货币种类', '4');
INSERT INTO `sys_dict` VALUES ('143197505', '4', '马币', 'currency.type', null, null, null, null, null, '货币种类', '5');
INSERT INTO `sys_dict` VALUES ('143227357', '7', '招待费申请', 'oa.record.type', null, null, null, null, null, '附件记录类型', '7');
INSERT INTO `sys_dict` VALUES ('143842574', '8', '09 其他 Others', 'expense.other.type', null, null, null, null, null, '费用报销其他类型', '8');
INSERT INTO `sys_dict` VALUES ('143842577', '7', '03 其他 Others', 'expense.tax.type', null, null, null, null, null, '费用报租赁费类型', '7');
INSERT INTO `sys_dict` VALUES ('143842578', '0', '01 会议费 Meeting Fee', 'expense.meeting.type', null, null, null, null, null, '费用报销会议费类型', '0');
INSERT INTO `sys_dict` VALUES ('143842580', '1', '02 住宿费 Accommodation Fee', 'expense.meeting.type', null, null, null, null, null, '费用报销会议费类型', '1');
INSERT INTO `sys_dict` VALUES ('143842595', '2', '02 餐费 Meal Fee', 'expense.meeting.type', null, null, null, null, null, '费用报销会议费类型', '2');
INSERT INTO `sys_dict` VALUES ('143842599', '3', '02 其他 Others', 'expense.meeting.type', null, null, null, null, null, '费用报销会议费类型', '3');
INSERT INTO `sys_dict` VALUES ('144247386', '2', '出差', 'business.trip.category', null, null, null, null, null, '差旅类型', '2');
INSERT INTO `sys_dict` VALUES ('144247387', '1', '休假', 'business.trip.category', null, null, null, null, null, '差旅类型', '1');
INSERT INTO `sys_dict` VALUES ('145867572', '2', '委托(新增)', 'change.mode', null, null, null, null, null, null, '2');
INSERT INTO `sys_dict` VALUES ('145890576', '1', '工程变更', 'change.mode', null, null, null, null, null, null, '0');
INSERT INTO `sys_dict` VALUES ('145895406', '5', '欧元', 'currency.type', null, null, null, null, null, '货币种类', '6');
INSERT INTO `sys_dict` VALUES ('145937891', '3', '签证(计日工)', 'change.mode', null, null, null, null, null, null, '3');
INSERT INTO `sys_dict` VALUES ('145955755', '300', '咨询', 'contract.role.type', null, '5', null, '合同咨询单位', null, '合同角色类型', '600');
INSERT INTO `sys_dict` VALUES ('146157336', '2', '测试合同', 'contract.type', null, null, null, null, null, '合同类型', '2');
INSERT INTO `sys_dict` VALUES ('146167433', 'integer', '整型', 'dynamic.form.field.type', null, null, null, null, null, '动态表单字段类型', '0');
INSERT INTO `sys_dict` VALUES ('146167434', 'double', '小数', 'dynamic.form.field.type', null, null, null, null, null, '动态表单字段类型', '1');
INSERT INTO `sys_dict` VALUES ('146167435', 'string', '字符串', 'dynamic.form.field.type', null, null, null, null, null, '动态表单字段类型', '2');
INSERT INTO `sys_dict` VALUES ('146167436', 'dict', '字典值', 'dynamic.form.field.type', null, null, null, null, null, '动态表单字段类型', '3');
INSERT INTO `sys_dict` VALUES ('146167437', 'entity', '实体', 'dynamic.form.field.type', null, null, null, null, null, '动态表单字段类型', '4');
INSERT INTO `sys_dict` VALUES ('500011093', '1', '是', 'sys.bool', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('500015000', 'zh-CN', '中文', 'locale.lang', 'locale.lang.chinese', null, null, null, null, '国际化编码', '1');
INSERT INTO `sys_dict` VALUES ('500015006', 'en-US', '英文', 'locale.lang', 'locale.lang.english', null, null, null, null, '国际化编码', '1');
INSERT INTO `sys_dict` VALUES ('500025000', '0', '正常', 'user.status', null, null, null, null, null, null, '1');
INSERT INTO `sys_dict` VALUES ('500025002', '1', '锁定', 'user.status', null, null, null, null, null, null, '2');
INSERT INTO `sys_dict` VALUES ('500035002', '1', '甲供材料', 'material.category', null, null, null, null, null, '物资类别', '1');
INSERT INTO `sys_dict` VALUES ('500035003', '2', '易损耗品', 'material.category', null, null, null, null, null, '物资类别', '2');
INSERT INTO `sys_dict` VALUES ('500043013', '0', '入库', 'invoice.type', null, null, null, null, null, '出入库类型', '10');
INSERT INTO `sys_dict` VALUES ('500043014', '1', '出库', 'invoice.type', null, null, null, null, null, '出入库类型', '20');

-- ----------------------------
-- Table structure for sys_locale
-- ----------------------------
DROP TABLE IF EXISTS `sys_locale`;
CREATE TABLE `sys_locale` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `lang` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '语言',
  `code` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '编码',
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `value` varchar(200) CHARACTER SET utf8mb4 NOT NULL COMMENT '键值',
  PRIMARY KEY (`id`),
  KEY `locale_code_lang` (`lang`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='国际化表';

-- ----------------------------
-- Records of sys_locale
-- ----------------------------
INSERT INTO `sys_locale` VALUES ('1', 'zh-CN', 'login.btn.text', '登陆', '登陆');
INSERT INTO `sys_locale` VALUES ('2', 'zh-CN', 'title', '集成化项目管理系统3.0', '集成化项目管理系统3.0');
INSERT INTO `sys_locale` VALUES ('3', 'zh-CN', 'personal.work', '个人办公', '个人办公');
INSERT INTO `sys_locale` VALUES ('4', 'en-US', 'login.btn.text', '登陆', 'Sign In');
INSERT INTO `sys_locale` VALUES ('5', 'en-US', 'title', '集成化项目管理系统3.0', 'Project Manage System 3.0');
INSERT INTO `sys_locale` VALUES ('6', 'en-US', 'personal.work', '个人办公', 'Task');
INSERT INTO `sys_locale` VALUES ('500011381', 'en-US', 'dialog.btn.ok.text', '确定', 'Confirm');
INSERT INTO `sys_locale` VALUES ('500011384', 'zh-CN', 'dialog.btn.ok.text', '确定', '确定');
INSERT INTO `sys_locale` VALUES ('500011405', 'zh-CN', 'dialog.btn.cancel.text', '取消', '取消');
INSERT INTO `sys_locale` VALUES ('500011408', 'en-US', 'dialog.btn.cancel.text', '取消', 'Cancel');
INSERT INTO `sys_locale` VALUES ('500011415', 'zh-CN', 'search.bar.btn.advanced.text', '高级查询', '高级查询');
INSERT INTO `sys_locale` VALUES ('500011418', 'en-US', 'search.bar.btn.advanced.text', '高级查询', 'Advanced Search');
INSERT INTO `sys_locale` VALUES ('500011433', 'zh-CN', 'search.bar.btn.search.text', '查询', '查询');
INSERT INTO `sys_locale` VALUES ('500011436', 'en-US', 'search.bar.btn.search.text', '查询', 'Search');
INSERT INTO `sys_locale` VALUES ('500011463', 'en-US', 'search.bar.btn.reset.text', '重置查询', 'Reset');
INSERT INTO `sys_locale` VALUES ('500011468', 'zh-CN', 'search.bar.btn.reset.text', '重置查询', '重置');
INSERT INTO `sys_locale` VALUES ('500011479', 'zh-CN', 'add', '添加', '添加');
INSERT INTO `sys_locale` VALUES ('500011482', 'en-US', 'add', '添加', 'Add');
INSERT INTO `sys_locale` VALUES ('500015017', 'zh-CN', 'modify', '修改', '修改');
INSERT INTO `sys_locale` VALUES ('500015023', 'en-US', 'modify', '修改', 'Modify');
INSERT INTO `sys_locale` VALUES ('500015089', 'zh-CN', 'delete', '删除按钮', '删除');
INSERT INTO `sys_locale` VALUES ('500015116', 'en-US', 'delete', '删除按钮', 'Delete');
INSERT INTO `sys_locale` VALUES ('500015199', 'zh-CN', 'locale.lang.chinese', '中文', '中文');
INSERT INTO `sys_locale` VALUES ('500015203', 'en-US', 'locale.lang.chinese', '中文', 'Chinese');
INSERT INTO `sys_locale` VALUES ('500015212', 'en-US', 'locale.lang.english', '英文', 'English');
INSERT INTO `sys_locale` VALUES ('500015216', 'zh-CN', 'locale.lang.english', '英文', '英文');
INSERT INTO `sys_locale` VALUES ('500016000', 'zh-CN', 'locale.lang', '语言', '语言');
INSERT INTO `sys_locale` VALUES ('500016001', 'en-US', 'locale.lang', '语言', 'Language');
INSERT INTO `sys_locale` VALUES ('500016002', 'zh-CN', 'locale.code', '编码', '编码');
INSERT INTO `sys_locale` VALUES ('500016003', 'en-US', 'locale.code', '编码', 'Code');
INSERT INTO `sys_locale` VALUES ('500016004', 'zh-CN', 'locale.name', '名称', '名称');
INSERT INTO `sys_locale` VALUES ('500016005', 'en-US', 'locale.name', '名称', 'Name');
INSERT INTO `sys_locale` VALUES ('500016006', 'en-US', 'locale.value', '键值', 'Value');
INSERT INTO `sys_locale` VALUES ('500016007', 'zh-CN', 'locale.value', '键值', '键值');
INSERT INTO `sys_locale` VALUES ('500016008', 'zh-CN', 'locale', '国际化', '国际化');
INSERT INTO `sys_locale` VALUES ('500016009', 'en-US', 'locale', '国际化', 'Locale');
INSERT INTO `sys_locale` VALUES ('500016010', 'zh-CN', 'data.unselect.notice', '数据未选择提醒', '请选择数据！');
INSERT INTO `sys_locale` VALUES ('500016011', 'en-US', 'data.unselect.notice', '数据未选择提醒', 'Must select the data first!');
INSERT INTO `sys_locale` VALUES ('500017000', 'zh-CN', 'locale.add.title', '国际化添加标题', '国际化添加');
INSERT INTO `sys_locale` VALUES ('500017001', 'en-US', 'locale.add.title', '国际化添加标题', 'Locale Add');
INSERT INTO `sys_locale` VALUES ('500017002', 'zh-CN', 'locale.modify.title', '国际化修改标题', '国际化修改');
INSERT INTO `sys_locale` VALUES ('500017003', 'en-US', 'locale.modify.title', '国际化修改标题', 'Locale Modify');
INSERT INTO `sys_locale` VALUES ('500017004', 'en-US', 'locale.delete.title', '国际化修改标题', 'Locale Delete');
INSERT INTO `sys_locale` VALUES ('500017005', 'zh-CN', 'locale.delete.title', '国际化修改标题', '国际化删除');
INSERT INTO `sys_locale` VALUES ('500017006', 'en-US', 'data.delete.notice', '数据删除提醒', 'Are you sure you want to delete this data ？');
INSERT INTO `sys_locale` VALUES ('500017007', 'zh-CN', 'data.delete.notice', '数据删除提醒', '确定要删除该数据吗？');
INSERT INTO `sys_locale` VALUES ('500018000', 'zh-CN', 'table.move.up', '上移', '上移');
INSERT INTO `sys_locale` VALUES ('500018001', 'en-US', 'table.move.up', '上移', 'Move Up');
INSERT INTO `sys_locale` VALUES ('500018002', 'en-US', 'table.move.down', '下移', 'Move Down');
INSERT INTO `sys_locale` VALUES ('500018003', 'zh-CN', 'table.move.down', '下移', '下移');
INSERT INTO `sys_locale` VALUES ('500018004', 'en-US', 'table.shift.up', '升级', 'Shift Up');
INSERT INTO `sys_locale` VALUES ('500018005', 'zh-CN', 'table.shift.up', '升级', '升级');
INSERT INTO `sys_locale` VALUES ('500018006', 'en-US', 'table.shift.down', '降级', 'Shift Down');
INSERT INTO `sys_locale` VALUES ('500018007', 'zh-CN', 'table.shift.down', '降级', '降级');
INSERT INTO `sys_locale` VALUES ('500018008', 'zh-CN', '1', '1', '1');
INSERT INTO `sys_locale` VALUES ('500018009', 'en-US', '1', '1', '1');
INSERT INTO `sys_locale` VALUES ('500018010', 'zh-CN', '2', '2', '2');
INSERT INTO `sys_locale` VALUES ('500018011', 'en-US', '2', '2', '2');
INSERT INTO `sys_locale` VALUES ('500018012', 'zh-CN', '3', '3', '3');
INSERT INTO `sys_locale` VALUES ('500018013', 'en-US', '3', '3', '3');
INSERT INTO `sys_locale` VALUES ('500018014', 'zh-CN', '4', '4', '4');
INSERT INTO `sys_locale` VALUES ('500018015', 'en-US', '4', '4', '4');
INSERT INTO `sys_locale` VALUES ('500018016', 'zh-CN', '5', '5', '5');
INSERT INTO `sys_locale` VALUES ('500018017', 'en-US', '5', '5', '5');
INSERT INTO `sys_locale` VALUES ('500018018', 'zh-CN', '6', '6', '6');
INSERT INTO `sys_locale` VALUES ('500018019', 'en-US', '6', '6', '6');
INSERT INTO `sys_locale` VALUES ('500018020', 'zh-CN', '7', '7', '7');
INSERT INTO `sys_locale` VALUES ('500018021', 'en-US', '7', '7', '7');
INSERT INTO `sys_locale` VALUES ('500018022', 'zh-CN', '8', '8', '8');
INSERT INTO `sys_locale` VALUES ('500018023', 'en-US', '8', '8', '8');
INSERT INTO `sys_locale` VALUES ('500018025', 'zh-CN', 'save', '保存', '保存');
INSERT INTO `sys_locale` VALUES ('500018026', 'en-US', 'save', '保存', 'Save');
INSERT INTO `sys_locale` VALUES ('500018027', 'en-US', 'data.delete.text', '数据删除', 'Data Delete');
INSERT INTO `sys_locale` VALUES ('500018028', 'zh-CN', 'data.delete.text', '数据删除', '数据删除');
INSERT INTO `sys_locale` VALUES ('500018029', 'en-US', 'data.add.text', '数据新增', 'Data Add');
INSERT INTO `sys_locale` VALUES ('500018030', 'zh-CN', 'data.add.text', '数据新增', '数据新增');
INSERT INTO `sys_locale` VALUES ('500018031', 'en-US', 'data.modify.text', '数据修改', 'Data Modify');
INSERT INTO `sys_locale` VALUES ('500018032', 'zh-CN', 'data.modify.text', '数据修改', '数据修改');
INSERT INTO `sys_locale` VALUES ('500019000', 'zh-CN', 'data.modify.notice', '数据修改提醒', '请问确定要保存这些修改吗？');
INSERT INTO `sys_locale` VALUES ('500019001', 'en-US', 'data.modify.notice', '数据修改提醒', 'Please confirm to save these modification ？');
INSERT INTO `sys_locale` VALUES ('500019002', 'zh-CN', 'task.start', '任务开始', '任务(开始)');
INSERT INTO `sys_locale` VALUES ('500019003', 'en-US', 'task.start', '任务开始', 'Start ');
INSERT INTO `sys_locale` VALUES ('500019004', 'zh-CN', 'task.complete', '任务完成', '任务(完成)');
INSERT INTO `sys_locale` VALUES ('500019005', 'en-US', 'task.complete', '任务完成', 'Historic');
INSERT INTO `sys_locale` VALUES ('500019006', 'zh-CN', 'task.statistic', '任务统计', '任务(统计)');
INSERT INTO `sys_locale` VALUES ('500019007', 'en-US', 'task.statistic', '任务统计', 'Statistic');
INSERT INTO `sys_locale` VALUES ('500019008', 'zh-CN', 'project.manage', '项目管理', '项目管理');
INSERT INTO `sys_locale` VALUES ('500019009', 'en-US', 'project.manage', '项目管理', 'Project');
INSERT INTO `sys_locale` VALUES ('500019010', 'zh-CN', 'infomation.manage', '信息管理', '信息管理');
INSERT INTO `sys_locale` VALUES ('500019011', 'en-US', 'infomation.manage', '信息管理', 'Information');
INSERT INTO `sys_locale` VALUES ('500019012', 'zh-CN', 'my.panel', '我的面板', '我的面板');
INSERT INTO `sys_locale` VALUES ('500019013', 'en-US', 'my.panel', '我的面板', 'Panel');
INSERT INTO `sys_locale` VALUES ('500019014', 'zh-CN', 'process.manage', '流程管理', '流程管理');
INSERT INTO `sys_locale` VALUES ('500019015', 'en-US', 'process.manage', '流程管理', 'Process');
INSERT INTO `sys_locale` VALUES ('500019016', 'en-US', 'code.generator', '代码生成', 'Generator');
INSERT INTO `sys_locale` VALUES ('500019017', 'zh-CN', 'code.generator', '代码生成', '代码生成');
INSERT INTO `sys_locale` VALUES ('500019018', 'zh-CN', 'sys.setting', '系统设置', '系统设置');
INSERT INTO `sys_locale` VALUES ('500019019', 'en-US', 'sys.setting', '系统设置', 'Setting');
INSERT INTO `sys_locale` VALUES ('500024001', 'zh-CN', 'true', '是', '是');
INSERT INTO `sys_locale` VALUES ('500024002', 'en-US', 'true', '是', 'True');
INSERT INTO `sys_locale` VALUES ('500024003', 'zh-CN', 'false', '否', '否');
INSERT INTO `sys_locale` VALUES ('500024004', 'en-US', 'false', '否', 'False');

-- ----------------------------
-- Table structure for sys_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_log`;
CREATE TABLE `sys_log` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `title` text COMMENT '标题',
  `content` blob COMMENT '错误描述',
  `create_by` bigint(20) NOT NULL COMMENT '创建者',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建者姓名',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新者',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新者姓名',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='错误日志表';

-- ----------------------------
-- Records of sys_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `parent_id` int(11) NOT NULL COMMENT '父编号',
  `parent_ids` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '所有父级编号',
  `leaf` tinyint(1) DEFAULT NULL COMMENT '是否叶子',
  `level` int(11) DEFAULT NULL COMMENT '层级',
  `code` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '编码',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `href` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '地址',
  `icon` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '图标',
  `permission` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '权限',
  `status` int(11) NOT NULL COMMENT '状态',
  `sort` int(11) NOT NULL COMMENT '排序号',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='菜单表';

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES ('100000020', '0', 'null,0', '0', '1', 'sys.setting', '系统设置', '', '', '', '0', '70', '100688', null, '2018-04-29 13:52:14', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000021', '100000030', '0,100000020,100000030', '1', '3', null, '菜单管理', '/system/menu/list', null, null, '1', '10', '100688', null, '2018-04-29 13:53:19', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000028', '100000035', '0,100000035', '0', '2', null, '代码生成', '', 'mail', '', '1', '10', '100688', null, '2018-04-29 16:37:21', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000030', '100000020', '0,100000020', '0', '2', null, '我的面板', '', 'HomeOutlined', '', '0', '10', '100688', null, '2018-04-29 16:05:07', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000035', '0', 'null,0', '0', '1', 'code.generator', '代码生成', '', '', '', '1', '60', '100688', null, '2018-04-29 16:10:42', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000036', '100000484', 'null,0,100000484', '0', '2', null, '投资管理', '', 'ApiOutlined', '', '1', '30', '100688', null, '2018-04-29 16:10:42', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000038', '100000020', '0,100000020', '0', '2', null, '参数设置', '', 'SettingOutlined', '', '0', '20', '100688', null, '2018-04-29 16:18:55', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000039', '100000038', '0,100000020,100000038', '1', '3', null, '字典管理', '/system/dict/list', '', null, '1', '10', '100688', null, '2018-04-29 16:18:55', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000128', '100000028', '0,100000035,100000028', '1', '3', null, '业务表配置', '/gen/table/list', '', '', '1', '10', '100688', null, '2018-04-29 16:55:47', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000484', '0', 'null,0', '0', '1', 'project.manage', '项目管理', '', '', '', '0', '10', '100688', null, '2018-04-29 19:20:19', '100225847', '于其先', '2020-06-01 21:47:09', null, '0');
INSERT INTO `sys_menu` VALUES ('100000485', '100000036', '0,100000484,100000036', '1', '3', 'project', '项目(信息)', '/repository/project/list', '', '', '1', '10', '100688', null, '2018-04-29 19:20:19', '100225847', '于其先', '2020-06-17 22:27:04', null, '0');
INSERT INTO `sys_menu` VALUES ('100000545', '100000546', '0,100000020,100000546', '1', '3', null, '机构用户', '/system/user/list', '', '', '0', '20', '100688', null, '2018-04-29 19:36:10', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000546', '100000020', '0,100000020', '0', '2', null, '用户管理', '、', 'TeamOutlined', '', '0', '30', '100688', null, '2018-04-29 19:36:10', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000547', '100000546', '0,100000020,100000546', '1', '3', null, '机构管理', '/system/office/list', '', '', '0', '10', '100688', null, '2018-04-29 19:36:10', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000931', '100000020', '0,100000020', '0', '2', null, '区域管理', '', 'GlobalOutlined', '', '1', '40', '100688', null, '2018-04-29 20:57:03', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100000932', '100000931', '0,100000020,100000931', '1', '3', null, '区域管理', '/system/area/list', '', '', '1', '10', '100688', null, '2018-04-29 20:57:03', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100002233', '100000484', 'null,0,100000484', '0', '2', null, '基础资料', '', 'ProfileOutlined', '', '1', '80', '100688', null, '2018-04-30 11:53:09', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100002234', '100002233', '0,100000484,100002233', '1', '3', null, '材料(基础)', '/pm/repository/material/list', '', '', '1', '10', '100688', null, '2018-04-30 11:53:09', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100002235', '100002233', '0,100000484,100002233', '1', '3', null, '配合比管理', '/pm/repository/mix/list', '', '', '1', '20', '100688', null, '2018-04-30 11:53:26', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100002843', '0', 'null,0', '0', '1', 'process.manage', '流程管理', '', '', '', '1', '50', '100688', null, '2018-04-30 14:57:53', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100002844', '100002847', '0,100002843,100002847', '1', '3', null, '模型管理', '/act/model/list', '', '', '1', '10', '100688', null, '2018-04-30 14:57:53', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100002845', '100002847', '0,100002843,100002847', '1', '3', null, '流程管理', '/act/process/list', '', '', '1', '20', '100688', null, '2018-04-30 14:57:53', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100002846', '100002847', '0,100002843,100002847', '1', '3', null, '任务管理', '/act/task/list', '', '', '1', '30', '100688', null, '2018-04-30 14:57:53', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100002847', '100002843', '0,100002843', '0', '2', 'process.manage', '流程管理', '', 'ForkOutlined', '', '1', '10', '100688', null, '2018-04-30 14:58:43', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100005894', '0', 'null,0', '0', '1', 'my.panel', '我的面板', '', '', '', '1', '40', '100688', null, '2018-05-01 09:27:47', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100005895', '100005894', '0,100005894', '0', '2', null, '个人办公', '', 'MailOutlined', '', '1', '10', '100688', null, '2018-05-01 09:27:47', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100005896', '100005895', '0,100005894,100005895', '1', '3', null, '我的任务', '/act/task/list', '', '', '0', '10', '100688', null, '2018-05-01 09:27:47', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100006436', '100005895', '0,100005894,100005895', '1', '3', null, '启动新任务', '/act/process/start', '', '', '1', '20', '100688', null, '2018-05-04 22:39:07', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100006528', '100000546', '0,100000020,100000546', '1', '3', null, '角色管理', '/system/role/list', '', '', '1', '30', '100688', null, '2018-05-04 23:01:52', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100015289', '100000036', '0,100000484,100000036', '1', '3', null, '合同(台账)', '/pm/repository/contract/account', 'pie-chart', '', '0', '20', '100014242', null, '2018-06-06 22:06:54', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100273861', '100000036', '0,100000484,100000036', '1', '3', null, '变更(审批)', '/pm/runtime/change/list?type=0', '', '', '0', '30', '100225847', null, '2018-07-01 15:14:34', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100273862', '100000036', '0,100000484,100000036', '1', '3', null, '变更(费用)', '/pm/runtime/change/list?type=1', '', '', '0', '40', '100225847', null, '2018-07-01 15:14:34', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100273863', '100000484', 'null,0,100000484', '0', '2', null, '费用控制', '', 'PayCircleOutlined', '', '1', '50', '100225847', null, '2018-07-01 15:15:48', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100273864', '100273863', '0,100000484,100273863', '1', '3', null, '计量(台账)', '/pm/runtime/visa/repository', '', '', '0', '20', '100225847', null, '2018-07-01 15:15:48', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100273865', '100273863', '0,100000484,100273863', '1', '3', null, '结算(台账)', '/pm/runtime/balance/repository', '', '', '0', '30', '100225847', null, '2018-07-01 15:15:48', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100273866', '100273863', '0,100000484,100273863', '1', '3', null, '核销(台账)', '/pm/runtime/materialDeducation/repository', '', '', '0', '40', '100225847', null, '2018-07-01 15:15:48', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100276386', '100005894', '0,100005894', '0', '2', null, '我的信息', '', 'SolutionOutlined', '', '1', '20', '100225719', null, '2018-07-04 21:58:11', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100276387', '100005894', '0,100005894', '1', '2', null, '密码修改', '', 'LockOutlined', '', '1', '30', '100225719', null, '2018-07-04 21:58:11', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100276388', '100005894', '0,100005894', '1', '2', null, '协同文档', '', 'FileOutlined', '', '1', '40', '100225719', null, '2018-07-04 21:58:11', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100276389', '0', 'null,0', '0', '1', 'infomation.manage', '信息管理', '', '', '', '1', '30', '100225719', null, '2018-07-04 22:01:46', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100276390', '100000484', 'null,0,100000484', '0', '2', null, '进度控制', '', 'BarChartOutlined', '', '1', '60', '100225719', null, '2018-07-04 22:01:46', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100276391', '100000484', 'null,0,100000484', '0', '2', null, '质量控制', '', 'ToolOutlined', '', '1', '70', '100225719', null, '2018-07-04 22:01:46', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100276392', '100273863', '0,100000484,100273863', '1', '3', null, '工程(计量)', '', '', '', '0', '10', '100225719', null, '2018-07-04 22:04:51', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100277688', '100000484', 'null,0,100000484', '0', '2', null, '材料管理', '', 'DatabaseOutlined', '', '1', '40', '100225719', null, '2018-07-05 23:50:24', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100281186', '100000020', '0,100000020', '0', '2', null, '通知公告', '', 'BellOutlined', '', '0', '50', '100225847', null, '2018-07-10 17:11:23', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100281187', '100281186', '0,100000020,100281186', '1', '3', null, '通知列表', '/system/notify/list', '', '', '0', '10', '100225847', null, '2018-07-10 17:11:23', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('100287287', '100000484', 'null,0,100000484', '0', '2', 'personal.work', '个人办公', '1', 'UserOutlined', '', '0', '10', '100225847', null, '2018-07-18 22:39:55', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('100287289', '100287287', '0,100000484,100287287', '1', '3', 'task.start', '任务(开始)', '/act/process/start', '', '', '0', '10', '100225847', null, '2018-07-18 22:39:55', '100225847', '于其先', '2020-06-16 21:56:06', null, '0');
INSERT INTO `sys_menu` VALUES ('100287367', '100287287', '0,100000484,100287287', '1', '3', 'task.complete', '任务(完成)', '/act/task/historicList', '', '', '1', '20', '100225847', null, '2018-07-18 22:51:09', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('100288466', '100002233', '0,100000484,100002233', '1', '3', null, '结算(模板)', '/pm/template/template/list', '', '', '1', '30', '100225847', null, '2018-07-24 21:37:39', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('102235132', '100277688', '0,100000484,100277688', '1', '3', null, '材料(管理)', '/pm/runtime/projectMaterialDeducationSummary/list', '', '', '1', '10', '100225847', null, '2018-09-05 21:16:50', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('110000039', '100000038', '0,100000020,100000038', '1', '3', '', '语言管理', '/system/locale/list', '', '', '1', '20', '100688', '', '2018-04-29 16:18:55', '100225847', '于其先', '2020-05-27 22:39:25', '', '0');
INSERT INTO `sys_menu` VALUES ('139039914', '100276389', '0,100276389', '1', '2', null, '规范 制度', '', '', '', '1', '10', '100225719', null, '2019-03-02 22:04:57', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('139039915', '100276389', '0,100276389', '1', '2', null, '消息 通知', '', '', '', '1', '20', '100225719', null, '2019-03-02 22:04:57', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('139039916', '100002233', '0,100000484,100002233', '1', '3', null, '项目(划分)', '/pm/repository/projectBranch/list', '', '', '1', '40', '100225719', null, '2019-03-02 22:05:56', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('139039917', '100276391', '0,100000484,100276391', '1', '3', null, '验收申请', '', '', '', '0', '10', '100225719', null, '2019-03-02 22:10:21', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('139039918', '100276391', '0,100000484,100276391', '1', '3', null, '验收评定', '', '', '', '0', '20', '100225719', null, '2019-03-02 22:10:21', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('139039919', '100276391', '0,100000484,100276391', '1', '3', null, '整改回访', '', '', '', '0', '30', '100225719', null, '2019-03-02 22:10:21', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('139039920', '0', 'null,0', '0', '1', 'material.manage', '物资管理', '', 'ShoppingCartOutlined', '', '1', '20', '100225719', null, '2019-03-02 22:14:25', '100225847', '于其先', '2020-06-27 21:16:20', null, '0');
INSERT INTO `sys_menu` VALUES ('139039921', '139039920', 'null,0,139039920', '0', '2', 'store.manage', '库房管理', '', '', '', '0', '10', '100225719', null, '2019-03-02 22:16:13', '100225847', '于其先', '2020-06-27 21:27:36', null, '0');
INSERT INTO `sys_menu` VALUES ('139039924', '100276390', '0,100000484,100276390', '1', '3', null, '总进度计划', '', '', '', '0', '10', '100225719', null, '2019-03-02 22:17:46', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('139039925', '100276390', '0,100000484,100276390', '1', '3', null, '跟踪进度', '', '', '', '0', '20', '100225719', null, '2019-03-02 22:17:46', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('139137285', '100000484', 'null,0,100000484', '0', '2', null, '综合管理', '', 'DesktopOutlined', '', '1', '20', '100225847', null, '2019-03-14 10:29:49', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('139237286', '100273863', '0,100000484,100273863', '1', '3', null, '支付申请', '/pm/pay/payApply/list', '', '', '0', '60', '100225847', null, '2019-03-18 11:23:08', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('139257286', '100273863', '0,100000484,100273863', '1', '3', null, '支付证书', '/pm/pay/payCertificate/list', '', '', '0', '50', '100225847', null, '2019-03-18 15:54:57', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('140447312', '100002847', '0,100002843,100002847', '1', '3', null, '流程授权', '/act/procDef/list', '', '', '1', '40', '100225847', null, '2019-05-05 11:16:42', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('140907401', '139137285', '0,100000484,139137285', '0', '3', null, '费用报销', '/pm/oa/expense/repository', 'schedule', '', '1', '10', '100225847', null, '2019-05-25 13:40:10', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('140907402', '140907401', '0,100000484,139137285,140907401', '1', '4', null, '用品报销', '/pm/oa/purchase/list', '', '', '1', '10', '100225847', null, '2019-05-25 13:40:34', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('140947321', '140907401', '0,100000484,139137285,140907401', '1', '4', null, '一般申请', '/pm/oa/apply/list', '', '', '1', '20', '100225847', null, '2019-05-27 13:10:37', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('140947322', '140907401', '0,100000484,139137285,140907401', '1', '4', null, '差旅报销', '/pm/oa/businessTrip/list', '', '', '1', '30', '100225847', null, '2019-05-27 13:11:34', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('140947323', '140907401', '0,100000484,139137285,140907401', '1', '4', null, '业务招待', '/pm/oa/hospitality/list', '', '', '1', '40', '100225847', null, '2019-05-27 13:11:34', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('140947324', '140907401', '0,100000484,139137285,140907401', '1', '4', null, '交际费', '/pm/oa/rebate/list', '', '', '1', '50', '100225847', null, '2019-05-27 13:11:34', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('140947325', '140907401', '0,100000484,139137285,140907401', '1', '4', null, '出国补贴', '/pm/oa/bonus/list', '', '', '1', '60', '100225847', null, '2019-05-27 13:11:34', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('142332526', '100276386', '0,100005894,100276386', '1', '3', null, '个人账户', '/pm/repository/bankAccount/list?type=0', '', '', '0', '10', '100225847', null, '2019-07-17 22:44:44', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('142332527', '100276386', '0,100005894,100276386', '1', '3', null, '公司账户', '/pm/repository/bankAccount/list?type=1', '', '', '0', '20', '100225847', null, '2019-07-17 22:44:44', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('142457340', '100000020', '0,100000020', '0', '2', null, '系统维护', '', 'RedoOutlined', '', '0', '70', '100225847', null, '2019-07-24 08:49:31', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('142457341', '142457340', '0,100000020,142457340', '1', '3', 'system.maintenance', '系统维护', '/system/logFile/list', '', '', '0', '30', '100225847', null, '2019-07-24 08:49:31', '100225847', '于其先', '2020-06-26 11:22:21', null, '0');
INSERT INTO `sys_menu` VALUES ('142617345', '100000546', '0,100000020,100000546', '1', '3', null, '用户角色', '/system/userRole/list', '', '', '0', '40', '100225847', null, '2019-08-03 03:52:43', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('142617346', '142457340', '0,100000020,142457340', '1', '3', 'update.infomation', '更新信息', '/system/schemasVersion/list', '', '', '0', '10', '100225847', null, '2019-08-03 03:52:43', '100225847', '于其先', '2020-06-26 11:20:54', null, '0');
INSERT INTO `sys_menu` VALUES ('142687377', '100000038', '0,100000020,100000038', '1', '3', null, '汇率管理', '/pm/repository/exchangeRate/list', '', '', '0', '30', '100225847', null, '2019-08-08 01:33:21', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('143457338', '100287287', '0,100000484,100287287', '1', '3', 'task.statistic', '任务(统计)', '/act/task/statistics', '', '', '0', '30', '100225847', null, '2019-09-19 15:33:01', '100225847', '于其先', '2020-05-27 22:39:24', null, '0');
INSERT INTO `sys_menu` VALUES ('145539380', '142457340', '0,100000020,142457340', '1', '3', 'system.log', '系统日志', '/system/log/list', '', '', '0', '20', '100225847', null, '2019-12-04 19:36:38', '100225847', '于其先', '2020-06-26 11:22:21', null, '0');
INSERT INTO `sys_menu` VALUES ('146167336', '100002843', '0,100002843', '0', '2', null, '动态表单', '', 'OrderedListOutlined', '', '0', '20', '100225847', null, '2020-02-29 11:09:59', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('146167337', '146167336', '0,100002843,146167336', '1', '3', null, '动态表单', '/pm/form/dynamicForm/list', 'mail', '', '0', '10', '100225847', null, '2020-02-29 11:09:59', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('146207429', '146167336', '0,100002843,146167336', '1', '3', null, '测试表单', '/pm/form/test/list', '', '', '0', '20', '100225847', null, '2020-03-01 12:47:43', '100225847', '于其先', '2020-05-27 22:39:25', null, '0');
INSERT INTO `sys_menu` VALUES ('500020001', '1', '0,1', '1', '2', '11.22', '测试1', null, null, null, '1', '10', '100225847', '于其先', '2020-05-21 20:53:56', '100225847', '于其先', '2020-05-27 22:38:31', null, '0');
INSERT INTO `sys_menu` VALUES ('500020003', '1', '0,1', '1', '2', '123', '我的测试', null, null, null, '1', '10', '100225847', '于其先', '2020-05-21 21:02:03', '100225847', '于其先', '2020-05-27 22:38:31', null, '0');
INSERT INTO `sys_menu` VALUES ('500020005', '3', '0,3', '1', '2', '23', '123', null, null, null, '1', '10', '100225847', '于其先', '2020-05-21 21:03:50', '100225847', '于其先', '2020-05-27 22:38:31', null, '0');
INSERT INTO `sys_menu` VALUES ('500021001', '5', '0,500020004,5', '1', '3', '123', '测试12', null, null, null, '1', '10', '100225847', '于其先', '2020-05-21 21:11:48', '100225847', '于其先', '2020-05-27 22:38:31', null, '0');
INSERT INTO `sys_menu` VALUES ('500021003', '8', '0,8', '1', '2', '3', '22', null, null, null, '1', '10', '100225847', '于其先', '2020-05-21 21:12:53', '100225847', '于其先', '2020-05-27 22:38:31', null, '0');
INSERT INTO `sys_menu` VALUES ('500021004', '500021000', '0,500020004,500021000', '1', '3', '123', '123', null, null, null, '1', '10', '100225847', '于其先', '2020-05-21 21:12:53', '100225847', '于其先', '2020-05-27 22:38:31', null, '0');
INSERT INTO `sys_menu` VALUES ('500021006', '10', '0,10', '1', '2', '2', '2', null, null, null, '1', '10', '100225847', '于其先', '2020-05-21 21:14:37', '100225847', '于其先', '2020-05-27 22:38:31', null, '0');
INSERT INTO `sys_menu` VALUES ('500021008', '500021007', '0,500021007', '1', '2', '1', '2', null, null, null, '1', '10', '100225847', '于其先', '2020-05-21 21:15:46', '100225847', '于其先', '2020-05-27 22:38:31', null, '0');
INSERT INTO `sys_menu` VALUES ('500021011', '500021010', '0,500021010', '1', '2', '2', '2', null, null, null, '1', '10', '100225847', '于其先', '2020-05-21 21:27:14', '100225847', '于其先', '2020-05-27 22:38:31', null, '0');
INSERT INTO `sys_menu` VALUES ('500029000', '100000020', '0,100000020', '0', '2', 'proprietor.manage', '业主管理', null, 'UserOutlined', null, '1', '60', '100225847', '于其先', '2020-06-14 14:55:04', '100225847', '于其先', '2020-06-14 14:55:04', null, '0');
INSERT INTO `sys_menu` VALUES ('500029001', '500029000', '0,100000020,500029000', '1', '3', 'proprietor.manage', '业主管理', '/sys/proprietor/list', null, null, '1', '10', '100225847', '于其先', '2020-06-14 14:56:03', '100225847', '于其先', '2020-06-14 14:59:22', null, '0');
INSERT INTO `sys_menu` VALUES ('500029002', '500029000', '0,100000020,500029000', '1', '3', 'proprietor.office', '业主机构', '/sys/proprietor/office', null, null, '1', '20', '100225847', '于其先', '2020-06-14 14:56:27', '100225847', '于其先', '2020-06-14 14:59:22', null, '0');
INSERT INTO `sys_menu` VALUES ('500034000', '139039920', 'null,0,139039920', '0', '2', 'material.manage', '物资管理', '', null, null, '1', '30', '100225847', '于其先', '2020-06-27 21:15:05', '100225847', '于其先', '2020-06-27 21:23:04', null, '0');
INSERT INTO `sys_menu` VALUES ('500034001', '139039921', 'null,0,139039920,139039921', '1', '3', 'store.manage', '库房管理', '/material/store/list', null, null, '1', '10', '100225847', '于其先', '2020-06-27 21:15:05', '100225847', '于其先', '2020-06-27 21:30:03', null, '0');
INSERT INTO `sys_menu` VALUES ('500034002', '500034000', 'null,0,139039920,500034000', '1', '3', 'material.manage', '物资管理', '/material/material/list', null, null, '1', '10', '100225847', '于其先', '2020-06-27 21:16:50', '100225847', '于其先', '2020-06-27 21:50:45', null, '0');
INSERT INTO `sys_menu` VALUES ('500034006', '139039920', 'null,0,139039920', '0', '2', 'store.manage', '库存管理', '', null, null, '1', '40', '100225847', '于其先', '2020-06-27 21:30:03', '100225847', '于其先', '2020-06-27 21:30:03', null, '0');
INSERT INTO `sys_menu` VALUES ('500034007', '500034006', 'null,0,139039920,500034006', '1', '3', 'store.manage', '库存管理', '/material/storeDetail/list', null, null, '1', '10', '100225847', '于其先', '2020-06-27 21:30:03', '100225847', '于其先', '2020-06-27 21:30:43', null, '0');
INSERT INTO `sys_menu` VALUES ('500034008', '500034006', 'null,0,139039920,500034006', '1', '3', 'store.log', '库存历史', '/material/storeLog/list', null, null, '1', '40', '100225847', '于其先', '2020-06-27 21:30:03', '100225847', '于其先', '2020-06-27 21:30:43', null, '0');
INSERT INTO `sys_menu` VALUES ('500039000', '500034000', 'null,0,139039920,500034000', '1', '3', 'material.inbound', '物资入库', '/material/inbound/inbound', null, null, '1', '20', '100225847', '于其先', '2020-06-28 21:19:51', '100225847', '于其先', '2020-06-28 23:01:00', null, '0');
INSERT INTO `sys_menu` VALUES ('500039001', '500034000', 'null,0,139039920,500034000', '1', '3', 'material.outbound', '物资出库', '/material/outbound/outbound', null, null, '1', '30', '100225847', '于其先', '2020-06-28 21:19:51', '100225847', '于其先', '2020-06-28 23:01:00', null, '0');

-- ----------------------------
-- Table structure for sys_notify
-- ----------------------------
DROP TABLE IF EXISTS `sys_notify`;
CREATE TABLE `sys_notify` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `title` varchar(100) NOT NULL COMMENT '标题',
  `type` int(50) NOT NULL COMMENT '类型',
  `content` varchar(2000) NOT NULL COMMENT '内容',
  `volume` int(11) NOT NULL COMMENT '浏览量',
  `status` int(20) NOT NULL COMMENT '状态',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='通知信息表';

-- ----------------------------
-- Records of sys_notify
-- ----------------------------

-- ----------------------------
-- Table structure for sys_office
-- ----------------------------
DROP TABLE IF EXISTS `sys_office`;
CREATE TABLE `sys_office` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `parent_id` bigint(20) NOT NULL COMMENT '父编号',
  `parent_ids` varchar(200) CHARACTER SET utf8mb4 NOT NULL DEFAULT '' COMMENT '父级编号集合',
  `level` int(11) DEFAULT NULL COMMENT '层级',
  `leaf` tinyint(1) DEFAULT NULL COMMENT '叶子',
  `area_id` bigint(20) NOT NULL COMMENT '区域',
  `name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `code` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '国际化编码',
  `type` int(50) NOT NULL COMMENT '类型',
  `grade` int(20) DEFAULT NULL COMMENT '级别',
  `address` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '地址',
  `phone` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '电话',
  `proprietor` tinyint(1) NOT NULL COMMENT '是否业主',
  `sort` int(11) NOT NULL DEFAULT '1' COMMENT '排序号',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `office_name` (`name`,`del_flag`,`parent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='机构表';

-- ----------------------------
-- Records of sys_office
-- ----------------------------
INSERT INTO `sys_office` VALUES ('100226032', '0', '0', null, null, '100226545', '业主单位', null, '0', '1', null, null, '0', '4350', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226033', '0', '0', null, null, '100226548', '施工单位', null, '0', '1', null, null, '0', '1240', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226034', '0', '0', null, null, '100226548', '中铁十八局集团有限公司', null, '0', '1', null, null, '0', '3950', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226035', '100226034', '0,100226034', null, null, '100226547', '综合部', null, '2', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226036', '100226032', '0,100226032', null, null, '100226545', '工程监理公司', 'null002', '0', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226037', '0', '0', null, null, '100226548', '中国水利水电第十六工程局有限公司', '001', '0', '1', null, null, '0', '4230', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226038', '0', '0', null, null, '100226548', '水利部丹江口水利枢纽管理局建设监理中心', '02002001', '0', '1', null, null, '0', '940', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226039', '0', '0', null, null, '100226548', '四川电力工程建设监理有限责任公司1', '02002002', '0', '1', null, null, '0', '2820', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226040', '0', '0', null, null, '100226545', '四川电力工程建设监理有限责任公司', '02', '0', '1', null, null, '0', '2830', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226041', '100226084', '0,100226084', null, null, '100226545', '华电国际电力股份有限公司', '', '0', '0', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226042', '100226041', '0,100226084,100226041', null, null, '100226545', '华电国际四川分公司', '002001', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226043', '100226041', '0,100226084,100226041', null, null, '100226548', '四川省水利电力工程局1', '002002', '0', '1', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226044', '0', '0', null, null, '100226548', '四川省水利电力工程局', '002', '0', '1', null, null, '0', '2490', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226045', '100226042', '0,100226084,100226041,100226042', null, null, '100226548', '四川凉山水洛河电力开发有限公司', '002003', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226046', '0', '0', null, null, '100226545', '设计单位', null, '0', '1', null, null, '0', '250', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226047', '0', '0', null, null, '100226548', '中国水利水电第十工程局有限公司', 'null004', '0', '1', '', '', '0', '4220', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226048', '100226033', '0,100226033', null, null, '100226548', '固滴项目施工单位', null, '0', '1', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226049', '100226033', '0,100226033', null, null, '100226548', '新藏项目施工单位', null, '0', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226050', '100226033', '0,100226033', null, null, '100226548', '博瓦项目施工单位', 'null003', '0', '1', null, null, '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226051', '0', '0', null, null, '100226545', '中国水电顾问集团贵阳勘测设计研究院', null, '0', '1', null, null, '0', '4160', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226052', '0', '0', null, null, '100226545', '四川省清源工程咨询有限公司', 'null002', '0', '1', null, null, '0', '2430', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226053', '0', '0', null, null, '100226548', '中国水利水电第十一工程局有限公司', '003', '0', '1', null, null, '0', '4240', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226054', '0', '0', null, null, '100226548', '中铁五局（集团）有限公司', '002', '0', '1', null, null, '0', '3980', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226055', '0', '0', null, null, '100226545', '监理单位', null, '0', '1', null, null, '0', '600', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226056', '100226055', '0,100226055', null, null, '100226548', '固滴项目监理单位', null, '0', '1', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226057', '100226055', '0,100226055', null, null, '100226548', '新藏项目监理单位', 'null002', '0', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226058', '100226055', '0,100226055', null, null, '100226548', '博瓦项目监理单位', 'null003', '0', '1', null, null, '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226059', '100226055', '0,100226055', null, null, '100226548', '公共项目监理单位', 'null004', '0', '1', null, null, '0', '40', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226060', '0', '0', null, null, '100226548', '水利部丹江口水利枢纽管理局建设监理中心固滴水电站工程监理部', 'null005', '2', '1', null, null, '0', '930', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226061', '0', '0', null, null, '100226548', '四川省水利电力工程局2', 'null004', '2', '1', null, null, '0', '2480', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226062', '0', '0', null, null, '100226548', '中国水利水电第五工程局有限公司', '004', '0', '1', null, null, '0', '4280', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226063', '0', '0', null, null, '100226548', '葛洲坝集团第二工程有限公司', '005', '0', '1', null, null, '0', '460', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226064', '100226046', '0,100226046', null, null, '100226545', '固滴项目设计单位', null, '0', '1', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226065', '100226046', '0,100226046', null, null, '100226545', '新藏项目设计单位', null, '0', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226066', '0', '0', null, null, '100226545', '博瓦项目设计单位', 'null005', '0', '1', null, null, '0', '3410', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226067', '0', '0', null, null, '100226545', '公共项目设计单位', 'null006', '0', '1', null, null, '0', '3810', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226068', '0', '0', null, null, '100226545', '四川省清源工程咨询有限公司', '005', '0', '1', null, null, '0', '2420', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226069', '0', '0', null, null, '100226548', '中国水利水电第八工程局有限公司博瓦水电站前期工程项目经理部', 'null004', '0', '1', '', '', '0', '4260', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226070', '0', '0', null, null, '100226548', '湖南水利水电工程监理承包总公司', '003002', '0', '1', null, null, '0', '680', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226071', '0', '0', null, null, '100226548', '四川省水利电力工程局3', null, '2', '1', null, null, '0', '2500', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226072', '0', '0', null, null, '100226548', '中国水利水电第八工程局有限公司', '003003', '0', '1', null, null, '0', '4270', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226073', '0', '0', null, null, '100226548', '中铁二十三局集团第四工程有限公司', '003004', '0', '1', null, null, '0', '3990', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226074', '0', '0', null, null, '100226548', '中国水利水电第十工程局有限公司', '003006', '0', '1', null, null, '0', '4210', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226075', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '博瓦项目建设管理部', '002001002', '1', '1', '', '', '0', '130', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226076', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '固滴项目建设管理部', '002003002', '1', '1', '', '', '0', '110', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226077', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '新藏项目建设管理部', '002003003', '1', '1', '', '', '0', '40', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226078', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '工程建设部', '002003004', '1', '1', '', '', '0', '80', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226079', '0', '0', null, null, '100226548', '四川伊蓝顿电气成套设备有限公司', null, '0', '1', null, null, '0', '3290', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226080', '0', '0', null, null, '100226548', '四川省天木建设有限公司', '', '0', '1', '', '', '0', '2670', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226081', '100226034', '0,100226034', null, null, '100226548', '中铁18局固滴引水1标项目部', 'null001', '0', '2', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226082', '0', '0', null, null, '100226548', '四川天木建设工程有限公司', null, '0', '1', null, null, '0', '3130', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226083', '0', '0', null, null, '100226548', '四川久联智业信息技术有限公司', null, '0', '1', null, null, '0', '3310', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226084', '0', '0', null, null, '100226547', '华电集团股份有限公司', '', '0', '0', '', '', '0', '3020', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226085', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '安全监察部', '002003005', '1', '1', '', '', '0', '90', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226086', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '水工部', '002003006', '1', '1', '', '', '0', '140', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226087', '100226042', '0,100226084,100226041,100226042', null, null, '100226548', '基建部', '002003007', '2', '3', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226088', '100226041', '0,100226084,100226041', null, null, '100226545', '工程部测试', '002001003', '2', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226089', '100226084', '0,100226084', null, null, '100226545', '工程部', '', '0', '1', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226090', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '董事长团队', '01003', '1', '1', '', '', '0', '70', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226091', '0', '0', null, null, '100226548', '四川蜀水生态环境建设有限责任公司', null, '0', '1', null, null, '0', '2230', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226092', '0', '0', null, null, '100226548', '四川蓬安马电机械化建筑工程有限公司', null, '0', '1', null, null, '0', '2250', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226093', '0', '0', null, null, '100226548', '宏盛建业投资集团有限公司', null, '0', '1', null, null, '0', '1970', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226094', '0', '0', null, null, '100226548', '重庆泉盛建设开发集团有限公司', '', '0', '1', '', '13908199103', '0', '430', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226095', '100226044', '0,100226044', null, null, '100226548', '固滴首部项目部', '002001', '0', '2', null, null, '0', '40', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226096', '100226044', '0,100226044', null, null, '100226548', '新藏引3项目部', '002002', '0', '2', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226097', '100226044', '0,100226044', null, null, '100226548', '博瓦首部项目部', '002003', '0', '2', null, null, '0', '50', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226098', '100226044', '0,100226044', null, null, '100226548', '钻根隧洞项目部', '002004', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226099', '100226044', '0,100226044', null, null, '100226548', '撒多水毁项目部', '002005', '0', '2', null, null, '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226100', '100226072', '0,100226072', null, null, '100226548', '博瓦安全监测项目部', '003003001', '0', '2', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226101', '100226072', '0,100226072', null, null, '100226548', '博瓦引1项目部', '003003002', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226102', '100226072', '0,100226072', null, null, '100226548', '博瓦前期1标项目部', '003003003', '0', '2', null, null, '0', '40', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226103', '100226072', '0,100226072', null, null, '100226548', '博瓦前期2标项目部', '003003004', '0', '2', null, null, '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226104', '100226037', '0,100226037', null, null, '100226548', '固滴引2项目部', '001001', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226105', '100226037', '0,100226037', null, null, '100226548', '博瓦引3项目部', '001002', '0', '2', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226106', '100226074', '0,100226074', null, null, '100226548', '固滴厂区项目部', '003006001', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226107', '100226074', '0,100226074', null, null, '100226548', '博瓦厂区项目部', '003006002', '0', '2', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226108', '0', '0', null, null, '100226548', '山东鲁能软件技术有限公司', null, '0', '1', null, null, '0', '1920', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226109', '0', '0', null, null, '100226548', '四川华祥工程建设有限公司', null, '0', '1', null, null, '0', '3230', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226110', '0', '0', null, null, '100226548', '溧阳市管夹厂', null, '0', '1', null, null, '0', '650', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226111', '0', '0', null, null, '100226548', '盐源金冠水泥有限公司', null, '0', '1', null, null, '0', '610', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226112', '0', '0', null, null, '100226548', '西昌锋行信息技术有限公司', null, '0', '1', null, null, '0', '290', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226113', '0', '0', null, null, '100226548', '中铁二十三局撒多水电站引水I标项目经理部', null, '0', '1', null, null, '0', '4030', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226114', '0', '0', null, null, '100226548', '核工业281大队', null, '0', '1', null, null, '0', '1020', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226115', '0', '0', null, null, '100226548', '西昌锋行信息公司', null, '0', '1', null, null, '0', '300', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226116', '0', '0', null, null, '100226548', '新乡市起重设备厂有限责任公司', null, '0', '1', null, null, '0', '1260', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226117', '0', '0', null, null, '100226548', '中铁十六局撒多引水III标项目经理部', '', '0', '1', '', '', '0', '3940', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226118', '0', '0', null, null, '100226548', '四川新中城城市规划设计有限公司', null, '0', '1', null, null, '0', '2960', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226119', '0', '0', null, null, '100226548', '四川腾达路桥有限责任公司', null, '0', '1', null, null, '0', '2260', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226120', '0', '0', null, null, '100226548', '常州卡森光电有限公司', null, '0', '1', null, null, '0', '1870', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226121', '100226038', '0,100226038', null, null, '100226548', '丹江口监理钻根隧道项目', '02002001002', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226122', '0', '0', null, null, '100226548', '江苏省纯江环保科技有限公司', null, '0', '1', null, null, '0', '820', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226123', '0', '0', null, null, '100226548', '中兴华会计师事务所', null, '0', '1', null, null, '0', '4330', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226124', '0', '0', null, null, '100226548', '中国水利水电工程局撒多机电安装项目经理部', '', '0', '1', '', '', '0', '4300', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226125', '0', '0', null, null, '100226548', '四川中天建筑工程有限公司', null, '0', '1', null, null, '0', '3370', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226126', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '生产技术部', '', '1', '1', '', '', '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226127', '0', '0', null, null, '100226548', '长江工程监理咨询有限公司（湖北）四川分公司', null, '0', '1', null, null, '0', '70', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226128', '0', '0', null, null, '100226548', '四川省川汇塑胶有限公司', null, '0', '1', null, null, '0', '2640', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226129', '0', '0', null, null, '100226548', '测试监理单位', null, '0', '1', null, null, '0', '740', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226130', '0', '0', null, null, '100226548', '四川省核工业地质局二八一大队', null, '0', '1', null, null, '0', '2540', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226131', '0', '0', null, null, '100226548', '四川省华电成套设备有限公司', null, '0', '1', null, null, '0', '2750', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226132', '0', '0', null, null, '100226548', '四川省鸿川建设工程有限公司', null, '0', '1', null, null, '0', '2350', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226133', '0', '0', null, null, '100226548', '四川蜀东地质勘察设计研究院有限公司', null, '0', '1', '达县南外镇华蜀南路200号', '0818-2685137', '0', '2240', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226134', '0', '0', null, null, '100226548', '四川中鼎自动控制有限公司', null, '0', '1', null, null, '0', '3320', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226135', '0', '0', null, null, '100226548', '成都杰翔科技有限公司', null, '0', '1', null, null, '0', '1500', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226136', '0', '0', null, null, '100226548', '四川腾达路桥有限公司', null, '0', '1', null, null, '0', '2270', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226137', '0', '0', null, null, '100226548', '凉山州蓉和生态工程设计咨询有限公司', null, '0', '1', null, null, '0', '3660', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226138', '0', '0', null, null, '100226545', '宁波理工监测科技股份有限公司', null, '0', '1', null, null, '0', '2010', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226139', '0', '0', null, null, '100226548', '四川光域电力设计有限公司', null, '0', '1', null, null, '0', '3280', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226140', '0', '0', null, null, '100226548', '四川川咨建设工程咨询有限公司', null, '0', '1', null, null, '0', '3080', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226141', '0', '0', null, null, '100226548', '四川华泰工程建设监理有限责任公司', null, '0', '1', null, null, '0', '3250', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226142', '0', '0', null, null, '100226548', '四川电力设计咨询有限责任公司', null, '0', '1', null, null, '0', '2810', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226143', '0', '0', null, null, '100226545', '北京海策工程咨询有限公司', null, '0', '1', null, null, '0', '3560', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226144', '0', '0', null, null, '100226548', '成都市越鑫电力物资有限公司', null, '0', '1', null, null, '0', '1590', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226145', '0', '0', null, null, '100226548', '凉山华峰', null, '0', '1', null, null, '0', '3760', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226146', '0', '0', null, null, '100226548', '成都众望博维网络系统工程有限公司', null, '0', '1', null, null, '0', '1730', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226147', '0', '0', null, null, '100226548', '成都亿峰钢结构有限公司', null, '0', '1', null, null, '0', '1750', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226148', '0', '0', null, null, '100226548', '四川力凡建筑工程有限公司', null, '0', '1', null, null, '0', '3260', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226149', '0', '0', null, null, '100226548', '攀枝花市三维天香物业管理有限责任公司', null, '0', '1', null, null, '0', '1290', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226150', '100226063', '0,100226063', null, null, '100226548', '葛洲坝2公司新藏厂区项目部', '005001', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226151', '100226039', '0,100226039', null, null, '100226548', '测试4', '02002002001', '2', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226152', '0', '0', null, null, '100226548', '木里县依吉乡人民政府', null, '0', '1', null, null, '0', '1210', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226153', '0', '0', null, null, '100226548', '武汉武大巨成加固实业有限公司', null, '0', '1', null, null, '0', '970', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226154', '0', '0', null, null, '100226548', '成都正和资讯有限责任公司', null, '0', '1', null, null, '0', '1460', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226155', '0', '0', null, null, '100226548', '凉山启天泰物流有限公司', null, '0', '1', null, null, '0', '3750', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226156', '0', '0', null, null, '100226548', '自宫真空过滤设备有限责任公司', null, '0', '1', null, null, '0', '510', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226157', '0', '0', null, null, '100226548', '四川省鑫川建筑工程检测有限公司', null, '0', '1', null, null, '0', '2360', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226158', '0', '0', null, null, '100226548', '宜兴市亨得利物资有限公司', null, '0', '1', null, null, '0', '1960', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226159', '0', '0', null, null, '100226548', '川开电气有限公司', null, '0', '1', null, null, '0', '1910', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226160', '0', '0', null, null, '100226548', '成都蜀能企业咨询有限公司', null, '0', '1', null, null, '0', '1370', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226161', '0', '0', null, null, '100226548', '湖北环宇工程建设监理有限公司', null, '0', '1', null, null, '0', '710', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226162', '0', '0', null, null, '100226548', '四川省正旭建设工程有限责任公司', null, '0', '1', null, null, '0', '2530', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226163', '100226042', '0,100226084,100226041,100226042', null, null, '100226548', '攀枝花三维发电有限责任公司', null, '0', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226164', '0', '0', null, null, '100226548', '扬州曙光电缆股份有限公司', null, '0', '1', null, null, '0', '1310', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226165', '0', '0', null, null, '100226548', '西昌市博源广告制作部', null, '0', '1', null, null, '0', '400', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226166', '0', '0', null, null, '100226548', '西昌富华园林工程有限责任公司', null, '0', '1', null, null, '0', '410', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226167', '0', '0', null, null, '100226548', '太极计算机股份有限公司', null, '2', '1', null, null, '0', '2060', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226168', '0', '0', null, null, '100226548', '四川恒宝建筑工程有限公司', null, '0', '1', null, null, '0', '2990', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226169', '0', '0', null, null, '100226545', '武汉长澳大地工程有限公司', null, '0', '1', null, null, '0', '950', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226170', '0', '0', null, null, '100226548', '正泰电气股份有限公司', null, '0', '1', null, null, '0', '990', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226171', '0', '0', null, null, '100226548', '华鸿建设集团有限公司', null, '0', '1', null, null, '0', '3490', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226172', '0', '0', null, null, '100226548', '安徽华海特种电缆集团有限公司', null, '0', '1', null, null, '0', '2000', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226173', '0', '0', null, null, '100226548', '沐川建筑工程有限责任公司', null, '0', '1', null, null, '0', '780', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226174', '0', '0', null, null, '100226548', '成都亚蒲耳照明电器有限公司', null, '0', '1', null, null, '0', '1760', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226175', '0', '0', null, null, '100226548', '南京南瑞继保工程技术有限公司', null, '0', '1', null, null, '0', '3480', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226176', '0', '0', null, null, '100226548', '保定天威恒通电气有限公司', null, '0', '1', null, null, '0', '3830', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226177', '0', '0', null, null, '100226548', '昆明电机厂有限责任公司', null, '0', '1', null, null, '0', '1230', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226178', '0', '0', null, null, '100226548', '四川晶基建设工程有限公司', null, '0', '1', null, null, '0', '2920', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226179', '0', '0', null, null, '100226545', '华电招标有限公司', null, '0', '1', null, null, '0', '3520', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226180', '0', '0', null, null, '100226548', '四川昊星', null, '0', '1', null, null, '0', '2930', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226181', '100226073', '0,100226073', null, null, '100226548', '中铁23局博瓦引水2标', '003004001', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226182', '0', '0', null, null, '100226548', '南通天诚电力设备有限公司', null, '0', '1', null, null, '0', '3430', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226183', '100226082', '0,100226082', null, null, '100226548', '四川天木水洛营地项目部', 'null001', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226184', '100226088', '0,100226084,100226041,100226088', null, null, '100226548', '测试2', '002001003001', '2', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226185', '0', '0', null, null, '100226548', '绵阳灵通电讯设备有限公司', null, '0', '1', null, null, '0', '520', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226186', '0', '0', null, null, '100226548', '成都宏达自动化控制有限公司', null, '0', '1', null, null, '0', '1630', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226187', '0', '0', null, null, '100226548', '村民曲珍', null, '0', '1', null, null, '0', '1040', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226188', '0', '0', null, null, '100226548', '四川中鼎科技有限公司', null, '0', '1', null, null, '0', '3330', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226189', '0', '0', null, null, '100226548', '华新水泥（丽江）有限公司', null, '0', '1', null, null, '0', '3550', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226190', '0', '0', null, null, '100226548', '江苏中超电缆股份有限公司', null, '0', '1', null, null, '0', '890', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226191', '0', '0', null, null, '100226548', '四川水韵五兴物业管理有限公司', '', '0', '1', '', '', '0', '2890', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226192', '0', '0', null, null, '100226548', '西昌汇鑫物资有限责任公司', null, '0', '1', null, null, '0', '340', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226193', '100226089', '0,100226084,100226089', null, null, '100226548', '测试1', '002003001', '2', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226194', '0', '0', null, null, '100226548', '木里蓝月山谷传媒工作室', null, '0', '1', null, null, '0', '1110', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226195', '0', '0', null, null, '100226548', '川开电气股份有限公司', null, '0', '1', null, null, '0', '1900', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226196', '0', '0', null, null, '100226548', '四川新恒轲商贸有限公司', null, '0', '1', null, null, '0', '2950', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226197', '0', '0', null, null, '100226548', '四川西星电力科技咨询有限公司', null, '0', '1', null, null, '0', '2180', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226198', '0', '0', null, null, '100226548', '四川蜀能电力有限公司电网运维分公司', null, '0', '1', null, null, '0', '2220', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226199', '0', '0', null, null, '100226548', '成都市雨田俊科技发展有限公司', null, '0', '1', null, null, '0', '1570', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226200', '0', '0', null, null, '100226548', '四川省林业勘察设计院', null, '0', '1', null, null, '0', '2560', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226201', '0', '0', null, null, '100226548', '四川重起起重设备有限公司', null, '0', '1', null, null, '0', '2140', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226202', '0', '0', null, null, '100226548', '中铁十六局撒多水电站项目经理部', '', '0', '1', '', '', '0', '3930', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226203', '0', '0', null, null, '100226548', '成都申海暖通环保设备有限公司', null, '0', '1', null, null, '0', '1450', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226204', '0', '0', null, null, '100226548', '凉山州林业局调查规划设计院', null, '0', '1', null, null, '0', '3680', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226205', '0', '0', null, null, '100226548', '河海大学', null, '0', '1', null, null, '0', '760', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226206', '0', '0', null, null, '100226548', '凉山州木里县林业局', null, '0', '1', null, null, '0', '3700', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226207', '0', '0', null, null, '100226548', '成都市雨田骏科技发展有限公司', null, '0', '1', null, null, '0', '1550', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226208', '0', '0', null, null, '100226548', '四川衡信诗杰机电成套设备有限公司', null, '0', '1', null, null, '0', '2210', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226209', '0', '0', null, null, '100226548', '成都能信旧房拆除有限责任公司', null, '0', '1', null, null, '0', '1390', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226210', '0', '0', null, null, '100226545', '北京格瑞拓动设备有限公司', null, '0', '1', null, null, '0', '3570', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226211', '0', '0', null, null, '100226548', '四川省凉山州木里县水洛乡政府', null, '0', '1', null, null, '0', '2770', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226212', '0', '0', null, null, '100226548', '水电十局', null, '0', '1', null, null, '0', '910', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226213', '0', '0', null, null, '100226548', '四川诗杰电力设备开发有限公司', null, '0', '1', null, null, '0', '2170', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226214', '0', '0', null, null, '100226548', '甲英扎西、达皮打珍、金西郎吉', null, '0', '1', null, null, '0', '620', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226215', '100226184', '0,100226084,100226041,100226088,100226184', null, null, '100226548', '测试3', '002001003001001', '2', '1', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226216', '0', '0', null, null, '100226545', '江苏中超电力股份有限公司', null, '0', '1', null, null, '0', '900', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226217', '0', '0', null, null, '100226548', '新隆兴土地整理规划勘测有限公司', null, '0', '1', null, null, '0', '1250', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226218', '0', '0', null, null, '100226548', '中铁十六局集团有限公司', null, '0', '1', null, null, '0', '3920', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226219', '0', '0', null, null, '100226548', '四川水电工程建设监理有限公司', '', '0', '1', '', '', '0', '2900', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226220', '0', '0', null, null, '100226548', '凉山州环境监测站', null, '0', '1', null, null, '0', '3670', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226221', '0', '0', null, null, '100226548', '上海西电高压开关有限公司', null, '0', '1', null, null, '0', '4360', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226222', '0', '0', null, null, '100226548', '四川吉豪昌隆', '', '0', '1', '', '', '0', '3200', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226223', '0', '0', null, null, '100226548', '湘潭中基电站辅机制造有限公司', null, '0', '1', null, null, '0', '660', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226224', '0', '0', null, null, '100226548', '自贡东方水利机械有限责任公司', null, '0', '1', null, null, '0', '500', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226225', '0', '0', null, null, '100226548', '大同经济保险公司', null, '0', '1', null, null, '0', '2070', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226226', '0', '0', null, null, '100226548', '四川新隆兴土地整理规划勘测有限公司', null, '0', '1', null, null, '0', '2940', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226227', '0', '0', null, null, '100226548', '中国电建集团中南勘测设计研究院有限公司', null, '0', '1', null, null, '0', '4110', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226228', '0', '0', null, null, '100226548', '木里藏族自治县移民局', null, '2', '1', null, null, '0', '1070', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226229', '0', '0', null, null, '100226548', '四川天之汇建筑工程有限公司', null, '0', '1', null, null, '0', '3150', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226230', '0', '0', null, null, '100226548', '四川省工程咨询研究院', null, '0', '1', null, null, '0', '2620', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226231', '0', '0', null, null, '100226548', '成都鸿策工程咨询有限公司', null, '0', '1', null, null, '0', '1330', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226232', '0', '0', null, null, '100226548', '木里县财政国库', null, '0', '1', null, null, '0', '1140', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226233', '0', '0', null, null, '100226548', '四川省西点电力设计有限公司', null, '0', '1', null, null, '0', '2390', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226234', '0', '0', null, null, '100226548', '成都伊顿科机电设备工程有限公司', null, '0', '1', null, null, '0', '1740', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226235', '0', '0', null, null, '100226548', '凉山科华水生态工程有限公司', null, '0', '1', null, null, '0', '3640', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226236', '0', '0', null, null, '100226548', '山东恒诚信工程项目管理有限公司', null, '0', '1', null, null, '0', '1950', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226237', '0', '0', null, null, '100226548', '中水水电顾问集团成都勘测设计研究院', null, '0', '1', null, null, '0', '4080', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226238', '0', '0', null, null, '100226548', '成都市华桦商务服务有限公司', null, '0', '1', null, null, '0', '1620', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226239', '0', '0', null, null, '100226548', '福建南平南电水电设备制造有限公司', null, '0', '1', null, null, '0', '580', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226240', '0', '0', null, null, '100226548', '四川威胜电气有限公司', null, '0', '1', null, null, '0', '3100', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226241', '0', '0', null, null, '100226545', '常州武进第一水利机械有限公司', null, '0', '1', null, null, '0', '1840', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226242', '0', '0', null, null, '100226548', '省水利院', null, '0', '1', null, null, '0', '590', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226243', '0', '0', null, null, '100226548', '攀枝花市精图测绘有限责任公司西昌分公司', null, '0', '1', null, null, '0', '1280', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226244', '0', '0', null, null, '100226548', '重庆交通规划勘察设计院', null, '0', '1', null, null, '0', '180', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226245', '0', '0', null, null, '100226548', '四川科锐电力通信技术有限公司', null, '0', '1', null, null, '0', '2300', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226246', '0', '0', null, null, '100226548', '凉山州政府移民局', null, '0', '1', null, null, '0', '3720', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226247', '0', '0', null, null, '100226548', '四川煤田一三七总公司', '', '0', '1', '', '', '0', '2860', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226248', '0', '0', null, null, '100226548', '四川省水利科学研究院技术咨询部', null, '0', '1', null, null, '0', '2440', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226249', '0', '0', null, null, '100226548', '甘孜州康达交通勘察设计有限公司', null, '0', '1', null, null, '0', '630', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226250', '0', '0', null, null, '100226548', '四川嘉润电力有限公司', null, '0', '1', null, null, '0', '3180', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226251', '0', '0', null, null, '100226548', '起点电器公司', null, '0', '1', null, null, '0', '220', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226252', '0', '0', null, null, '100226548', '四川省水利水电勘测设计研究院', null, '0', '1', null, null, '0', '2520', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226253', '0', '0', null, null, '100226548', '中铁十八局撒多水电站项目经理部', '', '0', '1', '', '', '0', '3960', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226254', '0', '0', null, null, '100226545', '南京南瑞集团公司', null, '0', '1', null, null, '0', '3470', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226255', '0', '0', null, null, '100226548', '中水环球(北京)科技有限公司', null, '0', '1', null, null, '0', '4070', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226256', '0', '0', null, null, '100226548', '四川科建工程质量检测有限公司', null, '0', '1', null, null, '0', '2340', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226257', '0', '0', null, null, '100226548', '木里藏族自治县电力公司', null, '0', '1', null, null, '0', '1080', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226258', '0', '0', null, null, '100226548', '京仪股份有限公司', null, '0', '1', null, null, '0', '3860', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226259', '0', '0', null, null, '100226548', '木里林业局', null, '0', '1', null, null, '0', '1120', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226260', '0', '0', null, null, '100226548', '贵州华电乌江水电工程项目管理有限公司', null, '0', '1', null, null, '0', '240', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226261', '0', '0', null, null, '100226548', '四川省今天工程建设技术有限公司', null, '0', '1', null, null, '0', '2800', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226262', '0', '0', null, null, '100226548', '开县水电建筑开发有限公司', null, '0', '1', null, null, '0', '1780', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226263', '0', '0', null, null, '100226548', '四川海德建筑装饰工程有限公司', null, '0', '1', null, null, '0', '2870', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226264', '0', '0', null, null, '100226548', '木里县国土资源局', null, '0', '1', null, null, '0', '1190', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226265', '0', '0', null, null, '100226548', '雅安公路设计院', null, '0', '1', null, null, '0', '40', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226266', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '计划合同部', '', '1', '1', '', '', '0', '150', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226267', '0', '0', null, null, '100226548', '四川三维测绘有限公司', null, '0', '1', null, null, '0', '3380', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226268', '0', '0', null, null, '100226548', '江苏省如高高压电器有限公司', null, '0', '1', null, null, '0', '830', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226269', '100226038', '0,100226038', null, null, '100226548', '丹江口监理水洛营地项目部', '02002001003', '0', '2', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226270', '0', '0', null, null, '100226548', '四川省通信产业服务有限公司凉山彝族自治州分公司', null, '0', '1', null, null, '0', '2370', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226271', '0', '0', null, null, '100226548', '华电电力科学研究院', null, '0', '1', null, null, '0', '3510', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226272', '0', '0', null, null, '100226548', '四川中成煤炭建设（集团）有限公司', null, '0', '1', null, null, '0', '3360', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226273', '0', '0', null, null, '100226548', '成都锦桦商务服务有限公司', null, '2', '1', null, null, '0', '1350', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226274', '0', '0', null, null, '100226548', '乐山远航石化有限责任公司', null, '0', '1', null, null, '0', '3880', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226275', '0', '0', null, null, '100226545', '江苏安靠超高压电缆附件有限公司', null, '0', '1', null, null, '0', '860', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226276', '0', '0', null, null, '100226545', '北京博电新力电气股份有限公司', null, '0', '1', null, null, '0', '3620', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226277', '0', '0', null, null, '100226548', '重庆市渝万建设集团有限公司', null, '0', '1', null, null, '0', '160', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226278', '0', '0', null, null, '100226548', '东方电气集团东方电机有限公司', null, '0', '1', null, null, '0', '4340', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226279', '0', '0', null, null, '100226548', '扬州成溪环保科技有限公司', null, '0', '1', null, null, '0', '1320', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226280', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '移民物资部', '', '1', '1', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226281', '0', '0', null, null, '100226548', '特变电工中发上海高压开关有限公司', null, '0', '1', null, null, '0', '640', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226282', '0', '0', null, null, '100226548', '中铁二十三局集团第三工程有限公司', '', '0', '1', '', '', '0', '4000', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226283', '0', '0', null, null, '100226548', '成都盛诺科技有限责任公司', null, '0', '1', null, null, '0', '1420', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226284', '0', '0', null, null, '100226548', '四川科锐得电力信息自动化技术有限责任公司', null, '0', '1', null, null, '0', '2330', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226285', '0', '0', null, null, '100226548', '西昌市皓升工贸有限公司', null, '0', '1', null, null, '0', '380', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226286', '0', '0', null, null, '100226548', '四川嘉实泰安科技有限公司', null, '0', '1', null, null, '0', '3190', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226287', '0', '0', null, null, '100226548', '人力部', '', '0', '0', '', '', '0', '3850', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226288', '0', '0', null, null, '100226548', '常州市武进第一水利机械有限公司', null, '0', '1', null, null, '0', '1850', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226289', '0', '0', null, null, '100226548', '宁朗供应商', null, '0', '1', null, null, '0', '2040', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226290', '0', '0', null, null, '100226545', '广州广哈通信股份有限公司', null, '0', '1', null, null, '0', '1810', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226291', '0', '0', null, null, '100226548', '四川省川虹建设工程有限公司', null, '0', '1', null, null, '0', '2630', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226292', '100226053', '0,100226053', null, null, '100226548', '中水11局新藏首部枢纽项目部', '003001', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226293', '0', '0', null, null, '100226548', '上海太比雅电力设备有限公司', null, '0', '1', null, null, '0', '4390', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226294', '0', '0', null, null, '100226548', '四川省岷源水利水电工程设计有限公司', null, '0', '1', null, null, '0', '2650', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226295', '0', '0', null, null, '100226548', '四川巨烽钢结构有限公司', null, '0', '1', null, null, '0', '3050', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226296', '0', '0', null, null, '100226548', '四川省冶金地质勘查局成都地质调查所', null, '0', '1', null, null, '0', '2780', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226297', '0', '0', null, null, '100226548', '四川科锐得电力通信技术有限公司', null, '0', '1', null, null, '0', '2320', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226298', '0', '0', null, null, '100226548', '供应商', null, '0', '1', null, null, '0', '3840', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226299', '0', '0', null, null, '100226548', '四川省华通凯能电气有限公司', null, '0', '1', null, null, '0', '2740', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226300', '0', '0', null, null, '100226548', '四川忠机电力工程设备有限公司', null, '0', '1', null, null, '0', '3000', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226301', '0', '0', null, null, '100226548', '重庆一铭电气自动化设备有限公司', null, '0', '1', null, null, '0', '190', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226302', '0', '0', null, null, '100226548', '凉山州公路工程勘察设计事务所', null, '0', '1', null, null, '0', '3730', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226303', '0', '0', null, null, '100226548', '乐山远航', null, '0', '1', null, null, '0', '3890', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226304', '0', '0', null, null, '100226548', '成都申海电力设备制造有限公司', null, '0', '1', null, null, '0', '1440', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226305', '0', '0', null, null, '100226548', '四川省地质矿产勘查开发局九0九水文地质工程地质队', null, '0', '1', null, null, '0', '2680', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226306', '0', '0', null, null, '100226548', '核工业西南勘察设计研究院有限公司', null, '2', '1', null, null, '0', '1000', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226307', '0', '0', null, null, '100226548', '中国平安财产保险股份有限公司四川分公司', null, '0', '1', null, null, '0', '4310', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226308', '0', '0', null, null, '100226548', '常州卡森照明电器有限公司', null, '0', '1', null, null, '0', '1860', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226309', '0', '0', null, null, '100226548', '四川省水利电力工程局有限公司撒多水电站项目经理部', null, '2', '1', null, null, '0', '2470', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226310', '0', '0', null, null, '100226548', '湖北洪城通用机械有限公司', null, '0', '1', null, null, '0', '720', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226311', '0', '0', null, null, '100226548', '自贡真空过滤设备有限责任公司', null, '0', '1', null, null, '0', '490', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226312', '100226042', '0,100226084,100226041,100226042', null, null, '100226548', '华电四川发电有限公司攀枝花分公司', null, '0', '1', null, null, '0', '60', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226313', '0', '0', null, null, '100226548', '四川中鼎', null, '0', '1', null, null, '0', '3340', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226314', '100226051', '0,100226051', null, null, '100226548', '贵阳院固滴项目部', 'null001', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226315', '0', '0', null, null, '100226548', '成都畅越机械工程有限公司', null, '0', '1', null, null, '0', '1430', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226316', '0', '0', null, null, '100226548', '高红华', null, '2', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226317', '0', '0', null, null, '100226548', '四川省电力工业调整试验所1', null, '2', '1', null, null, '0', '2400', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226318', '0', '0', null, null, '100226548', '西昌市景程通信器材有限公司', null, '0', '1', null, null, '0', '390', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226319', '0', '0', null, null, '100226548', '西昌水利电力建设监理有限责任公司', null, '2', '1', null, null, '0', '360', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226320', '0', '0', null, null, '100226548', '安徽天康（集团）股份有限公司', null, '0', '1', null, null, '0', '1990', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226321', '100226062', '0,100226062', null, null, '100226548', '中水5局新藏引水2标项目部', '004001', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226322', '0', '0', null, null, '100226548', '葛洲坝集团二公司撒多厂区枢纽项目经理部', null, '2', '1', null, null, '0', '470', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226323', '0', '0', null, null, '100226548', '攀枝花钢城集团瑞丰水泥有限公司', null, '0', '1', null, null, '0', '1270', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226324', '0', '0', null, null, '100226548', '木里县民和水电开发公司', null, '0', '1', null, null, '0', '1170', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226325', '0', '0', null, null, '100226548', '北京捷福士电子技术有限公司', null, '0', '1', null, null, '0', '3580', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226326', '0', '0', null, null, '100226545', '江苏安靠智能输电工程科技股份有限公司', null, '0', '1', null, null, '0', '870', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226327', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '撒多项目建设管理部', '', '1', '1', '', '', '0', '50', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226328', '0', '0', null, null, '100226548', '蓬安马电供电有限责任公司', null, '0', '1', null, null, '0', '440', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226329', '0', '0', null, null, '100226548', '积成电子股份有限公司', null, '0', '1', null, null, '0', '560', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226330', '100226082', '0,100226082', null, null, '100226548', '四川天木宁朗水毁项目部', 'null002', '0', '2', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226331', '0', '0', null, null, '100226548', '中电建五兴物业管理有限公司', null, '0', '1', null, null, '0', '4060', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226332', '0', '0', null, null, '100226548', '江苏武进液压启闭机有限公司', null, '0', '1', null, null, '0', '840', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226333', '0', '0', null, null, '100226548', '成都艺峰钢构活动板房公司', null, '0', '1', null, null, '0', '1380', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226334', '0', '0', null, null, '100226548', '成都墨安科技有限公司', '', '0', '1', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226335', '0', '0', null, null, '100226548', '北京四方继保自动化股份有限公司', null, '0', '1', null, null, '0', '3610', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226336', '0', '0', null, null, '100226548', '内江市润泽物业管理有限责任公司', null, '0', '1', null, null, '0', '3800', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226337', '0', '0', null, null, '100226548', '西昌顺鑫园林有限责任公司', null, '0', '1', null, null, '0', '270', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226338', '0', '0', null, null, '100226548', '四川省地政地籍事务中心', null, '0', '1', null, null, '0', '2730', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226339', '0', '0', null, null, '100226548', '葛洲坝集团第二工程有限公司撒多水电站厂区枢纽工程施工项目部', null, '2', '1', null, null, '0', '450', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226340', '0', '0', null, null, '100226548', '西昌市锋行信息技术有限公司', null, '0', '1', null, null, '0', '370', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226341', '0', '0', null, null, '100226548', '迅达（中国）电梯有限公司', null, '2', '1', null, null, '0', '210', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226342', '0', '0', null, null, '100226548', '成都和滢水利工程咨询有限公司', null, '0', '1', null, null, '0', '1690', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226343', '0', '0', null, null, '100226548', '四川天逊通信技术有限公司', null, '0', '1', null, null, '0', '3120', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226344', '0', '0', null, null, '100226548', '乐山一拉得电网自动化有限公司', null, '0', '1', null, null, '0', '3900', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226345', '0', '0', null, null, '100226548', '湖南娄建基础建筑工程有限公司', '', '0', '1', '', '', '0', '810', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226346', '0', '0', null, null, '100226548', '四川红叶建设有限公司', null, '0', '1', null, null, '0', '2290', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226347', '0', '0', null, null, '100226548', '华电国际项目管理有限公司', null, '0', '1', null, null, '0', '3530', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226348', '0', '0', null, null, '100226548', '木里四通商贸', null, '0', '1', null, null, '0', '1130', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226349', '0', '0', null, null, '100226548', '四川川起起重设备有限公司', null, '0', '1', null, null, '0', '3060', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226350', '0', '0', null, null, '100226548', '木里县移民局', null, '0', '1', null, null, '0', '1150', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226351', '0', '0', null, null, '100226548', '中国电信股份有限公司凉山分公司', null, '0', '1', null, null, '0', '4150', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226352', '0', '0', null, null, '100226548', '成都科润实业有限公司', null, '0', '1', null, null, '0', '1410', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226353', '0', '0', null, null, '100226548', '广州擎天实业有限公司', null, '0', '1', null, null, '0', '1800', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226354', '0', '0', null, null, '100226548', '启天泰物流公司', null, '0', '1', null, null, '0', '3390', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226355', '0', '0', null, null, '100226548', '青岛正利电力装备有限公司', '', '0', '1', '', '', '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226356', '0', '0', null, null, '100226548', '长园共创电力安全技术股份有限公司', null, '0', '1', null, null, '0', '90', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226357', '0', '0', null, null, '100226548', '四川志明建设工程有限公司', null, '0', '1', null, null, '0', '3020', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226358', '0', '0', null, null, '100226548', '中国水电基础局有限公司', null, '0', '1', null, null, '0', '4180', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226359', '100226068', '0,100226068', null, null, '100226548', '清源公司新藏项目部', '005002', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226360', '0', '0', null, null, '100226548', '云南善友经贸有限公司', null, '0', '1', null, null, '0', '3870', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226361', '0', '0', null, null, '100226548', '木里藏族自治县环境保护局', null, '0', '1', null, null, '0', '1090', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226362', '0', '0', null, null, '100226548', '镇江市晨阳电力设备有限公司', null, '0', '1', null, null, '0', '100', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226363', '0', '0', null, null, '100226548', '北京四方继电保护自动化股份有限公司', null, '0', '1', null, null, '0', '3600', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226364', '0', '0', null, null, '100226548', '中国水利水电科学研究院', '', '0', '1', '', '', '0', '110', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226365', '0', '0', null, null, '100226548', '四川智诚天逸科技有限公司', null, '0', '1', null, null, '0', '2910', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226366', '0', '0', null, null, '100226548', '四川凉山地质勘察施工公司', null, '0', '1', null, null, '0', '3270', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226367', '0', '0', null, null, '100226548', '上海申瑞电力科技股份有限公司', null, '0', '1', null, null, '0', '4370', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226368', '0', '0', null, null, '100226548', '国电南京自动化股份有限公司', null, '0', '1', null, null, '0', '2090', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226369', '0', '0', null, null, '100226548', '测试承包商', null, '0', '1', null, null, '0', '750', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226370', '0', '0', null, null, '100226548', '南京申瑞电气系统控制有限公司', null, '0', '1', null, null, '0', '3440', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226371', '0', '0', null, null, '100226548', '四川奥深达电气有限公司', null, '0', '1', null, null, '0', '3110', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226372', '0', '0', null, null, '100226548', '成都府河电力自动化成套设备有限责任公司', null, '0', '1', null, null, '0', '1540', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226373', '0', '0', null, null, '100226548', '木里县林业局', null, '0', '1', null, null, '0', '1180', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226374', '0', '0', null, null, '100226548', '中电建成都铁塔有限公司', null, '0', '1', null, null, '0', '4050', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226375', '100226068', '0,100226068', null, null, '100226548', '清源公司博瓦项目部', '005001', '0', '2', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226376', '0', '0', null, null, '100226548', '南京南瑞集团公司水利水电技术分公司', '', '0', '1', '', '', '0', '3460', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226377', '0', '0', null, null, '100226545', '山东省金曼电气集团股份有限公司', null, '0', '1', null, null, '0', '1930', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226378', '0', '0', null, null, '100226548', '青岛汉缆股份有限公司', null, '0', '1', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226379', '0', '0', null, null, '100226548', '重庆起重机厂有限责任公司', null, '0', '1', null, null, '0', '140', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226380', '0', '0', null, null, '100226548', '西昌水文水资源科技咨询部', null, '0', '1', null, null, '0', '350', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226381', '0', '0', null, null, '100226548', '宁朗承包商', null, '0', '1', null, null, '0', '2030', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226382', '0', '0', null, null, '100226548', '福建南电股份有限公司', null, '0', '1', null, null, '0', '570', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226383', '100226042', '0,100226084,100226041,100226042', null, null, '100226548', '四川华电黄桷庄发电有限公司', null, '0', '1', null, null, '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226384', '0', '0', null, null, '100226548', '开县建筑开发有限公司', null, '0', '1', null, null, '0', '1790', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226385', '0', '0', null, null, '100226548', '四川诚实华龙钢建工程有限公司', null, '0', '1', null, null, '0', '2160', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226386', '0', '0', null, null, '100226548', '成都正和信息资讯有限责任公司', null, '2', '1', null, null, '0', '1470', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226387', '0', '0', null, null, '100226548', '四川省林业勘察设计研究院', null, '0', '1', null, null, '0', '2570', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226388', '0', '0', null, null, '100226548', '四川高地工程设计咨询有限公司攀枝花分公司', null, '0', '1', null, null, '0', '2110', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226389', '0', '0', null, null, '100226548', '四川沙河消防设备工程有限责任公司西昌分公司', null, '0', '1', null, null, '0', '2880', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226390', '0', '0', null, null, '100226545', '中国水电顾问集团中南勘测设计研究院', null, '0', '1', null, null, '0', '4170', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226391', '0', '0', null, null, '100226548', '贵阳路丰贸易有限公司', null, '0', '1', null, null, '0', '230', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226392', '0', '0', null, null, '100226548', '四川通源电力科技有限公司', null, '0', '1', null, null, '0', '2150', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226393', '0', '0', null, null, '100226548', '四川安迪科技实业有限公司', '', '0', '1', '', '', '0', '3090', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226394', '0', '0', null, null, '100226548', '成都天玑源科技有限公司', null, '0', '1', null, null, '0', '1640', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226395', '0', '0', null, null, '100226548', '中国电信股份有限公司成都分公司', '', '0', '1', '', '', '0', '4140', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226396', '100226054', '0,100226054', null, null, '100226548', '中铁五局新藏引水1标项目部', '002001', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226397', '0', '0', null, null, '100226545', '山东电力建设第一工程公司', null, '0', '1', null, null, '0', '1940', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226398', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '发电运行部', '', '1', '1', '', '', '0', '120', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226399', '0', '0', null, null, '100226548', '四川开拓信通科技有限公司', null, '0', '1', null, null, '0', '3030', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226400', '0', '0', null, null, '100226548', '中国电器科学研究院有限公司', null, '0', '1', null, null, '0', '4120', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226401', '0', '0', null, null, '100226548', '中铁二十三局集团有限公司', null, '0', '1', null, null, '0', '4020', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226402', '0', '0', null, null, '100226548', '中铁二十三局集团有限公司撒多水电站项目经理部', '', '0', '1', '', '', '0', '4010', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226403', '0', '0', null, null, '100226548', '四川锦美环保股份有限公司', null, '0', '1', null, null, '0', '2120', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226404', '0', '0', null, null, '100226548', '水利部丹江口水利管理局建设监理中心撒多水电站工程监理部', null, '2', '1', null, null, '0', '920', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226405', '0', '0', null, null, '100226548', '四川省凯利工业设备有限公司', null, '0', '1', null, null, '0', '2760', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226406', '0', '0', null, null, '100226548', '稻城县移民局', null, '0', '1', null, null, '0', '530', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226407', '0', '0', null, null, '100226548', '江苏纯江', null, '0', '1', null, null, '0', '810', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226408', '0', '0', null, null, '100226548', '沃茨阀门（长沙）有限公司', null, '0', '1', null, null, '0', '790', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226409', '0', '0', null, null, '100226548', '成都树子科技有限公司', null, '0', '1', null, null, '0', '1490', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226410', '0', '0', null, null, '100226548', '国网四川省电力公司', null, '2', '1', null, null, '0', '2080', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226411', '0', '0', null, null, '100226545', '中国水利水电第六工程局有限公司', null, '0', '1', null, null, '0', '4250', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226412', '0', '0', null, null, '100226548', '四川省水利水电工程局撒多水电站I标3#施工支洞工程项目经理部', '', '0', '1', '', '', '0', '2510', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226413', '0', '0', null, null, '100226548', '成都大冲通风管', null, '0', '1', null, null, '0', '1650', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226414', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '财务资产部', '', '1', '1', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226415', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '办公室', '', '1', '1', '', '', '0', '60', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226416', '0', '0', null, null, '100226548', '四川天成安全科技评估咨询有限公司', null, '0', '1', null, null, '0', '3140', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226417', '0', '0', null, null, '100226548', '稻城县人民政府大中型水电工程移民局', null, '0', '1', null, null, '0', '540', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226418', '0', '0', null, null, '100226548', '成都德菲环境工程有限公司', null, '0', '1', null, null, '0', '1520', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226419', '0', '0', null, null, '100226548', '中国电力工程顾问集团西南电力设计院有限公司', null, '0', '1', null, null, '0', '4130', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226420', '0', '0', null, null, '100226548', '四川广安智丰建设工程有限公司', null, '0', '1', null, null, '0', '3040', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226421', '0', '0', null, null, '100226548', '四川华远建设工程有限公司', null, '0', '1', null, null, '0', '3210', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226422', '0', '0', null, null, '100226548', '四川红明工程管理咨询有限公司', null, '0', '1', null, null, '0', '2280', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226423', '0', '0', null, null, '100226548', '广元宝珠机械电气工程有限责任公司', null, '0', '1', null, null, '0', '1820', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226424', '0', '0', null, null, '100226548', '木里藏族自治县水务局', null, '0', '1', null, null, '0', '1100', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226425', '0', '0', null, null, '100226548', '江苏中野交通工程有限公司', null, '2', '1', null, null, '0', '880', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226426', '0', '0', null, null, '100226548', '江西省水电工程局', null, '0', '1', null, null, '0', '800', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226427', '0', '0', null, null, '100226548', '木里县电力公司', null, '0', '1', null, null, '0', '1160', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226428', '0', '0', null, null, '100226548', '四川川康公路规划勘察设计有限责任公司', null, '0', '1', null, null, '0', '3070', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226429', '0', '0', null, null, '100226548', '四川省地质矿产勘查开发局101地质队', null, '0', '1', null, null, '0', '2690', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226430', '0', '0', null, null, '100226548', '西昌长城物资公司', null, '0', '1', null, null, '0', '280', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226431', '0', '0', null, null, '100226548', '曲阜恒威水工机械有限公司', null, '0', '1', null, null, '0', '1220', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226432', '0', '0', null, null, '100226548', '公共监理部', null, '0', '1', null, null, '0', '3820', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226433', '0', '0', null, null, '100226545', '吉林龙鼎电气股份有限公司', null, '0', '1', null, null, '0', '3400', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226434', '0', '0', null, null, '100226548', '北京中水科水电科技开发有限公司', null, '0', '1', null, null, '0', '3630', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226435', '0', '0', null, null, '100226548', '四川煤田地质一三七总公司', null, '0', '1', null, null, '0', '2850', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226436', '0', '0', null, null, '100226548', '四川忠机电力工程有限公司', null, '0', '1', null, null, '0', '3010', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226437', '0', '0', null, null, '100226548', '武汉四创自动控制技术有限责任公司', null, '0', '1', null, null, '0', '980', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226438', '0', '0', null, null, '100226548', '成都市华测检测技术有限公司', null, '0', '1', null, null, '0', '1610', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226439', '0', '0', null, null, '100226548', '四川捷通公路工程勘察设计有限公司', null, '0', '1', null, null, '0', '2970', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226440', '0', '0', null, null, '100226548', '上海欧特莱阀门机械有限公司', null, '0', '1', null, null, '0', '4380', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226441', '0', '0', null, null, '100226548', '葛洲坝第二工程有限公司', null, '0', '1', null, null, '0', '480', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226442', '0', '0', null, null, '100226548', '四川华瑞信息咨询有限责任公司', null, '0', '1', null, null, '0', '3240', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226443', '0', '0', null, null, '100226548', '四川省众信建设工程监理有限公司', null, '0', '1', null, null, '0', '2790', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226444', '0', '0', null, null, '100226548', '西安高压电器研究所电器制造有限责任公司', null, '0', '1', null, null, '0', '430', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226445', '0', '0', null, null, '100226548', '稻城县交通运输局', null, '0', '1', null, null, '0', '550', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226446', '0', '0', null, null, '100226548', '四川省建筑科学研究院', null, '0', '1', null, null, '0', '2610', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226447', '0', '0', null, null, '100226548', '宁朗指挥部厂', null, '0', '1', null, null, '0', '2020', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226448', '0', '0', null, null, '100226548', '凉山公路建设有限公司', null, '0', '1', null, null, '0', '3770', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226449', '0', '0', null, null, '100226548', '华电国际山东信息管理有限公司', null, '0', '1', null, null, '0', '3540', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226450', '0', '0', null, null, '100226548', '四川国泰民安科技有限公司', null, '0', '1', null, null, '0', '3170', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226451', '0', '0', null, null, '100226548', '中国水电十局压力管道制作项目经理部', '', '0', '1', '', '', '0', '4200', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226452', '0', '0', null, null, '100226548', '四川电力公司', '', '0', '1', '', '', '0', '2840', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226453', '0', '0', null, null, '100226548', '长沙中南工程试验检测咨询中心', null, '0', '1', null, null, '0', '50', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226454', '0', '0', null, null, '100226548', '成都市永盛林业设计咨询有限公司', null, '0', '1', null, null, '0', '1600', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226455', '0', '0', null, null, '100226548', '西昌鸿林装饰部', null, '0', '1', null, null, '0', '260', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226456', '0', '0', null, null, '100226548', '四川省地质测绘院', null, '0', '1', null, null, '0', '2700', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226457', '0', '0', null, null, '100226548', '四川省水利电力科学研究院', null, '0', '1', null, null, '0', '2450', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226458', '0', '0', null, null, '100226548', '凉山州交通局勘察设计队', null, '0', '1', null, null, '0', '3740', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226459', '0', '0', null, null, '100226548', '长江地球物理探测（武汉）有限公司', null, '0', '1', null, null, '0', '80', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226460', '0', '0', null, null, '100226548', '西昌君达环保科技咨询有限公司', null, '2', '1', null, null, '0', '420', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226461', '100226193', '0,100226084,100226089,100226193', null, null, '100226548', '测试33', '002003001001', '2', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226462', '0', '0', null, null, '100226548', '成都树林人科技有限公司', null, '0', '1', null, null, '0', '1480', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226463', '0', '0', null, null, '100226548', '四川省林业调查规划院', null, '0', '1', null, null, '0', '2550', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226464', '0', '0', null, null, '100226548', '湖北绍新特种电气股份有限公司', '', '0', '1', '', '', '0', '890', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226465', '0', '0', null, null, '100226548', '成都西电蜀能电器', null, '0', '1', null, null, '0', '1360', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226466', '0', '0', null, null, '100226548', '中国华电集团电力建设技术经济咨询中心', '', '0', '1', '', '', '0', '110', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226467', '0', '0', null, null, '100226545', '杭州力源发电设备有限公司', null, '0', '1', null, null, '0', '1030', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226468', '0', '0', null, null, '100226548', '中铁18局第I工程公司', null, '0', '1', null, null, '0', '4040', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226469', '0', '0', null, null, '100226548', '四川西南交大土木工程设计有限公司加固工程设计有限公司', null, '0', '1', null, null, '0', '2200', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226470', '100226042', '0,100226084,100226041,100226042', null, null, '100226548', '四川华电宜宾发电有限责任公司', null, '0', '1', null, null, '0', '40', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226471', '0', '0', null, null, '100226548', '攀枝花发康商贸', null, '0', '1', null, null, '0', '1300', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226472', '0', '0', null, null, '100226548', '四川省水利电力工程建设监理中心', null, '2', '1', null, null, '0', '2460', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226473', '0', '0', null, null, '100226548', '常州液压成套设备厂有限公司', null, '0', '1', null, null, '0', '1830', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226474', '0', '0', null, null, '100226548', '长江水利委员会工程建设监理公司', null, '0', '1', null, null, '0', '60', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226475', '0', '0', null, null, '100226548', '州移民局', null, '0', '1', null, null, '0', '1880', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226476', '0', '0', null, null, '100226548', '成都德玥环保科技有限公司', null, '0', '1', null, null, '0', '1530', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226477', '0', '0', null, null, '100226548', '南京晨光东螺波纹管有限公司', null, '0', '1', null, null, '0', '3450', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226478', '0', '0', null, null, '100226548', '四川省木里林业局', null, '0', '1', null, null, '0', '2580', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226479', '0', '0', null, null, '100226548', '四川天之汇建筑工程有限公司1', null, '0', '1', null, null, '0', '3160', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226480', '0', '0', null, null, '100226548', '凉山州木里县国土局', null, '0', '1', null, null, '0', '3710', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226481', '0', '0', null, null, '100226548', '迅达（中国）电梯有限公司成都分公司', null, '0', '1', null, null, '0', '200', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226482', '0', '0', null, null, '100226548', '成都合达恒兴科技发展有限公司', null, '0', '1', null, null, '0', '1710', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226483', '0', '0', null, null, '100226548', '西昌聚能电力开发有限责任公司', null, '0', '1', null, null, '0', '320', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226484', '0', '0', null, null, '100226548', '中国水电十局撒多首部枢纽工程项目经理部', '', '0', '1', '', '', '0', '4190', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226485', '0', '0', null, null, '100226548', '川电工程建设监理有限责任公司', null, '0', '1', null, null, '0', '1890', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226486', '0', '0', null, null, '100226548', '宏峰集团（福建）有限公司', null, '0', '1', null, null, '0', '1980', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226487', '0', '0', null, null, '100226548', '四川省送变电建设有限责任公司公司', null, '0', '1', null, null, '0', '2380', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226488', '0', '0', null, null, '100226548', '重庆路威土木工程设计有限公司', null, '0', '1', null, null, '0', '130', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226489', '0', '0', null, null, '100226548', '四川华豫博兴机电设备有限公司', null, '0', '1', null, null, '0', '3220', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226490', '0', '0', null, null, '100226548', '成都合达恒兴流体设备有限公司', null, '0', '1', null, null, '0', '1720', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226491', '0', '0', null, null, '100226548', '成都捷展诚科技发展有限公司', null, '0', '1', null, null, '0', '1510', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226492', '0', '0', null, null, '100226548', '四川今天工程建设技术有限公司', null, '0', '1', null, null, '0', '3300', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226493', '0', '0', null, null, '100226548', '凉山州木里林业局', null, '0', '1', null, null, '0', '3690', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226494', '0', '0', null, null, '100226548', '成都雨田骏', null, '0', '1', null, null, '0', '1340', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226495', '0', '0', null, null, '100226548', '中国葛洲坝集团股份有限公司', '', '0', '0', '', '', '0', '3860', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226496', '0', '0', null, null, '100226548', '村民扎西', null, '0', '1', null, null, '0', '1050', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226497', '0', '0', null, null, '100226548', '四川省环境工程评估中心', null, '0', '1', null, null, '0', '2410', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226498', '0', '0', null, null, '100226545', '河北安电电力器材有限公司', null, '0', '1', null, null, '0', '770', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226499', '0', '0', null, null, '100226548', '成都市锦桦商务服务有限公司', null, '0', '1', null, null, '0', '1580', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226500', '0', '0', null, null, '100226548', '西昌电业局', null, '0', '1', null, null, '0', '330', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226501', '0', '0', null, null, '100226548', '四川科锐电力信息自动化技术有限责任公司', null, '0', '1', null, null, '0', '2310', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226502', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226548', '宁朗项目建设管理部', '', '1', '1', '', '', '0', '100', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226503', '0', '0', null, null, '100226548', '西昌航天水泥有限责任公司', null, '0', '1', null, null, '0', '310', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226504', '0', '0', null, null, '100226545', '镇江华东电力设备制造厂', null, '0', '1', null, null, '0', '110', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226505', '100226063', '0,100226063', null, null, '100226548', '葛洲坝2公司撒多项目部', '005002', '0', '2', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226506', '100226513', '0,100226084,100226041,100226088,100226184,100226513', null, null, '100226548', '开发部', null, '2', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226507', '0', '0', null, null, '100226548', '四川鸿进达卫生技术服务有限公司', null, '2', '1', null, null, '0', '2100', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226508', '0', '0', null, null, '100226548', '威远县顺达物资经营部', null, '0', '1', null, null, '0', '2050', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226509', '0', '0', null, null, '100226548', '海南金盘电气有限公司', '', '0', '1', '', '', '0', '780', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226510', '0', '0', null, null, '100226548', '成都立本工程勘察设计有限责任公司', null, '0', '1', null, null, '0', '1400', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226511', '0', '0', null, null, '100226548', '成都嘉善商务服务管理有限公司', '', '0', '1', '', '', '0', '1680', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226512', '0', '0', null, null, '100226548', '四川省地质工程勘察院', null, '0', '1', null, null, '0', '2720', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226513', '100226184', '0,100226084,100226041,100226088,100226184', null, null, '100226548', '测试34', '002001003001002', '2', '1', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226514', '0', '0', null, null, '100226548', '成都和源渔业科技有限公司', null, '0', '1', null, null, '0', '1700', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226515', '0', '0', null, null, '100226548', '中铁十八局撒多水电站引水II标项目经理部', '', '0', '1', '', '', '0', '3970', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226516', '0', '0', null, null, '100226548', '四川中环康源卫生技术服务有限公司', null, '0', '1', '成都市高新区科园南路88号', null, '0', '3350', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226517', '0', '0', null, null, '100226548', '木里郎信电力开发有限公司', null, '0', '1', null, null, '0', '1060', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226518', '0', '0', null, null, '100226545', '江苏星光发电设备有限公司', null, '0', '1', null, null, '0', '850', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226519', '0', '0', null, null, '100226548', '彭州市先锋铭牌制版厂', null, '0', '1', null, null, '0', '1770', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226520', '0', '0', null, null, '100226548', '四川省地质工程集团公司', null, '0', '1', null, null, '0', '2710', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226521', '0', '0', null, null, '100226548', '凉 山 公 路 建 设 有 限 公 司', null, '0', '1', null, null, '0', '3780', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226522', '100226038', '0,100226038', null, null, '100226548', '丹江口监理固滴项目部', '02002001001', '0', '2', null, null, '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226523', '0', '0', null, null, '100226548', '四川金安水利科技有限责任公司', null, '2', '1', null, null, '0', '2130', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226524', '0', '0', null, null, '100226548', '四川省明远电力集团有限公司', null, '0', '1', null, null, '0', '2590', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226525', '0', '0', null, null, '100226548', '四川懿科网络信息有限公司', null, '0', '1', null, null, '0', '2980', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226526', '0', '0', null, null, '100226548', '四川西昌攀西地质勘察院', null, '0', '1', null, null, '0', '2190', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226527', '0', '0', null, null, '100226548', '成都大冲机电科技有限公司', null, '0', '1', null, null, '0', '1660', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226528', '0', '0', null, null, '100226548', '凉山彝族自治州移民工作局', null, '0', '1', null, null, '0', '3650', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226529', '0', '0', null, null, '100226548', '湖南省娄底市建设工程有限公司', null, '0', '1', null, null, '0', '670', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226530', '0', '0', null, null, '100226548', '重庆市亚东亚集团变压器有限公司', null, '0', '1', null, null, '0', '170', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226531', '0', '0', null, null, '100226548', '成都市雨田骏可以发展有限公司', null, '0', '1', null, null, '0', '1560', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226532', '100226039', '0,100226039', null, null, '100226548', '川电监理新藏项目部', '02002002001', '0', '2', null, null, '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226533', '0', '0', null, null, '100226548', '核工业地勘二八一大队科技开发部', null, '2', '1', null, null, '0', '1010', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226534', '0', '0', null, null, '100226548', '木里县俄亚乡人民政府', null, '0', '1', null, null, '0', '1200', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226535', '0', '0', null, null, '100226548', '南通晓星变压器有限公司', null, '0', '1', null, null, '0', '3420', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226536', '0', '0', null, null, '100226548', '中国电建集团贵阳勘测设计研究院有限公司', null, '0', '1', null, null, '0', '4100', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226537', '0', '0', null, null, '100226548', '农行、工行、中行、建行、商行', null, '0', '1', null, null, '0', '3790', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226538', '0', '0', null, null, '100226545', '武汉洪山电工科技有限公司', null, '0', '1', null, null, '0', '960', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226539', '0', '0', null, null, '100226548', '中铁十局', null, '0', '1', null, null, '0', '3910', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226540', '0', '0', null, null, '100226548', '四川省岳池电力建设总公司', null, '0', '1', null, null, '0', '2660', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226541', '100226070', '0,100226070', null, null, '100226548', '湖南监理博瓦项目部', '003002001', '0', '2', null, null, '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226542', '0', '0', null, null, '100226548', '北京恒诚信工程咨询有限公司', '', '0', '1', '', '', '0', '3590', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226543', '0', '0', null, null, '100226548', '铁岭特种阀门股份有限公司', null, '0', '1', null, null, '0', '120', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('100226544', '0', '0', null, null, '100226548', '四川省文物考古研究院', null, '0', '1', null, null, '0', '2600', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('102244798', '0', '0', null, null, '100226547', '四川四达通实业发展有限公司', '', '0', '0', '', '', '0', '4400', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('103094975', '0', '0', null, null, '100226547', '四川忠机电力设备工程有限公司', '', '0', '1', '', '', '0', '4410', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('103094976', '0', '0', null, null, '100226558', '西昌市江龙机电有限责任公司', '', '0', '1', '', '', '0', '4420', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('103132041', '0', '0', null, null, '100226549', '四川盛唐建设工程有限公司', '', '0', '0', '', '', '0', '4430', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('106001521', '0', '0', null, null, '100226547', '成都浚川工程设计咨询有限公司', '', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('119430327', '0', '0', null, null, '100226547', '成都鑫欣兄弟商贸有限公司', '', '0', '0', '', '', '0', '4140', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('119439848', '0', '0', null, null, '100226547', '四川唯诺家私有限公司', '', '0', '0', '', '', '0', '4150', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('119441899', '0', '0', null, null, '100226549', '成都启阳华通丰田汽车销售服务有限公司', '', '0', '0', '', '', '0', '4160', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('119441900', '0', '0', null, null, '100226547', '高新区车宠宠汽车服务中心', '', '0', '0', '', '', '0', '4170', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('121718833', '0', '0', null, null, '100226547', '四川川交路桥有限责任公司', '', '0', '0', '', '', '0', '4170', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('124613931', '0', '0', null, null, '100226547', '四川鸿达伟业建设工程有限公司', '', '0', '0', '', '', '0', '4190', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('124900910', '0', '0', null, null, '100226549', '湖南湘能卓信会计事务所有限公司', '', '0', '0', '', '', '0', '4200', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('124904778', '100226045', '0,100226084,100226041,100226042,100226045', null, null, '100226547', '党群工作部', '', '1', '1', '', '', '0', '160', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('125172676', '0', '0', null, null, '100226549', '成都运州电力设备有限公司', '', '0', '0', '', '', '0', '4210', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('125192162', '0', '0', null, null, '100226549', '成都智深自动化控制工程有限公司', '', '0', '0', '', '', '0', '4220', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('125689940', '0', '0', null, null, '100226549', '国电南瑞科技股份有限公司', '', '0', '0', '', '', '0', '4230', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('125698583', '0', '0', null, null, '100226549', '成都新路电脑有限公司', '', '0', '0', '', '', '0', '4240', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('125731540', '0', '0', null, null, '100226549', '成都华澳兴业科技有限公司', '', '0', '0', '', '', '0', '4250', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('125765584', '0', '0', null, null, '100226549', '成都天时利自动化设备有限责任公司', '', '0', '0', '', '', '0', '4260', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126002589', '0', '0', null, null, '100226549', '四川省电力工业调整试验所2', '', '0', '0', '', '', '0', '4270', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126002591', '0', '0', null, null, '100226549', '四川省电力工业调整试验所', '', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126023778', '0', '0', null, null, '100226549', '四川荣世达电气有限公司', '', '0', '0', '', '', '0', '4290', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126070610', '0', '0', null, null, '100226549', '南平市鑫林发电设备有限责任公司', '', '0', '0', '', '', '0', '4300', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126626762', '0', '0', null, null, '100226551', '四川华电工程技术有限公司攀枝花分公司', '', '0', '0', '', '', '0', '4310', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126716676', '0', '0', null, null, '100226550', '自贡市顺安水下作业有限公司', '', '0', '0', '', '', '0', '4320', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126730302', '0', '0', null, null, '100226549', '成都卓越华安信息技术服务有限公司', '', '0', '0', '', '', '0', '4330', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126788768', '0', '0', null, null, '100226549', '重庆宇来通信设备有限公司', '', '0', '0', '', '', '0', '4340', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126811783', '0', '0', null, null, '100226549', '四川天杰机电设备有限公司', '', '0', '0', '', '', '0', '4350', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126813840', '0', '0', null, null, '100226549', '泸定县金鹏广告有限责任公司', '', '0', '0', '', '', '0', '4360', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126819439', '0', '0', null, null, '100226549', '四川鑫炜吉建筑工程有限公司', '', '0', '0', '', '', '0', '4370', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126821625', '0', '0', null, null, '100226549', '四川瑞安消防工程有限公司', '', '0', '0', '', '', '0', '4380', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126827524', '0', '0', null, null, '100226549', '四川省祥善丰商贸有限公司', '', '0', '0', '', '', '0', '4390', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126977885', '0', '0', null, null, '100226549', '泸县万里广告有限公司', '', '0', '0', '', '', '0', '4400', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('126997775', '0', '0', null, null, '100226549', '云南善友经贸有限公司1', '', '0', '0', '', '', '0', '40', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('127427522', '0', '0', null, null, '100226558', '凉山州白金安装工程有限责任公司', '', '0', '0', '', '', '0', '4420', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('127454000', '0', '0', null, null, '100226549', '河北信高真空开关电器有限公司', '', '0', '0', '', '', '0', '4430', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('128182388', '0', '0', null, null, '100226549', '成都立德机电设备有限公司', '', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('132304213', '0', '0', null, null, '100226549', '泸定县五湖通贸易部', '', '0', '0', '', '', '0', '4450', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('132329085', '0', '0', null, null, '100226549', '浙江日新电气有限公司', '', '0', '0', '', '', '0', '4460', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('132741133', '0', '0', null, null, '100226549', '四川省兴发规划建筑设计有限公司', '', '0', '0', '', '', '0', '4470', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('132754077', '0', '0', null, null, '100226549', '成都易普集动力技术有限公司', '', '0', '0', '', '', '0', '4480', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('134362220', '0', '0', null, null, '100226549', '中国电建集团昆明勘测设计研究院有限公司', '', '0', '0', '', '', '0', '4490', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('138631661', '0', '0', null, null, '100226549', '重庆星利源科技有限公司', '', '0', '0', '', '', '0', '4500', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139038997', '100226084', '0,100226084', null, null, '100226547', '中国华电香港有限公司', '', '0', '0', '', '', '1', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039015', '139038997', '0,100226084,139038997', null, null, '100226547', '综合管理部', '', '1', '1', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039111', '0', '0', null, null, '100226547', 'PT. ZHEJIANG TENAGA PEMBANGUNAN INDONESIA', '', '0', '0', '', '', '0', '210', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039112', '0', '0', null, null, '100226547', 'PT.WUHAN SOUTHERN GEO ENGINEERING  INDONESIA', '', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039113', '0', '0', null, null, '100226547', 'PT.UNISON KARYATAMA', '', '0', '0', '', '', '0', '240', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039114', '0', '0', null, null, '100226547', '中国电建集团核电工程有限公司', '', '0', '0', '', '', '0', '4540', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039115', '0', '0', null, null, '100226547', 'PT. MUTIARA INDAH ANUGRAH', '', '0', '0', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039116', '0', '0', null, null, '100226547', '北京新能建电力工程咨询有限公司', '', '0', '0', '', '', '0', '4560', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039117', '0', '0', null, null, '100226547', '电力规划总院有限公司', '', '0', '0', '', '', '0', '4570', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039118', '0', '0', null, null, '100226547', '远光软件股份有限公司', '', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039119', '0', '0', null, null, '100226547', 'PT. RAMDAV PALEMBANG', '', '0', '0', '', '', '0', '4590', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039770', '0', '0', null, null, '100226547', '1暂无', '', '0', '0', '', '', '0', '4750', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039901', '139038997', '0,100226084,139038997', null, null, '100226547', '财务资产部', '', '1', '1', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039902', '139038997', '0,100226084,139038997', null, null, '100226547', '项目一部', '', '1', '1', '', '', '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039903', '139038997', '0,100226084,139038997', null, null, '100226547', '项目二部', '', '1', '1', '', '', '0', '40', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039904', '139038997', '0,100226084,139038997', null, null, '100226547', '计划发展部', '', '1', '1', '', '', '0', '50', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039905', '139038997', '0,100226084,139038997', null, null, '100226547', '中国华电香港有限公司印尼PE公司', '', '0', '0', '', '', '1', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039906', '139039905', '0,100226084,139038997,139039905', null, null, '100226547', '工程管理部', '', '1', '1', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039907', '139039905', '0,100226084,139038997,139039905', null, null, '100226547', '工程技术部', '', '1', '1', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039908', '139039905', '0,100226084,139038997,139039905', null, null, '100226547', '质量安全部', '', '1', '1', '', '', '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039909', '139039905', '0,100226084,139038997,139039905', null, null, '100226547', '商法部', '', '1', '1', '', '', '0', '40', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039910', '139039905', '0,100226084,139038997,139039905', null, null, '100226547', '财务部', '', '1', '1', '', '', '0', '50', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139039911', '139039905', '0,100226084,139038997,139039905', null, null, '100226547', '综合办公室', '', '1', '1', '', '', '0', '60', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139057288', '139038997', '0,100226084,139038997', null, null, '100226547', '领导班子', '', '1', '0', '', '', '0', '70', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139057289', '139039905', '0,100226084,139038997,139039905', null, null, '100226547', '项目经理部', '', '1', '1', '', '', '0', '70', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139057317', '139038997', '0,100226084,139038997', null, null, '100226547', '中国华电香港公司印尼PE公司', '', '0', '0', '', '', '1', '80', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139057386', '139039905', '0,100226084,139038997,139039905', null, null, '100226547', '物资采购部', '', '1', '1', '', '', '0', '80', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139069371', '0', '0', null, null, '100226547', '山东电力工程咨询院有限公司', '', '0', '0', '', '', '0', '4600', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090605', '0', '0', null, null, '100226547', '东方电气股份有限公司', '', '0', '0', '', '', '0', '540', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090606', '0', '0', null, null, '100226547', '哈尔滨电气股份有限公司', '', '0', '0', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090607', '0', '0', null, null, '100226547', '北京电力设备总厂有限公司', '', '0', '0', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090608', '0', '0', null, null, '100226547', '特而特涡轮（四川）有限公司', '', '0', '0', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090609', '0', '0', null, null, '100226547', '浙江菲达环保科技股份有限公司', '', '0', '0', '', '', '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090610', '0', '0', null, null, '100226547', '上海凯士比泵有限公司', '', '0', '0', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090611', '0', '0', null, null, '100226547', '东方电气集团东方锅炉股份有限公司', '', '0', '0', '', '', '0', '4670', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090612', '0', '0', null, null, '100226547', '江苏丰泰节能环保科技有限公司', '', '0', '0', '', '', '0', '4680', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090613', '0', '0', null, null, '100226547', '特变电工股份有限公司', '', '0', '0', '', '', '0', '4690', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090614', '0', '0', null, null, '100226547', '杭州汽轮机股份有限公司', '', '0', '0', '', '', '0', '4700', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090621', '0', '0', null, null, '100226547', '沈阳华电电站工程有限公司', '', '0', '0', '', '', '0', '4710', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090622', '0', '0', null, null, '100226547', '荏原机械淄博有限公司', '', '0', '0', '', '', '0', '4720', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090624', '0', '0', null, null, '100226547', '杭州华新机电工程有限公司', '', '0', '0', '', '', '0', '4730', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090625', '0', '0', null, null, '100226547', '湖北省神珑泵业有限责任公司', '', '0', '0', '', '', '0', '4740', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139090626', '0', '0', null, null, '100226547', '吉林光大电力设备股份有限公司', '', '0', '0', '', '', '0', '4750', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139118219', '0', '0', null, null, '100226547', '中南电力项目管理咨询（湖北）有限公司', '', '0', '0', '武昌区中南二路12号2栋（A栋）三层', '02765263852', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139178554', '0', '0', null, null, '100226547', '华电环球（北京）贸易发展有限公司', '', '0', '0', '', '', '0', '4770', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139288056', '0', '0', null, null, '100226547', 'EAST LEGEND LIMITED', '', '0', '0', '', '', '0', '4780', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139288057', '0', '0', null, null, '100226547', 'PT. Bukit Prima Bahari', '', '0', '0', '', '', '0', '4820', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139289157', '0', '0', null, null, '141480422', 'PT.HUADIAN BUKIT ASAM POWER', '', '0', '0', '', '', '1', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139337436', '139289157', '0,139289157', null, null, '100226547', '工程技术部', '', '1', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139337437', '139289157', '0,139289157', null, null, '100226547', '商法部', '', '1', '0', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139337438', '139289157', '0,139289157', null, null, '100226547', '财务部', '', '1', '0', '', '', '0', '30', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139337439', '139289157', '0,139289157', null, null, '100226547', '监察部', '', '1', '0', '', '', '0', '40', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139391011', '0', '0', null, null, '100226547', '中国太平洋人寿保险股份有限公司', '', '0', '0', '', '', '0', '4810', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139391012', '0', '0', null, null, '100226547', '中国人民财产保险股份有限公司', '', '0', '0', '', '', '0', '4820', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('139612043', '0', '0', null, null, '100226547', 'PT SEPCO II INDO', '', '0', '0', '', '', '0', '70', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('140207289', '0', '0', null, null, '100226549', 'CV. Media Teknik', '', '0', '0', '', '', '0', '4840', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('140407318', '0', '0', null, null, '100226547', 'EDDY BUDIMAN ALIWARGA', '', '0', '0', '', '', '0', '60', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('140409072', '0', '0', null, null, '100226547', 'CHRISTINE ALI', '', '0', '0', '', '', '0', '4860', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('140585552', '0', '0', null, null, '100226547', 'ANTING MS. NASUTION DRA', '', '0', '0', '', '', '0', '4870', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('140746338', '0', '0', null, null, '100226547', 'PT.DUTA SECURITY', '', '0', '0', '', '', '0', '60', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('140801179', '0', '0', null, null, '100226547', 'PT. CIPUTRA ADIGRAHA', '', '0', '0', '', '', '0', '90', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('140957440', '0', '0', null, null, '100226547', 'PT.Multi Perkasa Indosakti', '', '0', '0', '', '', '0', '4900', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('140959622', '0', '0', null, null, '100226547', 'PT.DAYA UTAMA TANGGUH AMANAH', '', '0', '0', '', '', '0', '4910', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('141487288', '0', '0', null, null, '100226557', 'BUT Hebei Research Institute Of Construction & Geotechnical Investigation Co,Ltd', '', '0', '0', '', '', '0', '4920', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('141637324', '0', '0', null, null, '141480422', 'BUT Hebei Research Institute Of Construction & Geotechnical Investigation Co,Ltd', '', '0', '0', '', '', '0', '4920', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('141689920', '0', '0', null, null, '100226549', '四川华电工程技术有限公司', '', '0', '0', '', '', '0', '1030', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('142053870', '0', '0', null, null, '141480422', 'PT.PUNDARIKA ATMA SEMESTA', '', '0', '0', '', '', '0', '4940', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('142085226', '0', '0', null, null, '141480422', 'PT.DOREMINDO INTERNATIONAL', '', '0', '0', '', '', '0', '4950', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('142095372', '0', '0', null, null, '141480422', 'BETTI SHADIQ PASADIGOE', '', '0', '0', '', '', '0', '4960', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('142342106', '0', '0', null, null, '141480422', 'PT MITRAJAYA ABADI NUSANTALA', '', '0', '0', '', '', '0', '150', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('142774519', '0', '0', null, null, '141480422', 'PT.CHINA TAIPING INSURANCE INDONESIA', '', '0', '0', '', '', '0', '4980', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('142893825', '0', '0', null, null, '100226547', '交通运输部天津水运工程科学研究所', '', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('142944172', '142774519', '0,142774519', null, null, '141480422', 'PT INNOPASSION INTERNATIONAL ENGINEERING INDONESIA', '', '0', '0', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('142947779', '0', '0', null, null, '141480422', 'PT INNOPASSION INTERNATIONAL ENGINEERING INDONESIA', '', '0', '0', '', '', '0', '170', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('143042743', '0', '0', null, null, '141480424', '深圳市一览网络股份有限公司', '', '0', '0', '', '', '0', '5010', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('143260858', '0', '0', null, null, '141480422', 'PT IF International', '', '0', '0', '', '', '0', '5020', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('143266464', '0', '0', null, null, '100226549', '中国人民财产保险股份有限公司四川省分公司', '', '0', '0', '', '', '0', '50', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('143400501', '0', '0', null, null, '100226547', '中国华电集团电力建设技术经济咨询中心有限公司', '', '0', '0', '', '', '0', '20', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('143490317', '0', '0', null, null, '141480422', 'PT DANA ARTHA MINING', '', '0', '0', '', '', '0', '5050', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('143629160', '0', '0', null, null, '141480422', 'PT.GREE ELECTRIC APPLIANCES INDONESIA', '', '0', '0', '', '', '0', '5060', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('143666675', '0', '0', null, null, '141480422', 'PT INDONESIA COMNETS PLUS', '', '0', '0', '', '', '0', '5070', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('143948651', '0', '0', null, null, '141480422', 'PT JIANGXI THERMAL POWER CONSTRUCTION', '', '0', '0', '', '', '0', '5080', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('144117740', '0', '0', null, null, '141480422', 'SUPERVISING ENGINEER', '', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('144117741', '0', '0', null, null, '141480422', 'CHDHK-MIA JOINT OPERATION', '', '0', '0', '', '', '0', '5100', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('144480308', '0', '0', null, null, '141480422', 'CHINA RAILWAY 11 BUREAU GROUP CORPORATION', '', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('144587147', '0', '0', null, null, '100226547', '中国华电科工集团有限公司', '', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('144589225', '0', '0', null, null, '141480422', 'PT. JIANGXI THERMAL POWER CONSTRUCTION', '', '0', '0', '', '', '0', '5130', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('145228084', '0', '0', null, null, '141480422', 'PT.SELINDO DWIJAYAABADI(海洋王）', '', '0', '0', '', '', '0', '5130', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('145228090', '0', '0', null, null, '141480422', 'PT.KARYATAMA MAKMUR PERKASA', '', '0', '0', '', '', '0', '5140', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('145252850', '0', '0', null, null, '141480422', 'PT. Karyatama Makmur Perkasa', '', '0', '0', '', '', '0', '5150', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('145277661', '0', '0', null, null, '141480422', 'PT.FUJIAN POWER ENGINEERING', '', '0', '0', '', '', '0', '5150', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('145278376', '0', '0', null, null, '100226547', '上海永顺丰国际物流有限公司', '', '0', '0', '', '', '0', '5160', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('145363520', '139039115', '0,139039115', null, null, '100226547', 'PT.MUTIARA INDAH ANUGRAH', '', '0', '0', '', '', '0', '10', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('145801553', '0', '0', null, null, '100226547', '中国能源建设集团安徽电力建设第二工程有限公司', '', '0', '0', '', '', '0', '5170', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');
INSERT INTO `sys_office` VALUES ('145867573', '0', '0', null, null, '141480422', 'PT NUSANTARA JAYA KONSTRINDO', '', '0', '0', '', '', '0', '270', '0', null, '2020-05-21 14:39:02', '0', null, '2020-05-21 14:39:11', null, '0');

-- ----------------------------
-- Table structure for sys_proprietor
-- ----------------------------
DROP TABLE IF EXISTS `sys_proprietor`;
CREATE TABLE `sys_proprietor` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `no` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '编号',
  `name` varchar(100) NOT NULL COMMENT '业主名称',
  `office_id` bigint(20) NOT NULL COMMENT '机构编码',
  `office_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '机构名称',
  `grant_date` datetime NOT NULL COMMENT '授权日期',
  `expire_date` datetime NOT NULL COMMENT '过期日期',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `office_name` (`no`,`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='业主信息表';

-- ----------------------------
-- Records of sys_proprietor
-- ----------------------------

-- ----------------------------
-- Table structure for sys_proprietor_office
-- ----------------------------
DROP TABLE IF EXISTS `sys_proprietor_office`;
CREATE TABLE `sys_proprietor_office` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `proprietor_id` bigint(20) NOT NULL COMMENT '业主编号',
  `proprietor_name` varchar(100) NOT NULL COMMENT '业主名称',
  `office_id` bigint(20) NOT NULL COMMENT '机构编码',
  `office_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '机构名称',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `office_name` (`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='业主-机构表';

-- ----------------------------
-- Records of sys_proprietor_office
-- ----------------------------

-- ----------------------------
-- Table structure for sys_report
-- ----------------------------
DROP TABLE IF EXISTS `sys_report`;
CREATE TABLE `sys_report` (
  `id` int(11) NOT NULL DEFAULT '0' COMMENT '编号',
  `company_id` int(11) NOT NULL COMMENT '公司',
  `office_id` int(11) NOT NULL COMMENT '部门',
  `project_id` int(11) NOT NULL COMMENT '登录名',
  `title` varchar(64) NOT NULL COMMENT '标题',
  `content` varchar(32) DEFAULT NULL COMMENT '编号',
  `type` varchar(32) NOT NULL COMMENT '类型',
  `auditor_id` varchar(20) DEFAULT NULL COMMENT '审核人',
  `auditor_name` varchar(100) DEFAULT NULL COMMENT '审核人名字',
  `audit_status` int(11) NOT NULL COMMENT '审核状态',
  `audit_date` datetime DEFAULT NULL COMMENT '审核日期',
  `create_by` int(11) NOT NULL DEFAULT '1' COMMENT '创建者',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` int(11) NOT NULL DEFAULT '1' COMMENT '更新者',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(10) DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(11) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_login_name` (`project_id`,`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户报告表';

-- ----------------------------
-- Records of sys_report
-- ----------------------------

-- ----------------------------
-- Table structure for sys_request_log
-- ----------------------------
DROP TABLE IF EXISTS `sys_request_log`;
CREATE TABLE `sys_request_log` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `remote_addr` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '远程地址',
  `x_forwarded_for` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT 'x-forwarded-for',
  `method` varchar(20) CHARACTER SET utf8mb4 NOT NULL COMMENT '请求类型',
  `url` varchar(200) CHARACTER SET utf8mb4 NOT NULL COMMENT 'URL',
  `paramters` text CHARACTER SET utf8mb4 COMMENT 'url参数',
  `body` text CHARACTER SET utf8mb4 COMMENT '请求body',
  `response_code` int(11) DEFAULT NULL COMMENT '回应code',
  `response_msg` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '回应信息',
  `response_data` text CHARACTER SET utf8mb4 COMMENT '返回数据',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新人名称',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='http请求日志';

-- ----------------------------
-- Records of sys_request_log
-- ----------------------------

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '公司编号',
  `company_name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '公司名称',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `en_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '英文名',
  `create_by` bigint(20) NOT NULL COMMENT '创建人编号',
  `create_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '创建人名称',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新人编号',
  `update_name` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '更新人名字',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL COMMENT '删除标志',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
INSERT INTO `sys_role` VALUES ('100724', '0', null, '超级用户', 'SUPER_ADMIN', '100688', null, '2018-04-29 13:33:43', '100225719', null, '2018-08-15 15:12:21', null, '0');
INSERT INTO `sys_role` VALUES ('100011831', '0', null, '施工单位用户', 'DEV_USER', '100688', null, '2018-05-30 22:53:47', '100688', null, '2018-05-30 22:53:47', null, '0');
INSERT INTO `sys_role` VALUES ('100011832', '0', null, '监理单位用户', 'SUP_USER', '100688', null, '2018-05-30 22:54:31', '100688', null, '2018-05-30 22:54:31', null, '0');
INSERT INTO `sys_role` VALUES ('100011833', '0', null, '公司合同管理员', 'CONTRACT_MANAGER', '100688', null, '2018-05-30 22:55:04', '100225719', null, '2018-08-15 15:12:56', null, '0');
INSERT INTO `sys_role` VALUES ('100279937', '0', null, '公司建管部用户', 'NORMAL_MG_USER', '100225719', null, '2018-07-08 16:28:14', '100225719', null, '2018-08-15 15:17:22', null, '0');
INSERT INTO `sys_role` VALUES ('100281424', '0', null, '公司领导班子', 'COMPANY_MANAGER', '100225847', null, '2018-07-10 21:33:14', '100225719', null, '2018-12-05 11:14:56', null, '0');
INSERT INTO `sys_role` VALUES ('100291476', '0', null, '公司职能部门用户', 'NORMAL_USER', '100225847', null, '2018-07-28 15:26:54', '100225719', null, '2018-12-05 16:18:22', null, '0');
INSERT INTO `sys_role` VALUES ('102546074', '0', null, '普通合同维护', 'CONTRACT-DEPARTMENT', '100225719', null, '2018-09-26 20:34:57', '100225719', null, '2018-12-05 16:26:20', null, '0');
INSERT INTO `sys_role` VALUES ('102561833', '0', null, '查询', 'SEARCH', '100225719', null, '2018-09-29 18:12:43', '100225719', null, '2018-12-05 17:43:21', null, '0');
INSERT INTO `sys_role` VALUES ('124949744', '0', null, '施工单位用户1', 'DEV_USER1', '100225854', null, '2018-12-14 10:33:46', '100225847', null, '2018-12-14 15:46:26', null, '0');
INSERT INTO `sys_role` VALUES ('142227389', '0', null, '公司财务', 'COMPANY_ACCOUNTANT', '100225847', null, '2019-07-03 11:51:44', '100225847', null, '2019-07-03 11:51:44', null, '0');

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `role_id` bigint(20) NOT NULL COMMENT '角色编号',
  `role_name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '角色名称',
  `menu_id` bigint(20) NOT NULL COMMENT '菜单编号',
  `menu_name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '菜单名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='角色菜单';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
INSERT INTO `sys_role_menu` VALUES ('110281461', '100724', '', '110000039', '');
INSERT INTO `sys_role_menu` VALUES ('143540701', '100724', null, '100006436', null);
INSERT INTO `sys_role_menu` VALUES ('143540702', '100724', null, '100005895', null);
INSERT INTO `sys_role_menu` VALUES ('143540703', '100724', null, '100005898', null);
INSERT INTO `sys_role_menu` VALUES ('143540704', '100724', null, '100005899', null);
INSERT INTO `sys_role_menu` VALUES ('143540705', '100724', null, '100005897', null);
INSERT INTO `sys_role_menu` VALUES ('143540706', '100724', null, '100007455', null);
INSERT INTO `sys_role_menu` VALUES ('143540707', '100724', null, '100007457', null);
INSERT INTO `sys_role_menu` VALUES ('143540708', '100724', null, '100007458', null);
INSERT INTO `sys_role_menu` VALUES ('143540709', '100724', null, '100007459', null);
INSERT INTO `sys_role_menu` VALUES ('143540710', '100724', null, '100007460', null);
INSERT INTO `sys_role_menu` VALUES ('143540711', '100724', null, '100007456', null);
INSERT INTO `sys_role_menu` VALUES ('143540712', '100724', null, '100008117', null);
INSERT INTO `sys_role_menu` VALUES ('143540713', '100724', null, '100008118', null);
INSERT INTO `sys_role_menu` VALUES ('143540714', '100724', null, '100008119', null);
INSERT INTO `sys_role_menu` VALUES ('143540715', '100724', null, '100008120', null);
INSERT INTO `sys_role_menu` VALUES ('143540716', '100724', null, '100008116', null);
INSERT INTO `sys_role_menu` VALUES ('143540717', '100724', null, '100000021', null);
INSERT INTO `sys_role_menu` VALUES ('143540718', '100724', null, '100000030', null);
INSERT INTO `sys_role_menu` VALUES ('143540719', '100724', null, '100000039', null);
INSERT INTO `sys_role_menu` VALUES ('143540720', '100724', null, '100000038', null);
INSERT INTO `sys_role_menu` VALUES ('143540721', '100724', null, '100000547', null);
INSERT INTO `sys_role_menu` VALUES ('143540722', '100724', null, '100000545', null);
INSERT INTO `sys_role_menu` VALUES ('143540723', '100724', null, '100006528', null);
INSERT INTO `sys_role_menu` VALUES ('143540724', '100724', null, '100000546', null);
INSERT INTO `sys_role_menu` VALUES ('143540725', '100724', null, '100000932', null);
INSERT INTO `sys_role_menu` VALUES ('143540726', '100724', null, '100000931', null);
INSERT INTO `sys_role_menu` VALUES ('143540727', '100724', null, '100000485', null);
INSERT INTO `sys_role_menu` VALUES ('143540728', '100724', null, '100000486', null);
INSERT INTO `sys_role_menu` VALUES ('143540729', '100724', null, '100000487', null);
INSERT INTO `sys_role_menu` VALUES ('143540730', '100724', null, '100005918', null);
INSERT INTO `sys_role_menu` VALUES ('143540731', '100724', null, '100000036', null);
INSERT INTO `sys_role_menu` VALUES ('143540732', '100724', null, '100000128', null);
INSERT INTO `sys_role_menu` VALUES ('143540733', '100724', null, '100000028', null);
INSERT INTO `sys_role_menu` VALUES ('143540734', '100724', null, '100002228', null);
INSERT INTO `sys_role_menu` VALUES ('143540735', '100724', null, '100002229', null);
INSERT INTO `sys_role_menu` VALUES ('143540736', '100724', null, '100002230', null);
INSERT INTO `sys_role_menu` VALUES ('143540737', '100724', null, '100002231', null);
INSERT INTO `sys_role_menu` VALUES ('143540738', '100724', null, '100015289', null);
INSERT INTO `sys_role_menu` VALUES ('143540739', '100724', null, '100002233', null);
INSERT INTO `sys_role_menu` VALUES ('143540740', '100724', null, '100002234', null);
INSERT INTO `sys_role_menu` VALUES ('143540741', '100724', null, '100002235', null);
INSERT INTO `sys_role_menu` VALUES ('143540742', '100724', null, '100011673', null);
INSERT INTO `sys_role_menu` VALUES ('143540743', '100724', null, '100270067', null);
INSERT INTO `sys_role_menu` VALUES ('143540744', '100724', null, '100272259', null);
INSERT INTO `sys_role_menu` VALUES ('143540745', '100724', null, '100273864', null);
INSERT INTO `sys_role_menu` VALUES ('143540746', '100724', null, '100273865', null);
INSERT INTO `sys_role_menu` VALUES ('143540747', '100724', null, '100273866', null);
INSERT INTO `sys_role_menu` VALUES ('143540748', '100724', null, '100273861', null);
INSERT INTO `sys_role_menu` VALUES ('143540749', '100724', null, '100273862', null);
INSERT INTO `sys_role_menu` VALUES ('143540750', '100724', null, '100002846', null);
INSERT INTO `sys_role_menu` VALUES ('143540751', '100724', null, '100002844', null);
INSERT INTO `sys_role_menu` VALUES ('143540752', '100724', null, '100002845', null);
INSERT INTO `sys_role_menu` VALUES ('143540753', '100724', null, '100002847', null);
INSERT INTO `sys_role_menu` VALUES ('143540754', '100724', null, '100276177', null);
INSERT INTO `sys_role_menu` VALUES ('143540755', '100724', null, '100276178', null);
INSERT INTO `sys_role_menu` VALUES ('143540756', '100724', null, '100276179', null);
INSERT INTO `sys_role_menu` VALUES ('143540757', '100724', null, '100276613', null);
INSERT INTO `sys_role_menu` VALUES ('143540758', '100724', null, '100276614', null);
INSERT INTO `sys_role_menu` VALUES ('143540759', '100724', null, '100276615', null);
INSERT INTO `sys_role_menu` VALUES ('143540760', '100724', null, '100276616', null);
INSERT INTO `sys_role_menu` VALUES ('143540761', '100724', null, '100276684', null);
INSERT INTO `sys_role_menu` VALUES ('143540762', '100724', null, '100276386', null);
INSERT INTO `sys_role_menu` VALUES ('143540763', '100724', null, '100276387', null);
INSERT INTO `sys_role_menu` VALUES ('143540764', '100724', null, '100276388', null);
INSERT INTO `sys_role_menu` VALUES ('143540765', '100724', null, '100287287', null);
INSERT INTO `sys_role_menu` VALUES ('143540766', '100724', null, '100287288', null);
INSERT INTO `sys_role_menu` VALUES ('143540767', '100724', null, '100287289', null);
INSERT INTO `sys_role_menu` VALUES ('143540768', '100724', null, '100273863', null);
INSERT INTO `sys_role_menu` VALUES ('143540769', '100724', null, '100288466', null);
INSERT INTO `sys_role_menu` VALUES ('143540770', '100724', null, '100276389', null);
INSERT INTO `sys_role_menu` VALUES ('143540771', '100724', null, '100277688', null);
INSERT INTO `sys_role_menu` VALUES ('143540772', '100724', null, '100276390', null);
INSERT INTO `sys_role_menu` VALUES ('143540773', '100724', null, '100276391', null);
INSERT INTO `sys_role_menu` VALUES ('143540774', '100724', null, '100000484', null);
INSERT INTO `sys_role_menu` VALUES ('143540775', '100724', null, '100005894', null);
INSERT INTO `sys_role_menu` VALUES ('143540776', '100724', null, '100002843', null);
INSERT INTO `sys_role_menu` VALUES ('143540777', '100724', null, '100000035', null);
INSERT INTO `sys_role_menu` VALUES ('143540778', '100724', null, '100000020', null);
INSERT INTO `sys_role_menu` VALUES ('143540779', '100724', null, '102235132', null);
INSERT INTO `sys_role_menu` VALUES ('143540780', '100724', null, '100287367', null);
INSERT INTO `sys_role_menu` VALUES ('143540781', '100724', null, '139039920', null);
INSERT INTO `sys_role_menu` VALUES ('143540782', '100724', null, '139039921', null);
INSERT INTO `sys_role_menu` VALUES ('143540784', '100724', null, '139039923', null);
INSERT INTO `sys_role_menu` VALUES ('143540785', '100724', null, '139039914', null);
INSERT INTO `sys_role_menu` VALUES ('143540786', '100724', null, '139039915', null);
INSERT INTO `sys_role_menu` VALUES ('143540787', '100724', null, '139039916', null);
INSERT INTO `sys_role_menu` VALUES ('143540788', '100724', null, '139039924', null);
INSERT INTO `sys_role_menu` VALUES ('143540789', '100724', null, '139039925', null);
INSERT INTO `sys_role_menu` VALUES ('143540790', '100724', null, '139039917', null);
INSERT INTO `sys_role_menu` VALUES ('143540791', '100724', null, '139039918', null);
INSERT INTO `sys_role_menu` VALUES ('143540792', '100724', null, '139039919', null);
INSERT INTO `sys_role_menu` VALUES ('143540793', '100724', null, '100005896', null);
INSERT INTO `sys_role_menu` VALUES ('143540794', '100724', null, '139137285', null);
INSERT INTO `sys_role_menu` VALUES ('143540795', '100724', null, '100281187', null);
INSERT INTO `sys_role_menu` VALUES ('143540796', '100724', null, '100281186', null);
INSERT INTO `sys_role_menu` VALUES ('143540797', '100724', null, '139237286', null);
INSERT INTO `sys_role_menu` VALUES ('143540798', '100724', null, '139257286', null);
INSERT INTO `sys_role_menu` VALUES ('143540799', '100724', null, '140447312', null);
INSERT INTO `sys_role_menu` VALUES ('143540800', '100724', null, '140907402', null);
INSERT INTO `sys_role_menu` VALUES ('143540801', '100724', null, '140907401', null);
INSERT INTO `sys_role_menu` VALUES ('143540802', '100724', null, '140947325', null);
INSERT INTO `sys_role_menu` VALUES ('143540803', '100724', null, '140947324', null);
INSERT INTO `sys_role_menu` VALUES ('143540804', '100724', null, '140947323', null);
INSERT INTO `sys_role_menu` VALUES ('143540805', '100724', null, '140947322', null);
INSERT INTO `sys_role_menu` VALUES ('143540806', '100724', null, '140947321', null);
INSERT INTO `sys_role_menu` VALUES ('143540807', '100724', null, '142457341', null);
INSERT INTO `sys_role_menu` VALUES ('143540808', '100724', null, '142457340', null);
INSERT INTO `sys_role_menu` VALUES ('143540809', '100724', null, '142617346', null);
INSERT INTO `sys_role_menu` VALUES ('143540810', '100724', null, '142617345', null);
INSERT INTO `sys_role_menu` VALUES ('143540811', '100724', null, '142332526', null);
INSERT INTO `sys_role_menu` VALUES ('143540812', '100724', null, '142332527', null);
INSERT INTO `sys_role_menu` VALUES ('143540813', '100724', null, '142687377', null);
INSERT INTO `sys_role_menu` VALUES ('143540814', '100724', null, '143457338', null);
INSERT INTO `sys_role_menu` VALUES ('145539381', '100724', null, '145539380', null);
INSERT INTO `sys_role_menu` VALUES ('146167338', '100724', null, '146167336', null);
INSERT INTO `sys_role_menu` VALUES ('146167339', '100724', null, '146167337', null);
INSERT INTO `sys_role_menu` VALUES ('146207430', '100724', null, '146207429', null);
INSERT INTO `sys_role_menu` VALUES ('500029091', '100291476', '公司职能部门用户', '100287287', '个人办公');
INSERT INTO `sys_role_menu` VALUES ('500029092', '100291476', '公司职能部门用户', '100000484', '项目管理');
INSERT INTO `sys_role_menu` VALUES ('500029093', '100279937', '公司建管部用户', '100287287', '个人办公');
INSERT INTO `sys_role_menu` VALUES ('500029094', '100279937', '公司建管部用户', '100000484', '项目管理');
INSERT INTO `sys_role_menu` VALUES ('500029103', '100011831', '施工单位用户', '100000484', '项目管理');
INSERT INTO `sys_role_menu` VALUES ('500029104', '100011831', '施工单位用户', '100287287', '个人办公');
INSERT INTO `sys_role_menu` VALUES ('500029105', '100011831', '施工单位用户', '100287289', '任务(开始)');
INSERT INTO `sys_role_menu` VALUES ('500029106', '100011831', '施工单位用户', '139137285', '综合管理');
INSERT INTO `sys_role_menu` VALUES ('500029107', '100011831', '施工单位用户', '140907401', '费用报销');
INSERT INTO `sys_role_menu` VALUES ('500029108', '100011831', '施工单位用户', '140907402', '用品报销');
INSERT INTO `sys_role_menu` VALUES ('500029109', '100011831', '施工单位用户', '140947321', '一般申请');
INSERT INTO `sys_role_menu` VALUES ('500029110', '100011831', '施工单位用户', '140947322', '差旅报销');
INSERT INTO `sys_role_menu` VALUES ('500029111', '100011831', '施工单位用户', '139039920', '库存管理');
INSERT INTO `sys_role_menu` VALUES ('500029112', '100011831', '施工单位用户', '139039921', '库存管理');
INSERT INTO `sys_role_menu` VALUES ('500029113', '100011831', '施工单位用户', '100000485', '项目(信息)');
INSERT INTO `sys_role_menu` VALUES ('500029114', '100011831', '施工单位用户', '100000036', '投资管理');
INSERT INTO `sys_role_menu` VALUES ('500031002', '100011833', '公司合同管理员', '100000484', '项目管理');
INSERT INTO `sys_role_menu` VALUES ('500031003', '100011833', '公司合同管理员', '100287287', '个人办公');
INSERT INTO `sys_role_menu` VALUES ('500031004', '100011833', '公司合同管理员', '100287289', '任务(开始)');
INSERT INTO `sys_role_menu` VALUES ('500031005', '100011833', '公司合同管理员', '100287367', '任务(完成)');
INSERT INTO `sys_role_menu` VALUES ('500031006', '100011833', '公司合同管理员', '143457338', '任务(统计)');
INSERT INTO `sys_role_menu` VALUES ('500031007', '100011833', '公司合同管理员', '139137285', '综合管理');
INSERT INTO `sys_role_menu` VALUES ('500031008', '100011833', '公司合同管理员', '140907401', '费用报销');
INSERT INTO `sys_role_menu` VALUES ('500031009', '100011833', '公司合同管理员', '140907402', '用品报销');
INSERT INTO `sys_role_menu` VALUES ('500031010', '100011833', '公司合同管理员', '140947321', '一般申请');
INSERT INTO `sys_role_menu` VALUES ('500031011', '100011833', '公司合同管理员', '140947322', '差旅报销');
INSERT INTO `sys_role_menu` VALUES ('500034003', '100724', '超级用户', '500034001', '库房管理');
INSERT INTO `sys_role_menu` VALUES ('500034004', '100724', '超级用户', '500034002', '物资管理');
INSERT INTO `sys_role_menu` VALUES ('500034005', '100724', '超级用户', '500034000', '物资管理');
INSERT INTO `sys_role_menu` VALUES ('500034009', '100724', '超级用户', '500034008', '库存历史');
INSERT INTO `sys_role_menu` VALUES ('500034010', '100724', '超级用户', '500034006', '库存管理');
INSERT INTO `sys_role_menu` VALUES ('500034011', '100724', '超级用户', '500034007', '库存管理');
INSERT INTO `sys_role_menu` VALUES ('500039002', '100724', '超级用户', '500039000', '物资入库');
INSERT INTO `sys_role_menu` VALUES ('500039003', '100724', '超级用户', '500039001', '物资出库');

-- ----------------------------
-- Table structure for sys_schemas_version
-- ----------------------------
DROP TABLE IF EXISTS `sys_schemas_version`;
CREATE TABLE `sys_schemas_version` (
  `installed_rank` int(11) NOT NULL COMMENT '序号',
  `version` varchar(50) DEFAULT NULL COMMENT '版本号',
  `description` varchar(200) NOT NULL COMMENT '描述',
  `type` varchar(20) NOT NULL COMMENT '类别',
  `script` varchar(1000) NOT NULL COMMENT '脚本',
  `checksum` int(11) DEFAULT NULL COMMENT '校验和',
  `installed_by` varchar(100) NOT NULL COMMENT '执行人',
  `installed_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '执行事件',
  `execution_time` int(11) NOT NULL COMMENT '执行耗时',
  `success` tinyint(1) NOT NULL COMMENT '成功标志',
  PRIMARY KEY (`installed_rank`) USING BTREE,
  KEY `sys_schemas_version_s_idx` (`success`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='更新记录表';

-- ----------------------------
-- Records of sys_schemas_version
-- ----------------------------

-- ----------------------------
-- Table structure for sys_sequence
-- ----------------------------
DROP TABLE IF EXISTS `sys_sequence`;
CREATE TABLE `sys_sequence` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '名称',
  `current_val` bigint(20) NOT NULL COMMENT '当前值',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `seq_name` (`name`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=10001 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='序列表';

-- ----------------------------
-- Records of sys_sequence
-- ----------------------------
INSERT INTO `sys_sequence` VALUES ('10000', 'id', '500044000');

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` bigint(20) NOT NULL DEFAULT '0' COMMENT '编号',
  `company_id` bigint(20) NOT NULL COMMENT '公司',
  `company_name` varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '公司名称',
  `office_id` bigint(20) DEFAULT NULL COMMENT '部门',
  `office_name` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '部门名称',
  `login_name` varchar(32) CHARACTER SET utf8mb4 NOT NULL COMMENT '登录名',
  `password` varchar(64) CHARACTER SET utf8mb4 NOT NULL COMMENT '密码',
  `no` varchar(32) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '编号',
  `name` varchar(32) CHARACTER SET utf8mb4 NOT NULL COMMENT '名字',
  `signature` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '签字链接',
  `address` varchar(200) DEFAULT NULL COMMENT '地址',
  `phone` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '电话',
  `mobile` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '手机',
  `email` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '邮箱',
  `photo` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '照片',
  `type` tinyint(1) NOT NULL COMMENT '类型',
  `status` tinyint(1) NOT NULL COMMENT '状态',
  `login_count` int(11) NOT NULL DEFAULT '0' COMMENT '登陆计数',
  `login_ip` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '登陆IP',
  `login_date` datetime DEFAULT NULL COMMENT '登陆日期',
  `login_flag` int(11) DEFAULT '0' COMMENT '登陆标志',
  `create_by` bigint(20) NOT NULL COMMENT '创建者',
  `create_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '创建者姓名',
  `create_date` datetime NOT NULL COMMENT '创建日期',
  `update_by` bigint(20) NOT NULL COMMENT '更新者',
  `update_name` varchar(50) CHARACTER SET utf8mb4 NOT NULL COMMENT '更新者姓名',
  `update_date` datetime NOT NULL COMMENT '更新日期',
  `remarks` varchar(20) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `del_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT '删除标志',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `user_login_name` (`login_name`,`del_flag`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户表';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
INSERT INTO `sys_user` VALUES ('100225847', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '于其先', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '1', '', '2017-04-19 12:41:01', '100225847', '于其先', '2020-06-13 20:21:19', '', '0');
INSERT INTO `sys_user` VALUES ('500026000', '100226334', '成都墨安科技有限公司', '1', '研发部', '于安若', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1001', '于安若', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-08 22:14:17', '100225847', '于其先', '2020-06-13 20:23:10', null, '0');
INSERT INTO `sys_user` VALUES ('500027000', '100226334', '成都墨安科技有限公司', null, null, '贾然', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '10002', '贾然', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 14:03:38', '100225847', '于其先', '2020-06-13 14:03:38', null, '0');
INSERT INTO `sys_user` VALUES ('500027001', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试1', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:03:13', '100225847', '于其先', '2020-06-13 15:03:13', null, '0');
INSERT INTO `sys_user` VALUES ('500027002', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试2', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:03:18', '100225847', '于其先', '2020-06-13 15:03:18', null, '0');
INSERT INTO `sys_user` VALUES ('500027003', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试3', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:03:22', '100225847', '于其先', '2020-06-13 15:03:22', null, '0');
INSERT INTO `sys_user` VALUES ('500027004', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试4', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:03:26', '100225847', '于其先', '2020-06-13 15:03:26', null, '0');
INSERT INTO `sys_user` VALUES ('500027005', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试5', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:03:29', '100225847', '于其先', '2020-06-13 15:03:29', null, '0');
INSERT INTO `sys_user` VALUES ('500027006', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试6', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:03:34', '100225847', '于其先', '2020-06-13 15:03:34', null, '0');
INSERT INTO `sys_user` VALUES ('500027007', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试7', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:03:38', '100225847', '于其先', '2020-06-13 15:03:38', null, '0');
INSERT INTO `sys_user` VALUES ('500027008', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试8', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:03:43', '100225847', '于其先', '2020-06-13 15:03:43', null, '0');
INSERT INTO `sys_user` VALUES ('500027009', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试9', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:03:47', '100225847', '于其先', '2020-06-13 15:03:47', null, '0');
INSERT INTO `sys_user` VALUES ('500027010', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试10', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:03:52', '100225847', '于其先', '2020-06-13 15:03:52', null, '0');
INSERT INTO `sys_user` VALUES ('500027011', '100226334', '成都墨安科技有限公司', '126002591', '研发部', '测试11', 'af17bc3b4a86a96a0f053a7e5f7c18ba', '1000', '于其先', '/files/2019\\05\\21\\李敏_20190521085853.png', null, '18200326536', '', '858385135@qq.com', '/images/default.jpg', '0', '0', '1471', '172.18.0.1', '2020-03-05 17:09:42', '1', '100225847', '于其先', '2020-06-13 15:04:06', '100225847', '于其先', '2020-06-13 15:04:06', null, '0');

-- ----------------------------
-- Table structure for sys_user_online_info
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_online_info`;
CREATE TABLE `sys_user_online_info` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `user_name` varchar(50) NOT NULL COMMENT '用户姓名',
  `login_ip` varchar(50) NOT NULL COMMENT '登录地址',
  `login_time` datetime NOT NULL COMMENT '登录时间',
  `online_duration` int(11) NOT NULL COMMENT '在线时长',
  `identity_type` int(11) NOT NULL COMMENT '登录类型'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户登录信息表';

-- ----------------------------
-- Records of sys_user_online_info
-- ----------------------------

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` bigint(20) NOT NULL COMMENT '编号',
  `user_id` bigint(20) NOT NULL COMMENT '用户编号',
  `user_name` varchar(50) DEFAULT NULL,
  `role_id` bigint(20) NOT NULL COMMENT '角色编号',
  `role_name` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='用户角色表';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
INSERT INTO `sys_user_role` VALUES ('142870541', '100225847', '于其先', '100724', '超级用户');
INSERT INTO `sys_user_role` VALUES ('500032002', '500027002', '于其先', '100011831', '施工单位用户');
INSERT INTO `sys_user_role` VALUES ('500032003', '500027002', '于其先', '100011832', '监理单位用户');
INSERT INTO `sys_user_role` VALUES ('500032044', '500026000', '于安若', '100011831', '施工单位用户');
INSERT INTO `sys_user_role` VALUES ('500032045', '500026000', '于安若', '100011832', '监理单位用户');

-- ----------------------------
-- Function structure for atoi
-- ----------------------------
DROP FUNCTION IF EXISTS `atoi`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `atoi`(sysno varchar(50)) RETURNS int(11)
BEGIN
	DECLARE value int;
	set value = CAST(right(sysno,3) AS SIGNED)*10;
	RETURN value;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for curval
-- ----------------------------
DROP FUNCTION IF EXISTS `curval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `curval`(v_seq_name VARCHAR(50)) RETURNS int(11)
begin        
    declare value integer;         
    set value = 0;         
    select current_val into value  from sys_sequence where seq_name = v_seq_name;   
   return value;   
end
;;
DELIMITER ;

-- ----------------------------
-- Function structure for nextval
-- ----------------------------
DROP FUNCTION IF EXISTS `nextval`;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `nextval`(v_seq_name VARCHAR(50)) RETURNS int(11)
begin  
    update sys_sequence set current_val = current_val + increment_val  where seq_name = v_seq_name;  
    return curval(v_seq_name);  
end
;;
DELIMITER ;
