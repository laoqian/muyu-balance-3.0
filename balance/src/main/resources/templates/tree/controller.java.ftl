package ${package.Controller};

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??>
import ${superControllerClassPackage};
</#if>
import ${package.Service}.${table.serviceName};
import ${package.Entity}.po.${entity};
import ${package.Entity}.vo.${entity}VO;
import ${package.Entity}.query.${entity}Query;

/**
 * <p>
 * ${table.comment!} 前端控制器
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName??>/${package.ModuleName}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen}<#else>${table.entityPath}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {
</#if>
     private static final ModelMapper mapper = new ModelMapper();

     @Autowired
     I${entity}Service ${entity?uncap_first}Service;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return ${entity?uncap_first}Service.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping
     public Object create(@RequestBody ${entity}VO ${entity?uncap_first}){
         return ${entity?uncap_first}Service.create(mapper.map(${entity?uncap_first}, ${entity}.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PutMapping("/{id}")
     public Object update(@RequestBody ${entity}VO ${entity?uncap_first}){
         return ${entity?uncap_first}Service.update(mapper.map(${entity?uncap_first},${entity}.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @DeleteMapping("/{id}")
     public Object delete(@PathVariable String id){
         return ${entity?uncap_first}Service.removeById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody ${entity}Query query){
         return ${entity?uncap_first}Service.findPage(query);
     }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-tree")
    public Object findTree(@RequestBody ${entity}Query query){
        return ${entity?uncap_first}Service.findTree(query);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-list")
    public Object findList(@RequestBody ${entity}Query query){
        return ${entity?uncap_first}Service.findList(query);
    }
}
</#if>
