package ${package.ServiceImpl};

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import ${package.Entity}.po.${entity};
import ${package.Entity}.vo.${entity}VO;
import ${package.Entity}.query.${entity}Query;
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${superServiceImplClassPackage};
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.util.List;
import muyu.common.bean.PageBean;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {
    final private ModelMapper mapper = new ModelMapper();

    private Wrapper<${entity}> createWrapper(${entity}Query query){
        return new QueryWrapper<${entity}>()
        .lambda();
    }

    @Override
    public PageBean<${entity}VO> findPage(${entity}Query query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<${entity}VO> findList(${entity}Query query) {
        return mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<${entity}VO>>(){}.getType());
    }

    @Override
    public PageBean<${entity}VO> findTree(${entity}Query query) {
       return new PageBean<>(this.findList(query));
    }

    @Override
    public ${entity}VO create(${entity} ${entity?uncap_first}) {
        super.save(${entity?uncap_first});
        return mapper.map(${entity?uncap_first}, ${entity}VO.class);
    }

    @Override
    public ${entity}VO update(${entity} ${entity?uncap_first}) {
        super.updateById(${entity?uncap_first});
        return mapper.map(${entity?uncap_first},${entity}VO.class);
    }
}
</#if>
