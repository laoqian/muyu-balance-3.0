package ${package.Service};

import ${superServiceClassPackage};
import ${package.Entity}.po.${entity};
import ${package.Entity}.vo.${entity}VO;
import ${package.Entity}.query.${entity}Query;
import java.util.List;
import muyu.common.bean.PageBean;
/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {

    PageBean<${entity}VO> findPage(${entity}Query query);

    List<${entity}VO> findList(${entity}Query query);

    PageBean<${entity}VO> findTree(${entity}Query query);

    ${entity}VO create(${entity} ${entity?uncap_first});

    ${entity}VO  update(${entity} ${entity?uncap_first});
}
</#if>
