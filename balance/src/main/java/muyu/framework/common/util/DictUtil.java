/*
 * Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author  于其先
 * @version 1.0.0
 * @since 2020-07-01 23:09:25
 */
package muyu.framework.common.util;

import muyu.common.util.ContextUtil;
import muyu.framework.system.model.po.SysDict;
import muyu.framework.system.model.query.SysDictQuery;
import muyu.framework.system.service.ISysDictService;
import org.apache.commons.lang3.StringUtils;

import java.util.Collections;
import java.util.List;

/**
 * 字典工具
 *
 * @author 于其先
 * @since 2020-07-01 23:09:25
 */
public class DictUtil {
    private static List<SysDict> list = Collections.emptyList();

    public static String getByLabel(String type,String name){
        return list.stream()
                .filter(t-> StringUtils.equals(t.getType(),type) && StringUtils.equals(t.getName(),name))
                .findFirst()
                .map(SysDict::getValue)
                .orElse(null);
    }

    public static  boolean equal(Object value,String type,String name){
        ISysDictService dictService = ContextUtil.getBean(ISysDictService.class);
        List<SysDict> list  = dictService.findList(SysDictQuery.builder().type(type).name(name).build());

        return list.stream()
                .filter(t-> StringUtils.equals(t.getType(),type) )
                .filter(t->StringUtils.equals(t.getName(),name))
                .anyMatch(t->StringUtils.equals(t.getValue(),value.toString()));
    }
}
