package muyu.framework.common.servlet;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import muyu.framework.common.config.RestProperty;
import muyu.framework.system.model.po.SysRequestLog;
import muyu.framework.system.service.ISysRequestLogService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.ReadListener;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;

@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class LoggableDispatcherServlet extends DispatcherServlet {

    private static final ObjectMapper mapper = new ObjectMapper();
    private static final int CONTENT_MAX_LEN = 4096;

    @Autowired
    RestProperty restProperty;

    @Autowired
    ISysRequestLogService requestLogService;

    @Autowired
    ExecutorService executorService;

    public static byte[] inputStream2byte(InputStream inputStream) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buff = new byte[100];
        int rc = 0;
        while ((rc = inputStream.read(buff, 0, 100)) > 0) {
            byteArrayOutputStream.write(buff, 0, rc);
        }
        return byteArrayOutputStream.toByteArray();
    }

    @Override
    protected void doDispatch(HttpServletRequest request, HttpServletResponse response) throws Exception {
        ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);
        BufferedServletRequestWrapper bufferedServletRequestWrapper = new BufferedServletRequestWrapper(request);
        ServletInputStream inputStream = bufferedServletRequestWrapper.getInputStream();
        setThrowExceptionIfNoHandlerFound(true);

        SysRequestLog log = SysRequestLog
                .builder()
                .url(request.getRequestURI().substring(request.getServletPath().length()))
                .method(request.getMethod())
                .remoteAddr(request.getRemoteAddr())
                .xForwardedFor(request.getHeader("x-forwarded-for"))
                .paramters(mapper.valueToTree(request.getParameterMap()).toString())
                .build();

        byte[] contentAsByteArray = inputStream2byte(inputStream);

         if (isJsonPost(request)) {
            log.setBody(mapper.readTree(contentAsByteArray).toString());
        } else if (isTextPost(request) || isXmlPost(request)) {
             log.setBody(new String(contentAsByteArray));
        } else if (isMediaPost(request)) {
             log.setBody("Media Request Body ContentLength = " + request.getContentLengthLong());
        } else  if(request.getContentLengthLong()>0){
             log.setBody("Unknown Request Body ContentLength = " + request.getContentLengthLong() + " body = " + (request.getContentLengthLong() > CONTENT_MAX_LEN ? "content is too long ,"+request.getContentLengthLong() : new String(contentAsByteArray)));
        }

        try {
            super.doDispatch(bufferedServletRequestWrapper, responseWrapper);
        } finally {
            try {
                JsonNode resultNode = mapper.readTree(responseWrapper.getContentAsByteArray());

                log.setResponseCode(Integer.valueOf(resultNode.get("code").toString()));
                log.setResponseMsg(resultNode.get("msg").toString());

                String data = resultNode.get("data").toString();
                log.setResponseData(StringUtils.isNotBlank(data) && data.length()<CONTENT_MAX_LEN?data:"content is too long,"+data.length());
            }catch (Exception ignored){

            }finally {
                responseWrapper.copyBodyToResponse();
            }

            if(restProperty.getRecordLog()){
                executorService.execute(()->requestLogService.save(log));
            }
        }
    }

    private Map<String, Object> getRequestHeaders(HttpServletRequest request) {
        Map<String, Object> headers = new HashMap<>();
        Enumeration<String> headerNames = request.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String headerName = headerNames.nextElement();
            headers.put(headerName, request.getHeader(headerName));
        }
        return headers;

    }

    private Map<String, Object> getResponseHeaders(HttpServletResponse response) {
        Map<String, Object> headers = new HashMap<>();
        Collection<String> headerNames = response.getHeaderNames();
        for (String headerName : headerNames) {
            headers.put(headerName, response.getHeader(headerName));
        }
        return headers;
    }

    private boolean isFormPost(HttpServletRequest request) {
        String contentType = request.getContentType();
        return (contentType != null && contentType.contains("x-www-form"));
    }

    private boolean isMediaPost(HttpServletRequest request) {
        String contentType = request.getContentType();
        if (contentType != null)
            return contentType.contains("stream") || contentType.contains("image") || contentType.contains("video") || contentType.contains("audio");
        return false;
    }

    private boolean isTextPost(HttpServletRequest request) {
        String contentType = request.getContentType();
        if (contentType != null)
            return contentType.contains("text/plain");
        return false;
    }

    private boolean isJsonPost(HttpServletRequest request) {
        String contentType = request.getContentType();
        if (contentType != null)
            return contentType.contains("application/json");
        return false;
    }

    private boolean isXmlPost(HttpServletRequest request) {
        String contentType = request.getContentType();
        if (contentType != null)
            return contentType.contains("application/xml");
        return false;
    }

    private boolean isProtoBufPost(HttpServletRequest request) {
        String contentType = request.getContentType();
        if (contentType != null)
            return contentType.contains("application") && contentType.contains("protobuf");
        return false;
    }

    private boolean isProtoBufPost(HttpServletResponse response) {
        String contentType = response.getContentType();
        if (contentType != null)
            return contentType.contains("application") && contentType.contains("protobuf");
        return false;
    }

    static class BufferedServletInputStream extends ServletInputStream {
        private ByteArrayInputStream inputStream;
        private ServletInputStream is;

        public BufferedServletInputStream(byte[] buffer, ServletInputStream is) {
            this.is = is;
            this.inputStream = new ByteArrayInputStream(buffer);
        }

        @Override
        public int available() {
            return inputStream.available();
        }

        @Override
        public int read() {
            return inputStream.read();
        }

        @Override
        public int read(@NotNull byte[] b, int off, int len) {
            return inputStream.read(b, off, len);
        }

        @Override
        public boolean isFinished() {
            return is.isFinished();
        }

        @Override
        public boolean isReady() {
            return is.isReady();
        }

        @Override
        public void setReadListener(ReadListener listener) {
            is.setReadListener(listener);
        }
    }

    static class BufferedServletRequestWrapper extends HttpServletRequestWrapper {
        private byte[] buffer;
        private ServletInputStream is;

        public BufferedServletRequestWrapper(HttpServletRequest request) throws IOException {
            super(request);
            this.is = request.getInputStream();
            this.buffer = inputStream2byte(this.is);
        }

        @Override
        public ServletInputStream getInputStream() {
            return new BufferedServletInputStream(this.buffer, this.is);
        }
    }
}

