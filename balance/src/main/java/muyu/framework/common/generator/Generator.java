package muyu.framework.common.generator;


import com.baomidou.mybatisplus.core.exceptions.MybatisPlusException;
import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.google.common.collect.Lists;
import muyu.common.bean.BaseEntity;
import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Scanner;

public class Generator {
    /**
     * <p>
     * 读取控制台内容
     * </p>
     */
    public static String scanner(String tip) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("请输入" + tip + "：");
        if (scanner.hasNext()) {
            String ipt = scanner.next();
            if (StringUtils.isNotEmpty(ipt)) {
                return ipt;
            }
        }
        throw new MybatisPlusException("请输入正确的" + tip + "！");
    }

    private static final String MODULE_NAME     = "repository";
    private static final String TABLE_NAMES     = "pm_re_contract_divide";
    private static final String TABLE_PREFIX    = "pm_re";
    private static final Boolean IS_TREE        = false;

    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + "/balance/src/main/java");
        gc.setAuthor("于其先");
        gc.setOpen(false);
        // gc.setSwagger2(true); 实体属性 Swagger2 注解
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl("jdbc:mysql://localhost:21003/muyu_balance?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=Asia/Shanghai");
        // dsc.setSchemaName("public");
        dsc.setDriverName("com.mysql.cj.jdbc.Driver");
        dsc.setUsername("root");
        dsc.setPassword("fly554433");
        mpg.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setModuleName(MODULE_NAME);
        pc.setParent("muyu.framework");
        pc.setEntity("model");
        mpg.setPackageInfo(pc);

        // 自定义配置
        InjectionConfig cfg = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };

        String basePath = projectPath + "/balance/src/main/java/" + StringUtils.replace(pc.getParent(),".","/");
        String tplPrefix = IS_TREE ? "/templates/tree/" : "/templates/list/";

        // 自定义输出配置
        // 自定义配置会被优先输出
        List<FileOutConfig> focList = Lists.newArrayList(
                new FileOutConfig(tplPrefix+"entity.java.ftl") {
                    @Override
                    public String outputFile(TableInfo tableInfo) {
                        return basePath + "/model/po/" + tableInfo.getEntityName() +  StringPool.DOT_JAVA;
                    }
                }
                , new FileOutConfig(tplPrefix+"entityVO.java.ftl") {
                    @Override
                    public String outputFile(TableInfo tableInfo) {
                        return basePath + "/model/vo/" + tableInfo.getEntityName() + "VO" + StringPool.DOT_JAVA;
                    }
                },
                new FileOutConfig(tplPrefix+"query.java.ftl") {
                    @Override
                    public String outputFile(TableInfo tableInfo) {
                        return basePath + "/model/query/" + tableInfo.getEntityName() + "Query" + StringPool.DOT_JAVA;
                    }
                }
                ,new FileOutConfig(tplPrefix+"mapper.xml.ftl") {
                    @Override
                    public String outputFile(TableInfo tableInfo) {
                        return projectPath + "/balance/src/main/resources/mapper/" + pc.getModuleName()
                                + "/" + tableInfo.getEntityName() + "Mapper" + StringPool.DOT_XML;
                    }
                }
                );

        cfg.setFileOutConfigList(focList);
        mpg.setCfg(cfg);

        // 配置模板
        TemplateConfig templateConfig = new TemplateConfig();

        templateConfig.setEntity(null)
                .setController(String.format("%s/controller.java", tplPrefix))
                .setService(String.format("%s/service.java", tplPrefix))
                .setServiceImpl(String.format("%s/serviceImpl.java", tplPrefix))
                .setMapper(String.format("%s/mapper.java", tplPrefix))
                .setXml(null);

        mpg.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);

        strategy.setEntityLombokModel(true);
        strategy.setRestControllerStyle(true);

        // 公共父类
//        strategy.setSuperControllerClass("你自己的父类控制器,没有就不用设置!");
        // 写于父类中的公共字段
        strategy.setSuperEntityColumns("id");
        strategy.setSuperEntityClass(BaseEntity.class);

        strategy.setInclude(StringUtils.split(TABLE_NAMES));
        strategy.setControllerMappingHyphenStyle(true);
        strategy.setTablePrefix(TABLE_PREFIX);

        mpg.setStrategy(strategy);
        mpg.setTemplateEngine(new FreemarkerTemplateEngine());
        mpg.execute();
    }

}
