package muyu.framework.common.config.mybatis;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import muyu.common.util.ContextUtil;
import muyu.common.util.UserUtil;
import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.service.ISysUserService;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class PoMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        LocalDateTime now = LocalDateTime.now();

        this.fillStrategy(metaObject,"createDate", now);
        this.fillStrategy(metaObject,"updateDate", now);

        User user = UserUtil.getUser();
        if(user!=null){
            ISysUserService userService = ContextUtil.getBean(ISysUserService.class);
            SysUser sysUser = userService.getByLoginName(UserUtil.getUser().getUsername());
            this.fillStrategy(metaObject,"createBy", sysUser.getId());
            this.fillStrategy(metaObject,"createName", sysUser.getName());

            this.fillStrategy(metaObject,"updateBy", sysUser.getId());
            this.fillStrategy(metaObject,"updateName", sysUser.getName());
        }
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.fillStrategy(metaObject,"updateDate", LocalDateTime.now());

        User user = UserUtil.getUser();
        if(user!=null){
            ISysUserService userService = ContextUtil.getBean(ISysUserService.class);
            SysUser sysUser = userService.getByLoginName(UserUtil.getUser().getUsername());

            this.fillStrategy(metaObject,"updateBy", sysUser.getId());
            this.fillStrategy(metaObject,"updateName", sysUser.getName());
        }
    }
}
