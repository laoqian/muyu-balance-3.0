package muyu.framework.common.config.security;

import com.yishuifengxiao.common.security.filter.TokenEndpointAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class BasicTokenEndpointAuthenticationFilter extends TokenEndpointAuthenticationFilter {
    public BasicTokenEndpointAuthenticationFilter(){
        super();
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws ServletException, IOException {
        super.doFilterInternal(request, response, chain);
    }
}
