package muyu.framework.common.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "muyu",ignoreInvalidFields = true)
@Getter
@Setter
public class RestProperty {

    /**
     * 超级管理员角色英文名
     */
    private String superAdminEnName = "SUPER_ADMIN";

    /**
     * 超级用户登录名
     */
    private String superAdminLoginName = "于其先";

    /**
     * 是否记录请求日志
     */
    private Boolean recordLog = false;

    /**
     * 使用云存储，如果不使用则使用本都存储
     */
    private Boolean useCloudStorage = true;

    /**
     * 本地存储地址
     */
    private String localStoragePath ="./storage";
}
