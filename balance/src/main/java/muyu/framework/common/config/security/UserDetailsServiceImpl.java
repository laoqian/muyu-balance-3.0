package muyu.framework.common.config.security;

import lombok.SneakyThrows;
import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    ISysUserService userService;

    @SneakyThrows
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        SysUser user = userService.getByLoginName(username);
        List<String> authorities = userService.getAuthorities(user.getLoginName());

        return new User(username, user.getPassword(), true, true, true, true,
                AuthorityUtils.commaSeparatedStringToAuthorityList(StringUtils.join(authorities,",")));
    }
}
