package muyu.framework.common.config.mybatis;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import muyu.common.util.ContextUtil;
import muyu.framework.system.service.ISysSequenceService;
import org.springframework.stereotype.Component;

@Component
public class CustomIdGenerator implements IdentifierGenerator {

    @Override
    public Long nextId(Object entity) {
        return Long.valueOf(ContextUtil.getBean(ISysSequenceService.class).getNextId());
    }
}
