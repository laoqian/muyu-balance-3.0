package muyu.framework.common.config;

import com.yishuifengxiao.common.security.filter.TokenEndpointAuthenticationFilter;
import lombok.Data;
import lombok.NoArgsConstructor;
import muyu.framework.common.config.security.BasicTokenEndpointAuthenticationFilter;
import muyu.framework.common.config.security.Md5PasswordEncoder;
import muyu.framework.common.config.security.UserDetailsServiceImpl;
import muyu.framework.common.servlet.LoggableDispatcherServlet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskDecorator;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.servlet.DispatcherServlet;

import javax.annotation.Nonnull;
import javax.servlet.MultipartConfigElement;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;

@Configuration
@Data
@NoArgsConstructor
public class RestConfig {

    @Autowired
    private MultipartConfigElement multipartConfigElement;

    @Bean("passwordEncoder")
    public PasswordEncoder passwordEncoder(){
        return new Md5PasswordEncoder();
    }

    @Bean
    public UserDetailsService userDetailsService(){
        return new UserDetailsServiceImpl();
    }

    @Bean("tokenEndpointAuthenticationFilter")
    public TokenEndpointAuthenticationFilter tokenEndpointAuthenticationFilter(ApplicationContext contentx,
                                                                               ClientDetailsService clientDetailsService, PasswordEncoder passwordEncoder) {
        TokenEndpointAuthenticationFilter tokenEndpointAuthenticationFilter = new BasicTokenEndpointAuthenticationFilter();
        tokenEndpointAuthenticationFilter.setClientDetailsService(clientDetailsService);
        tokenEndpointAuthenticationFilter.setPasswordEncoder(passwordEncoder);
        tokenEndpointAuthenticationFilter.setContentx(contentx);
        return tokenEndpointAuthenticationFilter;
    }

    @Bean("executorService")
    public ExecutorService executorService(){
        int maximumPoolSize = Math.max(Runtime.getRuntime().availableProcessors() + 1, 20);
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();

        // 设置线程池核心容量
        executor.setCorePoolSize(2);
        // 设置线程池最大容量
        executor.setMaxPoolSize(maximumPoolSize);
        // 设置任务队列长度
        executor.setQueueCapacity(200);
        // 设置线程超时时间
        executor.setKeepAliveSeconds(60);
        // 设置线程名称前缀
        executor.setThreadNamePrefix("task-executor-");
        // 设置任务丢弃后的处理策略
        executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        // 设置任务的装饰
        executor.setTaskDecorator(new ContextCopyingDecorator());
        executor.initialize();

        return executor.getThreadPoolExecutor();
    }

    static class ContextCopyingDecorator implements TaskDecorator {
        @Nonnull
        @Override
        public Runnable decorate(@Nonnull Runnable runnable) {
            RequestAttributes context = RequestContextHolder.currentRequestAttributes();
            SecurityContext securityContext = SecurityContextHolder.getContext();
            return () -> {
                try {
                    RequestContextHolder.setRequestAttributes(context);
                    SecurityContextHolder.setContext(securityContext);
                    runnable.run();
                } finally {
                    SecurityContextHolder.clearContext();
                    RequestContextHolder.resetRequestAttributes();
                }
            };
        }
    }

    @Bean
    public ServletRegistrationBean<DispatcherServlet> dispatcherRegistration() {
        ServletRegistrationBean<DispatcherServlet> bean = new ServletRegistrationBean<>(dispatcherServlet());
        bean.setMultipartConfig(multipartConfigElement);
        bean.setName("LoggableDispatcherServlet");

        return bean;
    }

    @Bean(name = DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)
    public DispatcherServlet dispatcherServlet() {
        return new LoggableDispatcherServlet();
    }
}
