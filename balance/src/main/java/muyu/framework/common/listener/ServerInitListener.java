package muyu.framework.common.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

public class ServerInitListener implements ApplicationListener<ContextRefreshedEvent> {
    private static final Logger logger = LoggerFactory.getLogger(ServerInitListener.class);

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        logger.debug("集成化项目管理系统3.0-启动完毕！");
    }
}
