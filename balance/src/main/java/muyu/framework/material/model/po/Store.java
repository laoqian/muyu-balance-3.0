package muyu.framework.material.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;

import java.time.LocalDateTime;

/**
 * <p>
 * 库存主表
 * </p>
 *
 * @author 于其先
 * @since 2020-06-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("mat_store")
public class Store extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 公司编号
     */
    private String companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 机构编号
     */
    private String officeId;

    /**
     * 机构名称
     */
    private String officeName;

    /**
     * 父级编号
     */
    private String parentId;

    /**
     * 所有父级编号
     */
    private String parentIds;

    /**
     * 层级
     */
    private Integer level;

    /**
     * 类别
     */
    private Integer type;

    /**
     * 叶子
     */
    private Boolean leaf;

    /**
     * 自编号
     */
    private String no;

    /**
     * 名称
     */
    private String name;

    /**
     * 管理员
     */
    private String manager;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建人编号
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建人名称
     */
    @TableField(fill = FieldFill.INSERT)
    private String createName;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新人名称
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateName;

    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;
}
