package muyu.framework.material.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;

import java.time.LocalDateTime;

/**
 * <p>
 * 出入库明细
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class InvoiceDetailVO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 公司编号
     */
    private String companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 库房编码
     */
    private String storeId;

    /**
     * 库房名称
     */
    private String storeName;

    /**
     * 物资
     */
    private String materialId;

    /**
     * 物资名称
     */
    private String materialName;

    /**
     * 单据编号
     */
    private String invoiceId;

    /**
     * 单据名称
     */
    private String invoiceName;

    /**
     * 单位
     */
    private String unit;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
