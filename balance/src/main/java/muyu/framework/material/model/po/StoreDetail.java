package muyu.framework.material.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;

import java.time.LocalDateTime;

/**
 * <p>
 * 库存明细表
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("mat_store_detail")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class StoreDetail extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 工程
     */
    private String companyId;

    /**
     * 工程名称
     */
    private String companyName;

    /**
     * 库房编号
     */
    private String storeId;

    /**
     * 库房名称
     */
    private String storeName;

    /**
     * 物资编号
     */
    private String materialId;

    /**
     * 物资名称
     */
    private String materialName;

    /**
     * 数量
     */
    private Integer num;

    /**
     * 累计入库数量
     */
    private Integer inboundNum;

    /**
     * 累计出库数量
     */
    private Integer outboundNum;

    /**
     * 预警数量
     */
    private Integer alarmNum;

    /**
     * 创建人编号
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建人名称
     */
    @TableField(fill = FieldFill.INSERT)
    private String createName;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新人名称
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateName;

    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;

}
