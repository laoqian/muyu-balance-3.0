package muyu.framework.material.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;

import java.time.LocalDateTime;

/**
 * <p>
 * 库存变更记录表
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class StoreLogVO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 公司编号
     */
    private Long companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 物品
     */
    private Long materialId;

    private String materialName;

    /**
     * 单据号
     */
    private Long invoiceId;

    /**
     * 单据名称
     */
    private String invoiceName;

    /**
     * 库房编号
     */
    private Long storeId;

    /**
     * 库房名称
     */
    private String storeName;

    /**
     * 类别
     */
    private Integer type;

    /**
     * 变更数量
     */
    private Integer num;

    /**
     * 变更前数量
     */
    private Integer beforeNum;

    /**
     * 当前数量
     */
    private Integer currentNum;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
