package muyu.framework.material.model.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;
import muyu.common.bean.BatchBean;
import muyu.framework.material.model.po.Invoice;
import muyu.framework.material.model.query.InvoiceDetailQuery;

/**
 * <p>
 * 工程项目表
 * </p>
 *
 * @author 于其先
 * @since 2020-06-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("pm_re_project")
public class InvoiceBatchVO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Invoice invoice;

    private BatchBean<InvoiceDetailVO, InvoiceDetailQuery> batch;
}
