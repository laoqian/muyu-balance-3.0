package muyu.framework.material.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;

import java.time.LocalDateTime;

/**
 * <p>
 * 库存主表
 * </p>
 *
 * @author 于其先
 * @since 2020-06-27
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class MaterialVO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 机构编号
     */
    private String companyId;

    /**
     * 机构名称
     */
    private String companyName;

    /**
     * 类别
     */
    private Integer category;

    /**
     * 编号
     */
    private String no;

    /**
     * 名称
     */
    private String name;

    /**
     *类别
     */
    private String type;

    /**
     * 单位
     */
    private String unit;

    /**
     * 规格
     */
    private String specification;

    /**
     * 创建人编号
     */
    private String createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private String updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
