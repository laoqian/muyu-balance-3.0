package muyu.framework.material.controller;

import muyu.common.bean.BatchBean;
import muyu.framework.material.model.po.Store;
import muyu.framework.material.model.query.StoreQuery;
import muyu.framework.material.model.vo.StoreVO;
import muyu.framework.material.service.IStoreService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 库存主表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-06-27
 */
@RestController
@RequestMapping("/material/store")
public class StoreController {
     private static final ModelMapper mapper = new ModelMapper();

     @Autowired
     IStoreService storeService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return storeService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping
     public Object create(@RequestBody StoreVO store){
         return storeService.create(mapper.map(store, Store.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PutMapping("/{id}")
     public Object update(@RequestBody StoreVO store){
         return storeService.update(mapper.map(store,Store.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @DeleteMapping("/{id}")
     public Object delete(@PathVariable String id){
         return storeService.removeById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody StoreQuery query){
         return storeService.findPage(query);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody StoreQuery query){
         return storeService.findList(query);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-tree")
     public Object findTree(@RequestBody StoreQuery query){
          return storeService.findTree(query);
     }

     @PostMapping("/saveBatch")
     public Object saveBatch(@RequestBody BatchBean<StoreVO, StoreQuery> beanVO){
          return storeService.saveBatch(beanVO);
     }
}
