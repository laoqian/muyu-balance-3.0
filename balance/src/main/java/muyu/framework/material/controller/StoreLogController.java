package muyu.framework.material.controller;

import muyu.framework.material.model.query.StoreLogQuery;
import muyu.framework.material.service.IStoreLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 库存变更记录表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@RestController
@RequestMapping("/material/store-log")
public class StoreLogController {

     @Autowired
     IStoreLogService storeLogService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return storeLogService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody StoreLogQuery query){
         return storeLogService.findPage(query);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody StoreLogQuery query){
         return storeLogService.findList(query);
     }
}
