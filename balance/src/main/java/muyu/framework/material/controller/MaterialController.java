package muyu.framework.material.controller;

import muyu.framework.material.model.po.Material;
import muyu.framework.material.model.query.MaterialQuery;
import muyu.framework.material.model.vo.MaterialVO;
import muyu.framework.material.service.IMaterialService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 库存主表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-06-27
 */
@RestController
@RequestMapping("/material/material")
public class MaterialController {
     private static final ModelMapper mapper = new ModelMapper();

     @Autowired
     IMaterialService materialService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return materialService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping
     public Object create(@RequestBody MaterialVO material){
         return materialService.create(mapper.map(material, Material.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PutMapping("/{id}")
     public Object update(@RequestBody MaterialVO material){
         return materialService.update(mapper.map(material,Material.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @DeleteMapping("/{id}")
     public Object delete(@PathVariable String id){
         return materialService.removeById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody MaterialQuery query){
         return materialService.findPage(query);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-tree")
     public Object findTree(@RequestBody MaterialQuery query){
          return materialService.findTree(query);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody MaterialQuery query){
         return materialService.findList(query);
     }
}
