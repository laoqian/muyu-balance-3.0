package muyu.framework.material.controller;

import muyu.framework.material.model.query.InvoiceDetailQuery;
import muyu.framework.material.model.vo.InvoiceDetailVO;
import muyu.framework.material.service.IInvoiceDetailService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Type;
import java.util.List;

/**
 * <p>
 * 出入库明细 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@RestController
@RequestMapping("/material/invoice-detail")
public class InvoiceDetailController {
     private static final ModelMapper mapper = new ModelMapper();
     private static final Type type =  new TypeToken<List<InvoiceDetailVO>>(){}.getType();

     @Autowired
     IInvoiceDetailService invoiceDetailService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return invoiceDetailService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @DeleteMapping("/{id}")
     public Object delete(@PathVariable String id){
         return invoiceDetailService.removeById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody InvoiceDetailQuery query){
         return invoiceDetailService.findPage(query).map(type);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody InvoiceDetailQuery query){
         return mapper.map(invoiceDetailService.findList(query),type);
     }
}
