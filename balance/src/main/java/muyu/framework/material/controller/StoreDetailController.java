package muyu.framework.material.controller;

import muyu.framework.material.model.query.StoreDetailQuery;
import muyu.framework.material.service.IStoreDetailService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 库存明细表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@RestController
@RequestMapping("/material/store-detail")
public class StoreDetailController {
     private static final ModelMapper mapper = new ModelMapper();

     @Autowired
     IStoreDetailService storeDetailService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return storeDetailService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @DeleteMapping("/{id}")
     public Object delete(@PathVariable String id){
         return storeDetailService.removeById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody StoreDetailQuery query){
         return storeDetailService.findPage(query);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody StoreDetailQuery query){
         return storeDetailService.findList(query);
     }
}
