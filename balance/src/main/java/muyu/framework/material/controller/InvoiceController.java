package muyu.framework.material.controller;

import muyu.framework.material.model.query.InvoiceQuery;
import muyu.framework.material.model.vo.InvoiceBatchVO;
import muyu.framework.material.model.vo.InvoiceVO;
import muyu.framework.material.service.IInvoiceService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 出入库单据 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@RestController
@RequestMapping("/material/invoice")
public class InvoiceController {
     private static final ModelMapper mapper = new ModelMapper();

     @Autowired
     IInvoiceService invoiceService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return invoiceService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/save")
     public Object save(@RequestBody InvoiceBatchVO batchVO){
          return mapper.map(invoiceService.save(batchVO),InvoiceVO.class);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @DeleteMapping("/{id}")
     public Object delete(@PathVariable String id){
         return invoiceService.removeById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody InvoiceQuery query){
         return invoiceService.findPage(query);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody InvoiceQuery query){
         return invoiceService.findList(query);
     }
}
