package muyu.framework.material.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import muyu.common.bean.BaseService;
import muyu.common.bean.BatchBean;
import muyu.common.bean.PageBean;
import muyu.framework.material.mapper.InvoiceDetailMapper;
import muyu.framework.material.model.po.InvoiceDetail;
import muyu.framework.material.model.query.InvoiceDetailQuery;
import muyu.framework.material.model.vo.InvoiceDetailVO;
import muyu.framework.material.service.IInvoiceDetailService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 出入库明细 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@Service
public class InvoiceDetailServiceImpl extends BaseService<InvoiceDetailMapper, InvoiceDetail> implements IInvoiceDetailService {
    final private ModelMapper mapper = new ModelMapper();

    private Wrapper<InvoiceDetail> createWrapper(InvoiceDetailQuery query){
        return new QueryWrapper<InvoiceDetail>()
        .lambda().eq(StringUtils.isNotBlank(query.getInvoiceId()),InvoiceDetail::getInvoiceId,query.getInvoiceId());
    }

    @Override
    public PageBean<InvoiceDetail> findPage(InvoiceDetailQuery query) {
        if(StringUtils.isBlank(query.getInvoiceId())){
            return PageBean.empty();
        }

        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<InvoiceDetail> findList(InvoiceDetailQuery query) {
        return super.list(this.createWrapper(query));
    }

    @Override
    public PageBean<InvoiceDetail> findTree(InvoiceDetailQuery query) {
       return new PageBean<>(this.findList(query));
    }

    @Override
    public boolean saveBatch(BatchBean<InvoiceDetailVO, InvoiceDetailQuery> bean) {
        return super.saveByBatchBean(bean.map(InvoiceDetail.class));
    }
}
