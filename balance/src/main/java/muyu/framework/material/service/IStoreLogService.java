package muyu.framework.material.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.material.model.po.StoreLog;
import muyu.framework.material.model.query.StoreLogQuery;
import muyu.framework.material.model.vo.StoreLogVO;

import java.util.List;
/**
 * <p>
 * 库存变更记录表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
public interface IStoreLogService extends IService<StoreLog> {

    PageBean<StoreLogVO> findPage(StoreLogQuery query);

    List<StoreLogVO> findList(StoreLogQuery query);

    PageBean<StoreLogVO> findTree(StoreLogQuery query);

}
