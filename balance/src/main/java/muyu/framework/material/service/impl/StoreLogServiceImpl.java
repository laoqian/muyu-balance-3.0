package muyu.framework.material.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.framework.material.mapper.StoreLogMapper;
import muyu.framework.material.model.po.StoreLog;
import muyu.framework.material.model.query.StoreLogQuery;
import muyu.framework.material.model.vo.StoreLogVO;
import muyu.framework.material.service.IStoreLogService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 库存变更记录表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@Service
public class StoreLogServiceImpl extends ServiceImpl<StoreLogMapper, StoreLog> implements IStoreLogService {
    final private ModelMapper mapper = new ModelMapper();

    private Wrapper<StoreLog> createWrapper(StoreLogQuery query){
        return new QueryWrapper<StoreLog>()
        .lambda();
    }

    @Override
    public PageBean<StoreLogVO> findPage(StoreLogQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<StoreLogVO> findList(StoreLogQuery query) {
        return mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<StoreLogVO>>(){}.getType());
    }

    @Override
    public PageBean<StoreLogVO> findTree(StoreLogQuery query) {
       return new PageBean<>(this.findList(query));
    }


}
