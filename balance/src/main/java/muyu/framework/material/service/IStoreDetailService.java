package muyu.framework.material.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.material.model.po.Invoice;
import muyu.framework.material.model.po.StoreDetail;
import muyu.framework.material.model.query.StoreDetailQuery;
import muyu.framework.material.model.vo.StoreDetailVO;

import java.util.List;
/**
 * <p>
 * 库存明细表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
public interface IStoreDetailService extends IService<StoreDetail> {

    PageBean<StoreDetailVO> findPage(StoreDetailQuery query);

    List<StoreDetailVO> findList(StoreDetailQuery query);

    PageBean<StoreDetailVO> findTree(StoreDetailQuery query);

    boolean modify(Invoice invoice , StoreDetail storeDetail);

    StoreDetail getDetail(String storeId,String materialId);
}
