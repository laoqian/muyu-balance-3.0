package muyu.framework.material.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.BatchBean;
import muyu.common.bean.PageBean;
import muyu.framework.material.model.po.InvoiceDetail;
import muyu.framework.material.model.query.InvoiceDetailQuery;
import muyu.framework.material.model.vo.InvoiceDetailVO;

import java.util.List;

/**
 * <p>
 * 出入库明细 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
public interface IInvoiceDetailService extends IService<InvoiceDetail> {

    PageBean<InvoiceDetail> findPage(InvoiceDetailQuery query);

    List<InvoiceDetail> findList(InvoiceDetailQuery query);

    PageBean<InvoiceDetail> findTree(InvoiceDetailQuery query);

    boolean saveBatch(BatchBean<InvoiceDetailVO, InvoiceDetailQuery> bean);
}
