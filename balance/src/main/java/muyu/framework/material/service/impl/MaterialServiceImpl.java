package muyu.framework.material.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.common.util.UserUtil;
import muyu.framework.material.mapper.MaterialMapper;
import muyu.framework.material.model.po.Material;
import muyu.framework.material.model.query.MaterialQuery;
import muyu.framework.material.model.vo.MaterialVO;
import muyu.framework.material.service.IMaterialService;
import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 库存主表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-27
 */
@Service
public class MaterialServiceImpl extends ServiceImpl<MaterialMapper, Material> implements IMaterialService {
    final private ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysUserService userService;

    private Wrapper<Material> createWrapper(MaterialQuery query){
        String name  = Optional.ofNullable(query.getName()).map(String::trim).orElse(null);

        return new QueryWrapper<Material>()
                .lambda()
                .like(StringUtils.isNotBlank(name),Material::getName, name);
    }

    @Override
    public PageBean<MaterialVO> findPage(MaterialQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<MaterialVO> findList(MaterialQuery query) {
        return mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<MaterialVO>>(){}.getType());
    }

    @Override
    public PageBean<MaterialVO> findTree(MaterialQuery query) {
       return new PageBean<>(this.findList(query));
    }

    @Override
    public MaterialVO create(Material material) {
        SysUser user = userService.getByLoginName(UserUtil.getUser().getUsername());

        material.setCompanyId(user.getCompanyId());
        material.setCompanyName(user.getCompanyName());

        super.save(material);
        return mapper.map(material, MaterialVO.class);
    }

    @Override
    public MaterialVO update(Material material) {
        super.updateById(material);
        return mapper.map(material,MaterialVO.class);
    }
}
