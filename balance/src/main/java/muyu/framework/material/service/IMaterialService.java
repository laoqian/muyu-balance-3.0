package muyu.framework.material.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.material.model.po.Material;
import muyu.framework.material.model.query.MaterialQuery;
import muyu.framework.material.model.vo.MaterialVO;

import java.util.List;
/**
 * <p>
 * 库存主表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-27
 */
public interface IMaterialService extends IService<Material> {

    PageBean<MaterialVO> findPage(MaterialQuery query);

    List<MaterialVO> findList(MaterialQuery query);

    PageBean<MaterialVO> findTree(MaterialQuery query);

    MaterialVO create(Material material);

    MaterialVO  update(Material material);
}
