package muyu.framework.material.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.common.exception.ServiceException;
import muyu.framework.material.mapper.StoreDetailMapper;
import muyu.framework.material.model.po.Invoice;
import muyu.framework.material.model.po.StoreDetail;
import muyu.framework.material.model.po.StoreLog;
import muyu.framework.material.model.query.StoreDetailQuery;
import muyu.framework.material.model.vo.StoreDetailVO;
import muyu.framework.material.service.IStoreDetailService;
import muyu.framework.material.service.IStoreLogService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 库存明细表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@Service
public class StoreDetailServiceImpl extends ServiceImpl<StoreDetailMapper, StoreDetail> implements IStoreDetailService {
    final private ModelMapper mapper = new ModelMapper();

    @Autowired
    private IStoreLogService storeLogService;

    private Wrapper<StoreDetail> createWrapper(StoreDetailQuery query) {
        return new QueryWrapper<StoreDetail>()
                .lambda();
    }

    @Override
    public PageBean<StoreDetailVO> findPage(StoreDetailQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<StoreDetailVO> findList(StoreDetailQuery query) {
        return mapper.map(super.list(this.createWrapper(query)), new TypeToken<List<StoreDetailVO>>() {
        }.getType());
    }

    @Override
    public PageBean<StoreDetailVO> findTree(StoreDetailQuery query) {
        return new PageBean<>(this.findList(query));
    }

    @Override
    @Transactional
    public boolean modify(Invoice invoice, StoreDetail modify) {
        StoreDetail origin = this.getDetail(modify.getStoreId(), modify.getMaterialId());
        int originNum = Optional.ofNullable(origin).map(StoreDetail::getNum).orElse(0);
        int num = originNum + modify.getNum();

        if (num < 0) {
            throw new ServiceException("库存不足");
        }

        if (origin == null) {
            this.saveLog(invoice,originNum, modify);
            return super.save(modify) ;
        }

        origin.setNum(num);
        origin.setInboundNum(origin.getInboundNum() + modify.getInboundNum());
        origin.setOutboundNum(origin.getOutboundNum() + modify.getOutboundNum());

        this.saveLog(invoice,originNum, origin);

        return super.updateById(origin);
    }

    private void saveLog(Invoice invoice, int originNum, StoreDetail current) {
        StoreLog log = mapper.map(current, StoreLog.class);

        log.setType(invoice.getType());
        log.setBeforeNum(originNum);
        log.setNum(current.getNum() - originNum);
        log.setCurrentNum(current.getNum());
        log.setInvoiceId(invoice.getId());
        log.setInvoiceName(invoice.getName());

        storeLogService.save(log);
    }


    public StoreDetail getDetail(String storeId, String materialId) {
        return super.getOne(new QueryWrapper<StoreDetail>()
                .lambda()
                .eq(StoreDetail::getMaterialId, materialId)
                .eq(StoreDetail::getStoreId, storeId));
    }
}
