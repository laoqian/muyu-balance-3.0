package muyu.framework.material.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import muyu.common.bean.BaseService;
import muyu.common.bean.BatchBean;
import muyu.common.bean.PageBean;
import muyu.common.util.UserUtil;
import muyu.framework.common.util.DictUtil;
import muyu.framework.material.mapper.StoreMapper;
import muyu.framework.material.model.po.Invoice;
import muyu.framework.material.model.po.InvoiceDetail;
import muyu.framework.material.model.po.Store;
import muyu.framework.material.model.po.StoreDetail;
import muyu.framework.material.model.query.InvoiceDetailQuery;
import muyu.framework.material.model.query.StoreQuery;
import muyu.framework.material.model.vo.StoreVO;
import muyu.framework.material.service.IInvoiceDetailService;
import muyu.framework.material.service.IStoreDetailService;
import muyu.framework.material.service.IStoreService;
import muyu.framework.system.service.ISystemService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 库存主表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-27
 */
@Service
public class StoreServiceImpl extends BaseService<StoreMapper, Store> implements IStoreService {
    final private ModelMapper mapper = new ModelMapper();

    @Autowired
    ISystemService systemService;

    @Autowired
    IInvoiceDetailService invoiceDetailService;

    @Autowired
    IStoreDetailService storeDetailService;

    private Wrapper<Store> createWrapper(StoreQuery query){
        return new QueryWrapper<Store>()
        .lambda();
    }

    @Override
    public PageBean<StoreVO> findPage(StoreQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<StoreVO> findList(StoreQuery query) {
        return mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<StoreVO>>(){}.getType());
    }

    @Override
    public PageBean<StoreVO> findTree(StoreQuery query) {
        if(!UserUtil.isSuperAdmin()){
            query.setCompanyId(systemService.getCurrentCompany().getId());
        }

        List<StoreVO> list = this.findList(query);
        return PageBean.<StoreVO>builder()
                .list(StoreVO.create(list))
                .total((long) list.size())
                .build();
    }

    @Override
    public StoreVO create(Store store) {
        super.save(store);
        return mapper.map(store, StoreVO.class);
    }

    @Override
    public StoreVO update(Store store) {
        super.updateById(store);
        return mapper.map(store,StoreVO.class);
    }

    @Override
    public boolean saveBatch(BatchBean<StoreVO, StoreQuery> bean) {
        return   super.saveByBatchBean(bean.map(Store.class));
    }

    @Override
    @Transactional
    public boolean modify(Invoice invoice) {
        List<InvoiceDetail> list = invoiceDetailService.findList(InvoiceDetailQuery.builder().invoiceId(invoice.getId()).build());
        Store store = super.getById(invoice.getStoreId());

        for(InvoiceDetail detail:list){
            int num = detail.getNum();
            boolean inbound = DictUtil.equal(invoice.getType(),"invoice.type","入库");

            StoreDetail storeDetail = StoreDetail.builder()
                    .storeId(store.getId())
                    .storeName(store.getName())
                    .companyId(store.getCompanyId())
                    .companyName(store.getCompanyName())
                    .materialId(detail.getMaterialId())
                    .materialName(detail.getMaterialName())
                    .inboundNum(inbound?num:0)
                    .outboundNum(inbound?0:num)
                    .num(inbound?num:-num)
                    .build();

            storeDetailService.modify(invoice,storeDetail);
        }

        return true;
    }
}
