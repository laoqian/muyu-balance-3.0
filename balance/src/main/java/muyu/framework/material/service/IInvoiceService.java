package muyu.framework.material.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.material.model.po.Invoice;
import muyu.framework.material.model.query.InvoiceQuery;
import muyu.framework.material.model.vo.InvoiceBatchVO;
import muyu.framework.material.model.vo.InvoiceVO;

import java.util.List;
/**
 * <p>
 * 出入库单据 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
public interface IInvoiceService extends IService<Invoice> {

    PageBean<InvoiceVO> findPage(InvoiceQuery query);

    List<InvoiceVO> findList(InvoiceQuery query);

    PageBean<InvoiceVO> findTree(InvoiceQuery query);

    Invoice  save(InvoiceBatchVO batchVO);
}
