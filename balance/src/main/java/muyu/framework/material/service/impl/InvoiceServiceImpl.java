package muyu.framework.material.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.framework.material.mapper.InvoiceMapper;
import muyu.framework.material.model.po.Invoice;
import muyu.framework.material.model.query.InvoiceQuery;
import muyu.framework.material.model.vo.InvoiceBatchVO;
import muyu.framework.material.model.vo.InvoiceVO;
import muyu.framework.material.service.IInvoiceDetailService;
import muyu.framework.material.service.IInvoiceService;
import muyu.framework.material.service.IStoreService;
import muyu.framework.system.model.po.SysOffice;
import muyu.framework.system.service.ISystemService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 出入库单据 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
@Service
public class InvoiceServiceImpl extends ServiceImpl<InvoiceMapper, Invoice> implements IInvoiceService {
    final private ModelMapper mapper = new ModelMapper();

    @Autowired
    IInvoiceDetailService detailService;

    @Autowired
    ISystemService systemService;

    @Autowired
    IStoreService storeService;

    private Wrapper<Invoice> createWrapper(InvoiceQuery query){
        return new QueryWrapper<Invoice>()
        .lambda();
    }

    @Override
    public PageBean<InvoiceVO> findPage(InvoiceQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<InvoiceVO> findList(InvoiceQuery query) {
        return mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<InvoiceVO>>(){}.getType());
    }

    @Override
    public PageBean<InvoiceVO> findTree(InvoiceQuery query) {
       return new PageBean<>(this.findList(query));
    }

    @Override
    @Transactional
    public Invoice save(InvoiceBatchVO batchVO) {
        Invoice invoice = batchVO.getInvoice();
        SysOffice company = systemService.getCurrentCompany();

        if(invoice.getIsNewRecord()){
            invoice.setCompanyId(company.getId());
            invoice.setCompanyName(company.getName());
            invoice.setStatus(0);
            super.save(invoice);
        }else{
            super.updateById(invoice);
        }

        batchVO.getBatch().forEach(detail->{
                    detail.setInvoiceId(invoice.getId());
                    detail.setInvoiceName(invoice.getName());
                    detail.setStoreId(invoice.getStoreId());
                    detail.setStoreName(invoice.getStoreName());
                    detail.setCompanyId(invoice.getCompanyId());
                    detail.setCompanyName(invoice.getCompanyName());
                });

        detailService.saveBatch(batchVO.getBatch());
        storeService.modify(invoice);

        return invoice;
    }
}
