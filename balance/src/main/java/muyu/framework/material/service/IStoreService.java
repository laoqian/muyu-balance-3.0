package muyu.framework.material.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.BatchBean;
import muyu.common.bean.PageBean;
import muyu.framework.material.model.po.Invoice;
import muyu.framework.material.model.po.Store;
import muyu.framework.material.model.query.StoreQuery;
import muyu.framework.material.model.vo.StoreVO;

import java.util.List;

/**
 * <p>
 * 库存主表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-27
 */
public interface IStoreService extends IService<Store> {

    PageBean<StoreVO> findPage(StoreQuery query);

    List<StoreVO> findList(StoreQuery query);

    PageBean<StoreVO> findTree(StoreQuery query);

    StoreVO create(Store store);

    StoreVO  update(Store store);

    boolean saveBatch(BatchBean<StoreVO, StoreQuery> beanVO);

    boolean modify(Invoice invoice);
}
