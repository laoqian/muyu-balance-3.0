package muyu.framework.material.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.material.model.po.Store;

/**
 * <p>
 * 库存主表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-06-27
 */
public interface StoreMapper extends BaseMapper<Store> {

}
