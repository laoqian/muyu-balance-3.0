package muyu.framework.material.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.material.model.po.StoreLog;

/**
 * <p>
 * 库存变更记录表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
public interface StoreLogMapper extends BaseMapper<StoreLog> {

}
