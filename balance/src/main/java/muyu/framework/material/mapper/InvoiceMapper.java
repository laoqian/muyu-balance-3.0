package muyu.framework.material.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.material.model.po.Invoice;

/**
 * <p>
 * 出入库单据 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
public interface InvoiceMapper extends BaseMapper<Invoice> {

}
