package muyu.framework.material.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.material.model.po.StoreDetail;

/**
 * <p>
 * 库存明细表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
public interface StoreDetailMapper extends BaseMapper<StoreDetail> {

}
