package muyu.framework.material.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.material.model.po.InvoiceDetail;

/**
 * <p>
 * 出入库明细 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-06-28
 */
public interface InvoiceDetailMapper extends BaseMapper<InvoiceDetail> {

}
