package muyu.framework.system.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;

import java.io.Serializable;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SysUserRole implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 用户编号
     */
    private String userId;

    /**
     * 用户名称
     */

    private String userName;

    /**
     * 角色编号
     */
    private String roleId;

    /**
     * 角色名称
     */

    private String roleName;


}
