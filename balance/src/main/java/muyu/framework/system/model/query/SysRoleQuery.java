package muyu.framework.system.model.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.BaseQuery;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysRoleQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 公司编号
     */
    private Integer companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 名称
     */
    private String name;

    /**
     * 英文名
     */
    private String enName;

    /**
     * 创建人编号
     */
    private Integer createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Integer updateBy;

    /**
     * 更新人名字
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Integer delFlag;


}
