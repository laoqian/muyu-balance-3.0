package muyu.framework.system.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.TreeEntity;

import java.time.LocalDateTime;

/**
 * <p>
 * 机构表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysOffice extends TreeEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 区域
     */
    private Long areaId;

    /**
     * 名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 级别
     */
    private Integer grade;

    /**
     * 地址
     */
    private String address;

    /**
     * 电话
     */
    private String phone;

    /**
     * 是否业主
     */
    private Boolean proprietor;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 创建人编号
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer createBy;

    /**
     * 创建人名称
     */
    @TableField(fill = FieldFill.INSERT)
    private String createName;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer updateBy;

    /**
     * 更新人名称
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateName;

    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
