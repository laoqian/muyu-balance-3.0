package muyu.framework.system.model.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.BaseQuery;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysMenuQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 父编号
     */
    private Integer parentId;

    /**
     * 所有父级编号
     */
    private String parentIds;

    /**
     * 是否叶子
     */
    private Boolean leaf;

    /**
     * 层级
     */
    private Integer level;

    /**
     * 名称
     */
    private String name;

    /**
     * 地址
     */
    private String href;

    /**
     * 图标
     */
    private String icon;

    /**
     * 权限
     */
    private String permission;

    /**
     * 状态
     */
    private Boolean status;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 创建人编号
     */
    private Integer createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Integer updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Integer delFlag;


}
