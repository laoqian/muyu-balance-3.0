package muyu.framework.system.model.enums;


import lombok.Getter;

@Getter
public enum BeanBeanActionEnum {

    CREATE("create"),
    MODIFY("modify"),
    REMOVE("remove");

    String symbol;

    BeanBeanActionEnum(String symbol){
        this.symbol = symbol;
    }
}
