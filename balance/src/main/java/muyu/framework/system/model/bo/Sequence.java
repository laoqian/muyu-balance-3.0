package muyu.framework.system.model.bo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Sequence {

    /**
     * 名称
     */
    private String  name;

    /**
     * 缓存大小
     */
    @Builder.Default
    private Integer cacheSize = 1000;

    /**
     *当前
     */
    @Builder.Default
    private long    currentValue = 0;

    /**
     * 最大值
     */
    @Builder.Default
    private long    maxValue  = 0;

    public String getName() {
        return name;
    }

    public Integer getCacheSize() {
        return cacheSize;
    }

    public boolean isValid(){
        return this.currentValue < this.maxValue;
    }

    public Long nextValue (){
        return this.currentValue++;
    }

    public void init(long currentValue){
        this.currentValue   = currentValue;
        this.maxValue       = currentValue + this.cacheSize;
    }
}
