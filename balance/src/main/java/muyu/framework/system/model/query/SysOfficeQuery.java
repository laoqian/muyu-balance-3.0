package muyu.framework.system.model.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.BaseQuery;

import java.time.LocalDateTime;

/**
 * <p>
 * 机构表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysOfficeQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 父编号
     */
    private String parentId;

    /**
     * 父级编号集合
     */
    private String parentIds;

    /**
     * 层级
     */
    private Integer level;

    /**
     * 叶子
     */
    private Boolean leaf;

    /**
     * 区域
     */
    private Long areaId;

    /**
     * 名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 级别
     */
    private Integer grade;

    /**
     * 地址
     */
    private String address;

    /**
     * 电话
     */
    private String phone;

    /**
     * 是否业主
     */
    private Boolean proprietor;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
