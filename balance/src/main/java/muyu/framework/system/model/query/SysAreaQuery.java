package muyu.framework.system.model.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.BaseQuery;

import java.time.LocalDateTime;

/**
 * <p>
 *
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysAreaQuery  extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 父编号
     */
    private Integer parentId;

    /**
     * 所有父级编号
     */
    private String parentIds;

    /**
     * 层级
     */
    private Integer level;

    /**
     * 是否叶子
     */
    private Boolean leaf;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 编码
     */
    private String code;

    /**
     * 创建者
     */
    private Integer createBy;

    /**
     * 创建者姓名
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Integer updateBy;

    /**
     * 更新者姓名
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Integer delFlag;


}
