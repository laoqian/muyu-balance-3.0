package muyu.framework.system.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysRoleVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 公司编号
     */
    private Integer companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 名称
     */
    private String name;

    /**
     * 英文名
     */
    private String enName;


}
