package muyu.framework.system.model.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;


@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class TransformBean<Q> {
    String action;

    Q query;

    List<String> ids;
}



