package muyu.framework.system.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.framework.system.model.po.SysMenu;
import muyu.framework.system.model.po.SysRole;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysRoleMenuGrantVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 角色编号
     */
    private SysRole role;

    /**
     * 菜单编号
     */
    private List<SysMenu> menuList;

}
