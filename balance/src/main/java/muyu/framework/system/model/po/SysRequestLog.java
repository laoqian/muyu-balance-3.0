package muyu.framework.system.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * http请求日志
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SysRequestLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 远程地址
     */
    private String remoteAddr;

    /**
     * x-forwarded-for
     */
    private String xForwardedFor;

    /**
     * 请求类型
     */
    private String method;

    /**
     * URL
     */
    private String url;

    /**
     * url参数
     */
    private String paramters;

    /**
     * 请求body
     */
    private String body;

    /**
     * 回应code
     */
    private Integer responseCode;

    /**
     * 回应信息
     */
    private String responseMsg;

    /**
     * 返回数据
     */
    private String responseData;

    /**
     * 创建人编号
     */
    @TableField(fill = FieldFill.INSERT)
    private Long createBy;

    /**
     * 创建人名称
     */
    @TableField(fill = FieldFill.INSERT)
    private String createName;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateBy;

    /**
     * 更新人名称
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateName;

    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
