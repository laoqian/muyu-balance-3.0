package muyu.framework.system.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.TreeEntity;


/**
 * <p>
 * 机构表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysOfficeVO  extends TreeEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 叶子
     */
    private Boolean leaf;

    /**
     * 区域
     */
    private Long areaId;

    /**
     * 名称
     */
    private String name;

    /**
     * 编码
     */
    private String code;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 级别
     */
    private Integer grade;

    /**
     * 地址
     */
    private String address;

    /**
     * 电话
     */
    private String phone;

    /**
     * 是否业主
     */
    private Boolean proprietor;

    /**
     * 排序号
     */
    private Integer sort;




}
