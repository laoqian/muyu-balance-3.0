package muyu.framework.system.model.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.BaseQuery;

/**
 * <p>
 * 国际化表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysLocaleQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;


    /**
     * 语言
     */
    private String lang;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 键值
     */
    private String value;


}
