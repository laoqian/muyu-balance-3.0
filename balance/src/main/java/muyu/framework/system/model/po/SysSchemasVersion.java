package muyu.framework.system.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 更新记录表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SysSchemasVersion implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 序号
     */
    private Integer installedRank;

    /**
     * 版本号
     */
    private String version;

    /**
     * 描述
     */
    private String description;

    /**
     * 类别
     */
    private String type;

    /**
     * 脚本
     */
    private String script;

    /**
     * 校验和
     */
    private Integer checksum;

    /**
     * 执行人
     */
    private String installedBy;

    /**
     * 执行事件
     */
    private LocalDateTime installedOn;

    /**
     * 执行耗时
     */
    private Integer executionTime;

    /**
     * 成功标志
     */
    private Boolean success;


}
