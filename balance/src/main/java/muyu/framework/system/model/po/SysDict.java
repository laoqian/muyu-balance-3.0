package muyu.framework.system.model.po;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class SysDict implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 键值
     */
    private String value;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private String type;

    /**
     * 编码
     */
    private String code;

    /**
     * 扩展属性
     */
    private String external1;

    /**
     * 扩展属性1描述
     */
    private String external1Desc;

    /**
     * 扩展属性
     */
    private String external2;

    /**
     * 扩展属性2描述
     */
    private String external2Desc;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 排序号
     */
    private Integer sort;


}
