package muyu.framework.system.model.query;

import lombok.*;
import muyu.common.bean.BaseQuery;

import java.time.LocalDateTime;

/**
 * <p>
 * 用户登录信息表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SysUserOnlineInfoQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;
    /**
     * 用户编号
     */
    private Long userId;

    /**
     * 用户姓名
     */
    private String userName;

    /**
     * 登录地址
     */
    private String loginIp;

    /**
     * 登录时间
     */
    private LocalDateTime loginTime;

    /**
     * 在线时长
     */
    private Integer onlineDuration;

    /**
     * 登录类型
     */
    private Integer identityType;


}
