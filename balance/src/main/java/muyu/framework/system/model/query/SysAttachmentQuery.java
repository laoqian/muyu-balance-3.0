package muyu.framework.system.model.query;

import lombok.*;
import muyu.common.bean.BaseQuery;

import java.time.LocalDateTime;

/**
 * <p>
 * 系统附件表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SysAttachmentQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 原始名称
     */
    private String originName;

    /**
     * 后缀名
     */
    private String suffix;

    /**
     * 大小
     */
    private Long size;

    /**
     * 保存路径
     */
    private String path;

    /**
     * 存储位置
     */
    private Integer location;

    /**
     * 业务表编号
     */
    private Long businessId;

    /**
     * 扩展编号
     */
    private Long externalId;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
