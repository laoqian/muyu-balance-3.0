package muyu.framework.system.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.framework.system.model.po.SysRole;
import muyu.framework.system.model.po.SysUser;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysUserRoleGrantVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户编号
     */
    private SysUser user;


    /**
     * 角色编号
     */
    private List<SysRole> roleList;

}
