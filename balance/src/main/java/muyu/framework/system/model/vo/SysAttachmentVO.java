package muyu.framework.system.model.vo;

import lombok.*;

import java.io.Serializable;

/**
 * <p>
 * 系统附件表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SysAttachmentVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * 原始名称
     */
    private String originName;

    /**
     * 后缀名
     */
    private String suffix;

    /**
     * 大小
     */
    private Long size;

    /**
     * 保存路径
     */
    private String path;

    /**
     * 存储位置
     */
    private Integer location;

    /**
     * 业务表编号
     */
    private Long businessId;

    /**
     * 扩展编号
     */
    private Long externalId;




}
