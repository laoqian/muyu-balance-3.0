package muyu.framework.system.model.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.BaseQuery;

import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysUserQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;
    /**
     * 公司
     */
    private Integer companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 部门
     */
    private Integer officeId;

    /**
     * 部门名称
     */
    private String officeName;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 密码
     */
    private String password;

    /**
     * 编号
     */
    private String no;

    /**
     * 名字
     */
    private String name;

    /**
     * 签字链接
     */
    private String signature;

    /**
     * 地址
     */
    private String adress;

    /**
     * 电话
     */
    private String phone;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 照片
     */
    private String photo;

    /**
     * 类型
     */
    private Boolean type;

    /**
     * 状态
     */
    private Boolean status;

    /**
     * 登陆计数
     */
    private Integer loginCount;

    /**
     * 登陆IP
     */
    private String loginIp;

    /**
     * 登陆日期
     */
    private LocalDateTime loginDate;

    /**
     * 登陆标志
     */
    private Integer loginFlag;

    /**
     * 创建者
     */
    private Integer createBy;

    /**
     * 创建者姓名
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新者
     */
    private Integer updateBy;

    /**
     * 更新者姓名
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Integer delFlag;
}
