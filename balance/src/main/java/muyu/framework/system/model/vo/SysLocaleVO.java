package muyu.framework.system.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * <p>
 * 国际化表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-03
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysLocaleVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 语言
     */
    private String lang;

    /**
     * 编码
     */
    private String code;

    /**
     * 名称
     */
    private String name;

    /**
     * 键值
     */
    private String value;


}
