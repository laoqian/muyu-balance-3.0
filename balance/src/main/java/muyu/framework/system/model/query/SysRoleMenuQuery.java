package muyu.framework.system.model.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.BaseQuery;

/**
 * <p>
 *
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysRoleMenuQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 角色编号
     */
    private Integer roleId;

    /**
     * 角色名称
     */
    private String roleName;

    /**
     * 菜单编号
     */
    private Integer menuId;

    /**
     * 菜单名称
     */
    private String menuName;


}
