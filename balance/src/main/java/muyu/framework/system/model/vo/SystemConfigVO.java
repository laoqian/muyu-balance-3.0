package muyu.framework.system.model.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
public class SystemConfigVO {
    List<SysMenuVO> menuList;
    List<SysDictVO> dictList;
    List<String> authorities;
}
