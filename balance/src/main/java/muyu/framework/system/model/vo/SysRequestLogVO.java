package muyu.framework.system.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * http请求日志
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysRequestLogVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;
    /**
     * 远程地址
     */
    private String remoteAddr;

    /**
     * x-forwarded-for
     */
    private String xForwardedFor;

    /**
     * 请求类型
     */
    private String method;

    /**
     * URL
     */
    private String url;

    /**
     * url参数
     */
    private String paramters;

    /**
     * 请求body
     */
    private String body;

    /**
     * 回应code
     */
    private Integer responseCode;

    /**
     * 回应信息
     */
    private String responseMsg;

    /**
     * 返回数据
     */
    private String responseData;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
