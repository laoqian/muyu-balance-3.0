package muyu.framework.system.model.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.BaseQuery;

/**
 * <p>
 * 用户角色表
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysUserRoleQuery  extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private String id;

    /**
     * 用户编号
     */
    private Integer userId;

    /**
     * 用户名称
     */

    private String userName;

    /**
     * 角色编号
     */
    private Integer roleId;

    /**
     * 角色名称
     */

    private String roleName;

}
