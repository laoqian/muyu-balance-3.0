package muyu.framework.system.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.TreeEntity;

/**
 * <p>
 *
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysMenuVO extends TreeEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 国际化编码
     */
    private String code;

    /**
     * 地址
     */
    private String href;

    /**
     * 图标
     */
    private String icon;

    /**
     * 权限
     */
    private String permission;

    /**
     * 状态
     */
    private Boolean status;

}
