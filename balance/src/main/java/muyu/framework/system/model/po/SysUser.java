package muyu.framework.system.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户表
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SysUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 公司
     */
    private String companyId;

    /**
     * 公司名称
     */
    private String companyName;

    /**
     * 部门
     */
    private String officeId;

    /**
     * 部门名称
     */
    private String officeName;

    /**
     * 登录名
     */
    private String loginName;

    /**
     * 密码
     */
    private String password;

    /**
     * 编号
     */
    private String no;

    /**
     * 名字
     */
    private String name;

    /**
     * 签字链接
     */
    private String signature;

    /**
     * 地址
     */
    private String address;

    /**
     * 电话
     */
    private String phone;

    /**
     * 手机
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 照片
     */
    private String photo;

    /**
     * 类型
     */
    private Boolean type;

    /**
     * 状态
     */
    private Boolean status;

    /**
     * 登陆计数
     */
    private Integer loginCount;

    /**
     * 登陆IP
     */
    private String loginIp;

    /**
     * 登陆日期
     */
    private LocalDateTime loginDate;

    /**
     * 登陆标志
     */
    private Integer loginFlag;

    /**
     * 创建人编号
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建人名称
     */
    @TableField(fill = FieldFill.INSERT)
    private String createName;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新人名称
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateName;

    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Integer delFlag;


}
