package muyu.framework.system.model.query;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseQuery;

/**
 * <p>
 *
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SysDictQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 键值
     */
    private String value;

    /**
     * 名称
     */
    private String name;

    /**
     * 类别
     */
    private String type;

    /**
     * 编码
     */
    private String code;

    /**
     * 扩展属性
     */
    private String external1;

    /**
     * 扩展属性1描述
     */
    private String external1Desc;

    /**
     * 扩展属性
     */
    private String external2;

    /**
     * 扩展属性2描述
     */
    private String external2Desc;

    /**
     * 描述信息
     */
    private String description;

    /**
     * 排序号
     */
    private Integer sort;


}
