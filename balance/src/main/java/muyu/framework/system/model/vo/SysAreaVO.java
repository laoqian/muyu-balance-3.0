package muyu.framework.system.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import muyu.common.bean.TreeEntity;

/**
 * <p>
 *
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class SysAreaVO extends TreeEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 编码
     */
    private String code;




}
