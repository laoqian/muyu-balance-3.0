package muyu.framework.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.system.model.po.SysUserRole;

/**
 * <p>
 * 用户角色表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

}
