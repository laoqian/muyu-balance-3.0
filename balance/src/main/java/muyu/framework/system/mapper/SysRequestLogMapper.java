package muyu.framework.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.system.model.po.SysRequestLog;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-03-22
 */
public interface SysRequestLogMapper extends BaseMapper<SysRequestLog> {

}
