package muyu.framework.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.system.model.po.SysSchemasVersion;

/**
 * <p>
 * 更新记录表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
public interface SysSchemasVersionMapper extends BaseMapper<SysSchemasVersion> {

}
