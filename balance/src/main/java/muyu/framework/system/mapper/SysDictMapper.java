package muyu.framework.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.system.model.po.SysDict;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-02-12
 */
public interface SysDictMapper extends BaseMapper<SysDict> {

}
