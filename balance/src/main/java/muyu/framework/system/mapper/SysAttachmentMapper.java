package muyu.framework.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.system.model.po.SysAttachment;

/**
 * <p>
 * 系统附件表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
public interface SysAttachmentMapper extends BaseMapper<SysAttachment> {

}
