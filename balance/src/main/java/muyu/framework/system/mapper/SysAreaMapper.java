package muyu.framework.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.system.model.po.SysArea;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
public interface SysAreaMapper extends BaseMapper<SysArea> {

}
