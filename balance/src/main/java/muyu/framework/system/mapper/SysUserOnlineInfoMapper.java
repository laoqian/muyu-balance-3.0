package muyu.framework.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.system.model.po.SysUserOnlineInfo;

/**
 * <p>
 * 用户登录信息表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
public interface SysUserOnlineInfoMapper extends BaseMapper<SysUserOnlineInfo> {

}
