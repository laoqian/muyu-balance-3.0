package muyu.framework.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.system.model.po.SysLocale;

/**
 * <p>
 * 国际化表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-05-03
 */
public interface SysLocaleMapper extends BaseMapper<SysLocale> {

}
