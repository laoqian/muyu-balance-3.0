package muyu.framework.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 错误日志表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@RestController
@RequestMapping("/sys-log")
public class SysLogController {

}
