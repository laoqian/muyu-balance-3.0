package muyu.framework.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-03-22
 */
@RestController
@RequestMapping("/sys-sequence")
public class SysSequenceController {

}
