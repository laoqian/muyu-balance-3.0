package muyu.framework.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 通知信息表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@RestController
@RequestMapping("/sys-notify")
public class SysNotifyController {

}
