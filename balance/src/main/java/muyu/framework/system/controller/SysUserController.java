package muyu.framework.system.controller;


import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.model.query.SysUserQuery;
import muyu.framework.system.model.vo.SysUserVO;
import muyu.framework.system.service.ISysUserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@RestController
@RequestMapping("/sys-user")
public class SysUserController {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysUserService userService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id){
        return userService.getById(id);
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping
    public Object create(@RequestBody SysUserVO user){
        return userService.create(mapper.map(user, SysUser.class));
    }

    @Secured({"ROLE_SUPER_ADMIN","ROLE_ADMIN"})
    @PutMapping("/{id}")
    public Object update(@RequestBody SysUserVO user){
        return userService.update(mapper.map(user,SysUser.class));
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
        return userService.removeById(id);
    }

    @PostMapping("/find-page")
    public Object findPage(@RequestBody SysUserQuery query){
        return userService.findPage(query);
    }

    @PostMapping("/find-tree")
    public Object findTree(@RequestBody SysUserQuery query){
        return userService.findTree(query);
    }
}
