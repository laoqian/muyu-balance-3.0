package muyu.framework.system.controller;


import muyu.framework.system.model.po.SysRole;
import muyu.framework.system.model.query.SysRoleQuery;
import muyu.framework.system.model.vo.SysRoleVO;
import muyu.framework.system.service.ISysRoleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@RestController
@RequestMapping("/sys-role")
public class SysRoleController {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysRoleService roleService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id){
        return roleService.getById(id);
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping
    public Object create(@RequestBody SysRoleVO role){
        return roleService.create(mapper.map(role, SysRole.class));
    }

    @Secured({"ROLE_SUPER_ADMIN","ROLE_ADMIN"})
    @PutMapping("/{id}")
    public Object update(@RequestBody SysRoleVO role){
        return roleService.update(mapper.map(role,SysRole.class));
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
        return roleService.removeById(id);
    }

    @PostMapping("/find-page")
    public Object findPage(@RequestBody SysRoleQuery query){
        return roleService.findPage(query);
    }

    @PostMapping("/find-list")
    public Object findList(@RequestBody SysRoleQuery query){
        return roleService.findList(query);
    }

    @PostMapping("/find-tree")
    public Object findTree(@RequestBody SysRoleQuery query){
        return roleService.findTree(query);
    }
}
