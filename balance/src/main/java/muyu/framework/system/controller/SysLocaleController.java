package muyu.framework.system.controller;


import io.swagger.annotations.Api;
import muyu.framework.system.model.po.SysLocale;
import muyu.framework.system.model.query.SysLocaleQuery;
import muyu.framework.system.service.ISysLocaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 国际化表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-05-03
 */
@Api(tags = "国际化配置")
@RestController
@RequestMapping("/sys-locale")
public class SysLocaleController {

    @Autowired
    ISysLocaleService localeService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id){
        return localeService.getById(id);
    }

    @PostMapping
    public Object create(@RequestBody SysLocale Locale){
        return localeService.create(Locale);
    }

    @PutMapping("/{id}")
    public Object update(@RequestBody SysLocale Locale){
        return localeService.update(Locale);
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
        return localeService.removeById(id);
    }

    @PostMapping("/find-page")
    public Object findPage(@RequestBody SysLocaleQuery query){
        return localeService.findPage(query);
    }

    @PostMapping("/get-locale")
    public Object getLocale(@RequestBody SysLocaleQuery query){
        return localeService.getLocale(query);
    }
}
