package muyu.framework.system.controller;


import muyu.framework.system.model.query.SysRoleMenuQuery;
import muyu.framework.system.model.vo.SysRoleMenuGrantVO;
import muyu.framework.system.service.ISysRoleMenuService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@RestController
@RequestMapping("/sys-role-menu")
public class SysRoleMenuController {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysRoleMenuService roleMenuService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id){
        return roleMenuService.getById(id);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
        return roleMenuService.removeById(id);
    }

    @PostMapping("/find-page")
    public Object findPage(@RequestBody SysRoleMenuQuery query){
        return roleMenuService.findPage(query);
    }

    @PostMapping("/find-list")
    public Object findList(@RequestBody SysRoleMenuQuery query){
        return roleMenuService.findList(query);
    }

    @PostMapping("/grant")
    public Object allocation(@RequestBody SysRoleMenuGrantVO grant){
        return roleMenuService.grant(grant);
    }
}
