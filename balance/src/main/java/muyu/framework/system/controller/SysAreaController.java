package muyu.framework.system.controller;


import muyu.framework.system.model.po.SysArea;
import muyu.framework.system.model.query.SysAreaQuery;
import muyu.framework.system.service.ISysAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@RestController
@RequestMapping("/sys-area")
public class SysAreaController {

    @Autowired
    ISysAreaService areaService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id){
        return areaService.getById(id);
    }

    @PostMapping
    public Object create(@RequestBody SysArea area){
        return areaService.save(area);
    }

    @PutMapping
    public Object update(@RequestBody SysArea area){
        return areaService.updateById(area);
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
        return areaService.removeById(id);
    }

    @PostMapping("/find-tree")
    public Object findTree(@RequestBody SysAreaQuery query){
        return areaService.findTree(query);
    }

}
