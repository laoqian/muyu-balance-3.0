package muyu.framework.system.controller;


import io.swagger.annotations.Api;
import muyu.framework.system.model.po.SysDict;
import muyu.framework.system.model.query.SysDictQuery;
import muyu.framework.system.service.ISysDictService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-02-12
 */
@Api(tags = "系统字典控制器")
@RestController
@RequestMapping("/sys-dict")
public class SysDictController {

    @Autowired
    ISysDictService dictService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id){
        return dictService.getById(id);
    }

    @PostMapping
    public Object create(@RequestBody SysDict dict){
        return dictService.save(dict);
    }

    @PutMapping("/{id}")
    public Object update(@RequestBody SysDict dict){
        return dictService.updateById(dict);
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
        return dictService.removeById(id);
    }

    @PostMapping("/find-page")
    public Object findPage(@RequestBody SysDictQuery query){
        return dictService.findPage(query);
    }

    @PostMapping("/find-list")
    public Object findList(@RequestBody SysDictQuery query){
        return dictService.findList(query);
    }
}
