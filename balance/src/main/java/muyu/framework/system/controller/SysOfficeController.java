package muyu.framework.system.controller;


import muyu.common.bean.BatchBean;
import muyu.framework.system.model.po.SysOffice;
import muyu.framework.system.model.query.SysOfficeQuery;
import muyu.framework.system.model.vo.SysOfficeVO;
import muyu.framework.system.service.ISysOfficeService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 机构表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@RestController
@RequestMapping("/sys-office")
public class SysOfficeController {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysOfficeService officeService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id){
        return officeService.getById(id);
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping
    public Object create(@RequestBody SysOfficeVO office){
        return officeService.create(mapper.map(office, SysOffice.class));
    }

    @PutMapping("/{id}")
    public Object update(@RequestBody SysOfficeVO office){
        return officeService.update(mapper.map(office,SysOffice.class));
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
        return officeService.deleteById(id);
    }

    @PostMapping("/find-page")
    public Object findPage(@RequestBody SysOfficeQuery query){
        return officeService.findPage(query);
    }

    @PostMapping("/find-tree")
    public Object findTree(@RequestBody SysOfficeQuery query){
        return officeService.findTree(query);
    }

    @PostMapping("/saveBatch")
    public Object saveBatch(@RequestBody BatchBean<SysOfficeVO,SysOfficeQuery> beanVO){
        return officeService.saveBatch(beanVO);
    }
}
