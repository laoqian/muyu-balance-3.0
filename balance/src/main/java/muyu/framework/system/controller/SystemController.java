package muyu.framework.system.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import muyu.common.util.StorageUtil;
import muyu.common.util.UserUtil;
import muyu.framework.system.service.ISystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;


@Api(tags = "基础控制器")
@RestController
@RequestMapping("/system")
public class SystemController {

    @Autowired
    ISystemService systemService;

    @ApiOperation("获取基础配置信息")
    @GetMapping(value = "/config")
    public Object config(){
        return systemService.config();
    }

    @ApiOperation("获取权限")
    @GetMapping(value = "/permissions")
    public Object permissions(){
        return UserUtil.getUser().getAuthorities();
    }

    @ApiOperation("获取当前登录用户")
    @GetMapping(value = "/current/user")
    public Object getCurrentUser(){
        return systemService.getCurrentUser();
    }

    @ApiOperation("获取当前登录用户所属公司")
    @GetMapping(value = "/current/company")
    public Object getCurrentCompany(){
        return systemService.getCurrentCompany();
    }

    @ApiOperation("上传文件测试")
    @PostMapping(value = "/upload",produces = MediaType.APPLICATION_JSON_VALUE,consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public Object upload(MultipartFile file) throws IOException {
        return StorageUtil.upload(file.getInputStream(),file.getOriginalFilename());
    }

    @ApiOperation("上传下载测试")
    @GetMapping(value = "/getUrl",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object getUrl(){
        return StorageUtil.authorize("于其先工作交接.docx",1000);
    }

    @ApiOperation("上传删除测试")
    @DeleteMapping(value = "/deleteUrl",produces = MediaType.APPLICATION_JSON_VALUE)
    public Object deleteUrl(){
        return StorageUtil.delete("于其先工作交接.docx");
    }
}
