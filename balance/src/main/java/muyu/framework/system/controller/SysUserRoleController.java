package muyu.framework.system.controller;


import muyu.framework.system.model.po.SysUserRole;
import muyu.framework.system.model.query.SysUserRoleQuery;
import muyu.framework.system.model.vo.SysUserRoleGrantVO;
import muyu.framework.system.model.vo.SysUserRoleVO;
import muyu.framework.system.service.ISysUserRoleService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 用户角色表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@RestController
@RequestMapping("/sys-user-role")
public class SysUserRoleController {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysUserRoleService userRoleService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id){
        return userRoleService.getById(id);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping
    public Object create(@RequestBody SysUserRoleVO role){
        return userRoleService.create(mapper.map(role, SysUserRole.class));
    }

    @Secured("ROLE_SUPER_ADMIN")
    @PutMapping("/{id}")
    public Object update(@RequestBody SysUserRoleVO role){
        return userRoleService.update(mapper.map(role,SysUserRole.class));
    }

    @Secured("ROLE_SUPER_ADMIN")
    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
        return userRoleService.removeById(id);
    }

    @PostMapping("/find-page")
    public Object findPage(@RequestBody SysUserRoleQuery query){
        return userRoleService.findPage(query);
    }

    @PostMapping("/find-list")
    public Object findList(@RequestBody SysUserRoleQuery query){
        return userRoleService.findList(query);
    }

    @PostMapping("/grant")
    public Object grant(@RequestBody SysUserRoleGrantVO grant){
        return userRoleService.grant(grant);
    }
}
