package muyu.framework.system.controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 更新记录表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@RestController
@RequestMapping("/sys-schemas-version")
public class SysSchemasVersionController {

}
