package muyu.framework.system.controller;


import io.swagger.annotations.Api;
import muyu.common.bean.BatchBean;
import muyu.framework.system.model.po.SysMenu;
import muyu.framework.system.model.query.SysMenuQuery;
import muyu.framework.system.model.vo.SysMenuVO;
import muyu.framework.system.model.vo.TransformBean;
import muyu.framework.system.service.ISysMenuService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@Api(tags = "系统菜单控制器")
@RestController
@RequestMapping("/sys-menu")
public class SysMenuController {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysMenuService menuService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id){
        return menuService.getById(id);
    }

    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping
    public Object create(@RequestBody SysMenuVO menu){
        return menuService.create(mapper.map(menu,SysMenu.class));
    }

    @PutMapping("/{id}")
    public Object update(@RequestBody SysMenuVO menu){
       return menuService.update(mapper.map(menu,SysMenu.class));
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
        return menuService.deleteById(id);
    }

    @PostMapping("/find-page")
    public Object findPage(@RequestBody SysMenuQuery query){
        return menuService.findPage(query);
    }

    @PostMapping("/find-tree")
    public Object findTree(@RequestBody SysMenuQuery query){
        return menuService.findTree(query);
    }

    @PostMapping("/saveBatch")
    public Object saveBatch(@RequestBody BatchBean<SysMenuVO,SysMenuQuery> beanVO){
        return menuService.saveBatch(beanVO);
    }

    @PostMapping("/translate")
    public Object translate(@RequestBody TransformBean<SysMenuQuery> transform){
        return menuService.translate(transform);
    }
}
