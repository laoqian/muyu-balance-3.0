package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import muyu.common.bean.BatchBean;
import muyu.common.bean.PageBean;
import muyu.common.bean.TreeService;
import muyu.framework.system.mapper.SysOfficeMapper;
import muyu.framework.system.model.po.SysOffice;
import muyu.framework.system.model.query.SysOfficeQuery;
import muyu.framework.system.model.vo.SysOfficeVO;
import muyu.framework.system.service.ISysOfficeService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 机构表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Service
public class SysOfficeServiceImpl extends TreeService<SysOfficeMapper, SysOffice> implements ISysOfficeService {
    private static final ModelMapper mapper = new ModelMapper();

    private Wrapper<SysOffice> createWrapper(SysOfficeQuery query){
        return new QueryWrapper<SysOffice>()
                .lambda()
                .like(StringUtils.isNotBlank(query.getParentId()),SysOffice::getParentId,Optional.ofNullable(query.getParentId()).map(String::trim).orElse(null))
                .like(StringUtils.isNotBlank(query.getName()),SysOffice::getName,Optional.ofNullable(query.getName()).map(String::trim).orElse(null));
    }

    @Override
    public PageBean<SysOfficeVO> findPage(SysOfficeQuery query) {
        return PageBean.create(super.page(query.toPage(), new QueryWrapper<>()));
    }

    @Override
    public PageBean<SysOfficeVO> findTree(SysOfficeQuery query) {
        List<SysOfficeVO> list = mapper.map(super.list(createWrapper(query)), new TypeToken<List<SysOfficeVO>>() {}.getType());
        List<SysOfficeVO> tree = SysOfficeVO.create(list);
        int current = query.getCurrent() * query.getPageSize();

        if(current > tree.size()){
            current = tree.size() - query.getPageSize();
        }

        int end = Math.min( current + query.getPageSize() , tree.size());
        if(query.getPageSize() == -1){
            return PageBean.<SysOfficeVO>builder().list(SysOfficeVO.create(list)).build();
        }

        return PageBean.<SysOfficeVO>builder()
                .list(SysOfficeVO.create(list).subList(current,end))
                .pageNum(query.getCurrent().longValue())
                .pageSize(query.getPageSize().longValue())
                .total((long) list.size())
                .build();
    }

    @Override
    public SysOfficeVO create(SysOffice office) {
        super.save(office);
        return mapper.map(office,SysOfficeVO.class);
    }

    @Override
    public SysOfficeVO update(SysOffice office) {
        super.updateById(office);
        return mapper.map(office,SysOfficeVO.class);
    }

    @Override
    public boolean deleteById(Serializable id) {
        return super.removeCascadeById(id.toString());
    }

    @Override
    public boolean saveBatch(BatchBean<SysOfficeVO, SysOfficeQuery> beanVO) {
        return false;
    }
}
