package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.framework.system.model.po.SysSchemasVersion;

/**
 * <p>
 * 更新记录表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
public interface ISysSchemasVersionService extends IService<SysSchemasVersion> {

}
