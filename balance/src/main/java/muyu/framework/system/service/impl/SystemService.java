package muyu.framework.system.service.impl;


import muyu.common.bean.TreeEntity;
import muyu.common.util.UserUtil;
import muyu.framework.system.model.po.SysOffice;
import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.model.vo.SysDictVO;
import muyu.framework.system.model.vo.SysMenuVO;
import muyu.framework.system.model.vo.SystemConfigVO;
import muyu.framework.system.service.*;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class SystemService implements ISystemService {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysMenuService menuService;

    @Autowired
    ISysDictService dictService;

    @Autowired
    ISysUserService userService;

    @Autowired
    ISysOfficeService officeService;

    @Override
    public SystemConfigVO config() {
        User user = UserUtil.getUser();
        SysUser sysUser = userService.getByLoginName(user.getUsername());
        List<SysMenuVO> list = mapper.map(menuService.findMenusByUserId(sysUser.getId()),new TypeToken<List<SysMenuVO>>(){}.getType());

        return SystemConfigVO.builder()
                .menuList(TreeEntity.create(list))
                .dictList(mapper.map(dictService.list(),new TypeToken<List<SysDictVO>>(){}.getType()))
                .authorities(UserUtil.getUser().getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList()))
                .build();
    }

    @Override
    public SysUser getCurrentUser() {
        return userService.getByLoginName(UserUtil.getUser().getUsername());
    }

    @Override
    public SysOffice getCurrentCompany() {
        SysUser user = this.getCurrentUser();
        return officeService.getById(user.getCompanyId());
    }
}
