package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Lists;
import muyu.common.bean.PageBean;
import muyu.common.exception.ServiceException;
import muyu.framework.common.config.RestProperty;
import muyu.framework.system.mapper.SysUserMapper;
import muyu.framework.system.model.po.SysMenu;
import muyu.framework.system.model.po.SysRole;
import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.model.query.SysUserQuery;
import muyu.framework.system.model.vo.SysUserVO;
import muyu.framework.system.service.ISysMenuService;
import muyu.framework.system.service.ISysRoleService;
import muyu.framework.system.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements ISysUserService {

    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysRoleService roleService;

    @Autowired
    ISysMenuService menuService;

    @Autowired
    RestProperty restProperty;

    @Override
    public List<String> getAuthorities(String loginName) {
        SysUser user = super.getOne(new QueryWrapper<SysUser>().lambda().eq(SysUser::getLoginName,loginName));
        List<String> authorityList = Lists.newArrayListWithCapacity(200);
        List<SysRole> roleList = roleService.getRolesByUserId(user.getId());
        List<SysMenu> menuList = menuService.findMenusByUserId(user.getId());

        authorityList.addAll(roleList.stream().map(t->"ROLE_"+t.getEnName()).collect(Collectors.toList()));
        authorityList.addAll(menuList.stream().filter(t-> t!=null && StringUtils.isNotBlank(t.getPermission())).map(SysMenu::getPermission).collect(Collectors.toList()));

        return authorityList;
    }

    private Wrapper<SysUser> createWrapper(SysUserQuery query){
        return new QueryWrapper<SysUser>()
            .lambda()
            .like(StringUtils.isNotBlank(query.getLoginName()),SysUser::getLoginName, Optional.ofNullable(query.getLoginName()).map(String::trim).orElse(null))
            .like(StringUtils.isNotBlank(query.getCompanyName()),SysUser::getCompanyName,Optional.ofNullable(query.getCompanyName()).map(String::trim).orElse(null))
            .like(StringUtils.isNotBlank(query.getOfficeName()),SysUser::getOfficeName,Optional.ofNullable(query.getOfficeName()).map(String::trim).orElse(null))
            .like(StringUtils.isNotBlank(query.getName()),SysUser::getLoginName,Optional.ofNullable(query.getName()).map(String::trim).orElse(null))
            .eq(query.getOfficeId()!=null,SysUser::getOfficeId,query.getOfficeId())
            .eq(query.getCompanyId()!=null,SysUser::getCompanyId,query.getCompanyId());
    }

    @Override
    public PageBean<SysUserVO> findPage(SysUserQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public PageBean<SysUserVO> findTree(SysUserQuery query){
        List<SysUserVO> list = mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<SysUserVO>>(){}.getType());
        return new PageBean<>(list);
    }

    @Transactional
    @Override
    public SysUserVO create(SysUser user) {
        if(getByLoginName(user.getLoginName())!=null){
            throw new RuntimeException("登录名不能重复");
        }

        super.save(user);
        return mapper.map(user,SysUserVO.class);
    }

    @Transactional
    @Override
    public SysUserVO update(SysUser user) {
        SysUser origin = getByLoginName(user.getLoginName());
        if(origin!=null && !StringUtils.equals(origin.getId(),user.getId())){
            throw new RuntimeException("登录名不能重复");
        }

        super.updateById(user);
        return mapper.map(user,SysUserVO.class);
    }

    @Override
    public boolean removeById(Serializable id) {
        SysUser origin  = super.getById(id);
        if(origin!=null && StringUtils.equals(origin.getLoginName(),restProperty.getSuperAdminLoginName())){
            throw new ServiceException("不能删除该用户");
        }

        SysUser user = SysUser.builder().id(id.toString()).status(false).build();
        return super.updateById(user);
    }

    @Override
    public SysUser getByLoginName(String loginName) {
        return super.getOne(new QueryWrapper<SysUser>().lambda().eq(SysUser::getLoginName,loginName));
    }
}
