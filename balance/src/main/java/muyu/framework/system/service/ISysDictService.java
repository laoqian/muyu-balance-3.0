package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.system.model.po.SysDict;
import muyu.framework.system.model.query.SysDictQuery;
import muyu.framework.system.model.vo.SysDictVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-12
 */
public interface ISysDictService extends IService<SysDict> {
    PageBean<SysDict> findPage(SysDictQuery query);

    List<SysDict> findList(SysDictQuery query);

    SysDictVO create(SysDict dict);

    SysDictVO update(SysDict dict);
}
