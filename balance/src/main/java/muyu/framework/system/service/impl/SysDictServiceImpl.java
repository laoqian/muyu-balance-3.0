package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.framework.system.mapper.SysDictMapper;
import muyu.framework.system.model.po.SysDict;
import muyu.framework.system.model.query.SysDictQuery;
import muyu.framework.system.model.vo.SysDictVO;
import muyu.framework.system.service.ISysDictService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-12
 */
@Service
public class SysDictServiceImpl extends ServiceImpl<SysDictMapper, SysDict> implements ISysDictService {
    private static final ModelMapper mapper = new ModelMapper();

    private Wrapper<SysDict> createWrapper(SysDictQuery query){
        return new QueryWrapper<SysDict>()
            .lambda()
            .like(StringUtils.isNotBlank(query.getType()),SysDict::getType,query.getType())
            .like(StringUtils.isNotBlank(query.getCode()),SysDict::getCode,query.getCode())
            .like(StringUtils.isNotBlank(query.getName()),SysDict::getName,query.getName());
    }

    @Override
    public PageBean<SysDict> findPage(SysDictQuery query) {
        return PageBean.create(super.page(query.toPage(),this.createWrapper(query)));
    }

    @Override
    public List<SysDict> findList(SysDictQuery query) {
        return super.list(this.createWrapper(query));
    }

    @Transactional
    @Override
    public SysDictVO create(SysDict user) {
        super.save(user);
        return mapper.map(user,SysDictVO.class);
    }

    @Transactional
    @Override
    public SysDictVO update(SysDict user) {
        super.updateById(user);
        return mapper.map(user,SysDictVO.class);
    }
}
