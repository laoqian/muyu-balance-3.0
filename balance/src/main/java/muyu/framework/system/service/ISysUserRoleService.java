package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.system.model.po.SysUserRole;
import muyu.framework.system.model.query.SysUserRoleQuery;
import muyu.framework.system.model.vo.SysUserRoleGrantVO;
import muyu.framework.system.model.vo.SysUserRoleVO;

import java.util.List;

/**
 * <p>
 * 用户角色表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
public interface ISysUserRoleService extends IService<SysUserRole> {

    List<SysUserRole> getByUserId(String userId);

    PageBean<SysUserRoleVO> findPage(SysUserRoleQuery query);

    List<SysUserRoleVO> findList(SysUserRoleQuery query);

    SysUserRoleVO create(SysUserRole user);

    SysUserRoleVO update(SysUserRole user);

    boolean grant(SysUserRoleGrantVO grant);
}
