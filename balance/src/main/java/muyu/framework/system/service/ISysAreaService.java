package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.system.model.po.SysArea;
import muyu.framework.system.model.query.SysAreaQuery;
import muyu.framework.system.model.vo.SysAreaVO;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
public interface ISysAreaService extends IService<SysArea> {

    PageBean<SysAreaVO> findPage(SysAreaQuery query);

    PageBean<SysAreaVO> findTree(SysAreaQuery query);

    SysAreaVO create(SysArea user);

    SysAreaVO update(SysArea user);
}
