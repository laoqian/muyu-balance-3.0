package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.system.model.po.SysLocale;
import muyu.framework.system.model.query.SysLocaleQuery;
import muyu.framework.system.model.vo.SysLocaleVO;

import java.util.Map;

/**
 * <p>
 * 国际化表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-03
 */
public interface ISysLocaleService extends IService<SysLocale> {

    PageBean<SysLocale> findPage(SysLocaleQuery query);

    Map<String,String> getLocale(SysLocaleQuery query);

    SysLocaleVO create(SysLocale locale);

    SysLocaleVO update(SysLocale locale);
}
