package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.framework.system.model.po.SysUserOnlineInfo;

/**
 * <p>
 * 用户登录信息表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
public interface ISysUserOnlineInfoService extends IService<SysUserOnlineInfo> {

}
