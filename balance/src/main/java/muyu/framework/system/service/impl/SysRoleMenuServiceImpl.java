package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.framework.common.config.RestProperty;
import muyu.framework.system.mapper.SysRoleMenuMapper;
import muyu.framework.system.model.po.SysMenu;
import muyu.framework.system.model.po.SysRole;
import muyu.framework.system.model.po.SysRoleMenu;
import muyu.framework.system.model.query.SysRoleMenuQuery;
import muyu.framework.system.model.vo.SysRoleMenuGrantVO;
import muyu.framework.system.model.vo.SysRoleMenuVO;
import muyu.framework.system.service.ISysRoleMenuService;
import muyu.framework.system.service.ISysRoleService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements ISysRoleMenuService {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    RestProperty property;

    @Autowired
    ISysRoleService roleService;

    private Wrapper<SysRoleMenu> createWrapper(SysRoleMenuQuery query){
        return new QueryWrapper<SysRoleMenu>()
                .lambda()
                .eq(query.getMenuId()!=null,SysRoleMenu::getMenuId,query.getMenuId())
                .eq(query.getRoleId()!=null,SysRoleMenu::getRoleId,query.getRoleId());
    }

    @Override
    public List<SysRoleMenu> getByRoleId(String roleId) {
        return super.list(new QueryWrapper<SysRoleMenu>().lambda().eq(SysRoleMenu::getRoleId,roleId));
    }

    public void grantSuperAdmin(SysMenu menu){
        super.save(create(roleService.getByEnName(property.getSuperAdminEnName()),menu));
    }

    @Override
    public PageBean<SysRoleMenuVO> findPage(SysRoleMenuQuery query) {
        return PageBean.create(super.page(query.toPage(),createWrapper(query)));
    }

    @Override
    public List<SysRoleMenuVO> findList(SysRoleMenuQuery query) {
        return mapper.map(super.list(createWrapper(query)),new TypeToken<List<SysRoleMenu>>(){}.getType());
    }

    @Override
    @Transactional
    public boolean grant(SysRoleMenuGrantVO allocation) {
        SysRole role = allocation.getRole();
        List<SysRoleMenu> list = allocation.getMenuList()
                .stream()
                .map(menu->create(role,menu))
                .collect(Collectors.toList());

        super.remove(new QueryWrapper<SysRoleMenu>().lambda().eq(SysRoleMenu::getRoleId,role.getId()));
        return super.saveBatch(list);
    }

    public SysRoleMenu create(SysRole role, SysMenu menu){
        return SysRoleMenu.builder()
            .roleId(role.getId())
            .roleName(role.getName())
            .menuId(menu.getId())
            .menuName(menu.getName())
            .build();
    }
}
