package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.framework.system.mapper.SysNotifyMapper;
import muyu.framework.system.model.po.SysNotify;
import muyu.framework.system.service.ISysNotifyService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 通知信息表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Service
public class SysNotifyServiceImpl extends ServiceImpl<SysNotifyMapper, SysNotify> implements ISysNotifyService {

}
