package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.common.exception.ServiceException;
import muyu.framework.common.config.RestProperty;
import muyu.framework.system.mapper.SysRoleMapper;
import muyu.framework.system.model.po.SysRole;
import muyu.framework.system.model.po.SysUserRole;
import muyu.framework.system.model.query.SysRoleQuery;
import muyu.framework.system.model.vo.SysRoleVO;
import muyu.framework.system.service.ISysRoleService;
import muyu.framework.system.service.ISysUserRoleService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole> implements ISysRoleService {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysUserRoleService userRoleService;

    @Autowired
    RestProperty restProperty;


    public List<SysRole> getRolesByUserId(String userId){
        List<SysUserRole> list = userRoleService.list(new QueryWrapper<SysUserRole>().lambda()
                .select(SysUserRole::getRoleId)
                .eq(SysUserRole::getUserId,userId));

        return super.list(new QueryWrapper<SysRole>().lambda().in(SysRole::getId,list.stream().map(SysUserRole::getRoleId).collect(Collectors.toList())));
    }

    private Wrapper<SysRole> createWrapper(SysRoleQuery query){
        return new QueryWrapper<SysRole>()
            .lambda()
            .like(StringUtils.isNotBlank(query.getCompanyName()),SysRole::getCompanyName, Optional.ofNullable(query.getCompanyName()).map(String::trim).orElse(null))
            .like(StringUtils.isNotBlank(query.getName()),SysRole::getName,Optional.ofNullable(query.getName()).map(String::trim).orElse(null))
            .like(StringUtils.isNotBlank(query.getEnName()),SysRole::getEnName,Optional.ofNullable(query.getEnName()).map(String::trim).orElse(null))
            .ne(SysRole::getEnName,restProperty.getSuperAdminEnName())
            .eq(query.getCompanyId()!=null,SysRole::getCompanyId,query.getCompanyId());
    }


    @Override
    public PageBean<SysRoleVO> findPage(SysRoleQuery query) {
        return PageBean.create(super.page(query.toPage(),this.createWrapper(query)));
    }

    @Override
    public List<SysRoleVO> findList(SysRoleQuery query) {
        return mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<SysRoleVO>>(){}.getType());
    }

    @Override
    public PageBean<SysRoleVO> findTree(SysRoleQuery query){
        List<SysRoleVO> list = mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<SysRoleVO>>(){}.getType());
        return new PageBean<>(list);
    }

    @Transactional
    @Override
    public SysRoleVO create(SysRole user) {
        super.save(user);
        return mapper.map(user,SysRoleVO.class);
    }

    @Transactional
    @Override
    public SysRoleVO update(SysRole user) {
        super.updateById(user);
        return mapper.map(user,SysRoleVO.class);
    }

    @Override
    @Transactional
    public boolean removeById(Serializable id) {
        SysRole role = super.getById(id);
        if(StringUtils.equals(role.getEnName(),restProperty.getSuperAdminEnName())){
            throw new ServiceException("不能删除");
        }

        userRoleService.remove(new QueryWrapper<SysUserRole>().lambda().eq(SysUserRole::getRoleId,id));
        return super.removeById(id);
    }

    @Override
    public SysRole getByEnName(String enName) {
        return super.getOne(new QueryWrapper<SysRole>().lambda().eq(SysRole::getEnName,enName));
    }
}
