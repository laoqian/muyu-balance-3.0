package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.system.model.po.SysMenu;
import muyu.framework.system.model.po.SysRoleMenu;
import muyu.framework.system.model.query.SysRoleMenuQuery;
import muyu.framework.system.model.vo.SysRoleMenuGrantVO;
import muyu.framework.system.model.vo.SysRoleMenuVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
public interface ISysRoleMenuService extends IService<SysRoleMenu> {

    List<SysRoleMenu> getByRoleId(String roleId);

    void grantSuperAdmin(SysMenu menu);

    PageBean<SysRoleMenuVO> findPage(SysRoleMenuQuery query);

    List<SysRoleMenuVO> findList(SysRoleMenuQuery query);

    boolean grant(SysRoleMenuGrantVO allocation);
}
