package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.model.query.SysUserQuery;
import muyu.framework.system.model.vo.SysUserVO;

import java.util.List;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
public interface ISysUserService extends IService<SysUser> {

    SysUser getByLoginName(String loginName);

    List<String> getAuthorities(String loginName);

    PageBean<SysUserVO> findPage(SysUserQuery query);

    PageBean<SysUserVO> findTree(SysUserQuery query);

    SysUserVO create(SysUser user);

    SysUserVO update(SysUser user);
}
