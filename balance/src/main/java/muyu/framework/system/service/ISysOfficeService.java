package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.BatchBean;
import muyu.common.bean.PageBean;
import muyu.framework.system.model.po.SysOffice;
import muyu.framework.system.model.query.SysOfficeQuery;
import muyu.framework.system.model.vo.SysOfficeVO;

import java.io.Serializable;

/**
 * <p>
 * 机构表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
public interface ISysOfficeService extends IService<SysOffice> {

    PageBean<SysOfficeVO> findPage(SysOfficeQuery query);

    PageBean<SysOfficeVO> findTree(SysOfficeQuery query);

    SysOfficeVO create(SysOffice office);

    SysOfficeVO update(SysOffice office);

    boolean deleteById(Serializable id);

    boolean saveBatch(BatchBean<SysOfficeVO,SysOfficeQuery> beanVO);
}
