package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import muyu.common.bean.BaseEntity;
import muyu.common.bean.BatchBean;
import muyu.common.bean.PageBean;
import muyu.common.bean.TreeService;
import muyu.framework.system.mapper.SysMenuMapper;
import muyu.framework.system.model.enums.BeanBeanActionEnum;
import muyu.framework.system.model.po.SysMenu;
import muyu.framework.system.model.po.SysRoleMenu;
import muyu.framework.system.model.po.SysUserRole;
import muyu.framework.system.model.query.SysMenuQuery;
import muyu.framework.system.model.vo.SysMenuVO;
import muyu.framework.system.model.vo.TransformBean;
import muyu.framework.system.service.ISysMenuService;
import muyu.framework.system.service.ISysRoleMenuService;
import muyu.framework.system.service.ISysUserRoleService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@Service
public class SysMenuServiceImpl extends TreeService<SysMenuMapper, SysMenu> implements
    ISysMenuService {

    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    ISysUserRoleService userRoleService;

    @Autowired
    ISysRoleMenuService roleMenuService;


    public List<SysMenu> findMenusByUserId(String userId) {
        List<SysUserRole> list = userRoleService.list(new QueryWrapper<SysUserRole>().lambda()
            .select(SysUserRole::getRoleId)
            .eq(SysUserRole::getUserId, userId));

        List<SysRoleMenu> roleMenuList = roleMenuService
            .list(new QueryWrapper<SysRoleMenu>().lambda()
                .select(SysRoleMenu::getMenuId)
                .in(SysRoleMenu::getRoleId,
                    list.stream().map(SysUserRole::getRoleId).collect(Collectors.toList())));

        return super.list(new QueryWrapper<SysMenu>().lambda()
            .in(SysMenu::getId,
                roleMenuList.stream().map(SysRoleMenu::getMenuId).collect(Collectors.toList())));
    }

    @Override
    public PageBean<SysMenuVO> findPage(SysMenuQuery query) {
        return PageBean.create(super.page(query.toPage(), new QueryWrapper<>()));
    }

    @Override
    public PageBean<SysMenuVO> findTree(SysMenuQuery query) {
        List<SysMenuVO> list = mapper.map(super.list(), new TypeToken<List<SysMenuVO>>() {}.getType());

        return PageBean.<SysMenuVO>builder()
            .list(SysMenuVO.create(list))
            .total((long) list.size())
            .build();
    }

    @Override
    @Transactional
    public SysMenuVO create(SysMenu menu) {
        super.save(menu);
        roleMenuService.grantSuperAdmin(menu);
        return mapper.map(menu, SysMenuVO.class);
    }

    @Override
    @Transactional
    public SysMenuVO update(SysMenu menu) {
        super.updateById(menu);
        return mapper.map(menu, SysMenuVO.class);
    }

    @Transactional
    public boolean deleteById(Serializable id) {
        List<SysMenu> list = super.getCascadeListById(id.toString());

        roleMenuService.remove(new QueryWrapper<SysRoleMenu>().lambda()
            .in(SysRoleMenu::getMenuId, list.stream()
                .map(SysMenu::getId)
                .collect(Collectors.toSet())));

        return super.removeCascadeById(id.toString());
    }

    @Override
    @Transactional
    public boolean saveBatch(BatchBean<SysMenuVO, SysMenuQuery> bean) {
        Map<Boolean, List<SysMenuVO>> map = bean.getList().stream()
            .collect(Collectors.groupingBy(
                t -> StringUtils.equals(t.getAction(), BeanBeanActionEnum.REMOVE.getSymbol()),
                Collectors.mapping(t -> {
                    SysMenuVO menu = t.getData();
                    if (StringUtils.equals(t.getAction(), BeanBeanActionEnum.CREATE.getSymbol())) {
                        menu.setIsNewRecord(true);
                        menu.setStatus(true);
                    }

                    return menu;
                }, Collectors.toList())));

        List<SysMenuVO> delList = map.get(true);
        List<SysMenuVO> saveList = map.get(false);

        if (!CollectionUtils.isEmpty(delList)) {
            delList.forEach(t->this.deleteById(t.getId()));
        }

        if (!CollectionUtils.isEmpty(saveList)) {
            List<SysMenu> list = mapper.map(saveList, new TypeToken<List<SysMenu>>() {}.getType());

            super.saveBatch(list);
            list.stream().filter(BaseEntity::getIsNewRecord).forEach(menu -> roleMenuService.grantSuperAdmin(menu));
        }

        return true;
    }

    @Transactional
    public boolean translate(TransformBean<SysMenuQuery> transform) {
        List<SysMenu> list = super.list(new QueryWrapper<SysMenu>()
                .lambda()
                );

        SysMenu root = new SysMenu("0");
        root.addChildren(list);
        root.translate(transform.getAction(),transform.getIds());
        return super.saveBatch(list);
    }
}
