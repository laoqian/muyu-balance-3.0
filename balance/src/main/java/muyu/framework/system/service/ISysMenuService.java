package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.BatchBean;
import muyu.common.bean.PageBean;
import muyu.framework.system.model.po.SysMenu;
import muyu.framework.system.model.query.SysMenuQuery;
import muyu.framework.system.model.vo.SysMenuVO;
import muyu.framework.system.model.vo.TransformBean;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
public interface ISysMenuService extends IService<SysMenu> {
    List<SysMenu> findMenusByUserId(String userId);

    PageBean<SysMenuVO> findPage(SysMenuQuery query);

    PageBean<SysMenuVO> findTree(SysMenuQuery query);

    SysMenuVO create(SysMenu menu);

    SysMenuVO update(SysMenu menu);

    boolean deleteById(Serializable id);

    boolean saveBatch(BatchBean<SysMenuVO, SysMenuQuery> beanVO);

    boolean translate(TransformBean<SysMenuQuery> transform);
}
