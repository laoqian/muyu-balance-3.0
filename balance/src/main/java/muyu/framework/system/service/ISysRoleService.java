package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.system.model.po.SysRole;
import muyu.framework.system.model.query.SysRoleQuery;
import muyu.framework.system.model.vo.SysRoleVO;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
public interface ISysRoleService extends IService<SysRole> {

    List<SysRole> getRolesByUserId(String userId);

    SysRole getByEnName(String enName);

    PageBean<SysRoleVO> findPage(SysRoleQuery query);

    List<SysRoleVO> findList(SysRoleQuery query);

    PageBean<SysRoleVO> findTree(SysRoleQuery query);

    SysRoleVO create(SysRole role);

    SysRoleVO update(SysRole role);
}
