package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.framework.system.mapper.SysAttachmentMapper;
import muyu.framework.system.model.po.SysAttachment;
import muyu.framework.system.service.ISysAttachmentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 系统附件表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Service
public class SysAttachmentServiceImpl extends ServiceImpl<SysAttachmentMapper, SysAttachment> implements ISysAttachmentService {

}
