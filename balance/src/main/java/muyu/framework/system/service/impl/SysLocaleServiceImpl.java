package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.common.exception.ServiceException;
import muyu.framework.system.mapper.SysLocaleMapper;
import muyu.framework.system.model.po.SysLocale;
import muyu.framework.system.model.query.SysLocaleQuery;
import muyu.framework.system.model.vo.SysLocaleVO;
import muyu.framework.system.service.ISysLocaleService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 国际化表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-03
 */
@Service
public class SysLocaleServiceImpl extends ServiceImpl<SysLocaleMapper, SysLocale> implements ISysLocaleService {
    private static final ModelMapper mapper = new ModelMapper();

    private LambdaQueryWrapper<SysLocale> createWrapper(SysLocaleQuery query){
        return new QueryWrapper<SysLocale>()
            .lambda()
            .eq(StringUtils.isNotBlank(query.getLang()),SysLocale::getLang,query.getLang())
            .like(StringUtils.isNotBlank(query.getCode()),SysLocale::getCode,query.getCode());
    }

    @Override
    @SuppressWarnings("unchecked")
    public PageBean<SysLocale> findPage(SysLocaleQuery query) {
        return PageBean.create(super.page(query.toPage(),this.createWrapper(query).orderBy(true,true,SysLocale::getCode)));
    }

    @Override
    public Map<String,String> getLocale(SysLocaleQuery query) {
        return super.list(this.createWrapper(query))
            .stream()
            .collect(Collectors.toMap(SysLocale::getCode, SysLocale::getValue));
    }

    @Override
    @Transactional
    public SysLocaleVO create(SysLocale locale) {
        int count = super.count(new QueryWrapper<SysLocale>().lambda()
            .eq(SysLocale::getLang,locale.getLang())
            .eq(SysLocale::getCode,locale.getCode()));

        if(count==1){
            throw new ServiceException("已存在语言和编码一致的数据");
        }

        super.save(locale);
        return mapper.map(locale,SysLocaleVO.class);
    }

    @Override
    @Transactional
    public SysLocaleVO update(SysLocale locale) {
        super.updateById(locale);
        return mapper.map(locale,SysLocaleVO.class);
    }

}
