package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.google.common.collect.Maps;
import muyu.framework.system.mapper.SysSequenceMapper;
import muyu.framework.system.model.bo.Sequence;
import muyu.framework.system.model.po.SysSequence;
import muyu.framework.system.service.ISysSequenceService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-03-22
 */
@Service
public class SysSequenceServiceImpl extends ServiceImpl<SysSequenceMapper, SysSequence> implements ISysSequenceService {

    private Map<String , Sequence> sequenceMap = Maps.newHashMap();

    public String getNextId(){
        return  getNextValue("id");
    }

    public synchronized String getNextValue(String name){
        Sequence sequence = sequenceMap.get(name);
        if(sequence  == null){
            sequence = Sequence.builder().name(name).build();
            sequenceMap.put(name,sequence);
        }

        if(!sequence.isValid()){
            long currentValue = this.getValueByName(sequence.getName(),sequence.getCacheSize());
            sequence.init(currentValue);
        }

        return sequence.nextValue().toString();
    }

    private long getValueByName(String name,int size){
        for(;;){
            SysSequence sequence = super.getOne(new QueryWrapper<SysSequence>().lambda().eq(SysSequence::getName,name));
            boolean ret;
            Long currentValue;

            if(sequence==null){
                currentValue = 10000L;
                ret = super.save(SysSequence.builder().name(name).currentVal(currentValue + size).build());
            }else {
                currentValue = sequence.getCurrentVal();
                sequence.setCurrentVal(currentValue + size);

                ret = super.update(sequence,new QueryWrapper<SysSequence>().lambda().eq(SysSequence::getName,name).eq(SysSequence::getCurrentVal,currentValue));
            }

            if(ret){
                return currentValue;
            }
        }
    }
}
