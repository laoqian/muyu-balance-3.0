package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.framework.system.model.po.SysRequestLog;


/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-03-22
 */
public interface ISysRequestLogService extends IService<SysRequestLog> {
    boolean save(SysRequestLog log);
}
