package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.common.exception.ServiceException;
import muyu.framework.common.config.RestProperty;
import muyu.framework.system.mapper.SysUserRoleMapper;
import muyu.framework.system.model.po.SysRole;
import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.model.po.SysUserRole;
import muyu.framework.system.model.query.SysUserRoleQuery;
import muyu.framework.system.model.vo.SysUserRoleGrantVO;
import muyu.framework.system.model.vo.SysUserRoleVO;
import muyu.framework.system.service.ISysRoleService;
import muyu.framework.system.service.ISysUserRoleService;
import muyu.framework.system.service.ISysUserService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户角色表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-02-11
 */
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole> implements ISysUserRoleService {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    RestProperty property;

    @Autowired
    ISysRoleService roleService;

    @Autowired
    ISysUserService userService;

    private Wrapper<SysUserRole> createWrapper(SysUserRoleQuery query){
        return new QueryWrapper<SysUserRole>()
                .lambda()
                .eq(query.getUserId()!=null,SysUserRole::getUserId,query.getUserId())
                .eq(query.getRoleId()!=null,SysUserRole::getRoleId,query.getRoleId());
    }

    @Override
    public List<SysUserRole> getByUserId(String userId) {
        return super.list(new QueryWrapper<SysUserRole>().lambda().eq(SysUserRole::getUserId,userId));
    }

    @Override
    public PageBean<SysUserRoleVO> findPage(SysUserRoleQuery query) {
        return PageBean.create(super.page(query.toPage(),createWrapper(query)));
    }

    @Override
    public List<SysUserRoleVO> findList(SysUserRoleQuery query) {
        return mapper.map(super.list(createWrapper(query)),new TypeToken<List<SysUserRole>>(){}.getType());
    }

    @Override
    public SysUserRoleVO create(SysUserRole userRole) {
        SysRole role = roleService.getById(userRole.getRoleId());
        if(StringUtils.equals(property.getSuperAdminEnName(),role.getEnName())){
            throw new ServiceException("不能分配该角色");
        }

        super.save(userRole);
        return mapper.map(userRole,SysUserRoleVO.class);
    }

    @Override
    public SysUserRoleVO update(SysUserRole userRole) {
        SysRole role = roleService.getById(userRole.getRoleId());
        if(StringUtils.equals(property.getSuperAdminEnName(),role.getEnName())){
            throw new ServiceException("不能分配该角色");
        }

        super.updateById(userRole);
        return mapper.map(userRole,SysUserRoleVO.class);
    }

    @Transactional
    @Override
    public boolean grant(SysUserRoleGrantVO grant){
        SysUser user = grant.getUser();
        SysRole superAdmin = roleService.getByEnName(property.getSuperAdminEnName());

        List<SysUserRole> list = grant.getRoleList().stream()
                .map(role-> SysUserRole.builder()
                        .userId(user.getId())
                        .userName(user.getName())
                        .roleId(role.getId())
                        .roleName(role.getName())
                        .build())
                .collect(Collectors.toList());

        if(list.stream().anyMatch(t->StringUtils.equals(t.getRoleId(),superAdmin.getId()))){
            throw new ServiceException("不能分配该角色");
        }

        super.remove(new QueryWrapper<SysUserRole>().lambda().eq(SysUserRole::getUserId,user.getId()));
        return super.saveBatch(list,100);
    }
}
