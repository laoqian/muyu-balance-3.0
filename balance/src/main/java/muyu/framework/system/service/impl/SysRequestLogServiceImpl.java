package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.util.UserUtil;
import muyu.framework.system.mapper.SysRequestLogMapper;
import muyu.framework.system.model.po.SysRequestLog;
import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.service.ISysRequestLogService;
import muyu.framework.system.service.ISysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-03-22
 */
@Service
public class SysRequestLogServiceImpl extends ServiceImpl<SysRequestLogMapper, SysRequestLog> implements ISysRequestLogService {

    @Autowired
    ISysUserService userService;

    @Override
    public boolean save(SysRequestLog log) {
        long userId   = 0L;
        String name  = "";
        User user = UserUtil.getUser();
        if(user!=null){
            SysUser sysUser = userService.getByLoginName(user.getUsername());
            userId  = Long.parseLong(sysUser.getId());
            name    = sysUser.getName();
        }

        log.setCreateBy(userId);
        log.setCreateName(name);
        log.setUpdateBy(userId);
        log.setUpdateName(name);

        return super.save(log);
    }
}
