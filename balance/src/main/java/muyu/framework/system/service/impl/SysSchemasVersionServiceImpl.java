package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.framework.system.mapper.SysSchemasVersionMapper;
import muyu.framework.system.model.po.SysSchemasVersion;
import muyu.framework.system.service.ISysSchemasVersionService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 更新记录表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Service
public class SysSchemasVersionServiceImpl extends ServiceImpl<SysSchemasVersionMapper, SysSchemasVersion> implements ISysSchemasVersionService {

}
