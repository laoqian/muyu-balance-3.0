package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.framework.system.mapper.SysAreaMapper;
import muyu.framework.system.model.po.SysArea;
import muyu.framework.system.model.query.SysAreaQuery;
import muyu.framework.system.model.vo.SysAreaVO;
import muyu.framework.system.service.ISysAreaService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Service
public class SysAreaServiceImpl extends ServiceImpl<SysAreaMapper, SysArea> implements ISysAreaService {

    private static final ModelMapper mapper = new ModelMapper();

    private Wrapper<SysArea> createWrapper(SysAreaQuery query){
        return new QueryWrapper<SysArea>()
                .lambda()
                .like(StringUtils.isNotBlank(query.getName()),SysArea::getName, Optional.ofNullable(query.getName()).map(String::trim).orElse(null))
                .like(StringUtils.isNotBlank(query.getCode()),SysArea::getCode,Optional.ofNullable(query.getCode()).map(String::trim).orElse(null));
    }

    @Override
    public PageBean<SysAreaVO> findPage(SysAreaQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public PageBean<SysAreaVO> findTree(SysAreaQuery query) {
        List<SysAreaVO> list = mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<SysAreaVO>>(){}.getType());
        return new PageBean<>(SysAreaVO.create(list));
    }

    @Override
    public SysAreaVO create(SysArea area) {
        super.save(area);
        return mapper.map(area,SysAreaVO.class);
    }

    @Override
    public SysAreaVO update(SysArea area) {
        super.updateById(area);
        return mapper.map(area,SysAreaVO.class);
    }
}
