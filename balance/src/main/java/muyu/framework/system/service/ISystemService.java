package muyu.framework.system.service;

import muyu.framework.system.model.po.SysOffice;
import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.model.vo.SystemConfigVO;

public interface ISystemService {
    SystemConfigVO config();

    SysUser getCurrentUser();

    SysOffice getCurrentCompany();
}
