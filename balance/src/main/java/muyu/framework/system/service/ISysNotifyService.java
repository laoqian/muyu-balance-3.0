package muyu.framework.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.framework.system.model.po.SysNotify;

/**
 * <p>
 * 通知信息表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
public interface ISysNotifyService extends IService<SysNotify> {

}
