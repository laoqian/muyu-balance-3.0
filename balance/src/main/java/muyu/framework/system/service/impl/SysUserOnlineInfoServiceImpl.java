package muyu.framework.system.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.framework.system.mapper.SysUserOnlineInfoMapper;
import muyu.framework.system.model.po.SysUserOnlineInfo;
import muyu.framework.system.service.ISysUserOnlineInfoService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户登录信息表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-05-02
 */
@Service
public class SysUserOnlineInfoServiceImpl extends ServiceImpl<SysUserOnlineInfoMapper, SysUserOnlineInfo> implements ISysUserOnlineInfoService {

}
