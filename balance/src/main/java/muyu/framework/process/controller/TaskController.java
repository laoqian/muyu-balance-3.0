package muyu.framework.process.controller;

import org.springframework.web.bind.annotation.*;



/**
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author 于其先
 * @since  2018年1月14日
 * @version 1.0.0
 */

@RestController
@RequestMapping("/process/task/")
public class TaskController  {

//    @Autowired
//    ActTaskService actTaskService;
//
//    @RequestMapping("findPage")
//    public ResultPageBean<Act> findPage(@RequestBody Act act, HttpServletRequest request) {
//        return actTaskService.findTaskPage(act,request);
//    }
//
//    @RequestMapping("findChangeTaskPage")
//    public ResultPageBean<Act> findChangeTaskPage(@RequestBody Act act, HttpServletRequest request) {
//        return actTaskService.findChangeTaskPage(act,request);
//    }
//
//    @RequestMapping("findHistoricTaskPage")
//    public ResultPageBean<Act> findHistoricTaskPage(@RequestBody Act act, HttpServletRequest request) {
//        return actTaskService.findHistoricTaskPage(act,request);
//    }
//
//    @RequestMapping("claim")
//    public ResultBean<String> claim(Act act){
//        return actTaskService.claim(act.getId(),UserUtils.getUser().getId());
//    }
//
//    @RequestMapping("claimBatch")
//    public ResultBean<String> claimBatch(String ids){
//        return actTaskService.claimBatch(ids,UserUtils.getUser().getId());
//    }
//
//    @RequestMapping("delegate")
//    public ResultBean<String> delegate(String taskId,String userId){
//        return actTaskService.delegate(taskId,userId);
//    }
//
//    @RequestMapping("getTaskInfo")
//    public ResultBean<ActInfo> getTaskInfo(String id){
//        return actTaskService.getTaskInfo(id);
//    }
//
//    @GetMapping("getActInfo")
//    public ResultBean<ActInfo> getActInfo(String procInstId){
//        return actTaskService.getTaskInfoByProcInstId(procInstId);
//    }
//
//    @RequestMapping("delete")
//    public ResultBean<String> delete(String id){
//        return actTaskService.deleteByTaskId(id);
//    }
//
//    @GetMapping("rollback")
//    public ResultBean<String> rollback(String taskId){
//        return actTaskService.rollback(taskId);
//    }
//
//    @RolesAllowed("ROLE_SUPER_ADMIN")
//    @RequestMapping("deleteProcOnly")
//    public ResultBean<String> deleteProcOnly(String procInstId){
//        return actTaskService.deleteProcOnly(procInstId);
//    }
//
//    @RequestMapping("getProcInfo")
//    public ResultBean<ActInfo> getProcInfo(String key, String contractId,@RequestParam(required=false)  Integer category
//            , @RequestParam(required=false)  String actMapper){
//        return actTaskService.getProcInfo(key,contractId,actMapper,category);
//    }
//
//    @RequestMapping("getProcComment")
//    public ResultBean<ProcessComments> getProcComment(String processId){
//        return actTaskService.getProcessComment(processId);
//    }
//
//    @GetMapping("getNextAuditor")
//    public ResultBean<TaskAuditor> getNextAuditor(String taskId,String pass){
//        return actTaskService.getNextAuditor(taskId,pass);
//    }
//
//    @GetMapping("lastMonthCompleted")
//    public ResultBean<Long> lastMonthCompleted(){
//        return actTaskService.lastMonthCompleted();
//    }
//
//    @GetMapping("countFinishedTask")
//    public ResultBean<Long> countFinishedTask(){
//        return actTaskService.countFinishedTask();
//    }
//
//    @GetMapping("statistics")
//    public ResultBean<List> statistics(String userId, Integer monthNum){
//        return actTaskService.statistics(userId,monthNum);
//    }
}

