package muyu.framework.process.controller;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import org.springframework.web.bind.annotation.RestController;
import muyu.framework.process.service.IExecutionExtService;
import muyu.framework.process.model.po.ExecutionExt;
import muyu.framework.process.model.vo.ExecutionExtVO;
import muyu.framework.process.model.query.ExecutionExtQuery;

/**
 * <p>
 * 业务流程表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
@RestController
@RequestMapping("/process/execution-ext")
public class ExecutionExtController {
     private static final ModelMapper mapper = new ModelMapper();

     @Autowired
     IExecutionExtService executionExtService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return executionExtService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping
     public Object create(@RequestBody ExecutionExtVO executionExt){
         return executionExtService.create(mapper.map(executionExt, ExecutionExt.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PutMapping("/{id}")
     public Object update(@RequestBody ExecutionExtVO executionExt){
         return executionExtService.update(mapper.map(executionExt,ExecutionExt.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @DeleteMapping("/{id}")
     public Object delete(@PathVariable String id){
         return executionExtService.removeById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody ExecutionExtQuery query){
         return executionExtService.findPage(query);
     }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-tree")
    public Object findTree(@RequestBody ExecutionExtQuery query){
    return executionExtService.findTree(query);
    }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody ExecutionExtQuery query){
         return executionExtService.findList(query);
     }
}
