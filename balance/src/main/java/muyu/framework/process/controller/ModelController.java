package muyu.framework.process.controller;

import muyu.common.bean.PageBean;
import muyu.framework.process.model.query.ModelQuery;
import muyu.framework.process.model.vo.ModelVO;
import muyu.framework.process.service.IModelService;
import org.activiti.engine.impl.persistence.entity.ModelEntity;
import org.activiti.engine.repository.Model;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;


/**
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author 于其先
 * @since  2017年12月19日
 * @version 1.0.0
 */

@RolesAllowed("ROLE_SUPER_ADMIN")
@RestController
@RequestMapping("/process/model")
public class ModelController {

    @Autowired
    IModelService modelService;

    @PostMapping
    public Object create(@RequestBody ModelVO modelVO){
        return modelService.create(modelVO);
    }

    @PutMapping("/{id}")
    public Object save(@RequestBody ModelEntity model){
        return modelService.save(model);
    }

    @GetMapping("/deploy")
    public Object deploy(String id) throws Exception {
        return modelService.deploy(id);
    }

    @GetMapping("/get")
    public Object get(Model model){
        return  modelService.get(model);
    }

    @PostMapping("/find-page")
    public Object findPage(@RequestBody ModelQuery query) {
        return modelService.findPage(query);
    }

    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
       return  modelService.delete(id);
    }

    @GetMapping("/upgrade")
    public Object upgrade(String key){
        return  modelService.upgrade(key);
    }
}
