package muyu.framework.process.controller;

import muyu.framework.process.model.query.ProcessQuery;
import muyu.framework.process.service.IProcessService;
import org.activiti.engine.repository.ProcessDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.io.IOException;


/**
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author 于其先
 * @since  2017年12月19日09:34:42
 * @version 1.0.0
 */

@RestController
@RequestMapping("/process/process")
public class ProcessController  {

    @Autowired
    IProcessService processService;

    @RequestMapping("getImage")
    public Object getImage(ProcessDefinition processDefinition) throws IOException {
        return processService.getImage(processDefinition);
    }

    @RequestMapping("findPage")
    public Object findPage(ProcessQuery query) {
        return processService.findPage(query);
    }

    @RolesAllowed("ROLE_SUPER_ADMIN")
    @RequestMapping("delete")
    public Object delete(String id){
        return  processService.delete(id);
    }

    @RolesAllowed("ROLE_SUPER_ADMIN")
    @RequestMapping("deleteProcIns")
    public Object deleteProcIns(String procInstId, String reason){
        return  processService.deleteProcIns(procInstId,reason);
    }
}
