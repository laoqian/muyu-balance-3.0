package muyu.framework.process.controller;

import muyu.framework.process.model.po.ProcessExt;
import muyu.framework.process.model.query.ProcessExtQuery;
import muyu.framework.process.service.IProcessExtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import javax.xml.stream.XMLStreamException;

/**
 * <p>
 * 流程定义 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
@RestController
@RequestMapping("/process/process-ext")
public class ProcessExtController {

    @Autowired
    IProcessExtService processExtService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id) {
        return processExtService.getById(id);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @PutMapping("/{id}")
    public Object update(@RequestBody ProcessExt ext) {
        return processExtService.updateById(ext);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @GetMapping("/toModel/{key}")
    public Object toModel(@PathVariable String key) throws XMLStreamException {
        return processExtService.toModel(key);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id) {
        return processExtService.removeById(id);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-page")
    public Object findPage(@RequestBody ProcessExtQuery query) {
        return processExtService.findPage(query);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-tree")
    public Object findTree(@RequestBody ProcessExtQuery query) {
        return processExtService.findTree(query);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-list")
    public Object findList(@RequestBody ProcessExtQuery query) {
        return processExtService.findList(query);
    }
}
