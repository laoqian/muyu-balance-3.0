package muyu.framework.process.controller;

import muyu.framework.process.service.ITraceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;


/**
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author 于其先
 * @since  2018年1月14日
 * @version 1.0.0
 */

@RestController
@RequestMapping("/process/trace")
public class TraceController  {

    @Autowired
    ITraceService traceService;

    @RequestMapping(value = "data/{executionId}")
    public void readResource(@PathVariable("executionId") String executionId, HttpServletResponse response) throws Exception {
        traceService.readResource(executionId,response);
    }

    @RequestMapping(value = "/{executionId}")
    public ModelAndView historyData(@PathVariable("executionId") String executionId) {
        return traceService.historyData(executionId);
    }
}

