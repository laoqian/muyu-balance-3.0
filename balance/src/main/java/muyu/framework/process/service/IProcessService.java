/*
 * Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author  于其先
 * @version 1.0.0
 * @since 2020-07-10 21:51:45
 */
package muyu.framework.process.service;

import muyu.framework.process.model.dto.ProcessDef;
import muyu.framework.process.model.query.ProcessQuery;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;

import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.List;

/**
 * 模型定义接口
 *
 * @author 于其先
 * @since 2020-07-10 21:51:45
 */
public interface IProcessService {

    String getImage(ProcessDefinition processDefinition) throws IOException;

    List<ProcessDef> findPage(ProcessQuery query);

    boolean delete(String id);

    boolean toModel(String key) throws XMLStreamException;

    boolean deleteProcIns(String procInstId, String deleteReason);

    ProcessInstance getProcessInst(String procInstId);
}
