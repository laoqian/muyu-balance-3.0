/*
 * Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author  于其先
 * @version 1.0.0
 * @since 2020-07-12 21:00:49
 */
package muyu.framework.process.service.impl;

import muyu.common.bean.PageBean;
import muyu.framework.process.model.query.TaskExtQuery;
import muyu.framework.process.model.vo.TaskExtVO;
import muyu.framework.process.service.ITaskExtService;
import muyu.framework.system.model.po.SysUser;
import muyu.framework.system.service.impl.SystemService;
import org.activiti.engine.TaskService;
import org.activiti.engine.task.Task;
import org.activiti.engine.task.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * 任务扩展
 *
 * @author 于其先
 * @since 2020-07-12 21:00:49
 */
@Service
public class TaskExtService implements ITaskExtService {

    @Autowired
    TaskService taskService;

    @Autowired
    SystemService systemService;

    @Override
    public PageBean<TaskExtVO> findPage(TaskExtQuery query) {
        SysUser user = systemService.getCurrentUser();

        TaskQuery taskQuery = taskService.createTaskQuery()
                .taskAssignee(user.getId())
                .or()
                .processInstanceBusinessKeyLike(this.createUserContractRoleLike())
                .taskName(query.getTaskName())
                .processDefinitionName(query.getProcessName())
                .orderByTaskCreateTime()
                .taskCreatedAfter(query.getStartDate())
                .taskCreatedBefore(query.getEndDate());

        List<Task> list     = taskQuery.listPage(query.getPageStart(),query.getPageSize());

        return PageBean.<TaskExtVO>builder()
                .total(taskQuery.count())
                .list(list.stream().map(this::toTaskExtVO).collect(Collectors.toList()))
                .pageSize((long)query.getPageSize())
                .pageNum((long)query.getCurrent())
                .build();
    }

    String createUserContractRoleLike(){

        return null;
    }

    TaskExtVO toTaskExtVO(Task task){
        return TaskExtVO.builder().build();
    }
}
