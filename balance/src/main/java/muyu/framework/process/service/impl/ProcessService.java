package muyu.framework.process.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import muyu.framework.process.mapper.ActMapper;
import muyu.framework.process.model.dto.FormDataInfo;
import muyu.framework.process.model.dto.ProcessDef;
import muyu.framework.process.model.query.ProcessQuery;
import muyu.framework.process.service.IProcessService;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.FormService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.form.FormData;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.ProcessInstance;
import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author 于其先
 * @version 1.0.0
 * @since 2017/12/19
 */

@Service
public class ProcessService implements IProcessService {

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    FormService formService;

    @Autowired
    ActMapper actMapper;

    public ProcessDef get(String procDefKey) {
        ProcessDefinition process = repositoryService.createProcessDefinitionQuery().processDefinitionKey(procDefKey).latestVersion().singleResult();
        FormData data = formService.getStartFormData(process.getId());
        FormDataInfo info = FormDataInfo.builder().formKey(data.getFormKey()).deploymentId(data.getDeploymentId()).formProperties(data.getFormProperties()).build();
        ProcessDef processDef = new ProcessDef(process);
        processDef.setFormData(info);
        return processDef;
    }

    public String getImage(ProcessDefinition processDefinition) throws IOException {
        ProcessDefinition process = repositoryService.createProcessDefinitionQuery().processDefinitionId(processDefinition.getId()).singleResult();
        InputStream resourceAsStream = repositoryService.getResourceAsStream(process.getDeploymentId(), process.getDiagramResourceName());
        Base64 base64 = new Base64();
        ByteArrayOutputStream swapStream = new ByteArrayOutputStream();

        byte[] buff = new byte[100];
        int rc;
        while ((rc = resourceAsStream.read(buff, 0, 100)) > 0) {
            swapStream.write(buff, 0, rc);
        }
        byte[] imageBytes = swapStream.toByteArray();

        return base64.encodeToString(imageBytes);
    }

    public boolean delete(String id) {
        ProcessDefinition process = repositoryService.createProcessDefinitionQuery().processDefinitionId(id).singleResult();
        repositoryService.deleteDeployment(process.getDeploymentId(), true); /*级联删除流程实例*/
        return true;
    }

    public List<ProcessDef> findPage(ProcessQuery query) {
        return repositoryService.createProcessDefinitionQuery()
                .latestVersion()
                .listPage(query.getPageSize() * (query.getPageSize() - 1), query.getPageSize())
                .stream()
                .map(ProcessDef::new)
                .collect(Collectors.toList());
    }

    public boolean toModel(String key) throws XMLStreamException {
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionKey(key).singleResult();
        InputStream bpmnStream = repositoryService.getResourceAsStream(processDefinition.getDeploymentId(),
                processDefinition.getResourceName());
        XMLInputFactory xif = XMLInputFactory.newInstance();
        InputStreamReader in = new InputStreamReader(bpmnStream, StandardCharsets.UTF_8);
        XMLStreamReader xtr = xif.createXMLStreamReader(in);
        BpmnModel bpmnModel = new BpmnXMLConverter().convertToBpmnModel(xtr);

        BpmnJsonConverter converter = new BpmnJsonConverter();
        ObjectNode modelNode = converter.convertToJson(bpmnModel);
        Model modelData = repositoryService.newModel();
        modelData.setKey(processDefinition.getKey());
        modelData.setName(processDefinition.getResourceName());
        modelData.setCategory(processDefinition.getCategory());//.getDeploymentId());
        modelData.setDeploymentId(processDefinition.getDeploymentId());
        modelData.setVersion(Integer.parseInt(String.valueOf(repositoryService.createModelQuery().modelKey(modelData.getKey()).count() + 1)));

        ObjectNode modelObjectNode = new ObjectMapper().createObjectNode();
        modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME, processDefinition.getName());
        modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, modelData.getVersion());
        modelObjectNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION, processDefinition.getDescription());
        modelData.setMetaInfo(modelObjectNode.toString());

        repositoryService.saveModel(modelData);

        repositoryService.addModelEditorSource(modelData.getId(), modelNode.toString().getBytes(StandardCharsets.UTF_8));

        return true;
    }

    public boolean deleteProcIns(String procInstId, String deleteReason) {
        runtimeService.deleteProcessInstance(procInstId, deleteReason);
        return true;
    }

    public ProcessInstance getProcessInst(String procInstId) {
        return runtimeService.createProcessInstanceQuery().processInstanceId(procInstId).singleResult();
    }
}
