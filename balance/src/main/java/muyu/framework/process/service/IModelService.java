/*
 * Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author  于其先
 * @version 1.0.0
 * @since 2020-07-10 21:51:45
 */
package muyu.framework.process.service;

import muyu.common.bean.PageBean;
import muyu.framework.process.model.query.ModelQuery;
import muyu.framework.process.model.vo.ModelVO;
import org.activiti.engine.repository.Model;


/**
 * 模型定义接口
 *
 * @author 于其先
 * @since 2020-07-10 21:51:45
 */
public interface IModelService {

    boolean upgrade(String key);

    boolean create(ModelVO model);

    boolean save(Model model);

    boolean delete(String id);

    boolean deploy(String id)  throws Exception ;

    Model get(Model model);

    PageBean<Model> findPage(ModelQuery query);
}
