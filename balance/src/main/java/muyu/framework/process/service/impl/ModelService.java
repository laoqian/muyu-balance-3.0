/*
 * Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author  于其先
 * @version 1.0.0
 * @since 2020-07-10 21:39:34
 */
package muyu.framework.process.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import muyu.common.bean.PageBean;
import muyu.common.exception.ServiceException;
import muyu.framework.process.config.SystemConfig;
import muyu.framework.process.mapper.ActMapper;
import muyu.framework.process.model.po.ProcessExt;
import muyu.framework.process.model.query.ModelQuery;
import muyu.framework.process.model.vo.ModelVO;
import muyu.framework.process.service.IModelService;
import muyu.framework.process.service.IProcessExtService;
import org.activiti.bpmn.converter.BpmnXMLConverter;
import org.activiti.bpmn.model.BpmnModel;
import org.activiti.editor.constants.ModelDataJsonConstants;
import org.activiti.editor.language.json.converter.BpmnJsonConverter;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.TaskService;
import org.activiti.engine.delegate.Expression;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.bpmn.behavior.UserTaskActivityBehavior;
import org.activiti.engine.impl.persistence.entity.IdentityLinkEntity;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * 流程模型处理
 *
 * @author 于其先
 * @since 2020-07-10 21:39:34
 */
@Service
public class ModelService implements IModelService {

    @Autowired
    RepositoryService repositoryService;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    ActMapper actMapper;

    @Autowired
    TaskService taskService;

    @Autowired
    IProcessExtService processExtService;

    public boolean create(ModelVO modelVO){
        ObjectNode editorNode = objectMapper.createObjectNode();
        editorNode.put("resourceId", "canvas");
        editorNode.put("id", "canvas");
        ObjectNode properties = objectMapper.createObjectNode();
        properties.put("process_author", "于其先");
        editorNode.set("properties", properties);
        ObjectNode stencilset = objectMapper.createObjectNode();
        stencilset.put("namespace", "http://b3mn.org/stencilset/bpmn2.0#");
        editorNode.set("stencilset", stencilset);

        Model model = repositoryService.newModel();
        model.setKey(StringUtils.defaultString(modelVO.getKey()));
        model.setName(modelVO.getName());
        model.setCategory(modelVO.getCategory());
        model.setVersion(Integer.parseInt(String.valueOf(repositoryService.createModelQuery().modelKey(model.getKey()).count()+1)));

        ObjectNode modelObjectNode = objectMapper.createObjectNode();
        modelObjectNode.put(ModelDataJsonConstants.MODEL_NAME, modelVO.getName());
        modelObjectNode.put(ModelDataJsonConstants.MODEL_REVISION, model.getVersion());
        modelObjectNode.put(ModelDataJsonConstants.MODEL_DESCRIPTION,StringUtils.defaultString(modelVO.getDescription()));
        model.setMetaInfo(modelObjectNode.toString());

        repositoryService.saveModel(model);
        repositoryService.addModelEditorSource(model.getId(),editorNode.toString().getBytes(StandardCharsets.UTF_8));

        return true;
    }

    public boolean deploy(String id) throws Exception {

        //获取模型
        Model modelData = repositoryService.getModel(id);
        byte[] bytes = repositoryService.getModelEditorSource(modelData.getId());

        if (bytes == null) {
            throw new ServiceException("模型数据为空，请先设计流程并成功保存，再进行发布。");
        }

        JsonNode modelNode = new ObjectMapper().readTree(bytes);

        BpmnModel model = new BpmnJsonConverter().convertToBpmnModel(modelNode);
        if(model.getProcesses().size()==0){
            throw new ServiceException("数据模型不符要求，请至少设计一条主线流程。");
        }

        byte[] bpmnBytes = new BpmnXMLConverter().convertToXML(model);

        //发布流程
        String processName = modelData.getName() + ".bpmn20.xml";
        Deployment deployment = repositoryService.createDeployment()
                .name(modelData.getName())
                .category(modelData.getCategory())
                .addString(processName, new String(bpmnBytes, StandardCharsets.UTF_8))
                .deploy();

        List<ProcessDefinition> list = repositoryService.createProcessDefinitionQuery().deploymentId(deployment.getId()).list();
        for (ProcessDefinition processDefinition : list) {
            repositoryService.setProcessDefinitionCategory(processDefinition.getId(), modelData.getCategory());

            ProcessExt ext=  processExtService.getOne(new QueryWrapper<ProcessExt>().lambda().eq(ProcessExt::getKey,processDefinition.getKey()));
            if(ext!=null && ext.getCategory()==1){

                /*
                 * 非合同流程不同步升级
                 */
                upgrade(processDefinition);
                updateTaskGroup(processDefinition);
            }
        }

        modelData.setDeploymentId(deployment.getId());
        repositoryService.saveModel(modelData);

        processExtService.save(modelData.getName(),modelData.getKey());

        return true;
    }

    public Model get(Model model) {
        return repositoryService.createModelQuery().modelId(model.getId()).singleResult();
    }

    public PageBean<Model> findPage(ModelQuery query) {
        List<Model> list = repositoryService.createModelQuery()
            .orderByModelKey()
            .asc()
            .listPage( (query.getCurrent()-1)*query.getPageSize(),query.getPageSize());

        return PageBean.<Model>builder()
            .pageNum((long)query.getCurrent())
            .pageSize((long)query.getPageSize())
            .list(list)
            .total(repositoryService.createModelQuery().count())
            .build();
    }

    public boolean save(Model model){
        Model m = repositoryService.getModel(model.getId());

        m.setKey(model.getKey());
        m.setName(model.getName());
        m.setCategory(model.getCategory());

        repositoryService.saveModel(m);

        return true;
    }

    public boolean delete(String id){
        repositoryService.deleteModel(id);
        return true;
    }

    private void upgrade(ProcessDefinition procDef){
        actMapper.updateRunTasks(procDef.getId(),procDef.getKey(),procDef.getVersion());
        actMapper.updateExecutes(procDef.getId(),procDef.getKey(),procDef.getVersion());
        actMapper.updateJobs(procDef.getId(),procDef.getKey(),procDef.getVersion());
        actMapper.updateIdentityLink(procDef.getId(),procDef.getKey(),procDef.getVersion());
        actMapper.updateTaskInst(procDef.getId(),procDef.getKey(),procDef.getVersion());
        actMapper.updateProcInst(procDef.getId(),procDef.getKey(),procDef.getVersion());
        actMapper.updateActInst(procDef.getId(),procDef.getKey(),procDef.getVersion());
    }

    private void updateTaskGroup(ProcessDefinition procDef){
        List<Task> list = taskService.createTaskQuery().processDefinitionId(procDef.getId()).taskUnassigned().list();
        ProcessDefinitionEntity processDefinitionEntity  = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService).getDeployedProcessDefinition(procDef.getId());
        List<ActivityImpl> activityList = processDefinitionEntity.getActivities();
        for(Task task : list){
            Optional<ActivityImpl> optional = activityList.stream().filter(act-> StringUtils.equals(task.getTaskDefinitionKey(),act.getId())).findFirst();
            optional.ifPresent(act-> {
                Set<Expression> set = ((UserTaskActivityBehavior)act.getActivityBehavior()).getTaskDefinition().getCandidateGroupIdExpressions();
                if(!CollectionUtils.isEmpty(set)){
                    String groupId = set.toArray()[0].toString();
                    modifyTaskGroupId(task.getId(),groupId);
                }
            });
        }
    }

    private void modifyTaskGroupId(String taskId,String groupId){
        List<IdentityLink> identityLinkList = taskService.getIdentityLinksForTask(taskId);
        for(IdentityLink link : identityLinkList){
            IdentityLinkEntity entity = (IdentityLinkEntity) link;
            if(StringUtils.equals("candidate",entity.getType()) && !StringUtils.equals(groupId,entity.getGroupId())){
                System.out.println("更新任务：old groupId:"+entity.getGroupId()+"-new groupId:"+groupId);
                taskService.deleteCandidateGroup(taskId,entity.getGroupId());
                taskService.addCandidateGroup(taskId,groupId);
            }
        }
    }

    @Override
    public boolean upgrade(String key){
        ProcessDefinition processDefinition = repositoryService.createProcessDefinitionQuery().processDefinitionKey(key).latestVersion().singleResult();

        upgrade(processDefinition);
        updateTaskGroup(processDefinition);

        switch (processDefinition.getKey()){
            case "proc_oa_business_trip_apply":
                actMapper.updateBusinessVersion("pm_oa_business_trip", SystemConfig.OA_BUSINESS_TRIP_VERSION);
                break;

            case "proc_oa_normal_apply":
                actMapper.updateBusinessVersion("pm_oa_apply", SystemConfig.OA_APPLY_VERSION);
                break;
            case "proc_oa_borrow_apply":
                actMapper.updateBusinessVersion("pm_oa_borrow", SystemConfig.OA_BORROW_VERSION);
                break;
            case "proc_budget_apply":
                actMapper.updateBusinessVersion("pm_ru_budget", SystemConfig.BUDGET_VERSION);
                break;
            default:
                throw new RuntimeException("该流程不支持升级");
        }

        return true;
    }
}
