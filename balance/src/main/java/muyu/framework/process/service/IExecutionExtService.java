package muyu.framework.process.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.framework.process.model.po.ExecutionExt;
import muyu.framework.process.model.vo.ExecutionExtVO;
import muyu.framework.process.model.query.ExecutionExtQuery;
import java.util.List;
import muyu.common.bean.PageBean;
/**
 * <p>
 * 业务流程表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
public interface IExecutionExtService extends IService<ExecutionExt> {

    PageBean<ExecutionExtVO> findPage(ExecutionExtQuery query);

    List<ExecutionExtVO> findList(ExecutionExtQuery query);

    PageBean<ExecutionExtVO> findTree(ExecutionExtQuery query);

    ExecutionExtVO create(ExecutionExt executionExt);

    ExecutionExtVO  update(ExecutionExt executionExt);

    boolean isFinished(String procInstId);
}
