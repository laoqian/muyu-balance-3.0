package muyu.framework.process.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.common.exception.ServiceException;
import muyu.framework.process.mapper.ProcessExtMapper;
import muyu.framework.process.model.po.ProcessExt;
import muyu.framework.process.model.query.ProcessExtQuery;
import muyu.framework.process.model.vo.ProcessExtVO;
import muyu.framework.process.service.IProcessExtService;
import muyu.framework.process.service.IProcessService;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.xml.stream.XMLStreamException;
import java.util.List;

/**
 * <p>
 * 流程定义 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
@Service
public class ProcessExtServiceImpl extends ServiceImpl<ProcessExtMapper, ProcessExt> implements IProcessExtService {
    final private ModelMapper mapper = new ModelMapper();

    @Autowired
    IProcessService processService;

    @Autowired
    RepositoryService repositoryService;

    private Wrapper<ProcessExt> createWrapper(ProcessExtQuery query) {
        return new QueryWrapper<ProcessExt>()
                .lambda();
    }

    @Override
    public PageBean<ProcessExtVO> findPage(ProcessExtQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<ProcessExtVO> findList(ProcessExtQuery query) {
        return mapper.map(super.list(this.createWrapper(query)), new TypeToken<List<ProcessExtVO>>() {
        }.getType());
    }

    @Override
    public PageBean<ProcessExtVO> findTree(ProcessExtQuery query) {
        return new PageBean<>(this.findList(query));
    }

    @Override
    public boolean save(String name, String key) {
        Wrapper<ProcessExt> wrapper = new QueryWrapper<ProcessExt>().lambda().eq(ProcessExt::getKey, key);
        return super.count(wrapper) == 0 ? super.save(this.create(name, key)) : super.update(ProcessExt.builder().name(name).key(key).build(), wrapper);
    }

    @Override
    public boolean toModel(String key) throws XMLStreamException {
//        Model model = repositoryService.createModelQuery().modelKey(key).latestVersion().singleResult();
//        return processService.toModel(model.getKey());
        throw new ServiceException("暂不支持");
    }

    private ProcessExt create(String name, String key) {
        return ProcessExt.builder()
                .name(name)
                .key(key)
                .type(0)
                .category(0)
                .permission(0)
                .status(0)
                .sort(10)
                .build();
    }
}
