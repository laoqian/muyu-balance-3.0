/*
 * Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author  于其先
 * @version 1.0.0
 * @since 2020-07-12 20:59:50
 */
package muyu.framework.process.service;

import muyu.common.bean.PageBean;
import muyu.framework.process.model.query.TaskExtQuery;
import muyu.framework.process.model.vo.TaskExtVO;

/**
 * 任务扩展
 *
 * @author 于其先
 * @since 2020-07-12 20:59:50
 */
public interface ITaskExtService {
    PageBean<TaskExtVO> findPage(TaskExtQuery taskExtQuery);
}
