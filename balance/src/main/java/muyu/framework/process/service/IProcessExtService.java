package muyu.framework.process.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.framework.process.model.po.ProcessExt;
import muyu.framework.process.model.vo.ProcessExtVO;
import muyu.framework.process.model.query.ProcessExtQuery;
import java.util.List;
import muyu.common.bean.PageBean;

import javax.xml.stream.XMLStreamException;

/**
 * <p>
 * 流程定义 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
public interface IProcessExtService extends IService<ProcessExt> {

    PageBean<ProcessExtVO> findPage(ProcessExtQuery query);

    List<ProcessExtVO> findList(ProcessExtQuery query);

    PageBean<ProcessExtVO> findTree(ProcessExtQuery query);

    boolean save(String name,String key);

    boolean toModel(String key) throws XMLStreamException;

}
