/*
 * Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author  于其先
 * @version 1.0.0
 * @since 2020-07-11 21:48:03
 */
package muyu.framework.process.service;

import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;

/**
 * 流程追踪信息
 *
 * @author 于其先
 * @since 2020-07-11 21:48:03
 */
public interface ITraceService {

    void readResource(String executionId, HttpServletResponse response) throws Exception ;

    ModelAndView historyData(String executionId);
}
