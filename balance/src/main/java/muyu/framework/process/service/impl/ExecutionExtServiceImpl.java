package muyu.framework.process.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import muyu.framework.process.model.po.ExecutionExt;
import muyu.framework.process.model.vo.ExecutionExtVO;
import muyu.framework.process.model.query.ExecutionExtQuery;
import muyu.framework.process.mapper.ExecutionExtMapper;
import muyu.framework.process.service.IExecutionExtService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.activiti.engine.HistoryService;
import org.activiti.engine.RuntimeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.util.List;
import muyu.common.bean.PageBean;

/**
 * <p>
 * 业务流程表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
@Service
public class ExecutionExtServiceImpl extends ServiceImpl<ExecutionExtMapper, ExecutionExt> implements IExecutionExtService {
    final private ModelMapper mapper = new ModelMapper();

    @Autowired
    RuntimeService runtimeService;

    @Autowired
    HistoryService historyService;

    private Wrapper<ExecutionExt> createWrapper(ExecutionExtQuery query){
        return new QueryWrapper<ExecutionExt>()
        .lambda();
    }

    @Override
    public PageBean<ExecutionExtVO> findPage(ExecutionExtQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<ExecutionExtVO> findList(ExecutionExtQuery query) {
        return mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<ExecutionExtVO>>(){}.getType());
    }

    @Override
    public PageBean<ExecutionExtVO> findTree(ExecutionExtQuery query) {
       return new PageBean<>(this.findList(query));
    }

    @Override
    public ExecutionExtVO create(ExecutionExt executionExt) {
        super.save(executionExt);
        return mapper.map(executionExt, ExecutionExtVO.class);
    }

    @Override
    public ExecutionExtVO update(ExecutionExt executionExt) {
        super.updateById(executionExt);
        return mapper.map(executionExt,ExecutionExtVO.class);
    }

    @Override
    public boolean isFinished(String procInstId) {
        long count = historyService.createHistoricProcessInstanceQuery().processInstanceId(procInstId).finished().count();
        return count > 0;
    }
}
