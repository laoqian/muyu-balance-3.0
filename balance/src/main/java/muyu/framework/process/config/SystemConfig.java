package muyu.framework.process.config;

import lombok.Data;

@Data
public class SystemConfig {
    /**
     * 业主单位上限
     */
    public static final int PROPRIETOR_NUM_MAX      = 5;

    /**
     * 工程上限
     */
    public static final int PROJECT_NUM_MAX         = 20;
    /**
     * 变更流程版本
     */
    public static final int CHANGE_VERSION          = 1;
    /**
     * 结算流程版本
     */
    public static final int BALANCE_VERSION         = 2;
    /**
     * 投资管理
     */
    public static final int INVESTMENT_VERSION      = 1;
    /**
     * 归概统计
     */
    public static final int BUDGET_VERSION      = 1;
    /**
     * 核销流程版本
     */
    public static final int DEDUCATION_VERSION      = 1;
    /**
     * 支付申请版本
     */
    public static final int PAY_APPLY_VERSION       = 2;
    /**
     * 支付证书版本
     */
    public static final int PAY_CERT_VERSION        = 5;

    /**
     * 用品采购
     */
    public static final int OA_PURCHASE_VERSION     = 2;

    /**
     * 普通申请
     */
    public static final int OA_APPLY_VERSION        = 2;

    /**
     * 差旅申请
     */
    public static final int OA_BUSINESS_TRIP_VERSION = 2;

    /**
     * 业务招待费
     */
    public static final int OA_HOSPITALITY_VERSION = 1;

    /**
     * 业务招待费申请
     */
    public static final int OA_HOSPITALITY_APPLY_VERSION = 1;

    /**
     * 现场交际费
     */
    public static final int OA_REBATE_VERSION = 1;

    /**
     * 出国补贴
     */
    public static final int OA_BONUS_VERSION = 1;

    /**
     * 费用报销
     */
    public static final int OA_EXPENSE_VERSION = 2;

    /**
     * 借款流程
     */
    public static final int OA_BORROW_VERSION = 1;
}
