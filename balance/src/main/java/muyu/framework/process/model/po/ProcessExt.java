package muyu.framework.process.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.*;
import muyu.common.bean.BaseEntity;
import java.time.LocalDateTime;

import lombok.experimental.Accessors;

/**
 * <p>
 * 流程定义
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("oa_process_ext")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProcessExt extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 标志
     */
    @TableField("key_")
    private String key;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 是否需要选择合同
     */
    private Integer category;

    /**
     * 授权
     */
    private Integer permission;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 创建人编号
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建人名称
     */
    @TableField(fill = FieldFill.INSERT)
    private String createName;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新人名称
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateName;

    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
