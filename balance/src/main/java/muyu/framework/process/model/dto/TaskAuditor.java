package muyu.framework.process.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class TaskAuditor implements Serializable{
    private static final long serialVersionUID = 1L;

    /*
     * 用户角色
     */
    String role;

    /**
     * 是否必须选择
     */
    Boolean need;

    /**
     * 任务名称
     */
    Object taskName;
}
