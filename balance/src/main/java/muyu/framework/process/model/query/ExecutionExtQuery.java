package muyu.framework.process.model.query;

import muyu.common.bean.BaseQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 业务流程表
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ExecutionExtQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 流程定义
     */
    private Long procKey;


    /**
     * 流程名称
     */
    private Long procName;

    /**
     * 流程实例
     */
    private Long procInstId;

    /**
     * 业务表编号
     */
    private Long businessId;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 流程版本
     */
    private Integer version;

    /**
     * 任务环节
     */
    private Long taskId;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 备注
     */
    private String remark;


}
