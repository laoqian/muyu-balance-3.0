package muyu.framework.process.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class ActInfo implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 任务信息
     */
    private Act act;


    /**
     * 流程表单地址
     */
    private String      href;

    /**
     * 用户任务下一级互斥网关流出条件信息
     */
    private List<TaskCondition> taskConditionList;

    /**
     * 是否具有启动流程权限
     */
    @Builder.Default
    private Boolean     processPermission = false;

    /**
     * 是否具有签收权限
     */
    @Builder.Default
    private Boolean     claimPermission  = false;

    /**
     * 是否具有操作权限
     */
    @Builder.Default
    private Boolean     executePermission  = false;

    @Builder.Default
    private Boolean     createEnable        = false;

    @Builder.Default
    private Boolean     submitEnable        = false;

    @Builder.Default
    private Boolean     saveEnable          = false;

    @Builder.Default
    private Boolean     passEnable          = false;

    @Builder.Default
    private Boolean     rejectEnable        = false;

    @Builder.Default
    private Boolean     delegateEnable      = false;

    @Builder.Default
    private Boolean     rollbackEnable      = false;

    @Builder.Default
    private Boolean     hideToolBar         = false;
    
    public void eval(){
        hideToolBar = !createEnable && !submitEnable && !saveEnable && !passEnable && !rejectEnable && !delegateEnable;
    }
}
