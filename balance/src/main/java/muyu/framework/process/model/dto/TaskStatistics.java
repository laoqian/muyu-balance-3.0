package muyu.framework.process.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class TaskStatistics implements Serializable {
    private static final long serialVersionUID = 1L;

    /**
     * 月份
     */
    private String month;

    /**
     * 已签收
     */

    @Builder.Default
    private Integer claimedNum = 0;

    /**
     * 已处理
     */
    @Builder.Default
    private Integer finishedNum = 0;

    /**
     * 未签收
     */
    @Builder.Default
    private Integer unClaimNum = 0;


    public void claimedIncrease(){
        this.claimedNum++;
    }


    public void finishedIncrease(){
        this.finishedNum++;
    }

    public void unClaimedIncrease(){
        this.unClaimNum++;
    }
}
