package muyu.framework.process.model.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ModelVO {

    /**
     * 名称
     */
    private String name;

    /**
     * 键值
     */
    private String key;

    /**
     * 描述
     */
    private String description;

    /**
     * 分类
     */
    private String category;

}
