package muyu.framework.process.model.dto;


import lombok.Data;

@Data
public class ProcessInfo<T> {
    /**
     * 流程信息
     */
    private Act     act;

    /**
     * 操作
     */
    private String action;

    /**
     * 业务数据
     */
    private T data;
}
