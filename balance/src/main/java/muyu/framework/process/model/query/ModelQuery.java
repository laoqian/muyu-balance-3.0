/*
 * Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author  于其先
 * @version 1.0.0
 * @since 2020-07-10 22:10:25
 */
package muyu.framework.process.model.query;

import muyu.common.bean.BaseQuery;

/**
 * 模型查询
 *
 * @author 于其先
 * @since 2020-07-10 22:10:25
 */
public class ModelQuery extends BaseQuery {
}
