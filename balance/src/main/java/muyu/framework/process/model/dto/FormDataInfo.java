package muyu.framework.process.model.dto;


import lombok.Builder;
import lombok.Data;
import org.activiti.engine.form.FormData;
import org.activiti.engine.form.FormProperty;

import java.io.Serializable;
import java.util.List;

@Builder
@Data
public class FormDataInfo implements FormData ,Serializable{
    private static final long serialVersionUID = 1L;

    String formKey;
    String deploymentId;
    List<FormProperty> formProperties;
}
