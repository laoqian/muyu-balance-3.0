package muyu.framework.process.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class TaskCondition implements Serializable{
    private static final long serialVersionUID = 1L;
    /*
     * 类型：0 ，退回 1 同意
     */
    Integer type;
    /**
     * 条件显示名称
     */
    String name;

    /**
     * 条件变量名
     */
    @Builder.Default
    String key = "pass";

    /**
     * 条件变量值
     */
    String value;

    String taskDefKey;
}
