package muyu.framework.process.model.query;

import java.time.LocalDateTime;
import muyu.common.bean.BaseQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 流程定义
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class ProcessExtQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 标志
     */
    private String key;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 是否需要选择合同
     */
    private Integer category;

    /**
     * 授权
     */
    private Integer permission;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
