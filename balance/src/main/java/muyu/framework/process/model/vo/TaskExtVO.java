package muyu.framework.process.model.vo;

import lombok.*;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 流程定义
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TaskExtVO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 标志
     */
    private String no;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private String contractName;

    /**
     * 流程名称
     */
    private String processName;

    /**
     * 任务名称
     */
    private  String taskName;

    /**
     * 签收人
     */
    private String assignee;

    /**
     * 创建日期
     */
    private Date createDate;

    /**
     * 状态
     */
    private Integer status;
}
