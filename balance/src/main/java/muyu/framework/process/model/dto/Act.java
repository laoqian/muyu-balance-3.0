
package muyu.framework.process.model.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import muyu.common.bean.BaseEntity;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author 于其先
 * @since  2018/1/14
 * @version 1.0.0
 */

@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@Data
public class Act extends BaseEntity {

	private static final long serialVersionUID = 1L;

	private String taskId; 		// 任务编号
	private String taskName; 	// 任务名称
	private String taskDefKey; 	// 任务定义Key（任务环节标识）

    private String procName; 	// 流程名称
    private String procInstId; 	// 流程实例ID
	private String procDefId; 	// 流程定义ID
	private String procDefKey; 	// 流程定义Key（流程定义标识）
	private String category; 	// 流程类型
	private Integer version; 	// 流程版本

	private String businessTable;	// 业务绑定Table
	private String actMapper;		// 业务绑定ID
	private Object businessInfo;	// 业务表信息

	private String status="todo"; 	    // 任务状态（todo/claim/finish）

	private int taskStatus; 	    // 任务状态（0:未签收,1：已签收 2:完成）

	private String comments; 		// 任务意见

	private String   assigneeId; 		// 任务执行人
	private String   assigneeName; 		// 任务执行人

	private String 		bsNo;  		/*业务信息编号*/
	private String 		bsName; 		/*业务信息名称*/

	String 				type;

	private Map<String,Object> vars = new HashMap<>(); 	// 流程变量

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date 		beginDate;	                // 开始查询日期

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date 		endDate;	                // 结束查询日期
    private String 		duration;	            // 持续时间

    private List<Act> 	historicList;     	//历史周转信息
}


