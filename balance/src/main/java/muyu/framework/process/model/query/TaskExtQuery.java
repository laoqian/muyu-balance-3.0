package muyu.framework.process.model.query;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseQuery;

import java.util.Date;

/**
 * <p>
 * 流程定义
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class TaskExtQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;


    /**
     * 合同编号
     */
    private String contractId;

    /**
     * 合同名称
     */
    private String contractName;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * 流程名称
     */
    private String processName;

    /**
     * 开始日期
     */
    private Date startDate;

    /**
     * 结束日期
     */
    private Date endDate;
}
