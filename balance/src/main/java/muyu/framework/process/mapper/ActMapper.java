/*
 * Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author  于其先
 * @version 1.0.0
 * @since 2020-07-10 21:39:34
 */
package muyu.framework.process.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.process.model.dto.Act;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;


/**
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author 于其先
 * @since  2020-07-10
 * @version 1.0.0
 */
public interface ActMapper extends BaseMapper<Object> {

	int updateProcInsIdById(Act act);

	int updateStatusById(@Param("businessTable") String businessTable, @Param("id") String id, @Param("status") Integer status);

    void updateRunTasks(@Param("procDefId") String procDefId,
                        @Param("key") String key,
                        @Param("version") int version);

    void updateExecutes(@Param("procDefId") String procDefId,
                        @Param("key") String key,
                        @Param("version") int version);

    void updateJobs(@Param("procDefId") String procDefId,
                    @Param("key") String key,
                    @Param("version") int version);

    void updateIdentityLink(@Param("procDefId") String procDefId,
                             @Param("key") String key,
                             @Param("version") int version);

    void updateTaskInst(@Param("procDefId") String procDefId,
                         @Param("key") String key,
                         @Param("version") int version);

    void updateProcInst(@Param("procDefId") String procDefId,
                         @Param("key") String key,
                         @Param("version") int version);

    void updateActInst(@Param("procDefId") String procDefId,
                        @Param("key") String key,
                        @Param("version") int version);

    @Update("update ${tableName} set version = #{version} where status != 2")
    void updateBusinessVersion(@Param("tableName") String tableName, @Param("version") Integer version);
}
