package muyu.framework.process.mapper;

import muyu.framework.process.model.po.ProcessExt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 流程定义 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
public interface ProcessExtMapper extends BaseMapper<ProcessExt> {

}
