package muyu.framework.process.mapper;

import muyu.framework.process.model.po.ExecutionExt;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 业务流程表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-07-11
 */
public interface ExecutionExtMapper extends BaseMapper<ExecutionExt> {

}
