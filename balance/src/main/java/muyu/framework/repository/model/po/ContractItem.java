package muyu.framework.repository.model.po;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;
import java.time.LocalDateTime;
import lombok.*;
import muyu.common.bean.TreeEntity;

/**
 * <p>
 * 合同台账
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
@TableName("pm_re_contract_item")
public class ContractItem extends TreeEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 业务表编号
     */
    private String businessId;

    /**
     * 项目编号
     */
    private String projectId;

    /**
     * 监理单位
     */
    private String supervisorId;

    /**
     * 名称
     */
    private String name;

    /**
     * 合同编号
     */
    private String contractNo;

    /**
     * 自编号
     */
    private String no;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 类别
     */
    private Integer category;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 概算金额
     */
    private BigDecimal estimateAmount;

    /**
     * 合同金额
     */
    private BigDecimal contractAmount;

    /**
     * 预算金额
     */
    private BigDecimal budgetAmount;

    /**
     * 执行金额
     */
    private BigDecimal executeAmount;

    /**
     * 材料价差
     */
    private BigDecimal materialMarginAmount;

    /**
     * 电费价差
     */
    private BigDecimal electricMarginAmount;

    /**
     * 税金
     */
    private BigDecimal taxAmount;

    /**
     * 合计
     */
    private BigDecimal totalAmount;

    /**
     * 执行部门
     */
    private String executorId;

    /**
     * 执行部门
     */
    private String executorName;

    /**
     * 施工单位
     */
    private String devCompanyId;

    /**
     * 施工单位名称
     */
    private String devCompanyName;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 签约日期
     */
    private LocalDateTime signDate;

    /**
     * 招标方式
     */
    private Integer bidType;

    /**
     * 招标类型
     */
    private Integer bidMode;

    /**
     * 资审方式
     */
    private Integer qualifyType;

    /**
     * 开始日期
     */
    private LocalDateTime startDate;

    /**
     * 竣工日期
     */
    private LocalDateTime endDate;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
