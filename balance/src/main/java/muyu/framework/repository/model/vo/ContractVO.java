package muyu.framework.repository.model.vo;

import java.math.BigDecimal;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;
import java.time.LocalDateTime;
import lombok.*;

/**
 * <p>
 * 合同
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContractVO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 业主单位
     */
    private Long companyId;

    /**
     * 业主单位名称
     */
    private String companyName;

    /**
     * 执行部门
     */
    private Long executorId;

    /**
     * 执行部门名称
     */
    private String executorName;

    /**
     * 施工单位
     */
    private Long devCompanyId;

    /**
     * 施工单位名称
     */
    private String devCompanyName;

    /**
     * 施工项目经理
     */
    private Long devManagerId;

    /**
     * 施工项目经理
     */
    private String devManagerName;

    /**
     * 监理单位
     */
    private Long supervisorId;

    /**
     * 监理单位名称
     */
    private String supervisorName;

    /**
     * 咨询单位
     */
    private Long consultId;

    /**
     * 咨询单位名称
     */
    private String consultName;

    /**
     * 设计单位
     */
    private Long designCompanyId;

    /**
     * 设计单位名称
     */
    private String designCompanyName;

    /**
     * 项目
     */
    private Long projectId;

    /**
     * 工程项目名称
     */
    private String projectName;

    /**
     * 概算
     */
    private Long estimateId;

    /**
     * 工程概算名称
     */
    private String estimateName;

    /**
     * 归概编号
     */
    private Long estimateSummaryId;

    /**
     * 归概名称
     */
    private String estimateSummaryName;

    /**
     * 自编号
     */
    private String no;

    /**
     * 名称
     */
    private String name;

    /**
     * 工程标段
     */
    private String section;

    /**
     * 工期
     */
    private Integer days;

    /**
     * 保修期
     */
    private LocalDateTime warrantyDate;

    /**
     * 签约日期
     */
    private LocalDateTime signDate;

    /**
     * 签约地
     */
    private String signPlace;

    /**
     * 开始日期
     */
    private LocalDateTime startDate;

    /**
     * 结束日期
     */
    private LocalDateTime endDate;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 类别
     */
    private Integer category;

    /**
     * 结算币种
     */
    private Integer currencyType;

    /**
     * 签约合同价
     */
    private BigDecimal amount;

    /**
     * 预算
     */
    private BigDecimal budgetAmount;

    /**
     * 执行金额
     */
    private BigDecimal executeAmount;

    /**
     * 税金
     */
    private BigDecimal taxAmount;

    /**
     * 内部序号
     */
    private String serial;

    /**
     * 状态
     */
    private Integer status;

    /**
     * 招标编号
     */
    private Integer bidNo;

    /**
     * 招标文件
     */
    private String bidFile;

    /**
     * 招标日期
     */
    private LocalDateTime bidDate;

    /**
     * 招标类型
     */
    private Integer bidMode;

    /**
     * 招标方式
     */
    private Integer bidType;

    /**
     * 开标日期
     */
    private LocalDateTime bidStartDate;

    /**
     * 招标结果
     */
    private String bidResult;

    /**
     * 结果批复
     */
    private String bidResultReply;

    /**
     * 中标通知书
     */
    private String bidNoticeNo;

    /**
     * 中标金额
     */
    private BigDecimal bidAmount;

    /**
     * 材料价差
     */
    private BigDecimal materialMarginAmount;

    /**
     * 电费价差
     */
    private BigDecimal electricMarginAmount;

    /**
     * 批复文号
     */
    private String replyDoc;

    /**
     * 批复日期
     */
    private LocalDateTime replyDate;

    /**
     * 批复文号
     */
    private String applyDoc;

    /**
     * 申请日期
     */
    private LocalDateTime applyDate;

    /**
     * 资质审查方式
     */
    private String qualifyType;

    /**
     * 联系电话
     */
    private String telephone;

    /**
     * 汇率系数
     */
    private BigDecimal exchangeRate;

    /**
     * 调整系数
     */
    private BigDecimal scaleRate;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
