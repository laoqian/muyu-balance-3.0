package muyu.framework.repository.model.po;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import muyu.common.annotations.SummaryByLevel;
import muyu.common.bean.TreeEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目概算表
 * </p>
 *
 * @author 于其先
 * @since 2020-06-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("pm_re_estimate")
@NoArgsConstructor
public class Estimate extends TreeEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 工程
     */
    private String projectId;

    /**
     * 工程名称
     */
    private String projectName;


    /**
     * 类别
     */
    private Integer type;


    /**
     * 名称
     */
    private String name;

    /**
     * 自编号
     */
    private String no;

    /**
     * 概算金额
     */
    @SummaryByLevel
    private BigDecimal amount;

    /**
     * 预算金额
     */
    @SummaryByLevel
    private BigDecimal budgetAmount;

    /**
     * 投资占比
     */
    @SummaryByLevel
    private BigDecimal percent;


    /**
     * 创建人编号
     */
    @TableField(fill = FieldFill.INSERT)
    private String createBy;

    /**
     * 创建人名称
     */
    @TableField(fill = FieldFill.INSERT)
    private String createName;

    /**
     * 创建日期
     */
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateBy;

    /**
     * 更新人名称
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private String updateName;

    /**
     * 更新日期
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;

    public Estimate(String id){
        super(id);
    }
}
