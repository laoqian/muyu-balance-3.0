package muyu.framework.repository.model.vo;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;
import java.time.LocalDateTime;
import lombok.*;

/**
 * <p>
 * 合同分摊表
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ContractDivideVO extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 合同
     */
    private Long contractId;

    /**
     * 合同名称
     */
    private String contractName;

    /**
     * 工程名称
     */
    private Long projectId;

    /**
     * 工程名称
     */
    private String projectName;

    /**
     * 概算名称
     */
    private Long estimateId;

    /**
     * 工程名称
     */
    private String estimateName;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 百分比
     */
    private BigDecimal percent;

    /**
     * 金额
     */
    private BigDecimal amount;

    /**
     * 合同总额
     */
    private BigDecimal contractAmount;

    /**
     * 备注
     */
    private String comment;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
