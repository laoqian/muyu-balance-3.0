package muyu.framework.repository.model.query;

import lombok.*;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseQuery;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目概算表
 * </p>
 *
 * @author 于其先
 * @since 2020-06-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EstimateQuery extends BaseQuery {

    private static final long serialVersionUID = 1L;

    /**
     * 工程
     */
    private String projectId;

    /**
     * 工程名称
     */
    private String projectName;

    /**
     * 父级编号
     */
    private Long parentId;

    /**
     * 所有父级编号
     */
    private String parentIds;

    /**
     * 层级
     */
    private Integer level;

    /**
     * 类别
     */
    private Integer type;

    /**
     * 叶子
     */
    private Boolean leaf;

    /**
     * 名称
     */
    private String name;

    /**
     * 自编号
     */
    private String no;

    /**
     * 概算金额
     */
    private BigDecimal amount;

    /**
     * 预算金额
     */
    private BigDecimal budgetAmount;

    /**
     * 投资占比
     */
    private BigDecimal percent;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
