package muyu.framework.repository.model.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import muyu.common.bean.TreeEntity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * <p>
 * 项目概算表
 * </p>
 *
 * @author 于其先
 * @since 2020-06-19
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class EstimateVO extends TreeEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 工程
     */
    private String projectId;

    /**
     * 工程名称
     */
    private String projectName;

    /**
     * 名称
     */
    private String name;

    /**
     * 自编号
     */
    private String no;

    /**
     * 概算金额
     */
    private BigDecimal amount;

    /**
     * 预算金额
     */
    private BigDecimal budgetAmount;

    /**
     * 投资占比
     */
    private BigDecimal percent;

    /**
     * 创建人编号
     */
    private String createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private String updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
