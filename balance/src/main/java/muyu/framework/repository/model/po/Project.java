package muyu.framework.repository.model.po;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import muyu.common.bean.BaseEntity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

/**
 * <p>
 * 工程项目表
 * </p>
 *
 * @author 于其先
 * @since 2020-06-16
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
@TableName("pm_re_project")
public class Project extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 业主公司
     */
    private String companyId;

    /**
     * 业务单位名称
     */
    private String companyName;

    /**
     * 设计单位
     */
    private String designerId;

    /**
     * 设计单位名称
     */
    private String designerName;

    /**
     * 编号
     */
    private String no;

    /**
     * 名称
     */
    private String name;

    /**
     * 类型
     */
    private Integer type;

    /**
     * 批复部门及文号
     */
    private String approveNo;

    /**
     * 概算金额
     */
    private BigDecimal estimateAmount;

    /**
     * 预算金额
     */
    private BigDecimal budgetAmount;

    /**
     * 开始日期
     */
    private LocalDate startDate;

    /**
     * 结束日期
     */
    private LocalDate endDate;

    /**
     * 合同工期
     */
    private Integer days;

    /**
     * 创建人编号
     */
    private Long createBy;

    /**
     * 创建人名称
     */
    private String createName;

    /**
     * 创建日期
     */
    private LocalDateTime createDate;

    /**
     * 更新人编号
     */
    private Long updateBy;

    /**
     * 更新人名称
     */
    private String updateName;

    /**
     * 更新日期
     */
    private LocalDateTime updateDate;

    /**
     * 备注
     */
    private String remarks;

    /**
     * 删除标志
     */
    private Boolean delFlag;


}
