package muyu.framework.repository.controller;


import muyu.framework.repository.model.query.ProjectQuery;
import muyu.framework.repository.model.vo.ProjectBatchVO;
import muyu.framework.repository.service.IProjectService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 工程项目表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-06-16
 */
@RestController
@RequestMapping("/repository/project")
public class ProjectController {
    private static final ModelMapper mapper = new ModelMapper();

    @Autowired
    IProjectService projectService;

    @GetMapping("/{id}")
    public Object get(@PathVariable String id){
        return projectService.getById(id);
    }

    @PostMapping("/save")
    public Object save(@RequestBody ProjectBatchVO bean){
        return projectService.save(bean);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @DeleteMapping("/{id}")
    public Object delete(@PathVariable String id){
        return projectService.removeById(id);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-page")
    public Object findPage(@RequestBody ProjectQuery query){
        return projectService.findPage(query);
    }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-list")
    public Object findList(@RequestBody ProjectQuery query){
        return projectService.findList(query);
    }
}
