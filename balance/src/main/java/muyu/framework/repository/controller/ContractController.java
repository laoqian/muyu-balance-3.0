package muyu.framework.repository.controller;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import org.springframework.web.bind.annotation.RestController;
import muyu.framework.repository.service.IContractService;
import muyu.framework.repository.model.po.Contract;
import muyu.framework.repository.model.vo.ContractVO;
import muyu.framework.repository.model.query.ContractQuery;

/**
 * <p>
 * 合同 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
@RestController
@RequestMapping("/repository/contract")
public class ContractController {
     private static final ModelMapper mapper = new ModelMapper();

     @Autowired
     IContractService contractService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return contractService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping
     public Object create(@RequestBody ContractVO contract){
         return contractService.create(mapper.map(contract, Contract.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PutMapping("/{id}")
     public Object update(@RequestBody ContractVO contract){
         return contractService.update(mapper.map(contract,Contract.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @DeleteMapping("/{id}")
     public Object delete(@PathVariable String id){
         return contractService.removeById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody ContractQuery query){
         return contractService.findPage(query);
     }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-tree")
    public Object findTree(@RequestBody ContractQuery query){
    return contractService.findTree(query);
    }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody ContractQuery query){
         return contractService.findList(query);
     }
}
