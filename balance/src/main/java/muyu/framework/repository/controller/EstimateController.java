package muyu.framework.repository.controller;

import muyu.framework.repository.model.po.Estimate;
import muyu.framework.repository.model.query.EstimateQuery;
import muyu.framework.repository.model.vo.EstimateVO;
import muyu.framework.repository.service.IEstimateService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 * 项目概算表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-06-19
 */
@RestController
@RequestMapping("/repository/estimate")
public class EstimateController {
     private static final ModelMapper mapper = new ModelMapper();

     @Autowired
     IEstimateService estimateService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return estimateService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping
     public Object create(@RequestBody EstimateVO estimate){
         return estimateService.create(mapper.map(estimate, Estimate.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PutMapping("/{id}")
     public Object update(@RequestBody EstimateVO estimate){
         return estimateService.update(mapper.map(estimate,Estimate.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @DeleteMapping("/{id}")
     public Object delete(@PathVariable String id){
         return estimateService.removeById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody EstimateQuery query){
         return estimateService.findPage(query);
     }

     @PostMapping("/find-tree")
     public Object findTree(@RequestBody EstimateQuery query){
          return estimateService.findTree(query);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody EstimateQuery query){
         return estimateService.findList(query);
     }
}
