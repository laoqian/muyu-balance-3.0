package muyu.framework.repository.controller;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import org.springframework.web.bind.annotation.RestController;
import muyu.framework.repository.service.IContractDivideService;
import muyu.framework.repository.model.po.ContractDivide;
import muyu.framework.repository.model.vo.ContractDivideVO;
import muyu.framework.repository.model.query.ContractDivideQuery;

/**
 * <p>
 * 合同分摊表 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
@RestController
@RequestMapping("/repository/contract-divide")
public class ContractDivideController {
     private static final ModelMapper mapper = new ModelMapper();

     @Autowired
     IContractDivideService contractDivideService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return contractDivideService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping
     public Object create(@RequestBody ContractDivideVO contractDivide){
         return contractDivideService.create(mapper.map(contractDivide, ContractDivide.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PutMapping("/{id}")
     public Object update(@RequestBody ContractDivideVO contractDivide){
         return contractDivideService.update(mapper.map(contractDivide,ContractDivide.class));
     }

     @Secured("ROLE_SUPER_ADMIN")
     @DeleteMapping("/{id}")
     public Object delete(@PathVariable String id){
         return contractDivideService.removeById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody ContractDivideQuery query){
         return contractDivideService.findPage(query);
     }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-tree")
    public Object findTree(@RequestBody ContractDivideQuery query){
    return contractDivideService.findTree(query);
    }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody ContractDivideQuery query){
         return contractDivideService.findList(query);
     }
}
