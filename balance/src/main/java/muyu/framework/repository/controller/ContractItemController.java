package muyu.framework.repository.controller;

import org.modelmapper.ModelMapper;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;

import org.springframework.web.bind.annotation.RestController;
import muyu.framework.repository.service.IContractItemService;
import muyu.framework.repository.model.po.ContractItem;
import muyu.framework.repository.model.vo.ContractItemVO;
import muyu.framework.repository.model.query.ContractItemQuery;

/**
 * <p>
 * 合同台账 前端控制器
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
@RestController
@RequestMapping("/repository/contract-item")
public class ContractItemController {
     private static final ModelMapper mapper = new ModelMapper();

     @Autowired
     IContractItemService contractItemService;

     @GetMapping("/{id}")
     public Object get(@PathVariable String id){
         return contractItemService.getById(id);
     }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-page")
     public Object findPage(@RequestBody ContractItemQuery query){
         return contractItemService.findPage(query);
     }

    @Secured("ROLE_SUPER_ADMIN")
    @PostMapping("/find-tree")
    public Object findTree(@RequestBody ContractItemQuery query){
        return contractItemService.findTree(query);
    }

     @Secured("ROLE_SUPER_ADMIN")
     @PostMapping("/find-list")
     public Object findList(@RequestBody ContractItemQuery query){
         return contractItemService.findList(query);
     }
}
