package muyu.framework.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.repository.model.po.Estimate;

/**
 * <p>
 * 项目概算表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-06-19
 */
public interface EstimateMapper extends BaseMapper<Estimate> {

}
