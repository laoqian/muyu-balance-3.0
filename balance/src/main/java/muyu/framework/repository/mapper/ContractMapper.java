package muyu.framework.repository.mapper;

import muyu.framework.repository.model.po.Contract;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 合同 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
public interface ContractMapper extends BaseMapper<Contract> {

}
