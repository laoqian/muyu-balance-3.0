package muyu.framework.repository.mapper;

import muyu.framework.repository.model.po.ContractDivide;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 合同分摊表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
public interface ContractDivideMapper extends BaseMapper<ContractDivide> {

}
