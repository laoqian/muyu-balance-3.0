package muyu.framework.repository.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.framework.repository.model.po.Project;

/**
 * <p>
 * 工程项目表 Mapper 接口
 * </p>
 *
 * @author 于其先
 * @since 2020-06-16
 */
public interface ProjectMapper extends BaseMapper<Project> {

}
