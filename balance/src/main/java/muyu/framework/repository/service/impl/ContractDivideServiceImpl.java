package muyu.framework.repository.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import muyu.framework.repository.model.po.ContractDivide;
import muyu.framework.repository.model.vo.ContractDivideVO;
import muyu.framework.repository.model.query.ContractDivideQuery;
import muyu.framework.repository.mapper.ContractDivideMapper;
import muyu.framework.repository.service.IContractDivideService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.util.List;
import muyu.common.bean.PageBean;

/**
 * <p>
 * 合同分摊表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
@Service
public class ContractDivideServiceImpl extends ServiceImpl<ContractDivideMapper, ContractDivide> implements IContractDivideService {
    final private ModelMapper mapper = new ModelMapper();

    private Wrapper<ContractDivide> createWrapper(ContractDivideQuery query){
        return new QueryWrapper<ContractDivide>()
        .lambda();
    }

    @Override
    public PageBean<ContractDivideVO> findPage(ContractDivideQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<ContractDivide> findList(ContractDivideQuery query) {
        return super.list(this.createWrapper(query));
    }

    @Override
    public PageBean<ContractDivideVO> findTree(ContractDivideQuery query) {
       return new PageBean<>( mapper.map(this.findList(query),new TypeToken<List<ContractDivideVO>>(){}.getType()));
    }

    @Override
    public ContractDivideVO create(ContractDivide contractDivide) {
        super.save(contractDivide);
        return mapper.map(contractDivide, ContractDivideVO.class);
    }

    @Override
    public ContractDivideVO update(ContractDivide contractDivide) {
        super.updateById(contractDivide);
        return mapper.map(contractDivide,ContractDivideVO.class);
    }
}
