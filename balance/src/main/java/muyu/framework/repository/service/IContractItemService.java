package muyu.framework.repository.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.framework.repository.model.po.ContractItem;
import muyu.framework.repository.model.po.Project;
import muyu.framework.repository.model.vo.ContractItemVO;
import muyu.framework.repository.model.query.ContractItemQuery;
import java.util.List;
import muyu.common.bean.PageBean;
/**
 * <p>
 * 合同台账 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
public interface IContractItemService extends IService<ContractItem> {

    PageBean<ContractItemVO> findPage(ContractItemQuery query);

    List<ContractItemVO> findList(ContractItemQuery query);

    PageBean<ContractItemVO> findTree(ContractItemQuery query);

    boolean rebuildByProject(Project project);

    boolean rebuild();
}
