package muyu.framework.repository.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.framework.repository.model.po.Contract;
import muyu.framework.repository.model.vo.ContractVO;
import muyu.framework.repository.model.query.ContractQuery;
import java.util.List;
import muyu.common.bean.PageBean;
/**
 * <p>
 * 合同 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
public interface IContractService extends IService<Contract> {

    PageBean<ContractVO> findPage(ContractQuery query);

    List<Contract> findList(ContractQuery query);

    PageBean<ContractVO> findTree(ContractQuery query);

    ContractVO create(Contract contract);

    ContractVO  update(Contract contract);
}
