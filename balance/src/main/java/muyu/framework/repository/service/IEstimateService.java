package muyu.framework.repository.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.BatchBean;
import muyu.common.bean.PageBean;
import muyu.framework.repository.model.po.Estimate;
import muyu.framework.repository.model.query.EstimateQuery;
import muyu.framework.repository.model.vo.EstimateVO;

import java.util.List;

/**
 * <p>
 * 项目概算表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-19
 */
public interface IEstimateService extends IService<Estimate> {

    PageBean<EstimateVO> findPage(EstimateQuery query);

    List<Estimate> findList(EstimateQuery query);

    PageBean<EstimateVO> findTree(EstimateQuery query);

    EstimateVO create(Estimate estimate);

    EstimateVO  update(Estimate estimate);

    boolean saveBatch(BatchBean<EstimateVO, EstimateQuery> bean);
}
