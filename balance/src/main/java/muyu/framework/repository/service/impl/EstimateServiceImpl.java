package muyu.framework.repository.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import muyu.common.bean.BatchBean;
import muyu.common.bean.PageBean;
import muyu.common.bean.TreeService;
import muyu.framework.repository.mapper.EstimateMapper;
import muyu.framework.repository.model.po.Estimate;
import muyu.framework.repository.model.query.EstimateQuery;
import muyu.framework.repository.model.vo.EstimateVO;
import muyu.framework.repository.service.IEstimateService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 项目概算表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-19
 */
@Service
public class EstimateServiceImpl extends TreeService<EstimateMapper, Estimate> implements IEstimateService {
    final private ModelMapper mapper = new ModelMapper();

    private Wrapper<Estimate> createWrapper(EstimateQuery query){
        return new QueryWrapper<Estimate>().lambda()
                .eq(StringUtils.isNotBlank(query.getProjectId()),Estimate::getProjectId,query.getProjectId());
    }

    @Override
    public PageBean<EstimateVO> findPage(EstimateQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<Estimate> findList(EstimateQuery query) {
        return super.list(this.createWrapper(query));
    }

    @Override
    public PageBean<EstimateVO> findTree(EstimateQuery query) {
        if(StringUtils.isBlank(query.getProjectId())){
            return PageBean.empty();
        }

        List<EstimateVO> list = mapper.map(this.findList(query),new TypeToken<List<EstimateVO>>(){}.getType());
        return PageBean.<EstimateVO>builder()
                .list(EstimateVO.create(list))
                .total((long)list.size())
                .build();
    }

    @Override
    public EstimateVO create(Estimate estimate) {
        super.save(estimate);
        return mapper.map(estimate, EstimateVO.class);
    }

    @Override
    public EstimateVO update(Estimate estimate) {
        super.updateById(estimate);
        return mapper.map(estimate,EstimateVO.class);
    }

    @Override
    public boolean saveBatch(BatchBean<EstimateVO, EstimateQuery> bean) {
        super.saveByBatchBean(bean.map(Estimate.class));

        Estimate root = new Estimate("0");
        root.addChildren(super.list(this.createWrapper(bean.getQuery())));

        super.summaryByLevel(root);
        super.saveBatch(root.toList());

        return true;
    }
}
