package muyu.framework.repository.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import muyu.framework.repository.model.po.Contract;
import muyu.framework.repository.model.po.ContractItem;
import muyu.framework.repository.model.vo.ContractVO;
import muyu.framework.repository.model.query.ContractQuery;
import muyu.framework.repository.mapper.ContractMapper;
import muyu.framework.repository.service.IContractService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.util.List;
import muyu.common.bean.PageBean;

/**
 * <p>
 * 合同 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
@Service
public class ContractServiceImpl extends ServiceImpl<ContractMapper, Contract> implements IContractService {
    final private ModelMapper mapper = new ModelMapper();

    private Wrapper<Contract> createWrapper(ContractQuery query){
        return new QueryWrapper<Contract>()
        .lambda();
    }

    @Override
    public PageBean<ContractVO> findPage(ContractQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<Contract> findList(ContractQuery query) {
        return super.list(this.createWrapper(query));
    }

    @Override
    public PageBean<ContractVO> findTree(ContractQuery query) {
       return new PageBean<>(mapper.map(this.findList(query),new TypeToken<List<ContractItem>>(){}.getType()));
    }

    @Override
    public ContractVO create(Contract contract) {
        super.save(contract);
        return mapper.map(contract, ContractVO.class);
    }

    @Override
    public ContractVO update(Contract contract) {
        super.updateById(contract);
        return mapper.map(contract,ContractVO.class);
    }
}
