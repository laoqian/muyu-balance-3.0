package muyu.framework.repository.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.framework.repository.model.po.ContractDivide;
import muyu.framework.repository.model.vo.ContractDivideVO;
import muyu.framework.repository.model.query.ContractDivideQuery;
import java.util.List;
import muyu.common.bean.PageBean;
/**
 * <p>
 * 合同分摊表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
public interface IContractDivideService extends IService<ContractDivide> {

    PageBean<ContractDivideVO> findPage(ContractDivideQuery query);

    List<ContractDivide> findList(ContractDivideQuery query);

    PageBean<ContractDivideVO> findTree(ContractDivideQuery query);

    ContractDivideVO create(ContractDivide contractDivide);

    ContractDivideVO  update(ContractDivide contractDivide);

}
