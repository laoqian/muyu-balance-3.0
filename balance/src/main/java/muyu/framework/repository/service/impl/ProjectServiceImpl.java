package muyu.framework.repository.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.bean.PageBean;
import muyu.framework.repository.mapper.ProjectMapper;
import muyu.framework.repository.model.po.Project;
import muyu.framework.repository.model.query.ProjectQuery;
import muyu.framework.repository.model.vo.ProjectBatchVO;
import muyu.framework.repository.model.vo.ProjectVO;
import muyu.framework.repository.service.IEstimateService;
import muyu.framework.repository.service.IProjectService;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;
import java.util.Optional;

/**
 * <p>
 * 工程项目表 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-16
 */
@Service
public class ProjectServiceImpl extends ServiceImpl<ProjectMapper, Project> implements IProjectService {

    final private ModelMapper mapper = new ModelMapper();

    @Autowired
    private IEstimateService estimateService;

    private Wrapper<Project> createWrapper(ProjectQuery query){
        return new QueryWrapper<Project>()
                .lambda()
                .like(StringUtils.isNotBlank(query.getCompanyName()),Project::getCompanyName,Optional.ofNullable(query.getCompanyName()).map(String::trim).orElse(null))
                .like(StringUtils.isNotBlank(query.getName()),Project::getName,Optional.ofNullable(query.getName()).map(String::trim).orElse(null))
                .eq(query.getCompanyId()!=null,Project::getCompanyId,query.getCompanyId());
    }

    @Override
    public PageBean<ProjectVO> findPage(ProjectQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<ProjectVO> findList(ProjectQuery query) {
        return mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<ProjectVO>>(){}.getType());
    }

    @Override
    public PageBean<ProjectVO> findTree(ProjectQuery query) {
        return new PageBean<>(this.findList(query));
    }

    @Override
    public boolean removeById(Serializable id) {
        return super.removeById(id);
    }

    @Override
    @Transactional
    public boolean save(ProjectBatchVO bean) {
        Project project = mapper.map(bean.getProject(),Project.class);

        if(project.getIsNewRecord()){
            super.save(project);
        }else{
            super.updateById(project);
        }

        bean.getBatch().forEach(t->{
                        t.setProjectId(project.getId());
                        t.setProjectName(project.getName());
                    });

        estimateService.saveBatch(bean.getBatch());

        return true;
    }
}
