package muyu.framework.repository.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.google.common.collect.Lists;
import muyu.common.bean.TreeService;
import muyu.framework.process.model.dto.ProcessDef;
import muyu.framework.repository.model.po.*;
import muyu.framework.repository.model.query.ContractDivideQuery;
import muyu.framework.repository.model.query.ContractQuery;
import muyu.framework.repository.model.query.EstimateQuery;
import muyu.framework.repository.model.vo.ContractItemVO;
import muyu.framework.repository.model.query.ContractItemQuery;
import muyu.framework.repository.mapper.ContractItemMapper;
import muyu.framework.repository.service.*;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import java.util.List;
import java.util.stream.Collectors;

import muyu.common.bean.PageBean;

import static java.util.stream.Collectors.toList;

/**
 * <p>
 * 合同台账 服务实现类
 * </p>
 *
 * @author 于其先
 * @since 2020-07-13
 */
@Service
public class ContractItemServiceImpl extends TreeService<ContractItemMapper, ContractItem> implements IContractItemService {
    final private ModelMapper mapper = new ModelMapper();

    @Autowired
    IProjectService projectService;

    @Autowired
    IEstimateService estimateService;

    @Autowired
    IContractService contractService;

    @Autowired
    IContractDivideService divideService;

    @Autowired
    IdentifierGenerator idGenerator;

    private Wrapper<ContractItem> createWrapper(ContractItemQuery query){
        return new QueryWrapper<ContractItem>()
        .lambda();
    }

    @Override
    public PageBean<ContractItemVO> findPage(ContractItemQuery query) {
        return PageBean.create(super.page(query.toPage(), this.createWrapper(query)));
    }

    @Override
    public List<ContractItemVO> findList(ContractItemQuery query) {
        return mapper.map(super.list(this.createWrapper(query)),new TypeToken<List<ContractItemVO>>(){}.getType());
    }

    @Override
    public PageBean<ContractItemVO> findTree(ContractItemQuery query) {
       return new PageBean<>(this.findList(query));
    }

    @Override
    public boolean rebuildByProject(Project project) {
        List<ContractItem> list = Lists.newArrayListWithCapacity(200);

        list.add(projectToItem(project));
        list.addAll(estimateToItem(project));
        list.addAll(contractToItem(project));
        list.addAll(contractDivideToItem(project));

        evaluate(list);

        return super.saveOrUpdateBatch(list,100);
    }

    public void evaluate(List<ContractItem> list){
        ContractItem root = new ContractItem();
        root.addChildren(list);
        super.summaryByLevel(root);
    }

    @Override
    public boolean rebuild() {
        List<Project> list = projectService.list();
        list.forEach(this::rebuildByProject);
        return true;
    }

    private ContractItem projectToItem(Project project){
        ContractItem item = ContractItem.builder()
                .businessId(project.getId())
                .name(project.getName())
                .type(0)
                .build();

        item.setParentId("0");
        item.setId(project.getId());

        return item;
    }

    private List<ContractItem> estimateToItem(Project project){
        List<Estimate> list = estimateService.findList(EstimateQuery.builder().projectId(project.getId()).build());

        return list.stream().map(t->{
            ContractItem item =  ContractItem.builder()
                    .businessId(t.getId())
                    .name(t.getName())
                    .type(1)
                    .build();

            item.setId(t.getId());
            item.setParentId(StringUtils.equals(t.getId(),"0")?t.getProjectId():t.getParentId());

            return item;
        }).collect(toList());
    }

    private List<ContractItem> contractToItem(Project project){
        List<Contract> list = contractService.findList(ContractQuery.builder().projectId(project.getId()).build());

        return list.stream().map(t->{
            ContractItem item =  ContractItem.builder()
                    .businessId(t.getId())
                    .name(t.getName())
                    .type(2)
                    .build();

            item.setId(t.getId());
            item.setParentId(t.getEstimateId());
            return item;
        }).collect(toList());

    }

    private List<ContractItem> contractDivideToItem(Project project){
        List<ContractDivide> list = divideService.findList(ContractDivideQuery.builder().projectId(project.getId()).build());

        return list.stream().map(t->{
            ContractItem item =   ContractItem.builder()
                    .businessId(t.getId())
                    .name(t.getContractName())
                    .type(3)
                    .build();

            item.setId(t.getId());
            item.setParentId(t.getEstimateId());

            return item;
        }).collect(toList());

    }
}
