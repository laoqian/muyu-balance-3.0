package muyu.framework.repository.service;

import com.baomidou.mybatisplus.extension.service.IService;
import muyu.common.bean.PageBean;
import muyu.framework.repository.model.po.Project;
import muyu.framework.repository.model.query.ProjectQuery;
import muyu.framework.repository.model.vo.ProjectBatchVO;
import muyu.framework.repository.model.vo.ProjectVO;

import java.util.List;

/**
 * <p>
 * 工程项目表 服务类
 * </p>
 *
 * @author 于其先
 * @since 2020-06-16
 */
public interface IProjectService extends IService<Project> {

    PageBean<ProjectVO> findPage(ProjectQuery query);

    List<ProjectVO> findList(ProjectQuery query);

    PageBean<ProjectVO> findTree(ProjectQuery query);

    boolean save(ProjectBatchVO bean);
}
