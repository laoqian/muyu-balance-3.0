package muyu.framework;

import com.yishuifengxiao.common.autoconfigure.Swagger2AutoConfiguration;
import de.codecentric.boot.admin.server.config.EnableAdminServer;
import muyu.common.util.ContextUtil;
import muyu.framework.common.listener.ServerInitListener;
import org.activiti.spring.boot.SecurityAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.context.MessageSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.EnableConfigurationProperties;


@EnableConfigurationProperties
@SpringBootApplication(exclude = {Swagger2AutoConfiguration.class,SecurityAutoConfiguration.class, MessageSourceAutoConfiguration.class})
@MapperScan("muyu.framework.**.mapper")
@EnableAdminServer
public class Application {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplicationBuilder(Application.class)
            .listeners(new ServerInitListener())
            .build();

        ContextUtil.setContext(app.run(args));
    }
}
