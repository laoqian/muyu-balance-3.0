const {override, addWebpackPlugin, fixBabelImports, addLessLoader} = require(
    'customize-cra');
const AntdDayjsWebpackPlugin = require('antd-dayjs-webpack-plugin');

module.exports = override(
    fixBabelImports('import', {
        libraryName: 'antd',
        libraryDirectory: 'es',
        style: false,
    }),
    addLessLoader({
        javascriptEnabled: true,
    }),
    addWebpackPlugin(new AntdDayjsWebpackPlugin())
);
