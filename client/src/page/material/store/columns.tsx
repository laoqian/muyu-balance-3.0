import {EditableColumnProps} from "../../../component/Table/EditableTable";
import {Material} from "../../../service/material/MaterialService";
import {Dialog} from "../../../component";
import SearchTree from "../../../component/Tree/SearchTree";
import OfficeService from "../../../service/system/SysOfficeService";
import {SysUser} from "../../../service/system/SysUserService";
import {message} from "antd";
import React from "react";

const columns: EditableColumnProps<Material>[] = [
    {
        label: '公司',
        name: 'companyName',
        ellipsis: true,
        editable: true,
        query: true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "company",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择公司",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择公司"),
                        afterOk: () => resolve(company)
                    });
                })
            },
            disabled:true
        },
        editRule: {required: true}
    },
    {
        label: '部门',
        name: 'officeName',
        ellipsis: true,
        editable: true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "office",
            popup: async ({companyId}: SysUser) => {
                if (!companyId) {
                    message.error("请先选择公司");
                    return Promise.reject();
                }

                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择部门",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={()=>OfficeService.findTreeData(companyId)}
                                              onSelect={(id, {node}) => company = node}/>,
                        afterOk: () => resolve(company)
                    });
                })
            }
        },
        editRule: {required: false}
    },
    {
        label: '编号',
        name: 'no',
        ellipsis: true,
        editable: true,
        width: 100,
        editRule: {maxLength: 20,required:true}
    },
    {
        label: '名字',
        name: 'name',
        ellipsis: true,
        editable: true,
        query: true,
        width: 100,
        editRule: {maxLength: 50,required:true}
    },
    {
        label: '类别',
        name: 'type',
        ellipsis: true,
        editable: true,
        width: 100,
        formatter:"select",
        editType:"select",
        formatOption:{type:"store.type"},
        editOption:{type:"store.type"},
        align:"center",
        editRule: {}
    },
    {
        label: '管理员',
        name: 'manager',
        ellipsis: true,
        editable: true,
        width: 100,
        align:"left",
        formatter:"text",
        editRule: {required:true}
    },
    {
        label: '备注',
        name: 'remarks',
        ellipsis: true,
        editable: true,
        width: 200,
        align:"left",
        formatter:"text",
        editRule: {}
    }
];

export default columns;
