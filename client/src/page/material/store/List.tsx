import React from "react";
import StoreService, {Store} from "../../../service/material/StoreService"
import columns from "./columns";
import {Dialog, EditableTable} from "../../../component";
import StoreForm from "./Form";
import {message} from "antd";
import TableUtil from "../../../component/Table/TableUtil";
import SystemService from "../../../service/system/SystemService";

export declare interface StoreListProps {
    onClick: (record: Store, event: any) => void
}

export declare interface StoreListState {
    query: object;
    record?: Store;
    refresh: boolean;
}

export default class List extends React.Component<StoreListProps, StoreListState> {
    tableRef = React.createRef<EditableTable<Store>>();

    state = {
        query: {},
        refresh: false
    };


    add = async () => {
        const { data } = await SystemService.getCurrentCompany();
        const record = this.tableRef.current?.util.getSelected() || {} as Store;

        delete record.id;

        Dialog.open({
            title: "概算新增",
            children: <StoreForm record={Object.assign({}, record, {companyId:data.id,companyName:data.name})}/>,
            width: 500,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "概算修改",
            children: <StoreForm record={record}/>,
            width: 500,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("概算删除", "确定要删除数据吗", async () => {
            const bean = await StoreService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<Store> =>this.tableRef.current?.util as TableUtil<Store>;

    render() {

        return (
            <React.Fragment>
                <EditableTable<Store>
                    ref={this.tableRef}
                    service={StoreService}
                    autoSave={true}
                    columns={columns}
                    isTree={true}
                    isEdit={true}
                    beforeAdd={ async (record:Store)=>{
                        const { data } = await SystemService.getCurrentCompany();
                        return Object.assign(record,{companyId:data.id,companyName:data.name});
                    }}
                    toolbar={
                        {
                            isMove:true,
                            isSave:true
                        }
                    }
                    loadData={StoreService.findTree}/>
            </React.Fragment>
        )
    }
}
