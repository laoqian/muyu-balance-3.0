import {EditableColumnProps} from "../../../component/Table/EditableTable";
import {Dialog} from "../../../component";
import SearchTree from "../../../component/Tree/SearchTree";
import StoreService from "../../../service/material/StoreService";
import {SysUser} from "../../../service/system/SysUserService";
import React from "react";
import {Invoice} from "../../../service/material/InvoiceService";

const columns: EditableColumnProps<Invoice>[] = [
    {
        label: '公司',
        name: 'companyName',
        ellipsis: true,
        editable: false,
        query: true,
        width: 150,
        span:12,
        editType: "pop-select",
        editOption: {
            prefix: "company",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择公司",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={StoreService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择公司"),
                        afterOk: () => resolve(company)
                    });
                })
            },
            disabled:true
        },
        editRule: {required: true}
    },
    {
        label: '库房',
        name: 'storeName',
        ellipsis: true,
        editable: true,
        width: 150,
        span:12,
        editType: "pop-select",
        editOption: {
            prefix: "store",
            popup: async ({companyId}: SysUser) => {
                return new Promise<any>(resolve => {
                    let store: any;
                    Dialog.open({
                        title: "请选择库房",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={()=>StoreService.findTreeData(companyId)}
                                              onSelect={(id, {node}) => store = node}/>,
                        afterOk: () => resolve(store)
                    });
                })
            }
        },
        editRule: {required: true}
    },
    {
        label: '编号',
        name: 'no',
        ellipsis: true,
        editable: true,
        width: 100,
        span:12,
        editRule: {maxLength: 20,required:true}
    },
    {
        label: '名称',
        name: 'name',
        ellipsis: true,
        editable: true,
        width: 100,
        span:12,
        editRule: {maxLength: 20,required:true}

    },
    {
        label: '管理员',
        name: 'manager',
        ellipsis: true,
        editable: true,
        width: 200,
        align:"left",
        formatter:"text",
        span:12,
        editRule: {}
    },
    {
        label: '日期',
        name: 'createDate',
        ellipsis: true,
        editable: true,
        width: 200,
        align:"left",
        formatter:"date",
        editType:"date",
        editOption:{pattern:"YYYY-MM-DD HH:mm:ss"},
        span:12,
        editRule: {}
    },
    {
        label: '备注',
        name: 'remarks',
        ellipsis: true,
        editable: true,
        width: 200,
        align:"left",
        formatter:"text",
        span:12,
        editRule: {}
    }
];

export default columns;
