import React from "react";
import InvoiceService, {Invoice} from "../../../service/material/InvoiceService"
import columns from "./columns";
import {Dialog, EditableTable} from "../../../component";
import InvoiceForm from "./Form";
import {message} from "antd";
import TableUtil from "../../../component/Table/TableUtil";
import SystemService from "../../../service/system/SystemService";

export declare interface InvoiceListProps {
    onClick: (record: Invoice, event: any) => void
}

export declare interface InvoiceListState {
    query: object;
    record?: Invoice;
    refresh: boolean;
}

export default class List extends React.Component<InvoiceListProps, InvoiceListState> {
    tableRef = React.createRef<EditableTable<Invoice>>();

    state = {
        query: {},
        refresh: false
    };


    add = async () => {
        const { data } = await SystemService.getCurrentCompany();
        const record = this.tableRef.current?.util.getSelected() || {} as Invoice;

        delete record.id;

        Dialog.open({
            title: "概算新增",
            children: <InvoiceForm record={Object.assign({}, record, {companyId:data.id,companyName:data.name})}/>,
            width: 500,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "概算修改",
            children: <InvoiceForm record={record}/>,
            width: 500,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("概算删除", "确定要删除数据吗", async () => {
            const bean = await InvoiceService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<Invoice> =>this.tableRef.current?.util as TableUtil<Invoice>;

    render() {

        return (
            <React.Fragment>
                <EditableTable<Invoice>
                    ref={this.tableRef}
                    service={InvoiceService}
                    autoSave={true}
                    columns={columns}
                    isTree={true}
                    isEdit={true}
                    beforeAdd={ async (record:Invoice)=>{
                        const { data } = await SystemService.getCurrentCompany();
                        return Object.assign(record,{companyId:data.id,companyName:data.name});
                    }}
                    toolbar={
                        {
                            isMove:true,
                            isSave:true
                        }
                    }
                    loadData={InvoiceService.findTree}/>
            </React.Fragment>
        )
    }
}
