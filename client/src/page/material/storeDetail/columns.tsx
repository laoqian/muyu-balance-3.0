import {EditableColumnProps} from "../../../component/Table/EditableTable";
import {Dialog} from "../../../component";
import SearchTree from "../../../component/Tree/SearchTree";
import OfficeService from "../../../service/system/SysOfficeService";
import React from "react";
import {StoreDetail} from "../../../service/material/StoreDetailService";

const columns: EditableColumnProps<StoreDetail>[] = [
    {
        label: '公司',
        name: 'companyName',
        ellipsis: true,
        editable: true,
        query: true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "company",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择公司",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择公司"),
                        afterOk: () => resolve(company)
                    });
                })
            },
            disabled:true
        },
        editRule: {required: true}
    },
    {
        label: '库房',
        name: 'storeName',
        ellipsis: true,
        editable: true,
        query: true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "store",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择库房",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择库房"),
                        afterOk: () => resolve(company)
                    });
                })
            },
            disabled:true
        },
        editRule: {required: true}
    },
    {
        label: '物资名称',
        name: 'materialName',
        ellipsis: true,
        editable: true,
        query: true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "material",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择物资名",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择物资"),
                        afterOk: () => resolve(company)
                    });
                })
            },
            disabled:true
        },
        editRule: {required: true}
    },
    {
        label: '当前数量',
        name: 'num',
        ellipsis: true,
        editable: true,
        width: 100,
        align: "right",
        editRule: {maxLength: 20,required:true}
    },
    {
        label: '累计入库数量',
        name: 'inboundNum',
        ellipsis: true,
        editable: true,
        width: 100,
        align: "right",
        editRule: {maxLength: 20,required:true}
    },
    {
        label: '累计出库数量',
        name: 'outboundNum',
        ellipsis: true,
        editable: true,
        width: 100,
        align: "right",
        editRule: {maxLength: 20,required:true}
    },
    {
        label: '预警数量',
        name: 'alarmNum',
        ellipsis: true,
        editable: true,
        query: false,
        width: 100,
        align: "right",
        editRule: {maxLength: 50,required:true}
    },
    {
        label: '备注',
        name: 'remarks',
        ellipsis: true,
        editable: true,
        width: 200,
        align:"left",
        formatter:"text",
        editRule: {}
    }
];

export default columns;
