import React from "react";
import StoreDetailService, {StoreDetail, StoreDetailQuery} from "../../../service/material/StoreDetailService"
import columns from "./columns";
import {Dialog, EditableTable, SearchBar} from "../../../component";
import StoreDetailForm from "./Form";
import {message} from "antd";
import TableUtil from "../../../component/Table/TableUtil";
import SystemService from "../../../service/system/SystemService";

export declare interface StoreDetailListProps {
    onClick: (record: StoreDetail, event: any) => void
}

export declare interface StoreDetailListState {
    query: object;
    record?: StoreDetail;
    refresh: boolean;
}

export default class List extends React.Component<StoreDetailListProps, StoreDetailListState> {
    tableRef = React.createRef<EditableTable<StoreDetail>>();

    state = {
        query: {},
        refresh: false
    };


    add = async () => {
        const { data } = await SystemService.getCurrentCompany();
        const record = this.tableRef.current?.util.getSelected() || {} as StoreDetail;

        delete record.id;

        Dialog.open({
            title: "概算新增",
            children: <StoreDetailForm record={Object.assign({}, record, {companyId:data.id,companyName:data.name})}/>,
            width: 500,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "概算修改",
            children: <StoreDetailForm record={record}/>,
            width: 500,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("概算删除", "确定要删除数据吗", async () => {
            const bean = await StoreDetailService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<StoreDetail> =>this.tableRef.current?.util as TableUtil<StoreDetail>;
    onQuery = (query: StoreDetailQuery) => this.getUtil().reload(query);

    render() {

        return (
            <React.Fragment>
                <SearchBar<StoreDetail> formStyle={{width:"1000px"}} columns={columns} onQuery={this.onQuery}/>
                <EditableTable<StoreDetail>
                    ref={this.tableRef}
                    service={StoreDetailService}
                    columns={columns}
                    loadData={StoreDetailService.findPage}/>
            </React.Fragment>
        )
    }
}
