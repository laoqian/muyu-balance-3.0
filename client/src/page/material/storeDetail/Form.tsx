import React from "react";
import StoreDetailService, {StoreDetail} from "../../../service/material/StoreDetailService"
import columns from "./columns";
import {WrappedForm} from "../../../component";
import {FormInstance} from "antd/lib/form";
import {message} from "antd";
import {ResultBean} from "../../../service/base/BaseService";

export declare interface StoreDetailFormProps {
    record: object;
    setHandler?: (ok: () => Promise<ResultBean<any>>, cancel?: () => void) => void
}

export declare interface StoreDetailFormState {

}

export default class StoreDetailForm extends React.Component<StoreDetailFormProps, StoreDetailFormState> {
    formRef = React.createRef<FormInstance>();
    save = async () => {
        const {record} = this.props;
        const data = await this.formRef.current?.validateFields();
        const bean = await StoreDetailService.save(Object.assign(record, data) as StoreDetail);
        if (bean.code === 200) {
            message.success(bean.msg);
        }

        return bean;
    };

    componentDidMount(): void {
        this.props.setHandler?.(this.save)
    }

    render() {
        const {record} = this.props;

        const props = {
            columns,
            record,
            span: 24,
            ref: this.formRef,
            style: {padding: "10px 20px"}
        };

        return (
            <WrappedForm {...props} />
        )
    }

}
