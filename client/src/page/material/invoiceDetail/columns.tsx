import {EditableColumnProps} from "../../../component/Table/EditableTable";
import {InvoiceDetail} from "../../../service/material/InvoiceDetailService";
import {Dialog} from "../../../component";
import SearchTree from "../../../component/Tree/SearchTree";
import {SysUser} from "../../../service/system/SysUserService";
import {message} from "antd";
import React from "react";
import MaterialService from "../../../service/material/MaterialService";

const columns: EditableColumnProps<InvoiceDetail>[] = [
    {
        label: '公司',
        name: 'companyName',
        ellipsis: true,
        editable: false,
        hidden:true,
        query: true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "company",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择公司",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={MaterialService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择公司"),
                        afterOk: () => resolve(company)
                    });
                })
            },
            disabled:true
        },
        editRule: {required: true}
    },
    {
        label: '库房',
        name: 'storeName',
        ellipsis: true,
        editable: false,
        hidden:true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "office",
            popup: async ({companyId}: SysUser) => {
                if (!companyId) {
                    message.error("请先选择公司");
                    return Promise.reject();
                }

                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择部门",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={()=>MaterialService.findTreeData()}
                                              onSelect={(id, {node}) => company = node}/>,
                        afterOk: () => resolve(company)
                    });
                })
            }
        },
        editRule: {required: false}
    }, {
        label: '材料',
        name: 'materialName',
        ellipsis: true,
        editable: true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "material",
            popup: async ({companyId}: SysUser) => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择材料",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={()=>MaterialService.findTreeData(companyId)}
                                              onSelect={(id, {node}) => company = node}/>,
                        afterOk: () => resolve(company)
                    });
                })
            }
        },
        editRule: {required: false}
    }, {
        label: '单据',
        name: 'invoice',
        ellipsis: true,
        editable: true,
        hidden:true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "office",
            popup: async ({companyId}: SysUser) => {
                if (!companyId) {
                    message.error("请先选择公司");
                    return Promise.reject();
                }

                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择部门",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={()=>MaterialService.findTreeData(companyId)}
                                              onSelect={(id, {node}) => company = node}/>,
                        afterOk: () => resolve(company)
                    });
                })
            }
        },
        editRule: {required: false}
    },
    {
        label: '单位',
        name: 'unit',
        ellipsis: true,
        editable: true,
        width: 100,
        editRule: {maxLength: 20,required:true}
    },
    {
        label: '数量',
        name: 'num',
        ellipsis: true,
        editable: true,
        width: 100,
        formatter:"number",
        formatOption:{precision:0},
        editRule: {maxLength: 20,required:true}
    },
    {
        label: '备注',
        name: 'remarks',
        ellipsis: true,
        editable: true,
        width: 200,
        align:"left",
        formatter:"text",
        editRule: {}
    }
];

export default columns;
