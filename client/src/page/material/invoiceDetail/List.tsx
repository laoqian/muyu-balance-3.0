import React from "react";
import InvoiceDetailService, {InvoiceDetail} from "../../../service/material/InvoiceDetailService"
import columns from "./columns";
import {Dialog, EditableTable} from "../../../component";
import InvoiceDetailForm from "./Form";
import {message} from "antd";
import TableUtil from "../../../component/Table/TableUtil";
import SystemService from "../../../service/system/SystemService";

export declare interface InvoiceDetailListProps {
    onClick: (record: InvoiceDetail, event: any) => void
}

export declare interface InvoiceDetailListState {
    query: object;
    record?: InvoiceDetail;
    refresh: boolean;
}

export default class List extends React.Component<InvoiceDetailListProps, InvoiceDetailListState> {
    tableRef = React.createRef<EditableTable<InvoiceDetail>>();

    state = {
        query: {},
        refresh: false
    };


    add = async () => {
        const { data } = await SystemService.getCurrentCompany();
        const record = this.tableRef.current?.util.getSelected() || {} as InvoiceDetail;

        delete record.id;

        Dialog.open({
            title: "概算新增",
            children: <InvoiceDetailForm record={Object.assign({}, record, {companyId:data.id,companyName:data.name})}/>,
            width: 500,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "概算修改",
            children: <InvoiceDetailForm record={record}/>,
            width: 500,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("概算删除", "确定要删除数据吗", async () => {
            const bean = await InvoiceDetailService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<InvoiceDetail> =>this.tableRef.current?.util as TableUtil<InvoiceDetail>;

    render() {

        return (
            <React.Fragment>
                <EditableTable<InvoiceDetail>
                    ref={this.tableRef}
                    service={InvoiceDetailService}
                    autoSave={true}
                    columns={columns}
                    isTree={true}
                    isEdit={true}
                    beforeAdd={ async (record:InvoiceDetail)=>{
                        const { data } = await SystemService.getCurrentCompany();
                        return Object.assign(record,{companyId:data.id,companyName:data.name});
                    }}
                    toolbar={
                        {
                            isMove:true,
                            isSave:true
                        }
                    }
                    loadData={InvoiceDetailService.findTree}/>
            </React.Fragment>
        )
    }
}
