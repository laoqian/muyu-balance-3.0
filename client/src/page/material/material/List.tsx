import React from "react";
import MaterialService, {Material, MaterialQuery} from "../../../service/material/MaterialService"
import columns from "./columns";
import {Dialog, EditableTable, SearchBar, Toolbar} from "../../../component";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons/lib";
import LocaleUtil from "../../../util/LocaleUtil";
import MaterialForm from "./Form";
import {message} from "antd";
import TableUtil from "../../../component/Table/TableUtil";

export declare interface MaterialListProps {
    onClick: (record: Material, event: any) => void
}

export declare interface MaterialListState {
    query: object;
    record?: Material;
    refresh: boolean;
}

export default class List extends React.Component<MaterialListProps, MaterialListState> {
    tableRef = React.createRef<EditableTable<Material>>();

    state = {
        query: {},
        refresh: false
    };


    add = () => {
        const record = this.tableRef.current?.util.getSelected() || {};
        Dialog.open({
            title: "概算新增",
            children: <MaterialForm record={Object.assign({}, record, {id: null})}/>,
            width: 600,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "概算修改",
            children: <MaterialForm record={record}/>,
            width: 600,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("概算删除", "确定要删除数据吗", async () => {
            const bean = await MaterialService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<Material> =>this.tableRef.current?.util as TableUtil<Material>;
    onQuery = (query: MaterialQuery) => this.getUtil().reload(query);

    render() {

        return (
            <React.Fragment>
                <SearchBar columns={columns} onQuery={this.onQuery}/>
                <Toolbar>
                    <Toolbar.Option type="primary" icon={<PlusOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.add}>{LocaleUtil.get("add")}</Toolbar.Option>
                    <Toolbar.Option type="primary" icon={<EditOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.modify}>{LocaleUtil.get("modify")}</Toolbar.Option>
                    <Toolbar.Option type="primary" danger icon={<DeleteOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.delete}>{LocaleUtil.get("delete")}</Toolbar.Option>
                </Toolbar>
                <EditableTable<Material>
                    ref={this.tableRef}
                    service={MaterialService}
                    autoSave={true}
                    columns={columns}
                    loadData={MaterialService.findPage}/>
            </React.Fragment>
        )
    }
}
