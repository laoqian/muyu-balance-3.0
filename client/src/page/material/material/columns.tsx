import {EditableColumnProps} from "../../../component/Table/EditableTable";
import {Material} from "../../../service/material/MaterialService";

const columns: EditableColumnProps<Material>[] = [
    {
        label: '编号',
        name: 'no',
        ellipsis: true,
        editable: true,
        width: 100,
        editRule: {maxLength: 20,required:true}
    },
    {
        label: '名字',
        name: 'name',
        ellipsis: true,
        editable: true,
        query: true,
        width: 150,
        editRule: {maxLength: 50,required:true}
    },
    {
        label: '类别',
        name: 'category',
        ellipsis: true,
        editable: true,
        width: 100,
        formatter:"select",
        editType:"select",
        formatOption:{type:"material.category"},
        editOption:{type:"material.category"},
        align:"center",
        editRule: {required:true}
    },
    {
        label: '单位',
        name: 'unit',
        ellipsis: true,
        editable: true,
        width: 100,
        formatter:"text",
        align:"left",
        editRule: {required:true}
    },
    {
        label: '规格',
        name: 'specification',
        ellipsis: true,
        editable: true,
        width: 100,
        formatter:"text",
        align:"left",
        editRule: {required:true}
    },
    {
        label: '备注',
        name: 'remarks',
        ellipsis: true,
        editable: true,
        width: 300,
        align:"left",
        formatter:"text",
        editRule: {}
    }
];

export default columns;
