import React from "react";
import StoreLogService, {StoreLog, StoreLogQuery} from "../../../service/material/StoreLogService"
import columns from "./columns";
import {Dialog, EditableTable, SearchBar} from "../../../component";
import StoreLogForm from "./Form";
import {message} from "antd";
import TableUtil from "../../../component/Table/TableUtil";
import SystemService from "../../../service/system/SystemService";

export declare interface StoreLogListProps {
    onClick: (record: StoreLog, event: any) => void
}

export declare interface StoreLogListState {
    query: object;
    record?: StoreLog;
    refresh: boolean;
}

export default class List extends React.Component<StoreLogListProps, StoreLogListState> {
    tableRef = React.createRef<EditableTable<StoreLog>>();

    state = {
        query: {},
        refresh: false
    };


    add = async () => {
        const { data } = await SystemService.getCurrentCompany();
        const record = this.tableRef.current?.util.getSelected() || {} as StoreLog;

        delete record.id;

        Dialog.open({
            title: "概算新增",
            children: <StoreLogForm record={Object.assign({}, record, {companyId:data.id,companyName:data.name})}/>,
            width: 500,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "概算修改",
            children: <StoreLogForm record={record}/>,
            width: 500,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("概算删除", "确定要删除数据吗", async () => {
            const bean = await StoreLogService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<StoreLog> =>this.tableRef.current?.util as TableUtil<StoreLog>;
    onQuery = (query: StoreLogQuery) => this.getUtil().reload(query);

    render() {

        return (
            <React.Fragment>
                <SearchBar<StoreLog> columns={columns} formStyle={{width:"1000px"}} onQuery={this.onQuery}/>
                <EditableTable<StoreLog>
                    ref={this.tableRef}
                    service={StoreLogService}
                    columns={columns}
                    loadData={StoreLogService.findPage}/>
            </React.Fragment>
        )
    }
}
