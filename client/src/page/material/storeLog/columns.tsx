import {EditableColumnProps} from "../../../component/Table/EditableTable";
import {Dialog} from "../../../component";
import SearchTree from "../../../component/Tree/SearchTree";
import OfficeService from "../../../service/system/SysOfficeService";
import React from "react";
import {StoreLog} from "../../../service/material/StoreLogService";
import FormatUtil from "../../../util/FormatUtil";

const columns: EditableColumnProps<StoreLog>[] = [
    {
        label: '公司',
        name: 'companyName',
        ellipsis: true,
        editable: true,
        query: false,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "company",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择公司",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择公司"),
                        afterOk: () => resolve(company)
                    });
                })
            },
            disabled:true
        },
        editRule: {required: true}
    },
    {
        label: '库房',
        name: 'storeName',
        ellipsis: true,
        editable: true,
        query: true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "store",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择库房",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择库房"),
                        afterOk: () => resolve(company)
                    });
                })
            },
            disabled:true
        },
        editRule: {required: true}
    },
    {
        label: '物资名称',
        name: 'materialName',
        ellipsis: true,
        editable: true,
        query: true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "material",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择物资名",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择物资"),
                        afterOk: () => resolve(company)
                    });
                })
            },
            disabled:true
        },
        editRule: {required: true}
    },    {
        label: '单据',
        name: 'invoiceName',
        ellipsis: true,
        editable: true,
        query: true,
        width: 150,
        editType: "pop-select",
        editOption: {
            prefix: "store",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择库房",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择库房"),
                        afterOk: () => resolve(company)
                    });
                })
            },
            disabled:true
        },
        editRule: {required: true}
    },
    {
        label: '类别',
        name: 'type',
        ellipsis: true,
        editable: true,
        width: 100,
        formatter:"select",
        formatOption:{type:"material.type"},
        align: "center",
        editRule: {maxLength: 20,required:true}
    },
    {
        label: '变更数量',
        name: 'num',
        ellipsis: true,
        editable: true,
        query: false,
        width: 100,
        formatter:"number",
        align: "right",
        render:(value)=><span style={{color:value>0?"red":"green"}}>{FormatUtil.format(value,"number",{precision:0})}</span>,
        editRule: {maxLength: 50,required:true}
    },
    {
        label: '变更前数量',
        name: 'beforeNum',
        ellipsis: true,
        editable: true,
        width: 100,
        formatter:"number",
        align: "right",
        editType:"text",
        formatOption:{precision:0},
        editOption:{},
        editRule: {}
    },
    {
        label: '当前数量',
        name: 'currentNum',
        ellipsis: true,
        editable: true,
        width: 100,
        formatter:"number",
        align: "right",
        editType:"text",
        formatOption:{precision:0},
        editRule: {required:true}
    },
    {
        label: '日期',
        name: 'createDate',
        ellipsis: true,
        editable: true,
        width: 200,
        align:"center",
        formatter:"text",
        editRule: {}
    },
    {
        label: '操作人',
        name: 'createName',
        ellipsis: true,
        editable: true,
        width: 200,
        align:"left",
        formatter:"text",
        editRule: {}
    },
    {
        label: '备注',
        name: 'remarks',
        ellipsis: true,
        editable: true,
        width: 200,
        align:"left",
        formatter:"text",
        editRule: {}
    }
];

export default columns;
