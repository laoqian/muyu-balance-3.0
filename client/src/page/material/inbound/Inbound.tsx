import React, {Fragment} from "react";
import invoiceColumns from "../invoice/columns";
import invoiceDetailColumns from "../invoiceDetail/columns";
import {Dialog, EditableTable, Toolbar, WrappedForm} from "../../../component";
import {FormInstance} from "antd/lib/form";
import {message} from "antd";
import {ResultBean} from "../../../service/base/BaseService";
import {PlusOutlined} from "@ant-design/icons/lib";
import LocaleUtil from "../../../util/LocaleUtil";
import InvoiceService, {Invoice} from "../../../service/material/InvoiceService";
import InvoiceDetailService, {InvoiceDetail} from "../../../service/material/InvoiceDetailService";

export declare interface InboundFormProps {
    record?: Invoice;
    setHandler?: (ok: () => Promise<ResultBean<any>>, cancel?: () => void) => void
}

export declare interface InboundFormState {
    record: Invoice;
}

export default class InboundForm extends React.Component<InboundFormProps, InboundFormState> {
    formRef = React.createRef<FormInstance>();
    tableRef = React.createRef<EditableTable<InvoiceDetail>>();

    state = {
        record: this.props.record || {isNewRecord: true, type: 0} as Invoice,
    };

    save = async () => {
        const {record} = this.state;
        const data = await this.formRef.current?.validateFields();

        Dialog.confirm("入库保存", "确定要保存入库明细吗？", async () => {
            const bean = await InvoiceService.saveByBatchBean({
                invoice: Object.assign(record, data),
                batch: {
                    query: {invoiceId: record.id},
                    list: this.tableRef.current?.util.save()
                } as any
            });

            if (bean.code === 200) {
                message.success(bean.msg);
                this.setState({record: bean.data});
            }

            return bean;
        })
    };

    render() {
        const {record} = this.state;

        const props = {
            columns: invoiceColumns,
            record,
            span: 12,
            ref: this.formRef,
            style: {padding: "10px 20px"}
        };

        return (
            <Fragment>
                <Toolbar>
                    <Toolbar.Option type="primary" icon={<PlusOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.save}>{LocaleUtil.get("save")}</Toolbar.Option>
                </Toolbar>
                <div style={{
                    padding: "10px 20px 0 20px",
                    border: "solid 1px #f0f0f0",
                    marginTop: "10px",
                    flex: "1",
                    display: "flex",
                    flexDirection: "column",
                    overflowY: "auto"
                }}>
                    <WrappedForm {...props} mode={"normal"}/>
                    <EditableTable<InvoiceDetail>
                        ref={this.tableRef}
                        service={InvoiceDetailService}
                        columns={invoiceDetailColumns}
                        isEdit={true}
                        query={record.isNewRecord ? {} : {storeId: record.id}}
                        toolbar={{isSave: false}}
                        loadData={InvoiceDetailService.findPage}/>
                </div>
            </Fragment>
        )
    }

}
