import React from "react";
import MenuService from "../../../service/system/SysMenuService"
import menuService, {SysMenu} from "../../../service/system/SysMenuService"
import columns from "./columns";
import {EditableTable} from "../../../component";


export declare interface MenuListProps {
    onClick: (record: SysMenu, event: any) => void
}

export declare interface MenuListState {
    record?: SysMenu;
}

export default class List extends React.Component<MenuListProps, MenuListState> {
    tableRef = React.createRef<EditableTable<SysMenu>>();

    render() {
        return (
            <React.Fragment>
                <EditableTable<SysMenu>
                    ref={this.tableRef}
                    service={MenuService}
                    autoSave={true}
                    columns={columns}
                    isEdit={true}
                    isTree={true}
                    toolbar={{expandNum: 8, isMove: true, isShift: true}}
                    loadData={menuService.findTree}/>
            </React.Fragment>
        )
    }
}
