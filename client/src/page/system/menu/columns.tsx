import {SysMenu} from "../../../service/system/SysMenuService";
import React from "react";
import {EditableColumnProps} from "../../../component/Table/EditableTable";
import WrappedCheckbox from "../../../component/Form/WrappedCheckbox";

const columns: EditableColumnProps<SysMenu>[] = [
    {
        label: '名称',
        name: 'name',
        ellipsis: true,
        editable: true,
        query:true,
        width: 100,
        editRule: {required: true, maxLength: 20}
    },
    {
        label: '国际化编码',
        name: 'code',
        ellipsis: true,
        editable: true,
        query:true,
        width: 100,
        editRule: {required: true, maxLength: 50}
    },
    {
        label: '链接',
        name: 'href',
        ellipsis: true,
        editable: true,
        query:true,
        width: 100,
        editRule: {maxLength: 200}
    },
    {
        label: '图标',
        ellipsis: true,
        editable: true,
        name: 'icon',
        width: 100,
        editRule: {maxLength: 50}
    },
    {
        label: '权限标志',
        ellipsis: true,
        editable: true,
        name: 'permission',
        width: 100,
        editRule: {maxLength: 50}
    },
    {
        label: '排序号',
        ellipsis: true,
        editable: true,
        name: 'sort',
        width: 50,
    },
    {
        label: '显示',
        name: 'status',
        ellipsis: true,
        editable: true,
        editType: "checkbox",
        render: (status: boolean) => <WrappedCheckbox disabled={true}  value={status} />,
        width: 50,
        editRule: {maxLength: 20}
    }
];

export default columns;
