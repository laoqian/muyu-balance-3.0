import {EditableColumnProps} from "../../../component/Table/EditableTable";
import {SysLocale} from "../../../service/system/SysLocaleService";
import LocaleUtil from "../../../util/LocaleUtil";

const columns: EditableColumnProps<SysLocale>[] = [
    {
        label: '语言',
        code: "locale.lang",
        name: 'lang',
        sorter: false,
        ellipsis: true,
        editable: true,
        query:true,
        editType: "radio",
        editOption: {type: "locale.lang"},
        render: (lang) => LocaleUtil.getByDict("locale.lang", lang),
        width: 100,
    },
    {
        label: '编码',
        code: "locale.code",
        name: 'code',
        sorter: false,
        ellipsis: true,
        editable: true,
        query:true,
        width: 100,
    },
    {
        label: '名称',
        code: "locale.name",
        name: 'name',
        ellipsis: true,
        editable: true,
        query:true,
        width: 100,
    },
    {
        label: '键值',
        code: "locale.value",
        ellipsis: true,
        editable: true,
        name: 'value',
        width: 100,
    }
];

export default columns;
