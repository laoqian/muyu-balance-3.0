import React from "react";
import columns from "./columns";
import {Dialog, EditableTable, SearchBar, Toolbar} from "../../../component";
import localeService, {SysLocale, SysLocaleQuery} from "../../../service/system/SysLocaleService";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons/lib";
import LocaleForm from "./Form";
import {message} from "antd";
import LocaleUtil from "../../../util/LocaleUtil";
import TableUtil from "../../../component/Table/TableUtil";

export declare interface LocaleListProps {
    onClick: (record: SysLocale, event: any) => void
}

export declare interface LocaleListState {
    record?: SysLocale;
    query: object;
    refresh: boolean;
}


export default class List extends React.Component<LocaleListProps, LocaleListState> {
    state = {
        query: {},
        refresh: false
    };

    tableRef = React.createRef<EditableTable<SysLocale>>();
    add = () => {
        const record = this.tableRef.current?.util.getSelected() || {};

        Dialog.open({
            title: LocaleUtil.get("locale.add.title"),
            children: <LocaleForm record={Object.assign({}, record, {id: null})}/>,
            width: 600,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error(LocaleUtil.get("data.unselect.notice"));
        }

        Dialog.open({
            title: LocaleUtil.get("locale.modify.title"),
            children: <LocaleForm record={record}/>,
            width: 600,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error(LocaleUtil.get("data.unselect.notice"));
        }

        Dialog.confirm(LocaleUtil.get("locale.delete.title"), LocaleUtil.get("data.delete.notice"), async () => {
            const bean = await localeService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<SysLocale> =>this.tableRef.current?.util as TableUtil<SysLocale>;
    onQuery = (query: SysLocaleQuery) => this.getUtil().reload(query);


    render() {

        return (
            <React.Fragment>
                <SearchBar columns={columns} onQuery={this.onQuery}/>
                <Toolbar>
                    <Toolbar.Option type="primary" icon={<PlusOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.add}>{LocaleUtil.get("add")}</Toolbar.Option>
                    <Toolbar.Option type="primary" icon={<EditOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.modify}>{LocaleUtil.get("modify")}</Toolbar.Option>
                    <Toolbar.Option type="primary" danger icon={<DeleteOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.delete}>{LocaleUtil.get("delete")}</Toolbar.Option>
                </Toolbar>
                <EditableTable<SysLocale>
                    ref={this.tableRef}
                    service={localeService}
                    autoSave={true}
                    columns={columns}
                    loadData={localeService.findPage}/>
            </React.Fragment>
        )
    }
}
