import React, {Dispatch, Fragment, SetStateAction} from "react"
import columns from "./columns";
import WrappedForm from "../../../component/Form/WrappedForm";
import {Button} from "antd";
import {FormProps} from "antd/es/form/Form";
import "./login.less"
import auth from "../../../service/system/AuthService"
import system from "../../../service/system/SystemService"
import Locale from "../../../component/Locale/Locale";
import {FormInstance} from "antd/lib/form";
import LocaleUtil from "../../../util/LocaleUtil";

interface LoginProps extends FormProps {
    setLogin: Dispatch<SetStateAction<boolean>>
}

interface LoginState {
    refresh: boolean
}

export default class Login extends React.Component<LoginProps, LoginState> {
    state = {
        refresh: false
    };

    formRef = React.createRef<FormInstance>();

    login = async () => {
        const user = await this.formRef.current?.validateFields();
        if (user) {
            await auth.login({username: user.username, password: user.password});
            await this.systemInit(true);
        }
    };

    systemInit = async (success: boolean) => {
        const {setLogin} = this.props;
        if (success) {
            const bean = await system.getConfig();
            if (bean && bean.code === 200) {
                setLogin(true);
            }
        } else {
            this.setState({refresh: true});
        }
    };

    async componentDidMount() {
        let success: boolean = false;
        try {
            await auth.refresh();
            success = true;
        } catch (e) {
            console.log(e)
        }

        await this.systemInit(success);
    }

    render() {
        const {refresh} = this.state;
        if (!refresh) {
            return <Fragment/>
        }

        return (
            <div style={{width: "100%", height: "100%"}}>
                <div className="login-bg">
                    <Locale/>
                </div>
                <div className="loginWrapper">
                    <h2>{LocaleUtil.get("title")}</h2>
                    <WrappedForm ref={this.formRef} record={{username: "于其先", password: "fly"}} columns={columns}/>
                    <Button type="primary" size="large" onClick={this.login}
                            block>{LocaleUtil.get("login.btn.text", "登录")}</Button>
                </div>
            </div>
        )
    }
}
