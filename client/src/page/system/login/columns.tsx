import {EditableColumnProps} from "../../../component/Table/EditableTable";
import React from "react";
import {LockOutlined, UserOutlined} from "@ant-design/icons/lib";


const columns: EditableColumnProps<any>[] = [
    {
        name: "username",
        label: "用户名",
        editType: "text",
        editable: true,
        editOption: {
            prefix: <UserOutlined/>,
            hideLabel: true
        },
        editRule: {required: true}
    },
    {
        name: "password",
        label: "密码",
        editType: "text",
        editable: true,
        editOption: {
            type: "password",
            prefix: <LockOutlined/>,
            hideLabel: true
        },
        editRule: {required: true}
    }
];

export default columns;
