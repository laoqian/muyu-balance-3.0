import React from "react";
import {Layout, Tabs} from "antd";
import "./home.less"
import Navigation from "./navigation/Navigation";
import SideMenu from "./sideMenu/SideMenu";
import {SysMenu} from "../../../service/system/SysMenuService";
import {connect} from "react-redux";
import store, {StoreState} from "../../../redux/store/configure";
import TabUtil, {TabsAction, TabsProps} from "../../../util/TabUtil";
import NotFound from "./notFound/NotFound";
import {castTo} from "../../../util/CommonUtil";
import LocaleUtil from "../../../util/LocaleUtil";


const {Header, Sider, Content} = Layout;

export declare interface HomePageProps {
    tabs: { activeKey: string, list: TabsProps[] },
}

export declare interface HomePageState {
    collapsed: boolean,
    menuList?: SysMenu[],
    activeKey?: string,
}


class Home extends React.Component<HomePageProps, HomePageState> {
    state = {
        collapsed: false,
        menuList: []
    };

    onCollapse = (collapsed: boolean) => {
        this.setState({collapsed});
    };

    setMenuList = (menuList: SysMenu[]) => {
        this.setState({menuList});
    };

    onChange = (activeKey: string) => {
        TabUtil.active(activeKey);
    };

    onEdit = (targetKey: any, action: "add" | "remove") => {
        if (action === "remove") {
            const {tabs} = this.props;
            TabUtil.close(tabs.list.filter(t => t.id === targetKey)[0]);
        }
    };

    cache = new Map<string,any>();

    renderContent = ({href, data}: TabsProps,active:boolean) => {
        try {
            let url: any = href || "";
            const i: number = href?.lastIndexOf("/") || -1;
            if (i !== -1) {
                const chr = url.charAt(i + 1);
                url = url.substr(0, i + 1) + chr.toUpperCase() + url.substr(i + 2);
            }

            const key = "../../../page" + url;

            let Page =  this.cache.get(key);
            if(Page === undefined){
                const node = require("../../../page" + url).default || NotFound;
                Page = React.memo(node);
                this.cache.set(key,Page)
            }

            return <Page data={data} active={active} locale={active?LocaleUtil.getLanguage():""} />
        } catch (e) {
            // console.error(e.toString());
            return <NotFound/>;
        }
    };

    componentDidMount(): void {
        const data = {
            id: "dashboard",
            href: "/sys/dashboard/dashboard",
            name: "系统首页",
            ename: "Home Page",
            closable: false
        };
        store.dispatch<TabsAction>({type: "TAB_ADD", data: castTo<TabsProps>(data)})
    }

    render() {
        const {collapsed, menuList} = this.state;
        const {tabs} = this.props;

        return (
            <Layout>
                <Header>
                    <Navigation setMenuList={this.setMenuList}/>
                </Header>
                <Layout>
                    <Sider collapsible collapsed={collapsed} onCollapse={this.onCollapse}>
                        <SideMenu menuList={menuList}/>
                    </Sider>
                    <Content>
                        <Tabs hideAdd animated type="editable-card" activeKey={tabs.activeKey} onChange={this.onChange}
                              onEdit={this.onEdit}>
                            {
                                tabs.list.map((tabProps) => {
                                    return (
                                        <Tabs.TabPane tab={tabProps.name} key={tabProps.id}
                                                      closable={tabProps.closable}>
                                            <div className="content-wrapper">
                                                <div className="content-wrapper-0">
                                                    {this.renderContent(tabProps,tabs.activeKey===tabProps.id)}
                                                </div>
                                            </div>
                                        </Tabs.TabPane>
                                    )
                                })
                            }
                        </Tabs>
                    </Content>
                </Layout>
            </Layout>
        )
    }
}

export default connect(({tabs}: StoreState) => ({tabs}), () => ({}))(Home)
