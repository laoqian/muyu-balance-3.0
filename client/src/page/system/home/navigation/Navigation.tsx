import React from "react";
import {Menu} from "antd"
import "./navigation.less"
import {config} from "../../../../service/system/SystemService"
import {SysMenu} from "../../../../service/system/SysMenuService";
import Locale from "../../../../component/Locale/Locale";
import LocaleUtil from "../../../../util/LocaleUtil";

export declare interface NavigationProps {
    setMenuList: any
}

export declare interface NavigationState {

}


export default class Navigation extends React.Component<NavigationProps, NavigationState> {
    onClick = (target: any) => {
        const {setMenuList} = this.props;
        const menu: SysMenu = config.menuList.filter(t => t.id === target.key)[0];
        setMenuList(menu.children)
    };

    componentDidMount(): void {
        const {setMenuList} = this.props;
        setMenuList(config.menuList[0].children)
    }

    render() {
        return (
            <div className="navigation">
                <span>{LocaleUtil.get("title")}</span>
                <Menu mode="horizontal" onClick={this.onClick}>
                    {config.menuList.map(({id, name, code}) => {
                        return <Menu.Item key={id}>{LocaleUtil.get(code, name)}</Menu.Item>;
                    })}
                </Menu>
                <Locale/>
            </div>
        );
    }
}
