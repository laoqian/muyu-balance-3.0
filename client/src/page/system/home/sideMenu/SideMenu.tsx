import React from "react";
import {Menu} from "antd";
import {SysMenu} from "../../../../service/system/SysMenuService";
import TabUtil from "../../../../util/TabUtil";
import * as icons from "@ant-design/icons"
import {getProperty} from "../../../../util/CommonUtil";
import LocaleUtil from "../../../../util/LocaleUtil";


export declare interface SideMenuProps {
    menuList: SysMenu[]

}

export declare interface SideMenuState {

}

export default class SideMenu extends React.Component<SideMenuProps, SideMenuState> {

    getMenu = (id: string, list?: SysMenu[]): any => {
        return list?.find(t => t.id === id) || list?.map(t => this.getMenu(id, t.children)).find(t => t && t.id === id)
    };

    onClick = ({key}: { item: any, key: string }) => {
        TabUtil.open(this.getMenu(key, this.props.menuList))
    };

    componentDidMount(): void {
    }

    render() {
        const {menuList} = this.props;
        const selectKey = menuList.length > 0 ? menuList[0].id : "";
        const DefaultIcon = getProperty<any>(icons, "AppleOutlined");

        return (
            <Menu mode="inline" defaultSelectedKeys={[selectKey]} theme="dark" onClick={this.onClick}>
                {menuList.map((t) => {
                        const Icon = getProperty<any>(icons, t.icon);

                        return (
                            <Menu.SubMenu key={t.id}
                                          title={
                                              <span>
                                                  {Icon ? <Icon/> : <DefaultIcon/>}
                                                  <span>{LocaleUtil.get(t.code, t.name)}</span>
                                              </span>
                                          }>
                                {t.children?.map(({id, code, name}) => <Menu.Item
                                    key={id}>{LocaleUtil.get(code, name)}</Menu.Item>)}
                            </Menu.SubMenu>
                        )
                    }
                )}
            </Menu>
        );
    }
}
