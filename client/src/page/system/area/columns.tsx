import {SysArea} from "../../../service/system/SysAreaService";
import {EditableColumnProps} from "../../../component/Table/EditableTable";

const columns: EditableColumnProps<SysArea>[] = [
    {
        label: '名称',
        name: 'name',
        ellipsis: true,
        editable: true,
        width: 200,
        editRule: {required: true, maxLength: 20}
    },
    {
        label: '类型',
        name: 'type',
        ellipsis: true,
        editable: true,
        formatter: "select",
        editType: "select",
        width: 200,
        formatOption: {type: "area.type"},
        editOption: {type: "area.type"},
        editRule: {maxLength: 20}
    },
    {
        label: '编码',
        name: 'code',
        ellipsis: true,
        editable: true,
        width: 300,
        editRule: {maxLength: 20}
    },
    {
        label: '排序号',
        ellipsis: true,
        editable: true,
        name: 'sort',
        width: 100,
    }
];

export default columns;
