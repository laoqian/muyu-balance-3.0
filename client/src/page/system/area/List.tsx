import React from "react";
import AreaService, {SysArea} from "../../../service/system/SysAreaService"
import columns from "./columns";
import {EditableTable} from "../../../component";

export declare interface AreaListProps {
    onClick: (record: SysArea, event: any) => void
}

export declare interface AreaListState {
    record?: SysArea;
}

export default class List extends React.Component<AreaListProps, AreaListState> {
    tableRef = React.createRef<EditableTable<SysArea>>();

    render() {
        return (
            <React.Fragment>
                <EditableTable<SysArea>
                    ref={this.tableRef}
                    service={AreaService}
                    autoSave={true}
                    columns={columns}
                    isEdit={true}
                    isTree={true}
                    toolbar={{expandNum: 8, isMove: true, isShift: true}}
                    loadData={AreaService.findTree}/>
            </React.Fragment>
        )
    }
}
