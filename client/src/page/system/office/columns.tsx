import OfficeService, {SysOffice} from "../../../service/system/SysOfficeService";
import {EditableColumnProps} from "../../../component/Table/EditableTable";
import {message, Tree} from "antd";
import {Dialog} from "../../../component";
import {toTreeData} from "../../../util/CommonUtil";
import React from "react";

const {DirectoryTree} = Tree;

const columns: EditableColumnProps<SysOffice>[] = [
    {
        label: '父级机构',
        name: 'parentId',
        ellipsis: true,
        editable: true,
        hidden: true,
        query: false,
        width: 200,
        editType:"pop-select",
        editOption:{
            prefix: "parent",
            getValue:async ({ parentId }:SysOffice)=>{
                let values;
                if(parentId && parentId!=="0"){
                    const {code, data} = await OfficeService.get(parentId);
                    if(code===200){
                        values = {
                            label:data.name,
                            value:data.id
                        }
                    }
                }

                return values;
            },
            popup: async () => {
                const {code, msg, data} = await OfficeService.findTree();
                if (code !== 200) {
                    message.error(msg);
                    return Promise.reject();
                }

                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择父级机构",
                        children: <DirectoryTree treeData={toTreeData(data.list)}
                                                 onSelect={(id, {node}) => company = node}/>,
                        afterOk: () => resolve(company)
                    });
                })
            }
        },
        editRule: { maxLength: 20}
    },
    {
        label: '名称',
        name: 'name',
        ellipsis: true,
        editable: true,
        query: true,
        width: 200,
        editRule: {required: true, maxLength: 100}
    },
    {
        label: '国际化编码',
        name: 'code',
        ellipsis: true,
        editable: true,
        query: false,
        width: 200,
        editOption:{span:16},
        editRule: {required: true, maxLength: 50}
    },
    {
        label: '类型',
        name: 'type',
        ellipsis: true,
        editable: true,
        width: 200,
        formatter:"select",
        editType:'select',
        formatOption:{type:"office.type"},
        editOption:{type:"office.type"},
        editRule: {maxLength: 20}
    },
    {
        label: '级别',
        name: 'grade',
        ellipsis: true,
        editable: true,
        width: 300,
        editRule: {maxLength: 20}
    },
    {
        label: '地址',
        name: 'address',
        ellipsis: true,
        editable: true,
        width: 400,
        editRule: {maxLength: 20}
    },
    {
        label: '电话',
        name: 'phone',
        ellipsis: true,
        editable: true,
        width: 100,
        editRule: {maxLength: 20}
    },
    {
        label: '是否业主',
        name: 'proprietor',
        hidden: true,
        ellipsis: true,
        editable: true,
        width: 100,
        formatter:"select",
        editType:"select",
        formatOption:{type: "sys.bool"},
        editOption:{type: "sys.bool"},
        editRule: {maxLength: 20}
    },
    {
        label: '排序号',
        ellipsis: true,
        editable: true,
        name: 'sort',
        width: 100,
    }
];

export default columns;
