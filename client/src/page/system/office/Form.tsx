import React from "react";
import OfficeService, {SysOffice} from "../../../service/system/SysOfficeService"
import columns from "./columns";
import {WrappedForm} from "../../../component";
import {FormInstance} from "antd/lib/form";
import {message} from "antd";
import {ResultBean} from "../../../service/base/BaseService";
import {castTo} from "../../../util/CommonUtil";

export declare interface OfficeListProps {
    record: object;
    setHandler?: (ok: () => Promise<ResultBean<any>>, cancel?: () => void) => void
}

export declare interface OfficeListState {

}

export default class OfficeForm extends React.Component<OfficeListProps, OfficeListState> {
    formRef = React.createRef<FormInstance>();
    save = async () => {
        const {record} = this.props;
        const data = await this.formRef.current?.validateFields();
        const bean = await OfficeService.save(castTo<SysOffice>(Object.assign(record, data)));
        if (bean.code === 200) {
            message.success(bean.msg);
        }

        return bean;
    };

    componentDidMount(): void {
        this.props.setHandler?.(this.save)
    }

    render() {
        const {record} = this.props;

        const props = {
            columns,
            record,
            span: 12,
            ref: this.formRef,
            style: {padding: "10px 20px"}
        };

        return (
            <WrappedForm {...props} />
        )
    }
}
