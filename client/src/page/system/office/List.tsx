import React from "react";
import OfficeService, {SysOffice, SysOfficeQuery} from "../../../service/system/SysOfficeService"
import columns from "./columns";
import {Dialog, EditableTable, SearchBar, Toolbar} from "../../../component";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons/lib";
import LocaleUtil from "../../../util/LocaleUtil";
import OfficeForm from "./Form";
import {message} from "antd";
import dictService from "../../../service/system/SysDictService";
import TableUtil from "../../../component/Table/TableUtil";

export declare interface OfficeListProps {
    onClick: (record: SysOffice, event: any) => void
}

export declare interface OfficeListState {
    query: object;
    record?: SysOffice;
    refresh: boolean;
}

export default class List extends React.Component<OfficeListProps, OfficeListState> {
    tableRef = React.createRef<EditableTable<SysOffice>>();

    state = {
        query: {},
        refresh: false
    };

    add = () => {
        const record = this.tableRef.current?.util.getSelected() || {};

        Dialog.open({
            title: "机构新增",
            children: <OfficeForm record={Object.assign({}, record, {id: null})}/>,
            width: 800,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "机构修改",
            children: <OfficeForm record={record}/>,
            width: 800,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("机构删除", "确定要删除数据吗", async () => {
            const bean = await dictService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };


    getUtil = ():TableUtil<SysOffice> =>this.tableRef.current?.util as TableUtil<SysOffice>;
    onQuery = (query: SysOfficeQuery) => this.getUtil().reload(query);

    render() {
        return (
            <React.Fragment>
                <SearchBar columns={columns} onQuery={this.onQuery} />
                <Toolbar>
                    <Toolbar.Option type="primary" icon={<PlusOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.add}>{LocaleUtil.get("add")}</Toolbar.Option>
                    <Toolbar.Option type="primary" icon={<EditOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.modify}>{LocaleUtil.get("modify")}</Toolbar.Option>
                    <Toolbar.Option type="primary" danger icon={<DeleteOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.delete}>{LocaleUtil.get("delete")}</Toolbar.Option>
                </Toolbar>
                <EditableTable<SysOffice>
                    ref={this.tableRef}
                    service={OfficeService}
                    autoSave={true}
                    columns={columns}
                    isEdit={false}
                    isTree={true}
                    toolbar={{expandNum: 8, isMove: true, isShift: true}}
                    loadData={OfficeService.findTree}/>
            </React.Fragment>
        )
    }
}
