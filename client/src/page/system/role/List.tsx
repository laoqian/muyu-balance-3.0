import React from "react";
import RoleService, {SysRole} from "../../../service/system/SysRoleService"
import MenuService from "../../../service/system/SysMenuService"
import RoleMenuService from "../../../service/system/SysRoleMenuService";

import columns from "./columns";
import {Dialog, EditableTable, SearchBar, Toolbar} from "../../../component";
import {DeleteOutlined, EditOutlined, IdcardOutlined, PlusOutlined} from "@ant-design/icons/lib";
import LocaleUtil from "../../../util/LocaleUtil";
import DictForm from "./Form";
import {message} from "antd";
import {castTo, clearChildren} from "../../../util/CommonUtil";
import SearchTree from "../../../component/Tree/SearchTree";
import TableUtil from "../../../component/Table/TableUtil";

export declare interface MenuListProps {
    onClick: (record: SysRole, event: any) => void
}

export declare interface MenuListState {
    query: object;
    record?: SysRole;
    refresh: boolean;
}

export default class List extends React.Component<MenuListProps, MenuListState> {
    tableRef = React.createRef<EditableTable<SysRole>>();

    state = {
        query: {},
        refresh: false
    };

    add = () => {
        const record = this.tableRef.current?.util.getSelected() || {};

        Dialog.open({
            title: "角色新增",
            children: <DictForm record={Object.assign({}, record, {id: null})}/>,
            width: 800,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "角色修改",
            children: <DictForm record={record}/>,
            width: 800,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("角色删除", "确定要删除数据吗", async () => {
            const bean = await RoleService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    grant= async ()=>{
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要授权的用户");
        }

        const role:SysRole = castTo<SysRole>(record);


        const roleMenuList = await RoleMenuService.listByRoleId(role.id);
        const menuIds = new Set<string>(roleMenuList?.map(({menuId})=>menuId));
        let menus:any[] =[];

        Dialog.open({
            title:"用户授权-" + role.name,
            width:"400px",
            children:<SearchTree
                           checkable
                           checkStrictly
                           style                = {{height:"600px",overflow:"auto"}}
                           defaultCheckedKeys   = {Array.from(menuIds)}
                           loadTree             ={MenuService.findMenuList}
                           onCheck              = {(keys,{checkedNodes})=>menus=checkedNodes}/>,
            ok:()=>RoleMenuService.grant({role, menuList:clearChildren(menus)})
        })
    };

    getUtil = ():TableUtil<SysRole> =>this.tableRef.current?.util as TableUtil<SysRole>;
    onQuery = (query: object)=>this.getUtil().reload(query);

    render() {
        return (
            <React.Fragment>
                <SearchBar columns={columns} onQuery={this.onQuery} formStyle={{width:"800px"}}/>
                <Toolbar>
                    <Toolbar.Option type="primary" icon={<PlusOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.add}>{LocaleUtil.get("add","添加")}</Toolbar.Option>
                    <Toolbar.Option type="primary" icon={<EditOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.modify}>{LocaleUtil.get("modify","修改")}</Toolbar.Option>
                    <Toolbar.Option type="primary" icon={<IdcardOutlined />} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.grant}>{LocaleUtil.get("grant","授权")}</Toolbar.Option>
                    <Toolbar.Option type="primary" danger icon={<DeleteOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.delete}>{LocaleUtil.get("delete","删除")}</Toolbar.Option>
                </Toolbar>
                <EditableTable<SysRole>
                    ref={this.tableRef}
                    service={RoleService}
                    autoSave={true}
                    columns={columns}
                    loadData={RoleService.findPage}/>
            </React.Fragment>
        )
    }
}
