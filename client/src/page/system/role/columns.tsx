import {SysRole} from "../../../service/system/SysRoleService";
import {EditableColumnProps} from "../../../component/Table/EditableTable";
import DictUtil from "../../../util/DictUtil";

const columns: EditableColumnProps<SysRole>[] = [
    {
        label: '公司',
        name: 'companyName',
        ellipsis: true,
        editable: true,
        query: true,
        width: 200,
        editRule: {required: true, maxLength: 20}
    },
    {
        label: '名字',
        name: 'name',
        ellipsis: true,
        editable: true,
        query: true,
        width: 300,
        editRule: {maxLength: 20}
    },
    {
        label: '英文名称',
        name: 'enName',
        ellipsis: true,
        editable: true,
        width: 400,
        editRule: {maxLength: 20}
    },
    {
        label: '状态',
        ellipsis: true,
        editable: false,
        name: 'status',
        render: (value = "") => DictUtil.getByValue("user.status", value),
        width: 100,
    }
];

export default columns;
