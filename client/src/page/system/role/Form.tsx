import React from "react";
import RoleService, {SysRole} from "../../../service/system/SysRoleService"
import columns from "./columns";
import {WrappedForm} from "../../../component";
import {FormInstance} from "antd/lib/form";
import {message} from "antd";
import {ResultBean} from "../../../service/base/BaseService";
import {castTo} from "../../../util/CommonUtil";

export declare interface RoleListProps {
    record: object;
    setHandler?: (ok: () => Promise<ResultBean<any>>, cancel?: () => void) => void
}

export declare interface RoleListState {

}

export default class RoleForm extends React.Component<RoleListProps, RoleListState> {
    formRef = React.createRef<FormInstance>();
    save = async () => {
        const {record} = this.props;
        const data = await this.formRef.current?.validateFields();
        const bean = await RoleService.save(castTo<SysRole>(Object.assign(record, data)));
        if (bean.code === 200) {
            message.success(bean.msg);
        }

        return bean;
    };

    componentDidMount(): void {
        this.props.setHandler?.(this.save)
    }

    render() {
        const {record} = this.props;
        const props = {
            columns,
            record,
            span: 24,
            ref: this.formRef,
            style: {padding: "10px 20px"}
        };

        return (
            <WrappedForm {...props} />
        )
    }

}
