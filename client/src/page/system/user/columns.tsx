import {SysUser} from "../../../service/system/SysUserService";
import {EditableColumnProps} from "../../../component/Table/EditableTable";
import DictUtil from "../../../util/DictUtil";
import {Dialog} from "../../../component";
import OfficeService from "../../../service/system/SysOfficeService"
import {message} from "antd";
import React from "react";
import SearchTree from "../../../component/Tree/SearchTree";

const columns: EditableColumnProps<SysUser>[] = [
    {
        label: '公司',
        name: 'companyName',
        ellipsis: true,
        editable: true,
        query: true,
        width: 200,
        editType: "pop-select",
        editOption: {
            prefix: "company",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择公司",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择公司"),
                        afterOk: () => resolve(company)
                    });
                })
            }
        },
        editRule: {required: true}
    },
    {
        label: '部门',
        name: 'officeName',
        ellipsis: true,
        editable: true,
        width: 200,
        editType: "pop-select",
        editOption: {
            prefix: "office",
            popup: async ({companyId}: SysUser) => {
                if (!companyId) {
                    message.error("请先选择公司");
                    return Promise.reject();
                }

                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择部门",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={()=>OfficeService.findTreeData(companyId)}
                                              onSelect={(id, {node}) => company = node}/>,
                        afterOk: () => resolve(company)
                    });
                })
            }
        },
        editRule: {required: false}
    },
    {
        label: '登录名',
        name: 'loginName',
        ellipsis: true,
        editable: true,
        query: true,
        width: 200,
        editRule: {required: true, maxLength: 20}
    },
    {
        label: '编号',
        name: 'no',
        ellipsis: true,
        editable: true,
        width: 200,
        editRule: {maxLength: 20}
    },
    {
        label: '名字',
        name: 'name',
        ellipsis: true,
        editable: true,
        query: true,
        width: 300,
        editRule: {maxLength: 20}
    },
    {
        label: '签字',
        name: 'signature',
        ellipsis: true,
        editable: false,
        width: 400,
        editRule: {maxLength: 20}
    },
    {
        label: '地址',
        name: 'address',
        ellipsis: true,
        editable: true,
        width: 100,
        editRule: {maxLength: 20}
    },
    {
        label: '电话',
        name: 'phone',
        ellipsis: true,
        editable: true,
        width: 100,
        editRule: {maxLength: 20}
    },
    {
        label: '手机',
        ellipsis: true,
        editable: true,
        name: 'mobile',
        width: 100,
    },
    {
        label: '邮箱',
        ellipsis: true,
        editable: true,
        name: 'email',
        width: 100,
    },
    {
        label: '照片',
        ellipsis: true,
        editable: false,
        name: 'photo',
        width: 100,
    },
    {
        label: '类别',
        ellipsis: true,
        editable: false,
        name: 'type',
        formatter: "select",
        editType: "select",
        formatOption: {type: "user.type"},
        editOption: {type: "user.type"},
        width: 100,
    },
    {
        label: '状态',
        ellipsis: true,
        editable: false,
        name: 'status',
        render: (value = "") => DictUtil.getByValue("user.status", value),
        width: 100,
    }
];

export default columns;
