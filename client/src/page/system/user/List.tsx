import React from "react";
import UserService, {SysUser, SysUserQuery} from "../../../service/system/SysUserService"
import RoleService from "../../../service/system/SysRoleService"
import UserRoleService from "../../../service/system/SysUserRoleService"
import columns from "./columns";
import {Dialog, EditableTable, SearchBar, Toolbar} from "../../../component";
import {DeleteOutlined, EditOutlined, IdcardOutlined, PlusOutlined} from "@ant-design/icons/lib";
import LocaleUtil from "../../../util/LocaleUtil";
import dictService from "../../../service/system/SysDictService";
import UserForm from "./Form";
import {message} from "antd";
import {castTo} from "../../../util/CommonUtil";
import SearchTree from "../../../component/Tree/SearchTree";
import TableUtil from "../../../component/Table/TableUtil";

export declare interface MenuListProps {
    onClick: (record: SysUser, event: any) => void
}

export declare interface MenuListState {
    query: object;
    record?: SysUser;
    refresh: boolean;
}

export default class List extends React.Component<MenuListProps, MenuListState> {
    tableRef = React.createRef<EditableTable<SysUser>>();

    state = {
        query: {},
        refresh: false
    };

    add = () => {
        const record = this.tableRef.current?.util.getSelected() || {};
        Dialog.open({
            title: "用户新增",
            children: <UserForm record={Object.assign({}, record, {id: null})}/>,
            width: 800,
            afterOk: this.tableRef.current?.util.reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "用户修改",
            children: <UserForm record={record}/>,
            width: 800,
            afterOk: this.tableRef.current?.util.reload
        })
    };

    delete = async () => {
        const util = this.tableRef.current?.util;
        const record = util?.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("用户删除", "确定要删除数据吗", async () => {
            const bean = await dictService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                await util?.reload();
            }

            return bean;
        })
    };

    grant= async ()=>{
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要授权的用户");
        }

        const user:SysUser = castTo<SysUser>(record);
        const userRoleList = await UserRoleService.listByUserId(user.id);
        const roleIds = new Set<string>(userRoleList?.map(({roleId})=>roleId));
        let roleList:any[] =[];

        Dialog.open({
            title:"用户授权-"+user.name,
            width:"400px",
            children:<SearchTree checkable
                                 style                = {{height:"600px",overflow:"auto"}}
                                 loadTree             = {RoleService.findRoleList}
                                 defaultCheckedKeys   = {Array.from(roleIds)}
                                 onCheck              = {(keys,{checkedNodes})=>roleList=checkedNodes}/>,
            ok:()=>UserRoleService.grant({user,roleList})
        })
    };

    getUtil = ():TableUtil<SysUser> =>this.tableRef.current?.util as TableUtil<SysUser>;
    onQuery = (query: SysUserQuery) => this.getUtil().reload(query);

    render() {
        const {query} = this.state;

        return (
            <React.Fragment>
                <SearchBar columns={columns} onQuery={this.onQuery}/>
                <Toolbar>
                    <Toolbar.Option type="primary" icon={<PlusOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.add}>{LocaleUtil.get("add")}</Toolbar.Option>
                    <Toolbar.Option type="primary" icon={<EditOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.modify}>{LocaleUtil.get("modify")}</Toolbar.Option>
                    <Toolbar.Option type="primary" icon={<IdcardOutlined />} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.grant}>{LocaleUtil.get("grant","授权")}</Toolbar.Option>
                    <Toolbar.Option type="primary" danger icon={<DeleteOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.delete}>{LocaleUtil.get("delete")}</Toolbar.Option>
                </Toolbar>
                <EditableTable<SysUser>
                    ref         = {this.tableRef}
                    service     = {UserService}
                    autoSave    = {true}
                    columns     = {columns}
                    query       = { query}
                    loadData    = {UserService.findPage}/>
            </React.Fragment>
        )
    }
}
