import React from "react";
import dictService, {SysDict} from "../../../service/system/SysDictService"
import columns from "./columns";
import {WrappedForm} from "../../../component";
import {FormInstance} from "antd/lib/form";
import {message} from "antd";
import {ResultBean} from "../../../service/base/BaseService";
import {castTo} from "../../../util/CommonUtil";

export declare interface DictListProps {
    record: object;
    setHandler?: (ok: () => Promise<ResultBean<any>>, cancel?: () => void) => void
}

export declare interface DictListState {

}

export default class DictForm extends React.Component<DictListProps, DictListState> {
    formRef = React.createRef<FormInstance>();
    save = async () => {
        const {record} = this.props;
        const data = await this.formRef.current?.validateFields();
        const bean = await dictService.save(castTo<SysDict>(Object.assign(record, data)));
        if (bean.code === 200) {
            message.success(bean.msg);
        }

        return bean;
    };

    componentDidMount(): void {
        this.props.setHandler?.(this.save)
    }

    render() {
        const {record} = this.props;

        const props = {
            columns,
            record,
            span: 12,
            ref: this.formRef,
            style: {padding: "10px 20px"}
        };

        return (
            <WrappedForm {...props} />
        )
    }

}
