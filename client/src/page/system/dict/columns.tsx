import {SysDict} from "../../../service/system/SysDictService";
import {EditableColumnProps} from "../../../component/Table/EditableTable";

const columns: EditableColumnProps<SysDict>[] = [
    {
        label: '标签',
        name: 'name',
        ellipsis: true,
        editable: true,
        sorter: false,
        query:true,
        span: 8,
        width: 100,
    },
    {
        label: '编码',
        key: "dict.type",
        name: 'type',
        sorter: false,
        ellipsis: true,
        editable: true,
        query:true,
        span: 8,
        width: 100,
    },
    {
        label: '键值',
        name: 'value',
        sorter: false,
        ellipsis: true,
        editable: true,
        span: 8,
        width: 50,
    },
    {
        label: '国际化编码',
        key: "dict.code",
        name: 'code',
        sorter: false,
        ellipsis: true,
        editable: true,
        query:true,
        span: 8,
        width: 100,
    },

    {
        label: '扩展字段1',
        name: 'external1',
        ellipsis: true,
        editable: true,
        sorter: false,
        width: 50,
    },
    {
        label: '扩展字段1描述',
        name: 'external1Desc',
        ellipsis: true,
        editable: true,
        sorter: false,
        width: 80,
    },
    {
        label: '扩展字段2',
        name: 'external2',
        ellipsis: true,
        editable: true,
        sorter: false,
        width: 50,
    },
    {
        label: '扩展字段2描述',
        name: 'external2Desc',
        ellipsis: true,
        editable: true,
        sorter: false,
        width: 80,
    },
    {
        label: '描述',
        name: 'description',
        ellipsis: true,
        editable: true,
        sorter: false,
        width: 80,
    },
    {
        label: '排序号',
        name: 'sort',
        ellipsis: true,
        editable: true,
        sorter: false,
        width: 50,
    }
];


export default columns;
