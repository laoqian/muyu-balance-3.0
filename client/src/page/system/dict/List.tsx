import React from "react";
import dictService, {SysDict, SysDictQuery} from "../../../service/system/SysDictService"
import columns from "./columns";
import {Dialog, EditableTable, SearchBar, Toolbar} from "../../../component";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons/lib";
import {message} from "antd";
import DictForm from "./Form";
import LocaleUtil from "../../../util/LocaleUtil";
import TableUtil from "../../../component/Table/TableUtil";

export declare interface DictListProps {
    onClick: (record: SysDict, event: any) => void
}

export declare interface DictListState {
    query: object;
    record?: SysDict;
    refresh: boolean;
}


export default class List extends React.Component<DictListProps, DictListState> {
    state = {
        query: {},
        refresh: false
    };

    tableRef = React.createRef<EditableTable<SysDict>>();

    add = () => {
        const record = this.tableRef.current?.util.getSelected() || {};

        Dialog.open({
            children: <DictForm record={Object.assign({}, record, {id: null})}/>,
            title: "字典新增",
            width: 800,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record =  this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "字典修改",
            children: <DictForm record={record}/>,
            width: 800,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("字典删除", "确定要删除数据吗", async () => {
            const bean = await dictService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<SysDict> =>this.tableRef.current?.util as TableUtil<SysDict>;
    onQuery = (query: SysDictQuery) => this.getUtil().reload(query);

    render() {
        return (
            <React.Fragment>
                <SearchBar columns={columns} onQuery={this.onQuery}/>
                <Toolbar>
                    <Toolbar.Option type="primary" icon={<PlusOutlined/>}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.add}>
                        {LocaleUtil.get("add")}
                    </Toolbar.Option>
                    <Toolbar.Option type="primary" icon={<EditOutlined/>}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.modify}>
                        {LocaleUtil.get("modify")}
                    </Toolbar.Option>
                    <Toolbar.Option type="primary"
                                    danger
                                    icon={<DeleteOutlined/>}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.delete}>
                        {LocaleUtil.get("delete")}
                    </Toolbar.Option>
                </Toolbar>
                <EditableTable<SysDict>
                    ref = {this.tableRef}
                    service={dictService}
                    autoSave={true}
                    columns={columns}
                    loadData={dictService.findPage}/>
            </React.Fragment>
        )
    }

}
