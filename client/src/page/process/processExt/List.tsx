import React from "react";
import ProcessExtService, {ProcessExt, ProcessExtQuery} from "../../../service/process/ProcessExtService"
import columns from "./columns";
import {Dialog, EditableTable, SearchBar, Toolbar} from "../../../component";
import {
    CheckOutlined,
    DeleteOutlined,
    EditOutlined,
    RollbackOutlined
} from "@ant-design/icons/lib";
import LocaleUtil from "../../../util/LocaleUtil";
import ProcessExtForm from "./Form";
import {message} from "antd";
import TableUtil from "../../../component/Table/TableUtil";

export declare interface ProcessExtListProps {
    onClick: (record: ProcessExt, event: any) => void
}

export declare interface ProcessExtListState {
    query: object;
    record?: ProcessExt;
    refresh: boolean;
}

export default class List extends React.Component<ProcessExtListProps, ProcessExtListState> {
    tableRef = React.createRef<EditableTable<ProcessExt>>();

    state = {
        query: {},
        refresh: false
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "模型修改",
            children: <ProcessExtForm record={record}/>,
            width: 600,
            afterOk: this.getUtil().reload
        })
    };

    toModel = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中流程");
        }

        Dialog.confirm("转为模型", `确定要流程【${record.name}】转为模型吗？`, async () => {
            const bean = await ProcessExtService.toModel(record.key);
            if (bean.code === 200) {
                message.success(bean.msg);
            }

            return bean;
        })
    };

    start = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要启用的数据");
        }

        Dialog.confirm("流程启用", `确定要启用流程【${record.name}】吗？`, async () => {
            const bean = await ProcessExtService.save(Object.assign(record,{status:1}));
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    stop = ()=>{
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要停用的数据");
        }

        Dialog.confirm("流程停用", `确定要停用流程【${record.name}】吗？`, async () => {
            const bean = await ProcessExtService.save(Object.assign(record,{status:0}));
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };


    getUtil = ():TableUtil<ProcessExt> =>this.tableRef.current?.util as TableUtil<ProcessExt>;
    onQuery = (query: ProcessExtQuery) => this.getUtil().reload(query);

    render() {

        return (
            <React.Fragment>
                <SearchBar columns={columns} onQuery={this.onQuery}/>
                <Toolbar>
                    <Toolbar.Option type="primary"
                                    icon={<EditOutlined/>}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.modify}>
                        {LocaleUtil.get("modify")}
                    </Toolbar.Option>
                    <Toolbar.Option type="primary"
                                    icon={<RollbackOutlined/>}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.toModel}>
                        {LocaleUtil.get("toModel","转为模型")}
                    </Toolbar.Option>
                    <Toolbar.Option type="primary"
                                    icon={<CheckOutlined />}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.start}>
                        {LocaleUtil.get("start","启用")}
                    </Toolbar.Option>
                    <Toolbar.Option type="primary"
                                    danger
                                    icon={<DeleteOutlined/>}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.stop}>
                        {LocaleUtil.get("stop","停用")}
                    </Toolbar.Option>
                </Toolbar>
                <EditableTable<ProcessExt>
                    ref={this.tableRef}
                    service={ProcessExtService}
                    autoSave={true}
                    columns={columns}
                    loadData={ProcessExtService.findPage}/>
            </React.Fragment>
        )
    }
}
