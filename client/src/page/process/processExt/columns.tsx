import React from "react";
import {ProcessExt} from "../../../service/process/ProcessExtService";
import {EditableColumnProps} from "../../../component/Table/EditableTable";
import WrappedCheckbox from "../../../component/Form/WrappedCheckbox";

const columns: EditableColumnProps<ProcessExt>[] = [
    {
        label: '名字',
        name: 'name',
        ellipsis: true,
        editable: true,
        query: true,
        width: 300,
        editRule: {maxLength: 50}
    },
    {
        label: '键值',
        name: 'key',
        ellipsis: true,
        editable: true,
        width: 200,
        editRule: {}
    },
    {
        label: '类型',
        name: 'type',
        ellipsis: true,
        editable: true,
        formatter:"select",
        formatOption:{type:"model.category"},
        editType:'select',
        editOption:{type:"model.category"},
        width: 200,
        editRule: {}
    },
    {
        label: '授权',
        name: 'permission',
        ellipsis: true,
        editable: true,
        formatter:"select",
        formatOption:{type:"proc.permission"},
        editType:'select',
        editOption:{type:"proc.permission"},
        width: 200,
        editRule: {}
    },
    {
        label: '状态',
        name: 'status',
        ellipsis: true,
        editable: true,
        formatter:"select",
        formatOption:{type:"sys.bool"},
        editType:'select',
        editOption:{type:"sys.bool"},
        render: (status: boolean) => <WrappedCheckbox disabled={true}  value={status} />,
        width: 200,
        editRule: {}
    },
    {
        label: '排序',
        name: 'sort',
        ellipsis: true,
        editable: true,
        width: 200,
        editRule: {}
    }
];

export default columns;
