import React from "react";
import ModelService, {Model, ModelQuery} from "../../../service/process/ModelService"
import columns from "./columns";
import {Dialog, EditableTable, SearchBar, Toolbar} from "../../../component";
import {
    ApartmentOutlined,
    DeleteOutlined,
    EditOutlined,
    ForkOutlined,
    PlusOutlined,
    RocketOutlined
} from "@ant-design/icons/lib";
import LocaleUtil from "../../../util/LocaleUtil";
import ModelForm from "./Form";
import {message} from "antd";
import TableUtil from "../../../component/Table/TableUtil";
import {openURL} from "../../../util/CommonUtil";

export declare interface ModelListProps {
    onClick: (record: Model, event: any) => void
}

export declare interface ModelListState {
    query: object;
    record?: Model;
    refresh: boolean;
}

export default class List extends React.Component<ModelListProps, ModelListState> {
    tableRef = React.createRef<EditableTable<Model>>();

    state = {
        query: {},
        refresh: false
    };


    add = () => {
        const record = this.tableRef.current?.util.getSelected() || {};
        Dialog.open({
            title: "模型新增",
            children: <ModelForm record={Object.assign({}, record, {id: null})}/>,
            width: 600,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "模型修改",
            children: <ModelForm record={record}/>,
            width: 600,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("模型删除", "确定要删除数据吗", async () => {
            const bean = await ModelService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    design = ()=>{
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要设计的模型");
        }

        openURL('/modeler.html?modelId='+record.id,record.name);
    };

    deploy =()=>{
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要发布的模型");
        }

        Dialog.confirm("流程发布", "确定要发布该流程吗", async () => {
            const bean = await ModelService.deploy(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
            }

            return bean;
        })
    };

    upgrade = ()=>{
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要升级的流程");
        }

        Dialog.confirm("流程升级", "确定要升级该流程吗", async () => {
            const bean = await ModelService.upgrade(record.key);
            if (bean.code === 200) {
                message.success(bean.msg);
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<Model> =>this.tableRef.current?.util as TableUtil<Model>;
    onQuery = (query: ModelQuery) => this.getUtil().reload(query);

    render() {

        return (
            <React.Fragment>
                <SearchBar columns={columns} onQuery={this.onQuery}/>
                <Toolbar>
                    <Toolbar.Option type="primary" icon={<PlusOutlined/>}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.add}>{LocaleUtil.get("add")}
                    </Toolbar.Option>
                    <Toolbar.Option type="primary"
                                    icon={<EditOutlined/>}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.modify}>
                        {LocaleUtil.get("modify")}
                    </Toolbar.Option>
                    <Toolbar.Option type="primary"
                                    icon={<ApartmentOutlined />}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.design}>
                        {LocaleUtil.get("design","设计")}
                    </Toolbar.Option>
                    <Toolbar.Option type="primary"
                                    icon={<RocketOutlined />}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.deploy}>
                        {LocaleUtil.get("design","发布")}
                    </Toolbar.Option>
                    <Toolbar.Option type="primary"
                                    icon={<ForkOutlined />}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.upgrade}>
                        {LocaleUtil.get("design","升级")}
                    </Toolbar.Option>
                    <Toolbar.Option type="primary"
                                    danger
                                    icon={<DeleteOutlined/>}
                                    permission="ROLE_SUPER_ADMIN"
                                    onClick={this.delete}>
                        {LocaleUtil.get("delete")}
                    </Toolbar.Option>
                </Toolbar>
                <EditableTable<Model>
                    ref={this.tableRef}
                    service={ModelService}
                    autoSave={true}
                    columns={columns}
                    loadData={ModelService.findPage}/>
            </React.Fragment>
        )
    }
}
