import React from "react";
import {Estimate} from "../../../service/repository/EstimateService";
import {EditableColumnProps} from "../../../component/Table/EditableTable";
import {Dialog, SearchTree} from "../../../component";
import OfficeService from "../../../service/system/SysOfficeService"

const columns: EditableColumnProps<Estimate>[] = [
    {
        label: '项目',
        name: 'projectName',
        ellipsis: true,
        editable: true,
        query: true,
        hidden: true,
        width: 200,
        editType: "pop-select",
        editOption: {
            prefix: "company",
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择项目",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择项目"),
                        afterOk: () => resolve(company)
                    });
                })
            }
        },
        editRule: {required: true}
    },
    {
        label: '编号',
        name: 'no',
        ellipsis: true,
        editable: true,
        width: 100,
        editRule: {maxLength: 20}
    },
    {
        label: '名字',
        name: 'name',
        ellipsis: true,
        editable: true,
        query: true,
        width: 300,
        editRule: {maxLength: 50}
    },
    {
        label: '概算金额',
        name: 'amount',
        ellipsis: true,
        editable: false,
        width: 200,
        formatter:"currency",
        align:"right",
        editRule: {}
    },
    {
        label: '预算金额',
        name: 'budgetAmount',
        ellipsis: true,
        editable: true,
        width: 200,
        align:"right",
        formatter:"currency",
        editRule: {}
    },
    {
        label: '投资占比',
        name: 'percent',
        ellipsis: true,
        editable: true,
        width: 200,
        formatter:"percent",
        align:"right",
        editRule: {}
    }
];

export default columns;
