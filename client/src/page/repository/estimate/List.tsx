import React from "react";
import EstimateService, {Estimate, EstimateQuery} from "../../../service/repository/EstimateService"
import columns from "./columns";
import {Dialog, EditableTable, SearchBar, Toolbar} from "../../../component";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons/lib";
import LocaleUtil from "../../../util/LocaleUtil";
import EstimateForm from "./Form";
import {message} from "antd";
import TableUtil from "../../../component/Table/TableUtil";

export declare interface EstimateListProps {
    onClick: (record: Estimate, event: any) => void
}

export declare interface EstimateListState {
    query: object;
    record?: Estimate;
    refresh: boolean;
}

export default class List extends React.Component<EstimateListProps, EstimateListState> {
    tableRef = React.createRef<EditableTable<Estimate>>();

    state = {
        query: {},
        refresh: false
    };


    add = () => {
        const record = this.tableRef.current?.util.getSelected() || {};
        Dialog.open({
            title: "概算新增",
            children: <EstimateForm record={Object.assign({}, record, {id: null})}/>,
            width: 800,
            afterOk: this.getUtil().reload
        })
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        Dialog.open({
            title: "概算修改",
            children: <EstimateForm record={record}/>,
            width: 800,
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("概算删除", "确定要删除数据吗", async () => {
            const bean = await EstimateService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<Estimate> =>this.tableRef.current?.util as TableUtil<Estimate>;
    onQuery = (query: EstimateQuery) => this.getUtil().reload(query);

    render() {

        return (
            <React.Fragment>
                <SearchBar columns={columns} onQuery={this.onQuery}/>
                <Toolbar>
                    <Toolbar.Option type="primary" icon={<PlusOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.add}>{LocaleUtil.get("add")}</Toolbar.Option>
                    <Toolbar.Option type="primary" icon={<EditOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.modify}>{LocaleUtil.get("modify")}</Toolbar.Option>
                    <Toolbar.Option type="primary" danger icon={<DeleteOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.delete}>{LocaleUtil.get("delete")}</Toolbar.Option>
                </Toolbar>
                <EditableTable<Estimate>
                    ref={this.tableRef}
                    service={EstimateService}
                    autoSave={true}
                    columns={columns}
                    loadData={EstimateService.findPage}/>
            </React.Fragment>
        )
    }
}
