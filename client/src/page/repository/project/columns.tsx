import React from "react";
import {Project} from "../../../service/repository/ProjectService";
import {EditableColumnProps} from "../../../component/Table/EditableTable";
import {Dialog, SearchTree} from "../../../component";
import OfficeService from "../../../service/system/SysOfficeService"

const columns: EditableColumnProps<Project>[] = [
    {
        label: '公司',
        name: 'companyName',
        ellipsis: true,
        editable: false,
        query: true,
        width: 200,
        span:12,
        editType: "pop-select",
        editOption: {
            prefix: "company",
            span:16,
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择公司",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择业主公司"),
                        afterOk: () => resolve(company)
                    });
                })
            }
        },
        editRule: {required: true}
    },
    {
        label: '设计单位',
        name: 'designerName',
        ellipsis: true,
        editable: true,
        width: 200,
        span:12,
        editType: "pop-select",
        editOption: {
            prefix: "office",
            span:16,
            popup: async () => {
                return new Promise<any>(resolve => {
                    let company: any;
                    Dialog.open({
                        title: "请选择设计单位",
                        children: <SearchTree type="directory"
                                              isFilter
                                              loadTree={OfficeService.findTreeData}
                                              onSelect={(id, {node}) => company = node}/>,
                        ok: () => company || new Error("没有选择设计单位"),
                        afterOk: () => resolve(company)
                    });
                })
            }
        },
        editRule: {required: false}
    },
    {
        label: '编号',
        name: 'no',
        ellipsis: true,
        editable: true,
        width: 200,
        span:12,
        editRule: {maxLength: 20,required: true}
    },
    {
        label: '名称',
        name: 'name',
        ellipsis: true,
        editable: true,
        query: true,
        width: 300,
        span:12,
        editOption:{
            span: 16
        },
        editRule: {maxLength: 100,required: true}
    },
    {
        label: '类型',
        name: 'type',
        ellipsis: true,
        editable: false,
        width: 100,
        span:12,
        editRule: {maxLength: 20}
    },
    {
        label: '批复部门及文号',
        name: 'approveNo',
        ellipsis: true,
        editable: true,
        hidden: true,
        width: 100,
        span:12,
        editRule: {maxLength: 20}
    },
    {
        label: '概算金额',
        name: 'estimateAmount',
        ellipsis: true,
        editable: true,
        width: 150,
        span:12,
        formatter:"currency",
        align:"right",
        editRule: {maxLength: 20}
    },
    {
        label: '预算金额',
        name: 'budgetAmount',
        ellipsis: true,
        editable: true,
        span:12,
        width: 150,
        formatter:"currency",
        align:"right"
    },
    {
        label: '开始日期',
        name: 'startDate',
        ellipsis: true,
        editable: true,
        span:12,
        width: 100,
    },
    {
        label: '结束日期',
        name: 'endDate',
        ellipsis: true,
        editable: false,
        span:12,
        width: 100,
    },
    {
        label: '合同工期',
        name: 'days',
        ellipsis: true,
        editable: false,
        span:12,
        width: 100,
    }
];

export default columns;
