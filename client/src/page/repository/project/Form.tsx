import React, {Fragment} from "react";
import ProjectService, {Project} from "../../../service/repository/ProjectService"
import columns from "./columns";
import estimateCols from "../estimate/columns";
import {EditableTable, WrappedForm} from "../../../component";
import {FormInstance} from "antd/lib/form";
import {message} from "antd";
import {ResultBean} from "../../../service/base/BaseService";
import EstimateService, {Estimate} from "../../../service/repository/EstimateService";

export declare interface ProjectFormProps {
    record: Project;
    setHandler?: (ok: () => Promise<ResultBean<any>>, cancel?: () => void) => void
}

export declare interface ProjectFormState {

}

export default class UserForm extends React.Component<ProjectFormProps, ProjectFormState> {
    formRef = React.createRef<FormInstance>();
    tableRef = React.createRef<EditableTable<Estimate>>();

    save = async () => {
        const { record } = this.props;
        const data = await this.formRef.current?.validateFields();
        const bean = await ProjectService.saveBy({
            project:Object.assign(record,data),
            batch:{
                query:{ projectId : record.id},
                list:this.tableRef.current?.util.getBatchList()
            }
        });

        if (bean.code === 200) {
            message.success(bean.msg);
        }

        return bean;
    };

    componentDidMount(): void {
        this.props.setHandler?.(this.save)
    }

    render() {
        const {record} = this.props;

        const props = {
            columns,
            record,
            ref: this.formRef,
            style: {padding: "10px 20px"}
        };

        return (
            <Fragment>
                <WrappedForm {...props} mode={"normal"}/>
                <EditableTable<Estimate>
                    ref={this.tableRef}
                    service={EstimateService}
                    autoSave={false}
                    columns={estimateCols}
                    isEdit={true}
                    isTree={true}
                    query={record.isNewRecord?{}:{projectId:record.id}}
                    toolbar={{
                        expandNum:4,
                        isMove:true,
                        isShift:true,
                        isSave:false,
                    }}
                    loadData={EstimateService.findTree}/>
            </Fragment>
        )
    }

}
