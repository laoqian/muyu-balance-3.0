import React, {RefObject} from "react";
import ProjectService, {Project, ProjectQuery} from "../../../service/repository/ProjectService"
import columns from "./columns";
import {Dialog, EditableTable, SearchBar, Toolbar} from "../../../component";
import {DeleteOutlined, EditOutlined, PlusOutlined} from "@ant-design/icons/lib";
import LocaleUtil from "../../../util/LocaleUtil";
import ProjectForm from "./Form";
import {message} from "antd";
import TableUtil from "../../../component/Table/TableUtil";
import {cloneDeep} from "lodash-es";

export declare interface ProjectListProps {
    onClick: (record: Project, event: any) => void
}

export declare interface ProjectListState {
    query: object;
    record?: Project;
    refresh: boolean;
}

export default class List extends React.Component<ProjectListProps, ProjectListState> {
    tableRef:RefObject<EditableTable<Project>> = React.createRef();

    state = {
        query: {},
        refresh: false
    };

    add = () => {
        const record:Project = cloneDeep(this.tableRef.current?.util.getSelected() || {} as Project);
        delete record.id;
        record.isNewRecord = true;

        this.edit("工程修改",record);
    };

    modify = () => {
        const record = this.tableRef.current?.util.getSelected();
        if (!record) {
            return message.error("未选中要修改的数据");
        }

        this.edit("工程修改",record);
    };

    edit = (title:string,record:Project)=>{
        Dialog.open({
            title: "工程修改",
            children: <ProjectForm record={record}/>,
            width: 1200,
            bodyStyle:{
                height:"700px"
            },
            afterOk: this.getUtil().reload
        })
    };

    delete = async () => {
        const record = this.getUtil().getSelected();
        if (!record) {
            return message.error("未选中要删除的数据");
        }

        Dialog.confirm("工程删除", "确定要删除数据吗", async () => {
            const bean = await ProjectService.delete(record.id);
            if (bean.code === 200) {
                message.success(bean.msg);
                this.getUtil().reload();
            }

            return bean;
        })
    };

    getUtil = ():TableUtil<Project> =>this.tableRef.current?.util as TableUtil<Project>;
    onQuery = (query: ProjectQuery) => this.getUtil().reload(query);

    render() {

        return (
            <React.Fragment>
                <SearchBar columns={columns} onQuery={this.onQuery}/>
                <Toolbar>
                    <Toolbar.Option type="primary" icon={<PlusOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.add}>{LocaleUtil.get("add")}</Toolbar.Option>
                    <Toolbar.Option type="primary" icon={<EditOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.modify}>{LocaleUtil.get("modify")}</Toolbar.Option>
                    <Toolbar.Option type="primary" danger icon={<DeleteOutlined/>} permission="ROLE_SUPER_ADMIN"
                                    onClick={this.delete}>{LocaleUtil.get("delete")}</Toolbar.Option>
                </Toolbar>
                <EditableTable<Project>
                    ref={this.tableRef}
                    service={ProjectService}
                    autoSave={true}
                    columns={columns}
                    loadData={ProjectService.findPage}/>
            </React.Fragment>
        )
    }
}
