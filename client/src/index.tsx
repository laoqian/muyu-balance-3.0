import React, {Fragment} from 'react';
import ReactDOM from 'react-dom';
import './index.less';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {connect, Provider} from 'react-redux'
import EventEmitter from "events"
import store, {StoreState} from "./redux/store/configure";
import {ConfigProvider} from "antd";
import LocaleUtil from "./util/LocaleUtil";


interface RootAppProps {
    locale: string
}

interface RootAppState {
    refresh: boolean;
}

class AppRoot extends React.Component<RootAppProps, RootAppState> {
    state = {
        refresh: false
    };

    loadLocale = async () => {
        await LocaleUtil.loadDefaultLocale();
        this.setState({refresh: true})
    };

    async componentDidMount() {
        const emitter = new EventEmitter.EventEmitter();
        emitter.on('change_language', locale => LocaleUtil.setLocale(locale));

        this.loadLocale();
    }

    onRender = (id: string,
                phase: "mount" | "update",
                actualDuration: number,
                baseDuration: number,
                startTime: number,
                commitTime: number,
                interactions:any)=>{

        // console.log("阶段",phase);
        // console.log("当前组件树更新所花费的时间(MS)",actualDuration.toFixed(2));
        // console.log("初始挂载组件树的时间",baseDuration.toFixed(2));
        // console.log("初始挂载组件树的时间",startTime.toFixed(2));
        // console.log("本轮更新的结束时间戳",commitTime.toFixed(2));
        // console.log("本轮更新的调度堆栈",interactions);
        // console.log("---------------------------------------------");
    };

    render() {
        const {refresh} = this.state;
        return !refresh ? <Fragment/> :
            (
                <ConfigProvider locale={{locale: LocaleUtil.getLanguage()}}>
                    <React.Profiler id="profiler" onRender={this.onRender}>
                        <App/>
                    </React.Profiler>
                </ConfigProvider>
            );
    }
}

const MuyuApp = connect(({locale}: StoreState) => ({locale}), () => ({}))(AppRoot);

ReactDOM.render(
    <Provider store={store}>
        <MuyuApp/>
    </Provider>
    , document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
