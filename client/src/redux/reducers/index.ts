import {LocaleAction} from "../actions/locale";
import {TabsAction, TabsProps} from "../../util/TabUtil";
import LocalStorageUtil from "../../util/LocalStorageUtil";


export const locale = (state: string = "zh-CN", action: LocaleAction) => {
    return action.type === "SET_LOCALE" ? action.locale : state;
};


const tabState: { activeKey?: string, list: TabsProps[] } = LocalStorageUtil.getItem("tabState") || {
    activeKey: undefined,
    list: []
};
export const tabs = (state: TabsProps, {type, data}: TabsAction) => {
    if (data) {
        const {list} = tabState;
        let i = list.findIndex(({id}) => id === data.id);

        switch (type) {
            case "TAB_ADD":
                if (i === -1) {
                    list.push(data);
                    tabState.activeKey = data.id;
                } else {
                    tabState.activeKey = list[i].id;
                }

                break;
            case "TAB_DEL":
                list.splice(i, 1);
                if (data.id === tabState.activeKey) {
                    const key = list.map(t => t.id).find((tab, n) => i === n);
                    tabState.activeKey = key || list.map(t => t.id).find((tab, n) => i - 1 === n);
                }
                break;
            case "TAB_SWITCH":
                tabState.activeKey = data.id;
        }
    }

    LocalStorageUtil.setItem("tabState", tabState);
    return {...tabState};
};
