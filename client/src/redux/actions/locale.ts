export declare interface LocaleAction {
    type: "SET_LOCALE",
    locale: string
}
