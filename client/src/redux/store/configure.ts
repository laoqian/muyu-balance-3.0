import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunk from 'redux-thunk';
import * as reducers from '../reducers';
import {TabsProps} from "../../util/TabUtil";
import {getProperty} from "../../util/CommonUtil";

const rootReducer = combineReducers(reducers);
let createStoreWithMiddleware = compose(
    applyMiddleware(thunk),
)(createStore);

const configureStore = (initialState?: any) => {
    const store = createStoreWithMiddleware(rootReducer, initialState);
    const hot = getProperty(module, "hot");

    if (hot) {
        store.replaceReducer(rootReducer)
    }

    return store;
};

const store = configureStore();
export default store;


export declare interface StoreState {
    locale: string,
    tabs: { activeKey: string, list: TabsProps[] }
}
