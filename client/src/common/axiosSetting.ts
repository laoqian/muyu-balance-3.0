import axios from 'axios';
import AuthService, {authorization} from "../service/system/AuthService";

axios.interceptors.request.use(
    (config: any) => {
        const {url} = config;
        if (url.indexOf('oauth/token') >= 0) {
            return config;
        } else {
            if (authorization.bearer !== null) {
                const {headers} = config;
                headers["Authorization"] = `Bearer ${authorization.bearer?.access_token}`;
                return config;
            } else {
                localStorage.setItem('waring', '登录超时,请重新登录');
                window.location.href = '/';
                return config;
            }
        }
    },
    (error: string) => {
        Promise.reject(error);
    },
);

let refresh = true;
axios.interceptors.response.use(
    response => {
        if (response && response.status === 200) {
            const value = response.config.url?.indexOf('oauth/token') || -1;
            if (value > 0 && response.data && (response.data.code === 401 || response.data.code === 403)) {
                if (refresh) {
                    refresh = false;
                    AuthService.refresh()
                        .then(() => {
                            refresh = true;
                            return axios(response.config)
                        })
                        .catch(() => {
                            window.location.href = './';
                        })
                }
            }
        }

        return Promise.resolve(response);
    },
    error => {
        // Do something with request error
        return Promise.reject(error);
    },
);

