import axios from 'axios';
import {message} from 'antd';
import qs from 'qs';
import {Base64} from 'js-base64';
import './axiosSetting';
import {checkStatus} from './response';
import loading from "../component/Loading/Loading"

const JSONbigString = require('json-bigint')({"storeAsString": true});
const codeMessage: any = {
    200: '服务器成功返回请求的数据。',
    201: '新建或修改数据成功。',
    202: '一个请求已经进入后台排队（异步任务）。',
    204: '删除数据成功。',
    400: '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
    401: '用户没有权限（令牌、用户名、密码错误）。',
    403: '用户得到授权，但是访问是被禁止的。',
    404: '发出的请求针对的是不存在的记录，服务器没有进行操作。',
    406: '请求的格式不可得。',
    410: '请求的资源被永久删除，且不会再得到的。',
    422: '当创建一个对象时，发生一个验证错误。',
    500: '服务器发生错误，请检查服务器。',
    502: '网关错误。',
    503: '服务不可用，服务器暂时过载或维护。',
    504: '网关超时。',
};
const responseStatus: any = (response: { status: number, data: object, statusText: string }, type: string) => {
    // console.log("response---", response);
    const status = response && response.status;

    if ((status && status >= 200 && status < 300)) {
        return type === 'excel' ? response : response.data;
    }

    if (!status) {
        message.error(`请求错误`);
        return {};
    }

    const errortext = codeMessage[status] || response.statusText;
    message.error(`请求错误 ${response.status}: ${errortext}`);
    throw response;
};

class Http {
    async get(url: string, data: object, long: boolean) {
        const URL = `${url}`;
        return await axios({
            method: 'get',
            url: URL,
            params: data,
            paramsSerializer: params => {
                return qs.stringify(params, {indices: false})
            },
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            },
            transformResponse: [function (data) {
                if (long) return JSONbigString.parse(data);
                else return JSON.parse(data);
            }],
        }).then(response => {
            return checkStatus(responseStatus(response));
        }).catch(response => {
            return responseStatus(response.status ? response : response.response);
        })
    }

    async post(url: string, data: object) {
        const URL = `${url}`;
        return await axios({
            method: 'post',
            url: URL,
            data: data || {},
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            }
        }).then(response => {
            return checkStatus(responseStatus(response));
        }).catch(response => {
            return responseStatus(response.status ? response : response.response);
        })
    }

    async delete(url: string, data: string) {
        const URL = data ? `${url}/${data}` : `${url}`;
        return await axios({
            method: 'delete',
            url: URL,
            data: data || {},
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            }
        }).then(response => {
            return checkStatus(responseStatus(response));
        }).catch(response => {
            return responseStatus(response.status ? response : response.response);
        })
    }

    async put(url: string, data: object) {
        const URL = `${url}`;
        return await axios({
            method: 'put',
            url: URL,
            data: data || {},
            headers: {
                'Content-Type': 'application/json;charset=UTF-8'
            }
        }).then(response => {
            return checkStatus(responseStatus(response));
        }).catch(response => {
            return responseStatus(response.status ? response : response.response);
        })
    }

    async postLogin(url: string, med: string, data: { username: string, password: string }) {
        const URL = `${url}`;
        return await axios({
            method: 'post',
            url: URL,
            data: qs.stringify(data) || {},
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8',
                'Authorization': `Basic ${Base64.encode(`${data.username}:${data.password}`)}`
            }
        }).then(response => {
            return checkStatus(responseStatus(response));
        }).catch(response => {
            return responseStatus(response.status ? response : response.response);
        })
    }
}

class WrappedHttp {
    static http: Http = new Http();
    static msg: string = "执行中，请稍等...";

    get = loading.wrapped(WrappedHttp.msg, WrappedHttp.http.get);
    post = loading.wrapped(WrappedHttp.msg, WrappedHttp.http.post);
    put = loading.wrapped(WrappedHttp.msg, WrappedHttp.http.put);
    delete = loading.wrapped(WrappedHttp.msg, WrappedHttp.http.delete);
    postLogin = loading.wrapped(WrappedHttp.msg, WrappedHttp.http.postLogin);
}

export {
    Http,
    WrappedHttp,
}

export default new WrappedHttp();
