import BaseService, {BaseEntity, BaseQuery} from "../base/BaseService";
import {API_URL} from "../url";
import {toTreeData} from "../../util/CommonUtil";

export interface Store extends BaseEntity<Store> {
    companyId: string;
    companyName: string;
    no: string;
    name: string;
    type: string;
    remarks: string;
}

export declare type  StoreQuery =  Partial<Store & BaseQuery>

class StoreService extends BaseService<Store, StoreQuery> {
    findTreeData = async (parentId:string="0")=>{
        const {code,data} = await this.findTree({parentId,pageSize:-1});
        return code===200?toTreeData(data.list):[];
    };
}

export default new StoreService(API_URL, "material/store");
