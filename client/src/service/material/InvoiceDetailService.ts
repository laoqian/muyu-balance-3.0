import BaseService, {BaseEntity, BaseQuery} from "../base/BaseService";
import {API_URL} from "../url";

export interface InvoiceDetail extends BaseEntity<InvoiceDetail> {
    companyId: string;
    companyName: string;
    no: string;
    name: string;
    type: string;
    remarks: string;
}

export declare type  InvoiceDetailQuery =  Partial<InvoiceDetail & BaseQuery>

class InvoiceDetailService extends BaseService<InvoiceDetail, InvoiceDetailQuery> {

}

export default new InvoiceDetailService(API_URL, "material/invoice-detail");
