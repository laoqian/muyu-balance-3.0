import BaseService, {BaseEntity, BaseQuery} from "../base/BaseService";
import {API_URL} from "../url";
import {toTreeData} from "../../util/CommonUtil";

export interface Material extends BaseEntity<Material> {
    companyId: string;
    companyName: string;
    no: string;
    name: string;
    type: string;
    remarks: string;
}

export declare type  MaterialQuery =  Partial<Material & BaseQuery>

class MaterialService extends BaseService<Material, MaterialQuery> {
    findTreeData = async (parentId:string="0")=>{
        const {code,data} = await this.findTree({parentId,pageSize:-1});
        return code===200?toTreeData(data.list):[];
    };
}

export default new MaterialService(API_URL, "material/material");
