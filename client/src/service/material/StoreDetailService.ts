import BaseService, {BaseEntity, BaseQuery} from "../base/BaseService";
import {API_URL} from "../url";

export interface StoreDetail extends BaseEntity<StoreDetail> {
    companyId: string;
    companyName: string;
    no: string;
    name: string;
    type: string;
    remarks: string;
}

export declare type  StoreDetailQuery =  Partial<StoreDetail & BaseQuery>

class StoreService extends BaseService<StoreDetail, StoreDetailQuery> {

}

export default new StoreService(API_URL, "material/store-detail");
