import BaseService, {BaseEntity, BaseQuery, BatchBeanWithQuery} from "../base/BaseService";
import {API_URL} from "../url";
import {InvoiceDetail} from "./InvoiceDetailService";

export interface Invoice extends BaseEntity<Invoice> {
    companyId: string;
    companyName: string;
    no: string;
    name: string;
    type: number;
    remarks: string;
}

export declare type  InvoiceQuery =  Partial<Invoice & BaseQuery>


export interface InvoiceBatchBean {
    invoice : Invoice;
    batch?:BatchBeanWithQuery<InvoiceDetail>
}


class InvoiceService extends BaseService<Invoice, InvoiceQuery> {
    saveByBatchBean =(bean:InvoiceBatchBean)=>this.request("post","save",bean);
}

export default new InvoiceService(API_URL, "material/invoice");
