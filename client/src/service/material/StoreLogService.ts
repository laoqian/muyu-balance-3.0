import BaseService, {BaseEntity, BaseQuery} from "../base/BaseService";
import {API_URL} from "../url";

export interface StoreLog extends BaseEntity<StoreLog> {
    companyId: string;
    companyName: string;
    no: string;
    name: string;
    type: string;
    remarks: string;
}

export declare type  StoreLogQuery =  Partial<StoreLog & BaseQuery>

class StoreService extends BaseService<StoreLog, StoreLogQuery> {

}

export default new StoreService(API_URL, "material/store-log");
