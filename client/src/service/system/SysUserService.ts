import BaseService, {BaseEntity, BaseQuery, BaseServiceProps} from "../base/BaseService";
import {API_URL} from "../url";

export interface SysUser extends BaseEntity<SysUser> {
    companyId: string;
    companyName: string;
    officeId: string;
    officeName: string;
    name: string,
    code: string,
    icon: string,
    href: string,
    permission: string,
    status: boolean
}


export interface SysUserQuery extends BaseQuery {

}

export interface SysUserServiceProps extends BaseServiceProps {

}

class SysUserService extends BaseService<SysUser, SysUserQuery> {

}

const menuService = new SysUserService(API_URL, "sys-user");

export default menuService;
