import BaseService, {BaseEntity, BaseQuery, BaseServiceProps, ResultBean,} from "../base/BaseService";
import {API_URL} from "../url";


export interface SysLocale extends BaseEntity<SysLocale> {
    lang: string;
    code: string;
    name: string;
    value: string;
}

export interface SysLocaleQuery extends BaseQuery {
    lang?: string
}

export interface LocaleServiceProps extends BaseServiceProps {

}

class SysLocaleService extends BaseService<SysLocale, SysLocaleQuery> {
    getLocale = (query: SysLocaleQuery): Promise<ResultBean<SysLocale[]>> => this.request("post", "get-locale", query);
}

const localeService = new SysLocaleService(API_URL, "sys-locale");

export default localeService;
