import BaseService, {BaseEntity, BaseQuery, BaseServiceProps} from "../base/BaseService";
import {API_URL} from "../url";
import {toTreeData} from "../../util/CommonUtil";

export interface SysOffice extends BaseEntity<SysOffice> {
    name: string,
    code: string,
    icon: string,
    href: string,
    key: string,
    permission: string,
    status: boolean
}


export interface SysOfficeQuery extends BaseQuery {
    parentId?: string;
}

export interface SysOfficeServiceProps extends BaseServiceProps {

}

class SysOfficeService extends BaseService<SysOffice, SysOfficeQuery> {

    findTreeData = async (parentId?:string)=>{
        const {code,data} = await this.findTree({parentId,pageSize:-1});
        return code===200?toTreeData(data.list):[];
    };
}

const menuService = new SysOfficeService(API_URL, "sys-office");
export default menuService;
