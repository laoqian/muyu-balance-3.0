import BaseService, {BaseEntity, BaseQuery, BaseServiceProps} from "../base/BaseService";
import {API_URL} from "../url";


export interface SysDict extends BaseEntity<SysDict> {
    type: string;
    value: string;
    code: string;
    label: string;
}



export interface SysDictQuery extends BaseQuery {

}

export interface DictServiceProps extends BaseServiceProps {

}

class SysDictService extends BaseService<SysDict, SysDictQuery> {

}

const dictService = new SysDictService(API_URL, "sys-dict");

export default dictService;
