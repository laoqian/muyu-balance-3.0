import BaseService, {BaseEntity, BaseQuery, BaseServiceProps} from "../base/BaseService";
import {API_URL} from "../url";

export interface SysArea extends BaseEntity<SysArea> {
    name: string,
    code: string,
    icon: string,
    href: string,
    permission: string,
    status: boolean
}


export interface SysAreaQuery extends BaseQuery {

}

export interface SysAreaServiceProps extends BaseServiceProps {

}

class SysAreaService extends BaseService<SysArea, SysAreaQuery> {

}

const menuService = new SysAreaService(API_URL, "sys-area");
export default menuService;
