import {message} from "antd";
import http from "../../common/http";
import LocalStorageUtil from "../../util/LocalStorageUtil"
import {API_URL} from "../url"

export declare interface User {
    id?: string,
    username: string,
    password: string
}

export declare interface Bearer {
    access_token: string,
    token_type: string,
    refresh_token: string,
    expires_in: number,
    scope: string,
    developer: string,
    code?: number,
    msg?: string,
    error?: string
}

export declare interface Authorization {
    grant_type?: "password" | "authorization_code" | "client_credentials" | "refresh_token";
    bearer?: Bearer;
    user?: User;
}

const authorization: Authorization = {};

export {
    authorization
}

const cachedAuthInfo = (baerer: Bearer, user: User) => {
    authorization.grant_type = "password";
    authorization.bearer = baerer;
    authorization.user = user;

    LocalStorageUtil.setItem("authorization", authorization, 60 * 60 * 12);
};

class AuthService {
    async refresh() {
        const auth: Authorization | undefined = LocalStorageUtil.getItem("authorization");
        if (auth === undefined) {
            throw new Error("失败");
        }

        const {user = {username: ""}, bearer} = auth;
        const data: Bearer = await http.postLogin(`${API_URL}/oauth/token?grant_type=refresh_token&refresh_token=${bearer?.refresh_token}`, "", auth.user);
        if (!data || data.access_token === undefined) {
            LocalStorageUtil.removeItem("authorization");
            throw data;
        }

        cachedAuthInfo(data, user as User);
        return data;
    }


    async login(user: User) {
        const data: Bearer = await http.postLogin(API_URL + "/oauth/token?grant_type=password", "", user);
        if (!data || data.code !== undefined) {
            throw data;
        }

        cachedAuthInfo(data, user);
        return data;
    }

    async logout() {
        const {code, msg} = await http.get(API_URL + "/logout", "", {}, false, "");
        if (code !== 200) {
            return message.error(msg);
        }

        message.success(msg);
        setTimeout(() => {
            window.location.href = "./";
        }, 1000)
    }
}

export default new AuthService();
