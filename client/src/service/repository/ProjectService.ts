import BaseService, {BaseEntity, BaseQuery, BatchBeanWithQuery} from "../base/BaseService";
import {API_URL} from "../url";
import {Estimate } from "./EstimateService";

export interface Project extends BaseEntity<Project> {
    companyId: string;
    companyName: string;
    designerId: string;
    designerName: string;
    no: string;
    name: string;
    type: string;
    approveNo: string;
    estimateAmount: number;
    budgetAmount: string;
    startDate: string;
    endDate: string
}


export declare type  ProjectQuery =  Project & BaseQuery


export interface ProjectBatchBean {
    project : Project;
    batch?:BatchBeanWithQuery<Estimate>
}

class ProjectService extends BaseService<Project, ProjectQuery> {

    saveBy =(bean:ProjectBatchBean)=>this.request("post","save",bean);
}

const projectService = new ProjectService(API_URL, "repository/project");
export default projectService;
