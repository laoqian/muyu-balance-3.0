import BaseService, {BaseEntity, BaseQuery} from "../base/BaseService";
import {API_URL} from "../url";

export interface Estimate extends BaseEntity<Estimate> {
    projectId: string;
    projectName: string;
    no: string;
    name: string;
    amount: number;
    budgetAmount: string;
}


export declare type  EstimateQuery =  Estimate & BaseQuery


class EstimateService extends BaseService<Estimate, EstimateQuery> {

}

const projectService = new EstimateService(API_URL, "repository/estimate");
export default projectService;
