import {Http, WrappedHttp} from "../../common/http";
import {getProperty} from "../../util/CommonUtil";
import {BatchBean} from "../../component/Table/TableUtil";


export declare interface BaseEntity<T extends BaseEntity<T>> {
    id: string,
    isNewRecord?: boolean,
    key?: string,
    name?: string,
    parentId?: string,
    parentIds?: string,
    level?: number,
    leaf?: boolean,
    sort?: number,
    children?: T[]
}


export declare interface BaseQuery {
    pageSize?: number,
    current?: number,
}

export declare interface ResultBean<T> {
    code: number,
    msg: string,
    useTime: number,
    data: T
}

export declare interface BatchBeanWithQuery<T> {
    query?: Partial<T & BaseQuery>,
    list?: BatchBean<T>[]
}

export declare interface TransformBean {
    action: string;
    query: BaseQuery;
    ids: string[];
}


export declare interface PageBean<T> {
    pageSize: number,
    pageNum: number,
    total: number,
    list: T[],
}

export interface BaseServiceProps {
    serviceName: string,
    url: string,
    http: Http,
    wrappedHttp: WrappedHttp
}


export default class BaseService<T extends BaseEntity<T>, Q extends BaseQuery> {
    props: Readonly<BaseServiceProps>;

    constructor(serviceName: string, url: string) {
        this.props = {
            serviceName,
            url,
            http: new Http(),
            wrappedHttp: new WrappedHttp()
        };
    }

    public request(type: string, method: string, data?: any): Promise<any> {
        const {wrappedHttp, serviceName, url} = this.props;
        return getProperty<WrappedHttp>(wrappedHttp, type)?.(`${serviceName}/${url}/${method}`, data);
    }

    getUrl = () => `${this.props.serviceName}/${this.props.url}/`;
    get = (id: string): Promise<ResultBean<T>> => this.request("get", `${id}`);
    save = (entity: T) => !entity.id ? this.create(entity) : this.update(entity);
    create = (entity: T): Promise<ResultBean<T>> => this.request("post", "", entity);
    update = (entity: T): Promise<ResultBean<T>> => this.request("put", `${entity.id}`, entity);
    findPage = (query: Q): Promise<ResultBean<PageBean<T>>> => this.request("post", "find-page", query);
    findList = (query: Q): Promise<ResultBean<T[]>> => this.request("post", "find-list", query);
    findTree = (query?: Q): Promise<ResultBean<PageBean<T>>> => this.request("post", "find-tree", query);
    delete = (id: string): Promise<ResultBean<T>> => this.request("delete", `${id}`);
    saveBatch = (data: BatchBeanWithQuery<T>): Promise<ResultBean<T>> => this.request("post", `saveBatch`, data);
    translate = (data: TransformBean): Promise<ResultBean<T>> => this.request("post", `translate`, data);
}
