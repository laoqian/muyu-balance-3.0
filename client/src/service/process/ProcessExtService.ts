import BaseService, {BaseEntity, BaseQuery} from "../base/BaseService";
import {API_URL} from "../url";

export interface ProcessExt extends BaseEntity<ProcessExt> {
    name: string;
    key:string;
    description:string;
    category:string;
}


export declare type  ProcessExtQuery =  ProcessExt & BaseQuery


class ProcessExtService extends BaseService<ProcessExt, ProcessExtQuery> {
    toModel = (key:string)=>this.request("get",`toModel?key=${key}`)
}



export default new ProcessExtService(API_URL, "process/process-ext");
