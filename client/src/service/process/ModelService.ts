import BaseService, {BaseEntity, BaseQuery} from "../base/BaseService";
import {API_URL} from "../url";

export interface Model extends BaseEntity<Model> {
    name: string;
    key:string;
    description:string;
    category:string;
}


export declare type  ModelQuery =  Model & BaseQuery


class ModelService extends BaseService<Model, ModelQuery> {
    deploy = (id: string) => this.request("get", "deploy?id=" + id);
    upgrade = (key: string) => this.request("get", "upgrade?key=" + key);
}


export default new ModelService(API_URL, "process/model");
