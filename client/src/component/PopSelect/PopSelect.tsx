import React from "react";
import {Select} from "antd";
import {values} from "lodash-es";
import {getProperty} from "../../util/CommonUtil";
import classNames from "classnames";
import "./PopSelect.less"


type PopSelectOption = { label: string, value: string };

export declare interface PopSelectProps {
    getValue: (data?: any) => any;
    mapValue: (value: object) => void;
    popup?: (value?: any) => Promise<any> | undefined;
    className?: string;
    placeholder: string;
    disabled?:boolean;
}

export declare interface PopSelectState {
    options: PopSelectOption []
}

export default class PopSelect extends React.Component<PopSelectProps, PopSelectState> {
    state = {
        options: []
    };

    async componentDidMount() {
        this.getOption();
    }

    getOption = async (data?: any) => {
        const {getValue} = this.props;
        const option = await getValue(data);
        if (option && option.value !== undefined && option.label) {
            this.setState({options: [option]})
        }
    };

    onDropdownVisibleChange = async (open: boolean) => {
        const {popup, mapValue} = this.props;
        if (open) {
            const value: PopSelectOption = await popup?.(values);
            if (value) {
                mapValue(value);
                this.getOption();
            }
        }
    };

    onChange = (value?: any) => {
        const {mapValue} = this.props;

        if (!value) {
            this.setState({options: []});
            mapValue?.(value);
        }
    };

    render() {
        const {options} = this.state;
        const {className, placeholder,disabled} = this.props;
        return (
            <Select value={options.length > 0 ? getProperty(options[0], "value") : undefined}
                    onChange={this.onChange}
                    className={classNames(className, "pop-select")}
                    dropdownClassName={"pop-select-dropdown"}
                    placeholder={placeholder}
                    disabled={disabled}
                    allowClear
                    onDropdownVisibleChange={this.onDropdownVisibleChange}>
                {options.map(({label, value}) => (<Select.Option key={value} value={value}>{label}</Select.Option>))}
            </Select>
        )
    }
}
