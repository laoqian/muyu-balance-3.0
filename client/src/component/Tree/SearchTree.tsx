/*
* Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
* 千山鸟飞绝，万径人踪灭。
* 孤舟蓑笠翁，独钓寒江雪。
*
* @author  于其先
* @version 1.0.0
* @since 2020-06-14 12:21:10
*/
import React, {ChangeEvent, Fragment, Key} from "react"
import {Input, Tree} from "antd";
import {TreeProps} from "antd/es/tree";
import {BaseEntity} from "../../service/base/BaseService";
import {cloneDeep, debounce} from "lodash-es";
import "./SearchTree.less"
import {getCheckKeys, toTreeData} from "../../util/CommonUtil";

export declare interface SearchTreeProps extends TreeProps {
    type?: "tree" | "directory",
    loadTree: () => Promise<any []>;
    isFilter?: boolean;
}

export declare interface SearchTreeState {
    originList: BaseEntity<any>[];
    list: BaseEntity<any>[];
    checkedKeys?: Key[]
}


export default class SearchTree extends React.Component<SearchTreeProps, SearchTreeState> {
    private readonly localTree: any = this.props.type !== "directory" ? Tree : Tree.DirectoryTree;

    state = {
        originList: [],
        list: [],
        checkedKeys:[]
    };

    constructor(props: SearchTreeProps) {
        super(props);
        this.onSearch = debounce(this.onSearch, 500);
        // @ts-ignore
        this.state.checkedKeys = this.props.defaultCheckedKeys;
    }

    async componentDidMount() {
        const {loadTree,onCheck,defaultCheckedKeys=[]} = this.props;
        const list = toTreeData(await loadTree());
        this.setState({originList: list, list});

        const [ checkedKeys, checkedNodes ] = getCheckKeys(defaultCheckedKeys, list);
        onCheck?.(checkedKeys, {checkedNodes} as any);
    }

    treeTravel = (data:BaseEntity<any>,func:(data:BaseEntity<any>)=>void)=>{
        func(data);
        data.children?.forEach(child=>this.treeTravel(child,func))
    };

    onSearch = (key: string) => {
        const {originList} = this.state;

        const list = cloneDeep(originList).filter(t => this.filter(t, key));
        this.setState({list});
    };

    onChange = (event: ChangeEvent<HTMLInputElement>) => this.onSearch(event.target.value);

    filter = <T extends BaseEntity<T>> (node: T, key: string): boolean => {
        const {name} = node;
        if (name?.indexOf(key) !== -1) {
            return true;
        }

        const {children} = node;
        if (!children) {
            return false;
        }

        const list = children?.filter(t => this.filter(t, key));
        node.children = list;

        return list.length > 0;
    };

    onCheck = (keys: any[] | any, {checkedNodes, ...rest}: any) => {
        const {checkStrictly,onCheck} = this.props;
        const [checkedKeys, nodes] = getCheckKeys(checkStrictly?keys.checked:keys, this.state.list);

        onCheck?.(checkedKeys, {checkedNodes: nodes, ...rest});

        this.setState({checkedKeys});
    };

    render() {
        const {list, checkedKeys} = this.state;
        const {type, isFilter = false, ...rest} = this.props;
        const LocalTree = this.localTree;

        return (
            <Fragment>
                {isFilter ? <Input.Search onSearch={this.onSearch} onChange={this.onChange}/> : <Fragment/>}
                <LocalTree treeData={list} {...rest}
                           className="muyu-search-tree"
                           checkedKeys={checkedKeys}
                           onCheck={this.onCheck}
                />
            </Fragment>
        )
    }
}
