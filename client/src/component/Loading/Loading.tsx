import React from "react"
import {createPortal, render, unmountComponentAtNode} from 'react-dom';
import "./Loading.less"

let root: any = null;

interface LoadingPropsType {
    root: any,
    size?: "small" | "normal" | "large" | number,
    parent?:()=>HTMLElement;
}

const Loading: React.FC<LoadingPropsType> = (props: LoadingPropsType) => {
    const {root, size = "normal"} = props;
    let width;
    switch (size) {
        case "small":
            width = 16;
            break;
        case "normal":
            width = 32;
            break;
        case "large":
            width = 48;
            break;
        default:
            width = size as number;
    }

    return createPortal(
        <div className="ant-modal-mask">
            <div className="loadingWrapper">
                <svg className="spinner" id="cloudos-loading-spinner"
                     style={{width: `${width}px`, height: `${width}px`}}
                     xmlns="http://www.w3.org/2000/svg" viewBox="-1 -1 2 2">
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.16863"
                          transform="rotate(0)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.2"
                          transform="rotate(30)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.21961"
                          transform="rotate(60)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.25882"
                          transform="rotate(90)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.32157"
                          transform="rotate(120)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.38039"
                          transform="rotate(150)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.43137"
                          transform="rotate(180)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.54118"
                          transform="rotate(210)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.65098"
                          transform="rotate(240)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.76078"
                          transform="rotate(270)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="0.90196"
                          transform="rotate(300)"/>
                    <rect x="0.45625" y="-0.09099" width="0.54375" height="0.18198" rx="0.09099"
                          ry="0.09099" fill="rgb(0,0,0)" fillOpacity="1"
                          transform="rotate(330)"/>
                </svg>
            </div>
        </div>, root)
};

const open = () => {
    if (root == null) {
        root = document.createElement('div');
        document.body.appendChild(root);

        render(<Loading root={root}/>, root)
    }
};

const close = () => {
    if (root != null) {
        unmountComponentAtNode(root);
        document.body.removeChild(root);
        root = null;
    }
};

const wrapped: any = (msg: string, func: any) => {
    return async function (...args: any[]) {
        let data: any;
        open();

        try {
            data = await func(...args);
        } catch (e) {
            console.error(e);
        }

        close();
        return data;
    }
};

export default {open, close, wrapped};
