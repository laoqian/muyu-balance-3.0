import React from "react"
import {WrappedFormItemProps} from "./WrappedFormItem";
import PopSelect from "../PopSelect/PopSelect";
import LocaleUtil from "../../util/LocaleUtil";
import {getProperty} from "../../util/CommonUtil";

const WrappedPopSelect: React.FC<WrappedFormItemProps<any>> = React.forwardRef((props: WrappedFormItemProps<any>, ref: React.Ref<any>) => {
    const {record, column: {editOption = {}, code, label, name}, onChange} = props;
    const {getValue, mapValue, prefix,disabled} = editOption;
    const mapProps = {
        getValue: (data: any) => getValue ? getValue(data || record) : getter(data || record, prefix),
        popup: () => editOption?.popup?.(record),
        placeholder: LocaleUtil.get(code, label),
        mapValue: (data: object) => {
            mapValue ? mapValue(record, data) : mapper(record, data, prefix);
            onChange?.(record[name])
        },
        disabled
    };

    return <PopSelect {...mapProps} />
});


export default WrappedPopSelect;

export const getter = (entity: any, prefix: string) => {
    let data;
    if (entity) {
        data = {
            label: getProperty(entity, prefix + "Name"),
            value: getProperty(entity, prefix + "Id")
        };
    }
    return Promise.resolve(data && data.label !== undefined && data.value !== undefined ? data : undefined)
};

export const mapper = (record: any, data: any, prefix: string) => {
    const labelKey = prefix + "Name";
    const valueKey = prefix + "Id";

    if (record) {
        if (data) {
            const {id, name} = data;

            record[labelKey] = name;
            record[valueKey] = id;
        } else {
            delete record[labelKey];
            delete record[valueKey];
        }
    }
};
