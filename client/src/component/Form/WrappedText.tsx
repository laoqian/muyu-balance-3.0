import React, {useState} from "react"
import {Input} from "antd";
import {WrappedFormItemProps} from "./WrappedFormItem";
import LocaleUtil from "../../util/LocaleUtil";
import FormatUtil from "../../util/FormatUtil";


const WrappedText: React.FC<WrappedFormItemProps<any>> = React.forwardRef((props: WrappedFormItemProps<any>, ref: React.Ref<any>) => {
    const {column, value, onChange} = props;
    const {editOption = {hideLabel: false},formatter,formatOption={}} = column;
    const {hideLabel, ...rest}  = editOption;
    const [nextValue, setValue] = useState<any>(FormatUtil.format(value,formatter,formatOption));
    const [timer, setTimer] = useState<any>(null);

    const mapProps = {
        placeholder: LocaleUtil.get(column.code) || column.label,
        value: nextValue,
        onChange: (event: any) => {
            const value = event.target.value;

            setValue(value);
            onChange?.(value);

            if (timer) {
                clearTimeout(timer);
            }

            setTimer(setTimeout(() =>setValue(FormatUtil.format(value,formatter,formatOption)), 1000));
        },
        ref
    };

    return <Input {...mapProps} {...rest}/>
});


export default WrappedText;
