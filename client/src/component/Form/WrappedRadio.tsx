import React from "react"
import {Radio} from "antd";
import {WrappedFormItemProps} from "./WrappedFormItem";
import DictUtil from "../../util/DictUtil";
import LocaleUtil from "../../util/LocaleUtil";


const WrappedRadio: React.FC<WrappedFormItemProps<any>> = React.forwardRef((props: WrappedFormItemProps<any>, ref: React.Ref<any>) => {
    const {column, value, defaultValue, onChange} = props;
    const {editOption} = column;
    const list = DictUtil.get(editOption?.type || "");

    const mapProps = {
        value,
        onChange,
        defaultValue,
    };

    return (
        <Radio.Group {...mapProps}>
            {list.map(({code, name, value}) => <Radio value={value} key={value}>{LocaleUtil.get(code, name)}</Radio>)}
        </Radio.Group>
    )
});


export default WrappedRadio;
