import React from "react"
import WrappedPopSelect from "./WrappedPopSelect";
import WrappedSelect from "./WrappedSelect";
import WrappedRadio from "./WrappedRadio";
import WrappedCheckbox from "./WrappedCheckbox";
import WrappedText from "./WrappedText";
import WrappedTextArea from "./WrappedTextArea";
import {EditableColumnProps} from "../Table/EditableTable";
import validator from "./validator";
import {Form} from "antd";
import LocaleUtil from "../../util/LocaleUtil";
import {FormProps} from "antd/lib/form";
import {getProperty} from "../../util/CommonUtil";
import WrappedDate from "./WrappedDate";

export declare interface WrappedFormItemProps<T> extends FormProps {
    column: EditableColumnProps<T>;
    value?: any;
    onChange?: any;
    record?: T,
    isTable?: boolean
}

const items = {
    "pop-select": WrappedPopSelect,
    "select": WrappedSelect,
    "radio": WrappedRadio,
    "checkbox": WrappedCheckbox,
    "text": WrappedText,
    "textarea": WrappedTextArea,
    "date": WrappedDate,
};

const WrappedFormItem: React.FC<WrappedFormItemProps<any>> = React.forwardRef((
    {isTable = false, ...rest}: WrappedFormItemProps<any>, ref: React.Ref<any>) => {
    const mapProps = {
        ref,
        ...rest,
    };

    const {editType = "text", editOption = {}, name, code, label} = rest.column;
    const {hideLabel,render,span} = editOption;
    const WrappedCtrl = getProperty(items, editType);

    const nonLabel = hideLabel || isTable;
    const layout = hideLabel ? {} : {
        labelCol: {span: nonLabel ? 0 : span!==undefined ? 24-span:8},
        wrapperCol: {span: nonLabel ? 24 : span!==undefined?span: 16},
    };

    // @ts-ignore
    const item = editType === "custom" ? React.cloneElement(render, mapProps) : <WrappedCtrl {...mapProps}/>;
    return (
        <Form.Item
            {...layout}
            name={name}
            rules={validator.create(rest.column)}
            label={nonLabel ? undefined : LocaleUtil.get(code, label)}>
            {item}
        </Form.Item>
    )
});


export default WrappedFormItem;
