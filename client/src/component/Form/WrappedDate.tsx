import React from "react"
import {DatePicker } from "antd";
import {WrappedFormItemProps} from "./WrappedFormItem";
import moment from "moment";
import LocaleUtil from "../../util/LocaleUtil";
import {getProperty} from "../../util/CommonUtil";


const locale ={
    "zh-CN":require("antd/es/date-picker/locale/zh_CN").default,
    "en-US":require("antd/es/date-picker/locale/en_US").default
};

const getLocale = ():any=>{
    const language:string = LocaleUtil.getLanguage();
    return getProperty(locale,locale.hasOwnProperty(language)?language:"zh-CN");
};

const WrappedDate: React.FC<WrappedFormItemProps<any>> = React.forwardRef((props: WrappedFormItemProps<any>, ref: React.Ref<any>) => {
    const {column, value, onChange} = props;
    const {editOption = {hideLabel: false}} = column;
    const {pattern="YYYY-MM-DD",type="date"}  = editOption;
    const locale = getLocale();
    const mapProps = {
        defaultValue: value? moment(value,pattern) as any: undefined,
        onChange: (value:any) =>onChange?.(value?.format(pattern)),
        ref,
        picker:type as any,
        locale,
        format:pattern
    };

    return <DatePicker {...mapProps} style={{width:"100%"}}  />
});


export default WrappedDate;
