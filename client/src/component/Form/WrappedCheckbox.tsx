import React from "react"
import {SwitchProps} from "antd/lib/switch";
import {Switch} from "antd";
import LocaleUtil from "../../util/LocaleUtil";

export declare interface WrappedCheckboxProps extends SwitchProps {
    value?: boolean;
    ref?: any;
}

export declare interface WrappedCheckboxState {
    currentValue?: boolean;
}


export default class WrappedCheckbox extends React.Component<WrappedCheckboxProps, WrappedCheckboxState> {
    state={
        currentValue:this.props.value
    };

    static getDerivedStateFromProps(nextProps: WrappedCheckboxProps, preState: WrappedCheckboxState) {
        return {
            currentValue: nextProps.value
        }
    }

    onChange = (currentValue: boolean, event: MouseEvent) => {
        this.setState({currentValue});
        this.props.onChange?.(currentValue, event);
    };

    render() {
        const {disabled, checked, value, defaultChecked, onChange, ...rest} = this.props;
        const {currentValue} = this.state;

        return (
            <Switch defaultChecked  = {value}
                    checked         = {currentValue}
                    disabled        = {disabled}
                    onChange        = {this.onChange}
                    {...rest}
                    checkedChildren = {LocaleUtil.get("true", "是")}
                    unCheckedChildren = {LocaleUtil.get("false", "否")}/>
        );
    }
}

