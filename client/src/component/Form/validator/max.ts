import {EditableColumnProps} from "../../Table/EditableTable";
import validator from "./index";

export default (column: EditableColumnProps<any>) => {
    const {max = -1} = column.editRule || {};
    const {label} = column;

    return {
        validator: (rule: any, value: number) => {
            const msg = validator.message("max",label,max);
            return value && value > max ? Promise.reject(msg) : Promise.resolve();
        }
    }
}
