import {EditableColumnProps} from "../../Table/EditableTable";
import {byteLength } from "../../../util/CommonUtil";
import validator from "./index";

export default (column: EditableColumnProps<any>) => {
    const {minLength = -1} = column.editRule || {};
    const {label} = column;

    return {
        validator: (rule: any, value: string) => {
            const msg = validator.message("minLength",label,minLength);
            return value && byteLength(value) < minLength ? Promise.reject(msg) : Promise.resolve();
        }
    }
}
