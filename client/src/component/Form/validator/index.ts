import {EditableColumnProps} from "../../Table/EditableTable";
import LocaleUtil from "../../../util/LocaleUtil";
import {getProperty} from "../../../util/CommonUtil";



class Validator {
    private static mapRules: object = {
        required: "required",
        number: "number",
        max: "max",
        min: "min",
        maxLength: "maxLength",
        minLength: "minLength",
    };

    private static getRuleFileName(key: string): string {
        let name = "";
        for (let [fileName, rules] of Object.entries(Validator.mapRules)) {
            if (rules.toString().includes(key)) {
                name = fileName;
                break;
            }
        }

        return name;
    }

    public message(key:string,...args:any[]){
        const msg = require(`../message/${LocaleUtil.getLanguage()}`);
        return LocaleUtil.template(getProperty(msg,key),...args);
    }

    create(column: EditableColumnProps<any>) {
        const {editRule = {}} = column;
        const rules: object[] = [];

        for (let key of Object.keys(editRule)) {
            const fileName: string = Validator.getRuleFileName(key);
            if (fileName !== "") {
                const func: any = require(`./${fileName}`).default;
                rules.push(func(column));
            }
        }
        return rules;
    }
}

const validator = new Validator();
export default validator;
