import {EditableColumnProps} from "../../Table/EditableTable";
import validator from "./index";

export default (column: EditableColumnProps<any>) => {
    const {min = -1} = column.editRule || {};
    const {label} = column;

    return {
        validator: (rule: any, value: number) => {
            const msg = validator.message("min",label,min);
            return value && value < min ? Promise.reject(msg) : Promise.resolve();
        }
    }
}
