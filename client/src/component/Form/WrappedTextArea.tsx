import React from "react"
import {Input} from "antd";
import {WrappedFormItemProps} from "./WrappedFormItem";


const WrappedTextArea: React.FC<WrappedFormItemProps<any>> = React.forwardRef((props: WrappedFormItemProps<any>, ref: React.Ref<any>) => {
    return (
        <div style={{paddingTop: "2px", paddingBottom: "2px", flex: 1}}>
            <Input.TextArea ref={ref}/>
        </div>
    )
});


export default WrappedTextArea;
