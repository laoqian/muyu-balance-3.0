import React, { DetailedReactHTMLElement, RefObject} from "react"
import {Col, Form, Row} from "antd"
import {FormProps} from "antd/es/form/Form";
import WrappedFormItem from "./WrappedFormItem";
import {EditableColumnProps} from "../Table/EditableTable";
import {FormInstance} from "antd/lib/form";
import {cloneDeep} from "lodash-es";

export declare interface ValidatorRule {
    required?: boolean,
    integer?: boolean,
    number?: boolean,
    max?: number,
    min?: number,
    maxLength?: number,
    minLength?: number,
    equal?: string
}

export declare interface WrappedFormProps<T> extends FormProps {
    columns: EditableColumnProps<T>[],
    record?: T,
    mode?: "simple" | "normal",
    span?: number,
    ref?: RefObject<FormInstance>,
    isGrid?: boolean;
}


export declare interface EditOption {
    block?: boolean;
    prefix?: any;
    type?: string;
    hideLabel?: boolean;
    span?: number;
    getValue?: (record: any) => Promise<any>;
    mapValue?: (record: any, data: any) => void;
    popup?: (record?: any) => Promise<any>;
    render?: DetailedReactHTMLElement<any, any>;
    disabled?:boolean;
    pattern?:string;
}


const parse = (props: WrappedFormProps<any>) => {
    const {mode = "simple", columns, span = 24} = props;
    const list = cloneDeep(columns);

    if (mode === "simple") {
        list.forEach(column => column.span = span);
    }

    const array: EditableColumnProps<any>[][] = [];
    let sub: EditableColumnProps<any>[] = [];
    let total = 0;

    const subList = list.filter(t => t.editable);
    subList.forEach((column, index) => {
        total += column.span || 0;

        if (total < 24 && index < subList.length - 1) {
            return sub.push(column);
        } else if (total > 24) {
            array.push(sub);
            sub = [column];
            total = column.span || 0;
        } else {
            sub.push(column);
        }

        if (total >= 24 || index === subList.length - 1) {
            array.push(sub);
            sub = [];
            total = 0;
        }
    });

    return array;
};

const WrappedForm: React.FC<WrappedFormProps<any>> = React.forwardRef((props: WrappedFormProps<any>, ref: React.Ref<any>) => {

    const {columns, record = {}, isGrid = false, ...rest} = props;
    const cols = parse(props);
    return (
        <Form initialValues={record}
              onValuesChange={values => Object.assign(record, values)}
              ref={ref}
              {...rest}>
            {cols.length === 1 && !isGrid ?
                cols[0].map((column) => (<WrappedFormItem column={column} key={column.name} record={record}/>))
                : cols.map((cols, key) => {
                    return (
                        <Row key={key}>
                            {cols.map((column) => (
                                <Col span={column.span} key={column.name}>
                                    {<WrappedFormItem column={column} record={record}/>}
                                </Col>
                            ))}
                        </Row>
                    )
                })}
        </Form>
    )
});

export default WrappedForm;
