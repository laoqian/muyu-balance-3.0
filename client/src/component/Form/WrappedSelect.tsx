import React from "react"
import {Select} from "antd";
import {WrappedFormItemProps} from "./WrappedFormItem";
import DictUtil from "../../util/DictUtil";
import LocaleUtil from "../../util/LocaleUtil";


const WrappedSelect: React.FC<WrappedFormItemProps<any>> = React.forwardRef((props: WrappedFormItemProps<any>, ref: React.Ref<any>) => {
    const {column, value, defaultValue, onChange} = props;
    const {editOption, code, label} = column;
    const list = DictUtil.get(editOption?.type || "");

    return (
        <Select ref={ref}
                placeholder={LocaleUtil.get(code, label)}
                value={value}
                defaultValue={defaultValue}
                onChange={onChange}
                allowClear>
            {list.map(({value, name, code}) => <Select.Option key={value}
                                                              value={value}> {LocaleUtil.get(code, name)} </Select.Option>)}
        </Select>
    )
});


export default WrappedSelect;
