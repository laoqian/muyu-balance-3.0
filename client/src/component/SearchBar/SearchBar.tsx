import React, {CSSProperties, Fragment} from "react";
import {EditableColumnProps} from "../Table/EditableTable";
import "./SearchBar.less"
import {Dialog, WrappedForm} from "../index";
import {Button} from "antd";
import {FunnelPlotOutlined, ReloadOutlined, SearchOutlined} from "@ant-design/icons/lib";
import {FormInstance} from "antd/lib/form";
import LocaleUtil from "../../util/LocaleUtil";
import {cloneDeep, throttle} from "lodash-es";
import {castTo, getProperty} from "../../util/CommonUtil";
import {BaseQuery} from "../../service/base/BaseService";


export declare interface SearchFormProps<T> {
    columns: EditableColumnProps<T>[];
    onQuery?: (query: T & BaseQuery) => void;
    length?:number;
    formStyle?:CSSProperties;
}

export declare interface SearchFormState<T> {
    basicColumns: EditableColumnProps<T>[];
    columns: EditableColumnProps<T>[];
}

export default class SearchBar<T> extends React.Component<SearchFormProps<T>, SearchFormState<T>> {
    private readonly formRef    = React.createRef<FormInstance>();
    private readonly baseLength = this.props.length || 4;
    private readonly record:T   = castTo({});

    constructor(props: SearchFormProps<T>) {
        super(props);

        const {columns} = this.props;
        const currentCols = cloneDeep(columns)
            .filter(({query})=>query)
            .map(t=>{
                t.editable = true;
                if(t.editOption){
                    t.editOption.disabled = false;
                }
                return t;
            });

        currentCols.forEach(row => {
            if (row.editType === "radio") {
                row.editType = "select";
            }

            row.editable = true;

            const {editRule={}} = row;
            const {required} = editRule;

            if(required){
                editRule.required = false;
            }

            if(!row.editOption){
                row.editOption = { span:18 }
            }else{
                row.editOption.span = 18;
            }
        });

        this.state = {
            columns: currentCols,
            basicColumns: columns.length > this.baseLength-1 ? Object.assign([], currentCols).splice(0, this.baseLength) : currentCols,
        };

        if(columns.length<this.baseLength){
            const more = {
                editType:"custom",
                name:"more",
                editOption:{
                    render:this.renderMore()
                }
            };

            this.state.basicColumns.push(more as EditableColumnProps<T>)
        }
    }

    renderMore = () => {
        const {columns } = this.state;
        if (columns.length <= this.baseLength) {
            return <Fragment/>
        }

        return (
            <Button style={{marginRight: "20px"}}
                    type="default"
                    onClick={this.openForm}
                    icon={<FunnelPlotOutlined/>}>
                {LocaleUtil.get("search.bar.btn.advanced.text")}
            </Button>
        )
    };

    openForm = ()=>{
        const {columns} = this.state;

        Dialog.open({
            title:"高级查询",
            children:<WrappedForm columns={columns}
                                  span={12}
                                  onValuesChange={this.onValuesChange}
                                  record={this.record}/>,
            width:"800px",
            afterOk:this.onSearch
        })
    };

    onSearch = async () => {
        const values = {};
        const { basicColumns } = this.state;
        basicColumns.forEach(({name})=>{
            const val = getProperty(this.record,name);
            if(val!==undefined){
                Object.assign(values,{[name]:val})
            }
        });

        this.formRef.current?.setFieldsValue(values);
        await this.formRef.current?.validateFields();
        this.props.onQuery?.(cloneDeep(this.record))
    };

    onReset = () => {
        this.props.onQuery?.({} as T & BaseQuery)
    };

    private fresh = throttle(()=>this.props.onQuery?.(this.record),1000);

    onValuesChange = (values: any) => {
        Object.assign(this.record,values);
        this.fresh();
    };

    render() {
        const {basicColumns} = this.state;
        const {formStyle} = this.props;
        return (
            <div className="muyu-search-form-wrapper">
                <WrappedForm columns={basicColumns}
                             isGrid={false}
                             layout={"inline"}
                             span={6}
                             record={this.record}
                             ref={this.formRef}
                             style={formStyle}
                             onValuesChange={this.onValuesChange}/>
                {this.renderMore()}
                <div className="muyu-search-form-group">
                    <Button type="primary" icon={<SearchOutlined/>}
                            onClick={this.onSearch}>{LocaleUtil.get("search.bar.btn.search.text","查询")}</Button>
                    <Button type="primary" icon={<ReloadOutlined/>}
                            onClick={this.onReset}> {LocaleUtil.get("search.bar.btn.reset.text","重置")}</Button>
                </div>
            </div>
        );
    }
}
