import React from "react";
import {Button, Radio} from "antd";
import {ToolbarGroupProps, ToolbarOptionProps, ToolbarProps, ToolbarState} from "./index";
import styles from "./Toolbar.module.css"
import {config} from "../../service/system/SystemService";
import {getProperty} from "../../util/CommonUtil";

const Option = (props: ToolbarOptionProps) => {
    return <Button {...props}/>;
};

const Group = (props: ToolbarGroupProps) => {
    return <Radio.Group {...props}/>;
};

export default class Toolbar extends React.Component<ToolbarProps, ToolbarState> {
    static Option = Option;
    static Group = Group;
    static GroupOption = Radio.Button;


    filter = () => {
        const {children} = this.props;
        const list = children instanceof Array ? [...children] : [children];
        return list.filter(t => {
            const {permission} = getProperty(t, "props");
            const isBoolean = typeof permission === "boolean";
            return config.authorities?.findIndex(t => isBoolean || permission === undefined ? permission : (permission as string).split(",")?.includes(t)) !== -1;
        });
    };

    render() {
        const children = this.filter();
        if (!children || children.length === 0) {
            return (
                <React.Fragment/>
            )
        }

        const {style, className = ""} = this.props;

        return (
            <div className={styles.toolbarWrapper + " " + className} style={style}>
                {children}
            </div>
        )
    }
}


