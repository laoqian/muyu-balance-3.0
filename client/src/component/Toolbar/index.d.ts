import {ButtonProps} from "antd/lib/button";
import {CSSProperties, ReactElement} from "react";
import {RadioGroupProps} from "antd/lib/radio/interface";
import Toolbar from "./Toolbar";


export declare interface ToolbarOptionProps extends ButtonProps {
    permission?: boolean | string;
}


export declare interface ToolbarGroupProps extends RadioGroupProps {
    permission?: boolean | string;
    children: Toolbar.Option[] | Toolbar.Option
}

export declare interface ToolbarProps {
    children: ReactElement[] | ReactElement;
    style?: CSSProperties;
    className?: string;
}

export declare interface ToolbarState {

}
