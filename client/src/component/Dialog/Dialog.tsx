import React, {ReactNode} from "react"
import {createPortal, render, unmountComponentAtNode} from 'react-dom';
import {message, Modal} from "antd";
import {ModalProps} from "antd/es/modal";
import {ResultBean} from "../../service/base/BaseService";
import LocaleUtil from "../../util/LocaleUtil";
import {createDomRoot, getProperty} from "../../util/CommonUtil";
import "./Dialog.less"

const confirm = (title: string, content: ReactNode, ok?: () => Promise<ResultBean<any>>) => {
    const root = createDomRoot();
    const props = {
        title,
        ok,
        root,
    };

    render(<Dialog {...props} children={<div>{content}</div>}/>, root)
};

export declare interface DialogsOpenProps extends DialogsProps {
    ok?: () => any;
}

const open = ({
                  children,
                  ok,
                  ...rest
              }: Partial<DialogsOpenProps>) => {
    const root = createDomRoot();
    render(<Dialog {...rest}
                   root={root}
                   ok={async () => {
                       const data = await ok?.();

                       if (data instanceof Error) {
                           return Promise.reject(data as Error)
                       }

                       return data && data?.code ? data : {code: 200, data};
                   }}
                   children={children}/>, root)
};


export declare interface DialogsProps extends ModalProps {
    ok?: () => Promise<ResultBean<any>>;
    afterOk?: () => void;
    cancel?: () => Promise<ResultBean<any>>;
    root: HTMLElement;
    children: ReactNode;
}

export declare interface DialogsState {
    onOk?: () => void;
    onCancel?: () => void;
    visible: boolean;
}

export default class Dialog extends React.Component<DialogsProps, DialogsState> {
    static confirm = confirm;
    static open = open;

    private readonly children: ReactNode;
    private ok?: () => Promise<ResultBean<any>> = this.props.ok;
    private cancel?: () => Promise<ResultBean<any>> = this.props.cancel;

    state = {
        visible: true
    };

    constructor(props: any) {
        super(props);
        const {children} = this.props;

        if (!children) {
            this.children = null;
            return;
        }

        const type = getProperty(children, "type");
        this.children = typeof type === "function" ? React.cloneElement<any, any>(children as any,
            {setHandler: this.setHandler}) : children;
    }

    setHandler = (ok: () => Promise<ResultBean<any>>, cancel?: () => Promise<ResultBean<any>>) => {
        this.ok = ok;
        this.cancel = cancel;
    };

    close = () => {
        this.setState({visible: false}, () => {
            const {root} = this.props;
            setTimeout(() => {
                unmountComponentAtNode(root);
                document.body.removeChild(root);
            }, 1500)
        })
    };

    onOk = async () => {
        try {
            const bean = await this.ok?.();
            if (bean?.code === 200) {
                this.close();
                this.props.afterOk?.();
            }
        } catch (e) {
            message.error(e.toString());
        }
    };

    onCancel = async () => {
        this.close();
        await this.cancel?.();
    };

    render() {
        const {root, okText, cancelText, ...rest} = this.props;
        const {visible} = this.state;

        const props = {
            destroyOnClose: true,
            visible,
            maskClosable: false,
            children: this.children,
            onOk: this.onOk,
            onCancel: this.onCancel,
            okText: okText || LocaleUtil.get("dialog.btn.ok.text"),
            cancelText: cancelText || LocaleUtil.get("dialog.btn.cancel.text")
        };

        return createPortal(<Modal {...rest} {...props} />, root)
    }
}

