import React, {CSSProperties, Fragment} from 'react'
import BaseService, {BaseEntity, BaseQuery} from "../../../service/base/BaseService";
import {Dialog, Toolbar} from "../../index";
import {RadioChangeEvent} from "antd/lib/radio/interface";
import "./TableToolBar.less"
import {message} from "antd";
import LocaleUtil from "../../../util/LocaleUtil";
import {getProperty} from "../../../util/CommonUtil";
import TableUtil from "../TableUtil";

export declare interface TableToolbarProps<T extends BaseEntity<T>> {
    style?: CSSProperties;
    className?: string;
    util: TableUtil<T>;
    service: BaseService<T, BaseQuery>;
    expandNum?: number;
    isMove?: boolean;
    isShift?: boolean;
    isEdit?: boolean;
    isSave?:boolean;
}

interface TableToolBarButtonProps {
    name: string;
    active?: boolean;
}

export default class TableToolbar<T extends BaseEntity<T>> extends React.Component<TableToolbarProps<T>, any> {
    state = {
        value: null
    };

    id = 100;

    onClick = (e: RadioChangeEvent) => {
        const key = e.target.value;
        const {util} = this.props;
        const action: (key: string) => void = getProperty(this.actions, key);
        if (action) {
            action(key);
        } else {
            const level = parseInt(key);
            if (!isNaN(level)) {
                util.expandByLevel(level);
            } else {
                console.log(key)
            }
        }
    };

    actions = {
        "save": async () => {
            const {service, util } = this.props;
            const list = util.getBatchList();

            if (list.length === 0) {
                return message.error("没有需要保存的数据.");
            }

            for(let {data} of list.values()){
                if(util.isEditing(data)){
                    Object.assign(data, util.getTable().map.get(data)?.current?.validateFields());
                }
            }

            Dialog.confirm(LocaleUtil.get("data.modify.text"), LocaleUtil.get("data.modify.notice"), async () => {
                const bean = await service.saveBatch({query: {} as T, list});
                if (bean.code === 200) {
                    util.clearStore();
                    await util.reload();
                }

                return bean;
            })
        },
        "add": () => {
            const { util } = this.props;
            util.create({id: (this.id++).toString()} as T,true);
        },
        "delete": () => {
            const { util} = this.props;
            const record = util.getSelected();
            if (!record) {
                return message.error(LocaleUtil.get("data.unselect.notice"));
            }

            Dialog.confirm(LocaleUtil.get("data.delete.text"), LocaleUtil.get("data.delete.notice"), async () => {
                util.remove(record);
                return Promise.resolve({code: 200, useTime: 0, msg: "", data: null});
            })
        },
        "table.move.up": () => this.translate("moveUp"),
        "table.move.down": () => this.translate("moveDown"),
        "table.shift.up": () => this.translate("shiftUp"),
        "table.shift.down": () => this.translate("shiftDown"),
    };

    translate = async (action: string) => {
        const {service, util} = this.props;
        const id = util.getSelected()?.id;
        if (id) {
            const {code} = await service.translate({action, query: {}, ids: [id]});
            if (code === 200) {
                await util.reload();
            }
        }
    };

    renderGroup = (list: TableToolBarButtonProps[]) => {
        const {value} = this.state;
        return (
            <Toolbar.Group onChange={this.onClick} permission={true} size={"small"} buttonStyle={"solid"} value={value}>
                {list.map(({name, active = false}) => (
                    <Toolbar.GroupOption key={name}
                                         value={name}
                                         className={active ? "muyu-radio-btn-red" : ""}>
                        {LocaleUtil.get(name)}
                    </Toolbar.GroupOption>
                ))}
            </Toolbar.Group>
        )
    };

    renderExpandGroup = () => {
        const {expandNum = 0} = this.props;
        const list = new Array(expandNum).fill(0);

        return expandNum > 0 ? this.renderGroup(list.map((a, i) => ({name: (i + 1).toString()}))) : <Fragment/>;
    };

    renderMoveGroup = () => {
        return this.props.isMove ? this.renderGroup([{name: "table.move.up"}, {name: "table.move.down"}]) : <Fragment/>;
    };

    renderShiftGroup = () => {
        return this.props.isShift ? this.renderGroup([{name: "table.shift.up"}, {name: "table.shift.down"}]) :
            <Fragment/>;
    };

    renderEditGroup = () => {
        const {util,isEdit,isSave=true} = this.props;
        const list :TableToolBarButtonProps[] = [
            {
                name: "add",
                active:util.getBatchList().filter(t=>t.action==="create").length>0 && !isSave
            },
            {
                name: "delete",
                active:util.getBatchList().filter(t=>t.action==="remove").length>0  && !isSave
            }];
        if(isSave){
            list.push({
                name: "save",
                active: util.getBatchList().length > 0
            });
        }

        return isEdit ? this.renderGroup(list) : <Fragment/>;
    };

    render() {
        const {style} = this.props;
        return (
            <Toolbar style={style} className="muyu-table-toolbar">
                {this.renderExpandGroup()}
                {this.renderMoveGroup()}
                {this.renderShiftGroup()}
                {this.renderEditGroup()}
            </Toolbar>
        );
    }
}
