import React from "react";
import {EditableColumnProps} from "./EditableTable"
import WrappedFormItem from "../Form/WrappedFormItem";


export declare interface EditableCellProps<T> {
    record: T;
    editing: boolean;
    column: EditableColumnProps<T>;
    index: number;
}


export default class EditableCell<T> extends React.Component<EditableCellProps<T>, any> {

    renderEditableCell = () => {
        const {index, record, children} = this.props;
        const child = (
            <WrappedFormItem key="next" isTable {...this.props} />
        );

        // @ts-ignore
        return index === 0 && record.children ? [children[0], child] : [children[0], child];
    };

    render() {
        const {editing, children, record, column, ...restProps} = this.props;

        return (
            <td {...restProps} >
                {editing ? this.renderEditableCell() : children}
            </td>
        );
    }
}
