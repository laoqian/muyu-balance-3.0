import React, {Fragment, RefObject} from "react";
import BaseService, {BaseEntity, BaseQuery, PageBean, ResultBean} from "../../service/base/BaseService";
import {ColumnProps} from "antd/es/table";
import {EditOption, ValidatorRule} from "../Form/WrappedForm";
import EditableCell from "./EditableCell";
import EditableRow from "./EditableRow";
import {FormInstance} from "antd/es/form/Form";
import {TableProps} from "antd/lib/table";
import "./EditableTable.less"
import TableToolbar from "./TableToobar/TableToolBar";
import {Key} from "antd/es/table/interface";
import classNames from 'classnames';
import {message, Table} from "antd";
import {cloneDeep} from "lodash-es";
import ResizeObserver from 'rc-resize-observer';
import TableUtil from "./TableUtil";
import {findDOMNode} from "react-dom";
import FormatUtil, {FormatOption} from "../../util/FormatUtil";

export declare interface EditableColumnProps<T> extends ColumnProps<T> {
    label: string,
    code?: string,
    name: string,
    editType?: "text" | "textarea" | "select" | "pop-select" | "radio" | "switch" | "checkbox"  |"date" | "custom",
    hidden?: boolean | (()=>boolean),
    editable?: boolean | (()=>boolean),
    query?: boolean  | (()=>boolean),
    span?: number,
    sort?: number,
    formatter?: "text" | "select"| "number" | "currency" | "date" | "percent" | "checkbox"
    formatOption?: FormatOption,
    editOption?: EditOption,
    editRule?: ValidatorRule,
    width?: number,
    ref?: any,
}

export declare interface TableToolBar {
    expandNum?: number,
    isShift?: boolean,
    isMove?: boolean,
    isSave?:boolean,
}

class EntityService<T extends BaseEntity<T>, Q extends BaseQuery> extends BaseService<T, Q> {
}

export declare interface EditableTableProps<T extends BaseEntity<T>> extends ColumnProps<T> {
    columns: EditableColumnProps<T>[];
    onClick?: (record?: T, event?: any) => void;
    onDoubleClick?: (record: T, event: any) => void;
    onContextMenu?: (record: T, event: any) => void;
    onMouseEnter?: (record: T, event: any) => void;
    onMouseLeave?: (record: T, event: any) => void;
    service: EntityService<T, any>;
    autoSave?: boolean;
    isTree?: boolean;
    isEdit?: boolean;
    editAfterAdd?: boolean;
    beforeAdd?: (record:T) => Promise<T>;
    query?: object;
    loadData: (query: any) => Promise<ResultBean<PageBean<T>>>;
    toolbar?: TableToolBar;
}

export declare interface EditableTableState<T extends BaseEntity<T>> {
    expandedRowKeys: Key[];
    dataSource: T[];
    useTime: number;
    totalTime: number;
    total: number;
    pageSize: number;
    current: number;
    baseQuery: BaseQuery;
    tableHeight: number;
}

export default class EditableTable<T extends BaseEntity<T> = any> extends React.Component<EditableTableProps<T>, EditableTableState<T>> {
    readonly state = {
        expandedRowKeys: [],
        dataSource: [],
        useTime: 0,
        totalTime: 0,
        baseQuery: {},
        total: 0,
        pageSize: 20,
        current: 1,
        tableHeight:600,
    };

    public util:TableUtil<T> = new TableUtil(this);
    private originColumns: EditableColumnProps<T>[];
    private originWidth = this.props.columns.map(({width = 100}) => width).reduce((a, b) => a + b);

    private width = this.originWidth;
    public map: Map<T, RefObject<FormInstance>> = new Map<T, RefObject<FormInstance>>();

    readonly defaultColumnProps: TableProps<T> = {
        rowKey: "id",
        bordered: true,
        size: "large",
        onRow: record => {
            return {
                record,
                editing: this.util.isEditing(record),
                onClick: event => this.onClick(record, event),
                onDoubleClick: event => this.onDoubleClick(record, event),
                onContextMenu: event => {
                },
                onMouseEnter: event => {
                },
                onMouseLeave: event => {
                },
                mountForm: (formRef: RefObject<FormInstance>) => this.map.set(record, formRef)
            }
        },
        rowClassName: (record) => {
            return classNames({
                "wrapped-table-row-highlight": this.util.getSelected() === record,
                "wrapped-table-row-editing": this.util.isEditing(record)
            });
        }
    };

    constructor(props: EditableTableProps<T>) {
        super(props);

        const columns: EditableColumnProps<T>[] = cloneDeep(this.props.columns).filter(({hidden=false})=>!hidden);

        columns.forEach((row) => {
            const {render,formatter,formatOption={}} = row;
            if (!render) {
                row.render = (value) => FormatUtil.format(value,formatter,formatOption);
            }
        });

        this.originColumns = columns;
    }

    onClick = async (record: T, event:React.MouseEvent) => {
        this.props.onClick?.(record,event);
        this.util.setSelected(record);
    };

    onDoubleClick = async (record: T, event: React.MouseEvent<HTMLElement, MouseEvent>) => {
        const {onDoubleClick, isEdit = false} = this.props;
        if (onDoubleClick) {
            return this.props.onDoubleClick?.(record, event);
        }

        if (isEdit) {
            if (this.util.isEditing(record)) {
                Object.assign(record, await this.map.get(record)?.current?.validateFields());
            }

            this.util.modify(record);
            this.forceUpdate();
        }
    };

    columns = (): any => {
        const {isTree} = this.props;
        const {pageSize = 15, current = 1} = this.state;
        const cols = this.originColumns?.map((col, index) => {
            const column = {
                ...col,
                dataIndex: col.name,
                title: col.label,
            };

            return !col.editable ? column : {
                ...column,
                onCell: (record: T) => {
                    return ({
                        record,
                        index,
                        column,
                        editing: this.util.isEditing(record),
                    })
                },
            };
        });

        const start = (current - 1) * pageSize;
        return isTree ? cols : [
            {
                label: '#',
                name: '__index',
                align: 'center',
                fixed: 'left',
                ellipsis: false,
                editable: false,
                sorter: false,
                width: 50,
                render: (text: string, record: T, index: number) => {
                    return start + index + 1;
                }
            }
            , ...cols];
    };



    onChange = async (page: any = {}) => {
        const {loadData, query = {}} = this.props;
        const {pageSize, current} = this.state;
        let currentTime = Date.now();
        try {
            const {code, data, useTime} = await loadData(Object.assign({}, query, {
                pageSize,
                current
            }, page));
            if (code === 200) {
                this.setState({
                    dataSource: data.list || [],
                    useTime,
                    totalTime:Date.now()-currentTime,
                    total:data.total,
                    pageSize: data.pageSize,
                    current: data.pageNum
                })
            }
        } catch (e) {
            message.error("刷新数据错误");
        }
    };

    itemRender = (current: number, type: string, originalElement: any) => {
        if (type === 'next' || type === 'prev') {
            const href = "#";

            return (
                <button title="2" style={{marginRight: 0}} className="ant-pagination-item">
                    <a href={href}>{type === 'prev' ? "上" : "下"}一页</a>
                </button>
            );
        }

        return originalElement;
    };

    totalRender = (total: number) => {
        const {useTime,totalTime} = this.state;
        return (
            <span>
                总共
                <span style={{color: 'green'}}>{total} </span>
                条记录，本次网络传输耗时
                <span style={{color: 'red'}}>{((totalTime-useTime) / 1000).toFixed(3)} </span>
                秒。后台耗时
                <span style={{color: 'red'}}>{(useTime / 1000).toFixed(3)} </span>
                秒。
            </span>
        )
    };

    onExpandedRowsChange = (expandedRowKeys: Key[]) => {
        this.setState({expandedRowKeys})
    };

    expandByLevel = (level: number) => {
        this.setState({expandedRowKeys: this.getExpandKeys(this.state.dataSource, level)})
    };

    setDataSource = (dataSource:T[])=>{
        const { current } = this.state;
        this.setState({
            dataSource:[...dataSource],
            current:current || 1,
        });
    };

    expandByKey = (key: never) => {
        const {expandedRowKeys=[]} = this.state;
        if (!expandedRowKeys.includes(key)) {
            this.setState({expandedRowKeys: [...expandedRowKeys, key]})
        }
    };

    getExpandKeys = (list: T[], level: number): Key[] => {
        const keys: Key[] = [];

        list.forEach(data => {
            const currentLevel = data?.level || 0;
            if (currentLevel < level) {
                keys.push(data?.key || "");
            }

            const children: T[] = data.children || [];
            if (children.length > 0) {
                keys.push(...this.getExpandKeys(children, level))
            }
        });

        return keys;
    };

    timer:any;
    componentDidMount() {
        this.onChange().then();
        this.timer = setInterval(()=>{
            this.fixHeight();
            this.fixWidth();
        },50);
    }

    componentWillUnmount(): void {
        clearInterval(this.timer);
    }

    private getElement = (className:string,wrapper:any=document):HTMLElement=>{
        const collect: HTMLCollectionOf<HTMLElement>= wrapper.getElementsByClassName(className)  as any;
        return collect?.item(0) as HTMLElement;
    };

    fixHeight = ()=>{
        const wrapper:HTMLElement = findDOMNode(this)?.parentNode as HTMLElement;
        if(wrapper){
            const list: HTMLCollectionOf<HTMLElement>= wrapper.getElementsByClassName("ant-table-body")  as any;
            const tableBody = list?.item(0) as  HTMLElement;
            const  { tableHeight } = this.state;

            if(tableBody){
                tableBody.style.height = (tableHeight-13)+"px";
            }
        }
    };

    fixWidth = ()=>{
        const wrapper:HTMLElement = findDOMNode(this)?.parentNode as HTMLElement;
        if(wrapper){
            const container = this.getElement("ant-table-container",wrapper);
            const header    = this.getElement("ant-table-header",container);
            const body      = this.getElement("ant-table-body",container);
            const headerCols= header?.getElementsByTagName("col");
            const bodyCols  = body?.getElementsByTagName("col");

            if(bodyCols && headerCols){
                const list = Array.from(bodyCols);
                list.forEach((col:HTMLElement,i)=>{
                    col.style.width     = headerCols.item(i)?.style.width || col.style.width;
                    col.style.maxWidth  = headerCols.item(i)?.style.maxWidth || col.style.maxWidth;
                })
            }
        }
    };

    resize = (width: number,height:number) => {
        if(width>this.width){
            const {isTree = false} = this.props;
            const indexWidth = isTree ? 0 : 51;
            const currentWidth = width - indexWidth - 10;
            const ratio = currentWidth/ this.originWidth;

            this.originWidth = currentWidth;
            this.originColumns.forEach((column) => {
                const {width = 100,name } = column;
                column.width = name==="__index"?50 : width * ratio;
            });
        }

        const headerHeight = 40;
        const footerHeight = 48;
        const tableHeight= height - headerHeight - footerHeight;
        this.setState({tableHeight})
    };

    render() {
        const {toolbar, service, isEdit, isTree = false} = this.props;
        const {expandedRowKeys, dataSource, total, pageSize, current,tableHeight} = this.state;

        const pagination = {
            disabled: false,
            total: total,
            showQuickJumper: true,
            itemRender: this.itemRender,
            showTotal: this.totalRender,
            defaultPageSize: pageSize,
            current: current,
            pageSize: pageSize,
            pageSizeOptions: ['20', '30', '50', '100', '200']
        };

        const expandable = {
            expandedRowKeys,
            onExpandedRowsChange: this.onExpandedRowsChange
        };

        const components = {
            body: {
                row: EditableRow,
                cell: EditableCell
            }
        };
        const columns = this.columns();
        const props = {
            ...this.defaultColumnProps,
            dataSource,
            onChange: this.onChange,
            expandRowByClick: false,
            pagination,
            expandable,
            components,
            columns,
            scroll:{
                y:tableHeight
            },
            className: classNames({"tree-table": isTree})
        };

        return (
            <Fragment>
                <TableToolbar service={service}
                              isEdit={isEdit}
                              util={this.util}
                              {...toolbar}/>
                <ResizeObserver
                    onResize={({ width,height }) => {
                        this.resize(width,height);
                    }}
                >
                    <Table<T> {...props}/>
                </ResizeObserver>
            </Fragment>
        )
    }
}
