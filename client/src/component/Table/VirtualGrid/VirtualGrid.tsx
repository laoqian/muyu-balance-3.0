/*
* Copyright (c) 2018. 成都墨安科技有限公司 保留所有权限.
* 千山鸟飞绝，万径人踪灭。
* 孤舟蓑笠翁，独钓寒江雪。
*
* @author  于其先
* @version 1.0.0
* @since 2020-06-01 22:21:45
*/

import React from "react"
import {Table} from "antd"
import {VariableSizeGrid as Grid} from 'react-window';
import ResizeObserver from 'rc-resize-observer';
import classNames from 'classnames';
import {castTo, getProperty} from "../../../util/CommonUtil";
import "./VirtualGrid.less"
import {BaseEntity} from "../../../service/base/BaseService";
import {TableProps} from "antd/lib/table";

export interface VirtualTableState {
    tableWidth: number;
    connectObject?: any;
}

export class VirtualTable<T extends BaseEntity<T> = any> extends React.Component<TableProps<T>, VirtualTableState> {

    private gridRef = React.createRef<any>();

    constructor(props: any) {
        super(props);

        this.state = {
            tableWidth: 0
        }
    }

    onClick = (e: any, record: T, rowIndex: number, colIndex: any) => {
        console.log(record);
    };


    renderVirtualList = (rawData: object[], {scrollbarSize, ref, onScroll}: any) => {
        const {columns = []} = this.props;
        const {tableWidth} = this.state;

        return (
            <Grid
                ref={this.gridRef}
                className="virtual-grid"
                columnCount={columns.length}
                columnWidth={function (index) {
                    const {width = 100} = columns[index];
                    const origin = castTo<number>(width);
                    return index === columns.length - 1 ? origin - scrollbarSize - 1 : origin;
                }}
                rowCount={rawData.length}
                rowHeight={() => 42}
                width={tableWidth - 2}
                height={600}
                onScroll={({scrollLeft}) => onScroll({scrollLeft})}
            >
                {({columnIndex, rowIndex, style}) => {
                    // @ts-ignore
                    const {render, dataIndex} = columns[columnIndex];
                    const data = castTo<T>(rawData[rowIndex]);
                    const value = getProperty(data, dataIndex);

                    return (
                        <div
                            className={classNames('virtual-table-cell', {'virtual-table-cell-last': columnIndex === columns.length - 1,})}
                            onClick={(e) => this.onClick(e, data, rowIndex, columnIndex)}
                            style={Object.assign({
                                wordWrap: 'break-word',
                                wordBreak: 'break-word',
                                textOverflow: "ellipsis"
                            }, style)}>
                            {render ? render(value, data, rowIndex) : value}
                        </div>
                    );
                }}
            </Grid>
        );
    };

    render() {
        const {className, title, ...rest} = this.props;

        return (
            <ResizeObserver onResize={({width}) => this.setState({tableWidth: width})}>
                <Table
                    {...rest}
                    className={classNames(className, 'virtual-table')}
                    pagination={false}
                    components={{
                        body: this.renderVirtualList,
                    }}
                />
            </ResizeObserver>
        );
    }
}
