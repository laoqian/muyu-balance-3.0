/*
* Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
* 千山鸟飞绝，万径人踪灭。
* 孤舟蓑笠翁，独钓寒江雪。
*
* @author  于其先
* @version 1.0.0
* @since 2020-06-20 22:58:44
*/


import EditableTable from "./EditableTable";
import {BaseEntity, BaseQuery} from "../../service/base/BaseService";
import {throttle} from "lodash-es";

export declare  interface TableUtilProps<T extends　BaseEntity<T>> {
    reload: <Q extends BaseQuery>(query: Q) => Promise<any>;
    expandByLevel: (level: number) => void;
    getRecord: (id: string) => T;
    getParent: (record:T) => T;
    getDataSource: () => T[];
    create: (record: T, editing?: boolean) => void;
    modify: (record: T, editing?: boolean) => void;
    edit: (record: T) =>void;
    remove: (record: T) => void;
    getEditedList: () => T[];
    isEditing: (record:T) => boolean;
    save: () => BatchBean<T>[];
    getBatchList: () => BatchBean<T>[];
    getSelected: () => T|undefined;
    getSelectedList: () => T[];
}

type Action = "create" | "modify" | "remove";


export declare interface BatchBean<T> {
    action: Action;
    key?: string;
    data: T;
}


class BatchStore<T extends　BaseEntity<T>> {
    private list: BatchBean<T>[] = [];

    private find = (key?: string): BatchBean<T> | undefined =>this.list.find(t => t.key === key);
    private push = (action:Action,data:T)=>{
        const t= this.find(data.key);
        if(!t){
           return this.list.push({
                action,
                key:data.key,
                data
            })
        }

        if(action==="remove" && t.action === "create"){
            this.list = this.list.filter(t=>t.data!==data);
        }
    };

    create = (record:T)=>this.push("create",record);
    modify = (record:T)=>this.push("modify",record);
    remove = (record:T)=>this.push("remove",record);
    get    = ()=>this.list;
    clear  = ()=>this.list=[];
}

/**
 * table实例引用主动操作Table时使用
 */
export default class TableUtil<T extends　BaseEntity<T>> implements TableUtilProps<T>{
    private readonly table : EditableTable<T>;
    private editSet = new Set<T>();
    private selected?:T=undefined;
    private selectedList:T[]=[];
    private store:BatchStore<T>= new BatchStore();

    constructor(table:EditableTable<T>) {
        this.table      = table;
    }

    private isTree = ()=> this.table.props.isTree || false;

    private fresh = throttle(()=>{
        this.table.setDataSource(this.getDataSource())
    },100);

    findById = (id:string,tree:T[]):T | undefined=>{
        let t = tree.find(t=>t.id ===id);
        if(!t){
            for(let  obj of tree.values()){
                const children = obj.children as T[];
                if(children){
                    t = this.findById(id,children);
                    if(t){
                        break;
                    }
                }
            }
        }
        return t ;
    };

    build =(current:T):T=>{
        const parent = this.selected || {} as T;
        if(this.isTree()){
            const {id="0",parentIds="",level=0,children=[]} = parent;
            const dataSource = this.getDataSource();

            Object.assign(current,{
                key:current.id,
                parentId:id,
                parentIds:parentIds===""?id:parentIds+","+id,
                level:(level as number) + 1,
                leaf:true,
                isNewRecord:true,
                sort:level===0?(dataSource.length+1) * 10 : (children.length+1) *10
            });

            if(children.length === 0){
                this.expandByKey(id);
            }
        }

        return current;
    };

    create = async (record: T, editing?: boolean) => {
        this.build(record);

        if(!this.isTree() || record.level===1) {
            this.getDataSource().push(record);
        }else{
            this.addChild(this.getParent(record),record)
        }

        await this.table.props.beforeAdd?.(record);
        this.edit(record,editing);
        this.store.create(record);
        this.fresh();
    };

    modify = (record:T,editing?:boolean)=>{
        if(editing === undefined){
            if(this.editSet.has(record)){
                this.edit(record,false);
            }else{
                this.edit(record,true);
                this.store.modify(record);
            }
        }else{
            this.edit(record,editing);
            this.store.modify(record);
        }
    };

    remove = (record: T): void =>{
        if(!this.isTree() || record.level===1) {
            const list:T[] = this.getDataSource();
            this.setDataSource(list.filter(t=>t!==record));
        }else{
            this.removeChild(this.getParent(record),record);
        }

        if(this.selected === record){
            this.selected = undefined;
        }

        this.store.remove(record);
        this.fresh();
    };

    private addChild        =(parent:T,child:T)=>{
        const children  = parent.children as T[];
        if(children){
            children.push(child);
        }else{
            parent.children = [child];
        }
    };

    private removeChild     =(parent:T,child:T)=>{
        const children  = parent.children as T[];
        parent.children = children.length>1 ? children.filter(t=>t!==child) : undefined;
    };

    edit   = (record: T,editing:boolean=false) =>{
        if(editing){
            this.editSet.add(record);
        }else{
            this.editSet.delete(record);
        }
    };

    save = ()=>{
        this.table.map.forEach(async (form,record)=>{
            await form.current?.validateFields();
            this.edit(record,false);
        });

        return this.store.get();
    };

    expandByLevel   = (level: number) => this.table.expandByLevel(level);
    expandByKey   = (key: string) => this.table.expandByKey(key as never);
    getDataSource   = () :T[] =>this.table.state.dataSource;
    setDataSource   = (dataSource:T[])=>this.table.state.dataSource = dataSource as never[];
    getParent       = (record:T) : T => this.findById(record.parentId as string,this.getDataSource()) as T;
    getRecord       = (id: string) => this.findById(id,this.getDataSource()) as T;
    getSelected     = () => this.selected;
    setSelected     = (record:T)=>{
        this.selected = record === this.selected ? undefined : record;
        this.fresh();
    };
    getSelectedList = () => this.selectedList;
    getEditedList   = () => Array.from<T>(this.editSet);
    getBatchList    = () => this.store.get();
    clearStore      = () => this.store.clear();
    getTable        = () => this.table;
    reload          = <Q extends BaseQuery>(query?: Q) => this.table.onChange(query);
    isEditing       = (record: T) => this.editSet.has(record);
}
