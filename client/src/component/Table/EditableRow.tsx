import React, {RefObject} from "react";
import {EditableColumnProps} from "./EditableTable"
import {FormInstance} from "antd/es/form/Form";
import {Form} from "antd";


export declare interface EditableRowProps<T> {
    form: FormInstance
    record: T;
    editing: boolean;
    column: EditableColumnProps<T>;
    index: number;
    mountForm: (form: RefObject<FormInstance>) => void;
}

export declare interface EditableRowState {
    formRef?: RefObject<FormInstance>;
}

export default class EditableRow<T> extends React.Component<EditableRowProps<T>, EditableRowState> {
    state: EditableRowState = {};

    static getDerivedStateFromProps(nextProps: any) {
        const {editing} = nextProps;
        return {
            formRef: editing ? React.createRef<FormInstance>() : undefined
        }
    }

    componentDidMount() {
        this.mountForm();
    }

    componentDidUpdate() {
        this.mountForm();
    }

    mountForm = () => {
        const {mountForm, editing} = this.props;
        if (editing && mountForm) {
            const {formRef} = this.state;
            mountForm(formRef as RefObject<FormInstance>);
        }
    };

    onValuesChange = (values: any) => {
        const {record} = this.props;
        Object.assign(record, values || {});
    };

    render() {
        const {mountForm, children, record, editing, ...rest} = this.props;
        if (editing) {
            const {formRef} = this.state;
            return (
                <tr {...rest}>
                    <Form component={false} onValuesChange={this.onValuesChange} ref={formRef} initialValues={record}>
                        {children}
                    </Form>
                </tr>
            )
        }

        return (
            <tr {...rest}>
                {children}
            </tr>
        )
    }
}
