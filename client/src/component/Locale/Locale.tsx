import React from "react";
import {Select} from "antd";
import store from "../../redux/store/configure";
import "./Locale.less"
import LocaleUtil from "../../util/LocaleUtil";

export default class Locale extends React.Component {
    locales = {
        "en-US": "English",
        "zh-CN": "中文"
    };

    state = {
        locale: LocaleUtil.getLanguage()
    };

    localeChange = async (locale: string) => {
        await LocaleUtil.setLocale(locale);
        store.dispatch({type: "SET_LOCALE", locale})
    };

    render() {
        const {locale} = this.state;
        return (
            <div className="locale-wrapper">
                <Select onChange={this.localeChange} defaultValue={locale}>
                    {Object.entries(this.locales).map(([value, name]) => <Select.Option key={value}
                                                                                        value={value}>{name}</Select.Option>)}
                </Select>
            </div>
        )
    }
}
