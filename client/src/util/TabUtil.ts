import store from "../redux/store/configure";
import {SysMenu} from "../service/system/SysMenuService";

export declare interface TabsAction {
    type: "TAB_ADD" | "TAB_DEL" | "TAB_SWITCH",
    data: TabsProps
}


export declare interface TabsProps extends SysMenu {
    closable?: boolean,
    data?: any,
}


class TabsUtil {
    open = (data: TabsProps) => store.dispatch<TabsAction>({type: "TAB_ADD", data});
    close = (data: TabsProps) => store.dispatch<TabsAction>({type: "TAB_DEL", data});
    // @ts-ignore
    active = (id: string) => store.dispatch<TabsAction>({type: "TAB_SWITCH", data: {id}});
}

export default new TabsUtil();
