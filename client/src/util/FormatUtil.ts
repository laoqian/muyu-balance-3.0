import DictUtil from "./DictUtil";

const accounting = require("accounting");

export declare interface FormatOption {
    type?: string;
    pattern?: string,
    separator?:string,
    precision ?:number,
}


class FormatUtil {
    formatter = {
        number(value:string,separator:string =",",precision:number=2){
            const num = parseFloat(value);
            return accounting.formatNumber(num,precision,separator);
        },
        percent:(value:string,separator:string =",",precision:number=2)=>{
            const num = parseFloat(value);
            return accounting.formatNumber(num*100,precision,separator)+"%"
        }
    };

    format(value: string, formatter:string="text", {type,separator,precision }:FormatOption={}) {
        switch (formatter) {
            case "number":
            case "currency":
                return this.formatter.number(value,separator,precision);
            case "date":
                return value;
            case "select":
                return DictUtil.getLocaledNameByValue(type,value);
            case "percent":
                return this.formatter.percent(value,separator,precision);
            case "text":
            default:
                return value;
        }
    }

    unformat(value: string, {type}: FormatOption) {
        switch (type) {
            case "number":
            case "currency":
                return accounting.unformat(value);
            case "text":
                return value.toString;
            case "date":
            default:
                return value;
        }
    }
}

export default new FormatUtil();
