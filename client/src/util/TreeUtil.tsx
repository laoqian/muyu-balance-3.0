/*
* Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
* 千山鸟飞绝，万径人踪灭。
* 孤舟蓑笠翁，独钓寒江雪。
*
* @author  于其先
* @version 1.0.0
* @since 2020-06-19 22:52:39
*/

import React from "react";
import {Dialog, SearchTree} from "../component";
import OfficeService from "../service/system/SysOfficeService";

export const officeSelection = async (title:string,query:object, beforeOpen?:()=>Promise<boolean>,errorMsg?:string)=>{
    const status = await beforeOpen?.();
    if(status === false ){
        return Promise.reject(errorMsg);
    }

    return new Promise<any>(resolve => {
        let company: any;
        Dialog.open({
            title: "请选择"+title,
            children: <SearchTree type="directory"
                                  isFilter
                                  loadTree={OfficeService.findTreeData}
                                  onSelect={(id, {node}) => company = node}/>,
            ok: () => company || new Error("没有选择"+title),
            afterOk: () => resolve(company)
        });
    })
};
