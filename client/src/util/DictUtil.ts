import dictService, {SysDict} from "../service/system/SysDictService";
import {message} from "antd";
import LocaleUtil from "./LocaleUtil";


class DictUtil {
    private list: SysDict[] = [];

    constructor() {
        this.init().then();
    }

    private init = async () => {
        const {code, data, msg} = await dictService.findList({pageSize: -1, current: 0});
        if (code !== 200) {
            return message.error(msg);
        }

        this.list = data;
    };

    get = (type: string): SysDict[] => {
        return this.list
            .filter(t => t.type === type)
            .sort((a, b) => {
                const sort1 = a.sort || 10000, sort2 = b.sort || 10000;
                return sort1 - sort2;
            });
    };

    getLocaledNameByValue = (type: string="", value: string="") => {
        const dict: SysDict = this.getByValue(type, value.toString());
        return LocaleUtil.get(dict?.code, dict?.name);
    };

    getByValue = (type: string, value: string) => this.get(type).filter(t => t.value === value)[0]
}


export default new DictUtil();
