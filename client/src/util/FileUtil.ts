import http from "../common/http";
import {message} from "antd";

class FileUtil {

    upload = (file: any, data: any) => {

    };

    download = async (url:string,dat:object={},fileName:string,tail:string) => {
       const {code,msg,data} = await http.post(url,dat);
       if(code !==200){
           return message.error(msg);
       }

        const link = document.createElement('a');
        const blob = new Blob([data], );
        link.setAttribute('href', window.URL.createObjectURL(blob));
        link.setAttribute('download', fileName + tail);
        link.style.visibility = 'hidden';
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    };

    preview = (url:string)=>{

    }
}


const fileUtil = new FileUtil();
export default fileUtil;
