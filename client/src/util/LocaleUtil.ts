import LocalStorageUtil from "./LocalStorageUtil";
import localeService from "../service/system/SysLocaleService";
import DictUtil from "./DictUtil";
import {getProperty} from "./CommonUtil";


class LocaleUtils {
    key = "zh-CN";
    locale: object = {};

    getLanguage = (): string => LocalStorageUtil.getItem(this.key) || "zh-CN";

    loadDefaultLocale = () => {
        return this.setLocale(this.getLanguage());
    };

    setLocale = async (locale: string) => {
        const {code, data} = await localeService.getLocale({lang: locale});
        if (code === 200) {
            this.locale = data;
            document.title = this.get("title");
            LocalStorageUtil.setItem(this.key, locale);
        }
    };

    getByDict = (dictType: string, dictValue: string) => {
        return this.get(DictUtil.getByValue(dictType, dictValue).code);
    };

    get = (key: string = "", defaultValue: string = "") => {
        return getProperty(this.locale, key) || defaultValue;
    };

    compile = (key: string = "", ...args: string[]) =>this.template(getProperty(this.locale, key),...args);
    template =(tpl:string,...args:any[])=>{
        args.forEach((arg, i) => {
            tpl = tpl.replace(`{${i}}`, arg);
        });

        return tpl;
    }
}

export default new LocaleUtils();
