import {BaseEntity} from "../service/base/BaseService";
import {AntTreeNodeProps} from "antd/lib/tree";
import {cloneDeep} from "lodash-es";
import {Key} from "react";
import {API_URL} from "../service/url";

export const getInstance = <T>(c: { new(): T; }): T => {
    return new c();
};

export const getProperty = <T>(obj: T, key: any) => {
    return obj[key as keyof typeof obj];
};

export const castTo = <T>(obj: any) => {
    return obj as T;
};

export const byteLength = (str: string) => {
    let totalLength = 0;
    let charCode;

    for (let i = 0; i < str.length; i++) {
         charCode = str.charCodeAt(i);
         if (charCode < 0x007f)  {
             totalLength++;
         } else if ((0x0080 <= charCode) && (charCode <= 0x07ff))  {
             totalLength += 2;
         } else if ((0x0800 <= charCode) && (charCode <= 0xffff))  {
             totalLength += 3;
         } else{
             totalLength += 4;
         }
     }

     return totalLength;
};

const treeMapping = <T extends BaseEntity<T>>(data: T) => {
    const tree = castTo<AntTreeNodeProps>(data);
    tree.key = data.id;
    tree.title = data.name;
    tree.isLeaf = data.children === undefined;
    data.children?.forEach(treeMapping);
};

export const toTreeData = <T extends BaseEntity<T>>(tree: T[]): any[] => {
    tree.forEach(treeMapping);
    return tree;
};


const checkKeys = <T extends BaseEntity<T>> (checkedKeys:Key[],data:T,checkedNodes:Set<T>) :boolean =>{
    const key:string = castTo(data.key);
    let status = false;

    if(checkedKeys.includes(key)){
        checkedNodes.add(data);
        status = true;
    }

    const length = data?.children?.map(child=>checkKeys(checkedKeys,child,checkedNodes)).filter(status=>status).length;
    if(length!==undefined && length>0){
        status = true;
    }

    if(status){
        checkedNodes.add(data);
    }

    return status;
};

export const getCheckKeys = <T extends BaseEntity<T>>(checkedKeys:Key[],list:T[]) : any[]=>{
    const checkedNodes = new Set<T>();
    list.forEach(data=>checkKeys(checkedKeys,data,checkedNodes));

    const nodeList = Array.from(checkedNodes);
    return [nodeList.map(({key})=>key),nodeList];
};


export const createDomRoot = () => {
    const root = document.createElement('div');
    document.body.appendChild(root);
    return root;
};

export const clearChildren = <T extends BaseEntity<T>> (list:T[]):T[]=>{
    const others = cloneDeep(list);
    others.forEach(row=>delete row.children);
    return others;
};

export const openURL = (url:string,name:string)=>{
    // @ts-ignore
    window.open(API_URL+url,name).focus();
}
