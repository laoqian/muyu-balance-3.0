/*
* Copyright (c) 2020. 成都墨安科技有限公司 保留所有权限.
* 千山鸟飞绝，万径人踪灭。
* 孤舟蓑笠翁，独钓寒江雪。
*
* @author  于其先
* @version 1.0.0
* @since 2020-06-14 0:21:37
*/


import SystemService from "../service/system/SystemService";

class UserUtil {
    private authorities:Set<string> = new Set<string>();

    constructor() {
      this.init().then();
    }

    private init = async ()=>{
        const {code,data} = await SystemService.permissions();
        if(code==200){
            this.authorities = new Set(data);
        }
    };

    hasAnyRole = (...roles:string[]):boolean=>roles.map(this.hasRole).filter(status=>status).length>0;
    hasRole = (role:string):boolean =>this.authorities.has(role);
    hasAnyPermisson = (...roles:string[]):boolean=>this.hasAnyRole(...roles);
    hasPermisson = (role:string):boolean=>this.hasRole(role);
}


export default new UserUtil();
