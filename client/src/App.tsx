import React, {useState} from 'react';
import './App.less';
import Login from "./page/system/login/Login";
import Home from "./page/system/home/Home";

const App = () => {
    const [login, setLogin] = useState<boolean>(false);

    return login ? <Home/> : <Login setLogin={setLogin}/>;
};

export default App;
