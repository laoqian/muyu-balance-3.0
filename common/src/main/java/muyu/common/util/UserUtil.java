package muyu.common.util;

import com.google.common.collect.Lists;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserUtil {
    public static User getUser() {
        return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    public static boolean hasAnyRoles(String... roles) {
        Set<String> set = new HashSet<>(Arrays.asList(roles));
        User user = getUser();
        return user != null && user.getAuthorities().stream().map(GrantedAuthority::getAuthority).anyMatch(set::contains);
    }

    public static boolean isSuperAdmin() {
        return hasAnyRoles("SUPER_ADMIN");
    }

    public static List<String> authorities() {
        User user = getUser();
        return user == null ? Lists.newArrayList() : user.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());
    }
}
