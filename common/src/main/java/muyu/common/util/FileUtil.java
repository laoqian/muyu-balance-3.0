package muyu.common.util;

import com.qiniu.util.IOUtils;
import org.springframework.util.DigestUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

/**
 * 文件工具类
 *
 *
 * @author 于其先
 * @since   2020年3月30日
 */
public class FileUtil {

    /**
     * 递归创建目录
     *
     * @param file 文件名
     */
    public static boolean createDir(File file) {
        return  file.getParentFile().exists()? file.mkdir() : createDir(file.getParentFile()) && file.mkdir();
    }

    /**
     * 确保目录存在
     * @param dirName 目录名称
     * @return 成功状态
     */
    public static boolean ensureDir(String dirName) {
        File dir = new File(dirName);
        return dir.exists() && dir.isDirectory()  ||  createDir(dir);
    }

    /**
     * 递归删除文件
     * @param file 文件
     */
    public static boolean delete(File file) {
        if (file.exists()) {
            if(file.isDirectory()){
                Arrays.stream(Objects.requireNonNull(file.listFiles())).forEach(FileUtil::delete);
            }

            return file.delete();
        }else{
            return true;
        }
    }

    /**
     * 文件移动
     * @param fileName 文件名
     * @param dstFileName 目标文件名
     * @return 是否成功
     */
    public static boolean moveTo(String fileName,String dstFileName){
        return new File(fileName).renameTo(new File(dstFileName));
    }

    /**
     * 计算文件MD5
     * @param fileName 文件名
     * @return MD5
     */
    public static String getMd5(String fileName){
        try {
           return  DigestUtils.md5DigestAsHex(IOUtils.toByteArray(new FileInputStream(new File(fileName))));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     *  获取文件名后缀
     * @param fileName 文件名
     * @return 后缀
     */
    public static String getSuffix(String fileName) {
        return Optional.ofNullable(fileName).map(name->name.substring(name.lastIndexOf(".") + 1)).orElse("none");
    }
}
