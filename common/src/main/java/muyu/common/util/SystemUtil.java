package muyu.common.util;


import muyu.common.os.IOsService;

import java.util.ArrayList;
import java.util.List;

/**
 * 系统工具类
 *
 *
 * @author 于其先
 * @date  2020年3月30日
 */
public class SystemUtil {
    private static final List<IOsService> services = new ArrayList<>();
//        new LinuxService(),
//        new WindowsService());

    public static String getOs(){
        return System.getProperty("os.name");
    }

    public static boolean isLinux(){
        return getOs().contains("linux");
    }

    public static boolean isWindows(){
        return getOs().contains("windows");
    }

    private static IOsService service(){
        return services.get(isLinux()?0:1);
    }

    public static boolean start(String serviceName) {
        return service().start(serviceName);
    }

    public static boolean close(String serviceName) {
        return service().close(serviceName);
    }

    public static String state(String serviceName) {
        return service().state(serviceName);
    }

    public static boolean restart(String serviceName) {
        return service().restart(serviceName);
    }
}


