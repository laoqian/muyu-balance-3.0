package muyu.common.util;

import org.springframework.context.ConfigurableApplicationContext;

public class ContextUtil {

    private static ConfigurableApplicationContext context;

    public static void setContext(ConfigurableApplicationContext context) {
        ContextUtil.context = context;
    }

    public static ConfigurableApplicationContext getContext() {
        return ContextUtil.context;
    }

    public static<T> T getBean(Class<T> clazz){
       return context.getBean(clazz);
    }
}
