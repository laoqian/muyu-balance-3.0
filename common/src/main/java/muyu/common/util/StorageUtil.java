package muyu.common.util;

import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * 云存储工具类
 */
public class StorageUtil {

    private static final String accessKey = "OVQ2ZnAASXg6q0d6nXSvNiWVY7SFOtbduk5JlMSA";
    private static final String secretKey = "b2dnUht3E0bmslqHWybBso6FbMzs0TekZwss_Fd4";
    private static final String bucket = "muyu-tek";
    private static final String domainOfBucket = "http://q6fwl1aey.bkt.clouddn.com";

    /**
     * @param inputStream 输入流
     * @param fileName 文件名称
     * @return 上传结果
     */
    public static DefaultPutRet upload(InputStream inputStream, String fileName) {
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);
        Configuration cfg = new Configuration(Region.region0());
        UploadManager uploadManager = new UploadManager(cfg);
        DefaultPutRet ret = null;

        try {
            Response response = uploadManager.put(inputStream, fileName, upToken, null, null);
            ret = response.jsonToObject(DefaultPutRet.class);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return ret;
    }

    /**
     * 文件授权
     *
     * @param fileName 文件名
     * @param expires 超时时间
     * @return 授权URL
     */
    public static String authorize(String fileName, Integer expires) {
        Auth auth = Auth.create(accessKey, secretKey);
        String url = null;

        try {
            String encodedFileName = URLEncoder.encode(fileName, String.valueOf(StandardCharsets.UTF_8))
                .replace("+", "%20");

            url = auth.privateDownloadUrl(String.format("%s/%s", domainOfBucket, encodedFileName),
                expires);

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        return url;
    }

    /**
     * 文件删除
     *
     * @param fileName 文件名
     * @return 是否成功
     */
    public static boolean delete(String fileName) {
        Auth auth = Auth.create(accessKey, secretKey);
        Configuration cfg = new Configuration(Region.region0());
        BucketManager bucketManager = new BucketManager(auth, cfg);
        boolean success = true;
        try {
            bucketManager.delete(bucket, fileName);
        } catch (QiniuException ex) {
            success = false;
        }

        return success;
    }
}
