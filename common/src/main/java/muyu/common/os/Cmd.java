package muyu.common.os;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.InputStreamReader;

@Slf4j
public class Cmd {
    public String execute(String cmd) {
        StringBuilder builder = new StringBuilder();
        log.debug(cmd);

        try {
            Process process = Runtime.getRuntime().exec(cmd);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(process.getInputStream(),"GBK"));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                builder.append(line).append("\n");
            }

            log.debug(builder.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return builder.toString();
    }
}
