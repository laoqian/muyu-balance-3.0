package muyu.common.os;


import lombok.NoArgsConstructor;

import java.util.Arrays;

@NoArgsConstructor
public class WindowsService implements IOsService {
    private final static Cmd cmd = new Cmd();

    private String execute(String operator,String name){
        return cmd.execute(String.format("sc %s %s",operator,name));
    }

    @Override
    public boolean start(String name) {
        execute("start",name);
        return isRunning(name);
    }

    @Override
    public boolean close(String name) {
        execute("stop",name);
        return isStopped(name);
    }

    @Override
    public String state(String name) {
        String result = execute("query",name);
        return Arrays.stream(result.split("\\n"))
                .map(String::trim)
                .filter(t -> t.trim().startsWith("STATE"))
                .findAny()
                .map(s -> s.split(":")[1].split(" {2}")[1])
                .orElse(null);
    }

    @Override
    public boolean restart(String name) {
        if(isRunning(name)){
            close(name);
        }

        start(name);
        return isRunning(name);
    }

    @Override
    public boolean isRunning(String name) {
        String state = state(name);
        return "RUNNING".equals(state);
    }

    @Override
    public boolean isStopped(String name) {
        String state = state(name);
        return "STOPPED".equals(state);
    }
}
