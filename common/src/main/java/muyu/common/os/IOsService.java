package muyu.common.os;

/**
 * 操作系统服务操作接口
 */
public interface IOsService {

    boolean start(String name);

    boolean close(String name);

    boolean restart(String name);

    String state(String name);

    boolean isRunning(String name);

    boolean isStopped(String name);
}
