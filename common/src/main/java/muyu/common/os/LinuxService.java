package muyu.common.os;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class LinuxService implements IOsService {

    @Override
    public boolean start(String name) {
        return false;
    }

    @Override
    public boolean close(String name) {
        return false;
    }

    @Override
    public String state(String name) {
        return null;
    }

    @Override
    public boolean isRunning(String name) {
        return false;
    }

    @Override
    public boolean isStopped(String name) {
        return false;
    }

    @Override
    public boolean restart(String name) {
        return false;
    }
}
