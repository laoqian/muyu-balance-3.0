package muyu.common.aop;

import lombok.extern.slf4j.Slf4j;
import muyu.common.bean.ResultBean;
import muyu.common.exception.ServiceException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

/**
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author 于其先
 * @since  2020/3/25
 * @version 1.0.0
 */
@Aspect
@Slf4j
public class ControllerAOP {

    @Pointcut("execution(public Object muyu..*.controller..*.*(..))")
    public void resultAOP(){}

    @Around("resultAOP()")
    public Object handlerControllerMethod(ProceedingJoinPoint pjp){
        long startTime = System.nanoTime();
        ResultBean<?> result;
        try {
            result = ResultBean.builder().data(pjp.proceed()).build();
        } catch (Throwable e) {
            result = handlerException(pjp, e);
        }

        double useTime = ((double)(System.nanoTime() - startTime))/1000000;

        log.error(pjp.getSignature() + "-控制器执行时间: " + useTime +" 毫秒");
        result.setUseTime(useTime);

        return result;
    }

    private ResultBean<?> handlerException(ProceedingJoinPoint pjp, Throwable e) {
        MethodSignature signature = (MethodSignature)pjp.getSignature();
        log.error(signature + " error ", e);

        return ResultBean.builder()
                .msg(e instanceof ServiceException? e.getLocalizedMessage() :"服务器异常，请稍后再试.")
                .code(-1)
                .build();
    }
}
