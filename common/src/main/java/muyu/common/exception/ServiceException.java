package muyu.common.exception;


public class ServiceException extends  RuntimeException {

    public ServiceException(String msg){
        super(msg);
    }

    public ServiceException(String msg,Throwable ex){
        super(msg,ex);
    }
}
