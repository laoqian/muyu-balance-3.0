package muyu.common.enums;


import lombok.Getter;

@Getter
public enum TransitActionEnum {

    SHIFT_UP("shiftUp","升级"),
    SHIFT_DOWN("shiftDown","降级"),
    MOVE_UP("moveUp","上移"),
    MOVE_DOWN("moveDown","下移");

    String action;
    String name;

    TransitActionEnum(String action,String name){
        this.action = action;
        this.name   = name;
    }
}
