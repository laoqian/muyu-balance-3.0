package muyu.common.enums;


import lombok.Getter;

@Getter
public enum BeanBeanActionEnum {

    CREATE("create"),
    UPDATE("update"),
    REMOVE("remove");

    String symbol;

    BeanBeanActionEnum(String symbol){
        this.symbol = symbol;
    }
}
