package muyu.common.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract  class BaseEntity implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    String id;

    @TableField(exist = false)
    Boolean isNewRecord;

    public BaseEntity(String id){
        this.id = id;
    }

    public boolean getIsNewRecord() {
        return isNewRecord!=null ? isNewRecord : id == null || "".equals(id);
    }
}
