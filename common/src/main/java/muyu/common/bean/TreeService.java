package muyu.common.bean;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import muyu.common.annotations.NullIfNonLeaf;
import muyu.common.annotations.SummaryByLevel;
import muyu.common.enums.BeanBeanActionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.*;
import java.util.stream.Collectors;

public class TreeService<M extends BaseMapper<T>, T extends TreeEntity> extends BaseService<M, T> {

    @Autowired
    IdentifierGenerator generator;

    /**
     * 保存
     *
     * @param entityList 列表
     * @return 是否成功
     */
    @Override
    public boolean saveBatch(Collection<T> entityList) {
        List<T> insertList = entityList.stream()
                .filter(BaseEntity::getIsNewRecord)
                .sorted(Comparator.comparing(TreeEntity::getLevel))
                .collect(Collectors.toList());

        if (!CollectionUtils.isEmpty(insertList)) {
            for (T entity : insertList) {
                String originId = entity.getId();
                entity.setId(generator.nextId(null).toString());

                List<T> children = insertList.stream()
                        .filter(t -> originId.equals(t.getParentId()))
                        .collect(Collectors.toList());

                if (!CollectionUtils.isEmpty(children)) {
                    entity.setLeaf(false);
                    children.forEach(t -> t.eval(entity));
                }
            }
        }

        super.saveBatch(entityList);

        return true;
    }

    public boolean saveByBatchBean(List<BatchBean.WithingActionBean<T>> list ) {
        Map<Boolean, List<T>> map = list.stream()
                .collect(Collectors.groupingBy(t->t.getAction().equals(BeanBeanActionEnum.REMOVE.getSymbol()),
                        Collectors.mapping(BatchBean.WithingActionBean::getData, Collectors.toList())));

        List<T> delList = map.get(true);
        List<T> saveList = map.get(false);

        if (!CollectionUtils.isEmpty(delList)) {
            delList.forEach(t->this.removeById(t.getId()));
        }

        if (!CollectionUtils.isEmpty(saveList)) {
            this.saveBatch(saveList);
        }

        return true;
    }

    /**
     * 获取级联列表
     *
     * @param id 主键
     * @return 级联列表
     */
    public List<T> getCascadeListById(String id) {
        QueryWrapper<T> wrapper = new QueryWrapper<>();

        wrapper.like("parent_ids", id)
                .or()
                .eq("id", id);

        return super.list(wrapper);
    }

    /**
     * 级联删除
     *
     * @param id 级联删除
     * @return 是否成功
     */
    public boolean removeCascadeById(String id) {
        QueryWrapper<T> wrapper = new QueryWrapper<>();

        wrapper.like("parent_ids", id)
                .or()
                .eq("id", id);

        return super.remove(wrapper);
    }

    /**
     * 分级汇总
     *
     * @param root root
     */
    public void summaryByLevel(T root) {
        List<Field> fields = Arrays.stream(root.getClass().getDeclaredFields())
                .filter(t -> t.getAnnotation(SummaryByLevel.class)!= null && (t.getType() == BigDecimal.class))
                .peek(f->f.setAccessible(true))
                .collect(Collectors.toList());

        root.DFSTraverse(t -> {
            if (!t.getLeaf()) {
                for(Field field:fields){
                    BigDecimal sum = t.getChildren().stream()
                            .map(c -> (BigDecimal) ReflectionUtils.getField(field, c))
                            .filter(Objects::nonNull)
                            .reduce(BigDecimal::add).orElse(BigDecimal.ZERO);

                    ReflectionUtils.setField(field, t, sum);
                }
            }
        });
    }

    /**
     * 不为叶子时，设置为null.
     * @param root root
     */
    public void clearIfNonLeaf(T root) {
        List<Field> fields = Arrays.stream(root.getClass().getFields())
                .filter(t -> t.getAnnotation(NullIfNonLeaf.class) != null)
                .collect(Collectors.toList());

        root.BFSTraverse(t -> {
            if (!t.getLeaf()) {
                fields.forEach(field -> ReflectionUtils.setField(field,t,null));
            }
        });
    }
}
