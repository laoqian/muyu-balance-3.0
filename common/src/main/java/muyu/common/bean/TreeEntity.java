package muyu.common.bean;


import com.baomidou.mybatisplus.annotation.TableField;
import com.google.common.collect.Lists;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import muyu.common.exception.ServiceException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Method;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TreeEntity extends BaseEntity {
    /**
     * 父级编号
     */
    private String parentId;

    /**
     * 所有父级编号
     */
    private String parentIds;

    /**
     * 层级
     */
    private Integer level;

    /**
     * 是否叶子
     */
    private Boolean leaf;

    /**
     * 排序号
     */
    private Integer sort;

    /**
     * 子节点
     */
    @TableField(exist = false)
    private List<TreeEntity> children;

    public TreeEntity(String id) {
        super(id);

        this.level = 0;
    }

    @SuppressWarnings("unchecked")
    public static <T extends TreeEntity> void create(T root,List<T> list) {
        root.setLeaf(false);
        root.setChildren((List<TreeEntity>)TreeEntity.create(root.getId(), list));
    }

    public static <T extends TreeEntity> List<T> create(List<T> list) {
        return TreeEntity.create("0", list);
    }

    public static <T extends TreeEntity> List<T> create(String rootId, List<T> list) {
        return list.stream()
                .filter(t -> rootId.equals(t.getParentId()))
                .sorted(Comparator.comparing(TreeEntity::getSort))
                .peek(t -> t.addChildren(list))
                .collect(Collectors.toList());
    }

    public <T extends TreeEntity> void addChildren(List<T> list) {
        List<TreeEntity> children = list.stream()
                .filter(t -> id != null && id.equals(t.getParentId()))
                .sorted(Comparator.comparing(TreeEntity::getSort))
                .peek(t -> t.addChildren(list))
                .collect(Collectors.toList());

        this.leaf = CollectionUtils.isEmpty(children);

        if (!this.leaf) {
            this.children = children;
        }
    }

    public void eval(TreeEntity parent) {
        this.level = parent.getLevel() + 1;
        this.parentIds = parent.getParentIds() + "," + parent.getId();
        this.parentId = parent.getId();
        this.leaf = CollectionUtils.isEmpty(children);
    }

    public void eval(TreeEntity parent, int sort) {
        this.eval(parent);
        this.sort = sort;
    }

    private void eval() {
        AtomicInteger i = new AtomicInteger(1);
        if (!CollectionUtils.isEmpty(this.children)) {
            this.children.forEach(t -> t.eval(this, (i.getAndIncrement()) * 10));
        }
    }

    public void BFSTraverse(Consumer<TreeEntity> action) {
        action.accept(this);

        if (!CollectionUtils.isEmpty(this.children)) {
            this.children.forEach(t -> t.BFSTraverse(action));
        }
    }

    public void DFSTraverse(Consumer<TreeEntity> action) {
        if (!CollectionUtils.isEmpty(this.children)) {
            this.children.forEach(t -> t.DFSTraverse(action));
        }

        action.accept(this);
    }

    protected TreeEntity findParentByChildId(String childId) {
        Optional<TreeEntity> optional = Optional.empty();

        if (!CollectionUtils.isEmpty(this.children)) {
            boolean isMatch = this.children.stream().anyMatch(t -> StringUtils.equals(t.getId(), childId));

            optional = isMatch ? Optional.of(this) :
                    this.children.stream()
                            .map(t -> t.findParentByChildId(childId))
                            .filter(Objects::nonNull)
                            .findAny();
        }

        return optional.orElse(null);
    }

    protected int indexOf(String childId) {
        int pos = -1;
        for (int i = 0; i < this.children.size(); i++) {
            if (StringUtils.equals(this.children.get(i).getId(), childId)) {
                pos = i;
                break;
            }
        }

        return pos;
    }

    protected void swap(int i, int k) {
        TreeEntity t = this.children.get(i);
        this.children.set(i, this.children.get(k));
        this.children.set(k, t);
    }

    public void moveUp(String childId) {
        TreeEntity p = this.findParentByChildId(childId);
        if (p != null) {
            int i = p.indexOf(childId);
            if (i > 0) {
                p.swap(i, i - 1);
                p.eval();
            }
        }
    }

    public void moveDown(String childId) {
        TreeEntity p = this.findParentByChildId(childId);
        if (p != null) {
            int i = p.indexOf(childId);
            if (i != -1 && i < this.children.size() - 1) {
                p.swap(i, i + 1);
                p.eval();
            }
        }
    }

    public void shiftUp(String childId) {
        TreeEntity p = this.findParentByChildId(childId);
        if (p != null && !StringUtils.equals(p.getId(), "0")) {
            TreeEntity pp = this.findParentByChildId(p.getId());
            int i = pp.indexOf(p.getId());
            int originPos = p.indexOf(childId);
            TreeEntity t = p.getChildren().get(originPos);

            p.getChildren().remove(originPos);
            p.eval();

            pp.getChildren().add(i + 1, t);
            pp.eval();
        }
    }

    public void shiftDown(String childId) {
        TreeEntity p = this.findParentByChildId(childId);
        if (p != null) {
            int i = p.indexOf(childId);
            if (i > 0) {
                TreeEntity t = p.getChildren().get(i);
                TreeEntity cp = p.getChildren().get(i - 1);

                if (CollectionUtils.isEmpty(cp.getChildren())) {
                    cp.setChildren(Lists.newArrayList());
                }

                cp.getChildren().add(t);
                cp.eval();

                p.getChildren().remove(i);
                p.eval();
            }
        }
    }

    /**
     * 生成不包含自己的列表
     *
     * @return list
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> toList() {
        List<TreeEntity> list = Lists.newArrayListWithCapacity(500);

        if (!CollectionUtils.isEmpty(this.children)) {
            this.children.forEach(c -> c.BFSTraverse(list::add));
        }

        return (List<T>) list;
    }

    /**
     * 层架变换
     *
     * @param action action
     * @param ids    ids
     */
    public void translate(String action, List<String> ids) {
        try {
            Method method = this.getClass().getMethod(action, String.class);
            for (String id : ids) {
                method.invoke(this, id);
            }
        } catch (Exception ex) {
            throw new ServiceException("不支持的操作",ex);
        }
    }
}
