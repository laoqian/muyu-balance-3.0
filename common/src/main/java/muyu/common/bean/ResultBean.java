package muyu.common.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ResultBean<T> {

    @Builder.Default
    private int code = 200;

    @Builder.Default
    private String msg = "成功";

    private double useTime;

    private T data;
}
