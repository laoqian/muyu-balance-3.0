package muyu.common.bean;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseQuery implements Serializable {

    /**
     * 分页大小
     */
    Integer pageSize    = 20;

    /**
     * 当前页码
     */
    Integer current     = 0;

    public <T> Page<T> toPage(){
        return new Page<>(this.current,this.pageSize);
    }

    public int getPageStart(){
        return (this.current -1) * this.pageSize;
    }
}
