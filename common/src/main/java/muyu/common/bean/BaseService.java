package muyu.common.bean;

import com.baomidou.mybatisplus.core.incrementer.IdentifierGenerator;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import muyu.common.enums.BeanBeanActionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BaseService<M extends BaseMapper<T>, T extends BaseEntity> extends ServiceImpl<M,T> {
    private static final int BATCH_SIZE     = 1000;
    private static final int ID_MIN_LENGTH  = 5;

    @Autowired
    IdentifierGenerator generator;

    @Override
    public boolean saveBatch(Collection<T> entityList) {
        return saveBatch(entityList,BATCH_SIZE);
    }

    @Override
    public boolean saveBatch(Collection<T> entityList, int batchSize) {
        Map<Boolean,List<T>> map = entityList.stream().collect(Collectors.groupingBy(BaseEntity::getIsNewRecord));

        if(!CollectionUtils.isEmpty(map.get(Boolean.TRUE))){
            super.saveBatch(map.get(Boolean.TRUE),batchSize);
        }

        if(!CollectionUtils.isEmpty(map.get(Boolean.FALSE))){
            super.updateBatchById(map.get(Boolean.FALSE),batchSize);
        }

        return true;
    }

    public boolean saveByBatchBean(List<BatchBean.WithingActionBean<T>> list ) {
        Map<String, List<T>> map = list.stream()
                .collect(Collectors.groupingBy(BatchBean.WithingActionBean::getAction,
                        Collectors.mapping(BatchBean.WithingActionBean::getData, Collectors.toList())));

        List<T> insertList = map.get(BeanBeanActionEnum.CREATE.getSymbol());
        List<T> updateList = map.get(BeanBeanActionEnum.UPDATE.getSymbol());
        List<T> delList = map.get(BeanBeanActionEnum.REMOVE.getSymbol());


        if (!CollectionUtils.isEmpty(delList)) {
            delList.forEach(t->this.removeById(t.getId()));
        }

        if (!CollectionUtils.isEmpty(insertList)) {
            insertList.stream()
                    .filter(t -> StringUtils.isEmpty(t.getId()) || t.getId().length()<ID_MIN_LENGTH)
                    .forEach(t -> t.setId(generator.nextId(null).toString()));

            super.saveBatch(insertList,BATCH_SIZE);
        }

        if (!CollectionUtils.isEmpty(updateList)) {
            super.updateBatchById(updateList,BATCH_SIZE);
        }

        return true;
    }
}
