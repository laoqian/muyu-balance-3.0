package muyu.common.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@AllArgsConstructor
@Setter
@Getter
@NoArgsConstructor
public class BatchBean<T,Q> {
    private final static ModelMapper mapper = new ModelMapper();

    Q query;

    List<WithingActionBean<T>> list;

    @Getter
    @Setter
    @NoArgsConstructor
    @AllArgsConstructor
    public static class WithingActionBean<T>{
        private String action;
        private T data;
    }

    public <S> List<WithingActionBean<S>> map(Class<S> clazz){
        return list.stream()
                .map(t-> new WithingActionBean<>(t.getAction(),mapper.map(t.getData(),clazz)))
                .collect(Collectors.toList());
    }

    public void forEach(Consumer<? super T> action){
        this.getList().stream().map(WithingActionBean::getData).forEach(action);
    }
}



