package muyu.common.bean;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.util.CollectionUtils;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.List;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class PageBean<T> implements Serializable {
    private static final ModelMapper mapper = new ModelMapper();

    /**
     * 分页大小
     */
    @Builder.Default
    private Long pageSize = 20L;
    /**
     * 当前页码
     */
    @Builder.Default
    private Long pageNum = 0L;

    /**
     * 总数量
     */
    @Builder.Default
    private Long total = 0L;

    /**
     * 数据
     */
    private List<T> list;

    public PageBean(List<T> list){
        this.list = list;
    }

    public static <T> PageBean<T> empty(){
        return PageBean.<T>builder().total(0L).build();
    }

    public static <T> PageBean<T> create(Page<?> page){
        return  PageBean.<T>builder()
            .pageSize(page.getSize())
            .pageNum(page.getCurrent())
            .list(mapper.map(page.getRecords(),new TypeToken<List<T>>(){}.getType()))
            .total(page.getTotal())
            .build();
    }

    public <E>  PageBean<E> map(Type type){
        if(CollectionUtils.isEmpty(this.list)){
            return PageBean.empty();
        }

        return   PageBean.<E>builder()
                .pageSize(this.getPageSize())
                .pageNum(this.getPageNum())
                .list(mapper.map(this.getList(),type))
                .total(this.getTotal())
                .build();
    }
}
