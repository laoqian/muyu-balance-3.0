/*
 * Copyright (c) 2018. 成都墨安科技有限公司 保留所有权限.
 * 千山鸟飞绝，万径人踪灭。
 * 孤舟蓑笠翁，独钓寒江雪。
 *
 * @author  于其先
 * @version 1.0.0
 * @since 2020-05-24 11:11:55
 */
package muyu.common.annotations;

import java.lang.annotation.*;

/**
 * 分级汇总注解
 *
 * @author 于其先
 * @since 2020-05-24 11:11:55
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface SummaryByLevel {

}
